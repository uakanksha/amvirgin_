package com.example.amvirgin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.Fragments.HomeFragment;
import com.example.amvirgin.Fragments.RegisterFragment;
import com.example.amvirgin.Fragments.SignInFragment;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static java.sql.DriverManager.println;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout signin_basic_ll, signin_red_ll, register_basic_ll, register_red_ll, google_login_ll;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    GoogleSignInClient mGoogleSignInClient;
    public static final int RC_SIGN_IN = 1;

    CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    SignInButton signInButton;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
//        updateUI(currentUser);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        signin_basic_ll = findViewById(R.id.signin_basic_ll);
        signin_red_ll = findViewById(R.id.signin_red_ll);
        register_basic_ll = findViewById(R.id.register_basic_ll);
        register_red_ll = findViewById(R.id.register_red_ll);
        google_login_ll = findViewById(R.id.google_login_ll);

        signin_basic_ll.setOnClickListener(this);
        register_basic_ll.setOnClickListener(this);

        if (getIntent().hasExtra("reg")){
            signin_red_ll.setVisibility(View.GONE);
            signin_basic_ll.setVisibility(View.VISIBLE);

            register_basic_ll.setVisibility(View.GONE);
            register_red_ll.setVisibility(View.VISIBLE);
            OpenFragment(new RegisterFragment());
        }
        else {
            signin_red_ll.setVisibility(View.VISIBLE);
            signin_basic_ll.setVisibility(View.GONE);

            register_basic_ll.setVisibility(View.VISIBLE);
            register_red_ll.setVisibility(View.GONE);
            OpenFragment(new SignInFragment());
        }

        google_login_ll.setOnClickListener(this);

        signInButton = findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(this);

        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.buttonFacebookLogin);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("loginFB", "facebook:onSuccess:" + loginResult);

            }

            @Override
            public void onCancel() {
                Log.d("loginFB", "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("loginFB", "facebook:onError", error);
                // ...
            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signin_basic_ll:
                signin_red_ll.setVisibility(View.VISIBLE);
                signin_basic_ll.setVisibility(View.GONE);

                register_basic_ll.setVisibility(View.VISIBLE);
                register_red_ll.setVisibility(View.GONE);

                OpenFragment(new SignInFragment());

                break;

            case R.id.register_basic_ll:
                signin_red_ll.setVisibility(View.GONE);
                signin_basic_ll.setVisibility(View.VISIBLE);

                register_basic_ll.setVisibility(View.GONE);
                register_red_ll.setVisibility(View.VISIBLE);

                OpenFragment(new RegisterFragment());

                break;

            case R.id.google_login_ll:
                signIn();
                break;

            case R.id.sign_in_button:
//                signIn();
                break;
        }
    }

    public void OpenFragment(Fragment fragment)
    {
        fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_out_right);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, fragment);
        fragmentTransaction.commit();
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }




}
