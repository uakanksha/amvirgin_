package com.example.amvirgin.payment.model;

import java.io.Serializable;

public class PaymentFieldModel implements Serializable {

    private String productId;
    private String productName;
    private String productDescription;
    private String image;
    private String subTotal;
    private String tax;
    private String totalAmount;
    private boolean delivery;
    private String deliveryPrice;
    private int address;

    public PaymentFieldModel(String productId, String productName,
                             String productDescription, String image,
                             String subTotal, String tax, String totalAmount,
                             boolean delivery, String deliveryPrice,
                             int address) {
        this.productId = productId;
        this.productName = productName;
        this.productDescription = productDescription;
        this.image = image;
        this.subTotal = subTotal;
        this.tax = tax;
        this.totalAmount = totalAmount;
        this.delivery = delivery;
        this.deliveryPrice = deliveryPrice;
        this.address = address;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public boolean isDelivery() {
        return delivery;
    }

    public void setDelivery(boolean delivery) {
        this.delivery = delivery;
    }

    public String getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(String deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }
}
