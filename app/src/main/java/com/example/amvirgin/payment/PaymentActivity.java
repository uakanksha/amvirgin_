package com.example.amvirgin.payment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.SplashActivity;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.payment.model.PaymentFieldModel;
import com.example.amvirgin.shopModule.Interceptor.BasicAuthInterceptor;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.AddAddressActivity;
import com.example.amvirgin.shopModule.activity.MyAddressActivity;
import com.example.amvirgin.shopModule.activity.OrderSuccessfulActivity;
import com.example.amvirgin.shopModule.interfaces.AddressSelectionListener;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.shopAdapter.AddressListAdapter;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;
import com.example.amvirgin.shopModule.shopModel.PlaceOrderModel;
import com.example.amvirgin.shopModule.shopModel.RazorPayOrdersModel;
import com.example.amvirgin.shopModule.shopModel.RetrieveCartModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.amvirgin.utils.Constants.CHECKOUT_DETAILS;
import static com.example.amvirgin.utils.Constants.brandList;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener, PaymentResultWithDataListener {
    private RelativeLayout back;
    private Button paymentMethodContinue;
    private RecyclerView recyclerView_paymentMethod;
    private RecyclerView.LayoutManager layoutManager;
    TextView tv_price_subtotal, tv_total_price, tv_tax, tv_total_price_, deliver_charge_tv;
    String price_subtotal, total_price_, tax;
    private Double total_price;
    private RadioGroup rg_payments;
    private int selected_payment_method;
    private int address_id;
    private String token, session_id;
    private RadioButton rb_card, rb_net_banking, wallet, rb_upi, cash_on_delivery;
    HashMap<String, String> value = new HashMap<>();
    List<RetrieveCartModel.Item> cartData;
    RegisterUserModel.userRegisterData userData;
    private final String TAG = "PAYMENT_INFO ";

    private String rp_orderId, desc, name_product;
    private String payment_method, transaction_id = null;

    private PaymentFieldModel paymentFieldModel;
    private LinearLayout delivery_layout_ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getString(getApplicationContext(), Constants.Key.ApiToken);
        }

        rb_card = findViewById(R.id.rb_card);
        rb_net_banking = findViewById(R.id.rb_net_banking);
        wallet = findViewById(R.id.wallet);
        rb_upi = findViewById(R.id.rb_upi);
        cash_on_delivery = findViewById(R.id.cash_on_delivery);

        tv_price_subtotal = findViewById(R.id.tv_price_subtotal);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_tax = findViewById(R.id.tv_tax);
        tv_total_price_ = findViewById(R.id.tv_total_price_);
        rg_payments = findViewById(R.id.rg_payments);
        delivery_layout_ll = findViewById(R.id.delivery_layout_ll);
        deliver_charge_tv  = findViewById(R.id.deliver_charge_tv);

        if (getIntent().getExtras()!=null)
        {
            paymentFieldModel = (PaymentFieldModel) getIntent().getExtras().getSerializable(Constants.CHECKOUT_DETAILS);
            tv_price_subtotal.setText("₹ "+paymentFieldModel.getSubTotal());
            tv_total_price.setText("₹ "+paymentFieldModel.getTotalAmount());
            tv_total_price_.setText("₹ "+paymentFieldModel.getTotalAmount());
            tv_tax.setText("₹ "+paymentFieldModel.getTax());
            address_id = paymentFieldModel.getAddress();

            if (paymentFieldModel.isDelivery())
            {
                delivery_layout_ll.setVisibility(View.VISIBLE);
                deliver_charge_tv.setText(paymentFieldModel.getDeliveryPrice());
//                address_layout_ll.setVisibility(View.VISIBLE);
                cash_on_delivery.setVisibility(View.VISIBLE);
//                getAddressListAddress(token);
            }
            else
                {
                cash_on_delivery.setVisibility(View.GONE);
                delivery_layout_ll.setVisibility(View.GONE);
            }
        }else {
            Log.e(TAG, "getIntent().getExtras()==null");
        }

        back = (RelativeLayout) findViewById(R.id.rlBack);
        paymentMethodContinue = (Button) findViewById(R.id.countinue_PaymentMethod);

        back.setOnClickListener(this);
        paymentMethodContinue.setOnClickListener(this);

        session_id = PreferenceManager.getString(getApplicationContext(), "session_id");

        if (token != null) {
            Log.e("dddddsd", "onCreate: " + token);
           // getUserProfile(token);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                finish();
                break;
            case R.id.countinue_PaymentMethod:
                if (rb_card.isChecked()) {
                    selected_payment_method = 1;
                    payment_method = "debit-card";
                } else if (rb_net_banking.isChecked()) {
                    selected_payment_method = 2;
                    payment_method = "net-banking";
                } else if (wallet.isChecked()) {
                    selected_payment_method = 3;
                    payment_method = "pay-tm";
                } else if (rb_upi.isChecked()) {
                    selected_payment_method = 4;
                    payment_method = "upi";
                }else if (cash_on_delivery.isChecked()) {
                    selected_payment_method = 5;
                    payment_method = "cash-on-delivery";
                }

                if (selected_payment_method > 0) {
                    if (selected_payment_method == 5){
                        placeOrder();
                    }else {
                       /* if (paymentFieldModel.isDelivery()){
                            address_id = paymentFieldModel.getAddress();
                            if (address_id!=-1){
                                Payment(payment_method);
                            }
                        }else {*/
                            Payment(payment_method);
//                        }

                    }
                } else {
                    ToastMessage.getInstance(PaymentActivity.this).showErrorShortCustomToast("Please Select a Payment Method!!");
                }
                break;
            case R.id.ll_add_address:
                startActivity(new Intent(PaymentActivity.this, AddAddressActivity.class));
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (paymentFieldModel.isDelivery()){
            delivery_layout_ll.setVisibility(View.VISIBLE);
            deliver_charge_tv.setText(paymentFieldModel.getDeliveryPrice());

        }else {
            delivery_layout_ll.setVisibility(View.GONE);
        }
    }

    private void addAddress(){

    }

    private void placeOrder() {
        JsonObject jsonObject = new JsonObject();

        if (payment_method.equalsIgnoreCase("cash-on-delivery")){
            jsonObject.addProperty("sessionId", session_id);
            jsonObject.addProperty("addressId", address_id);
            jsonObject.addProperty("paymentMode", payment_method);
        }
        else {
            jsonObject.addProperty("sessionId", session_id);
            jsonObject.addProperty("addressId", address_id);
            jsonObject.addProperty("paymentMode", payment_method);
            jsonObject.addProperty("transactionId", transaction_id);
        }


        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<PlaceOrderModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .placeOrder("application/json", "Bearer " + token, jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<PlaceOrderModel>(PaymentActivity.this) {
            @Override
            public void onResponse(PlaceOrderModel response) {
                if (response.getStatus() == 200) {
                    try {
                        ToastMessage.getInstance(PaymentActivity.this).showSuccessShortCustomToast(response.getMessage());

                        PreferenceManager.delete(PaymentActivity.this, "session_id");
                        PreferenceManager.saveInt(getApplicationContext(), "cart_count", 0);
                        SplashActivity.cart_products.clear();

                        Intent intent = new Intent(PaymentActivity.this, OrderSuccessfulActivity.class);
                        intent.putExtra("order_id", response.getOrderNumber());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } catch (Exception e) {
                        SnackbarManager.sException(PaymentActivity.this, response.getMessage());
                    }
                } else {
                    SnackbarManager.sException(PaymentActivity.this, response.getMessage());
                }
            }
        });
    }


    private void Payment(String payment_method) {

        final Activity activity = this;
        final Checkout co = new Checkout();
        co.setImage(R.drawable.logo);
        try {
            JSONObject options = new JSONObject();
            options.put("name", paymentFieldModel.getProductName());
            options.put("description", paymentFieldModel.getProductDescription());
//            options.put("image", R.drawable.logo);
            options.put("currency", "INR");
            options.put("amount", getCalculatedPrice(paymentFieldModel.getTotalAmount()));
            options.put("order_id", rp_orderId);
            JSONObject preFill = new JSONObject();
            preFill.put("email", PreferenceManager.getString(PaymentActivity.this, Constants.Key.EMAIL));
            preFill.put("contact", PreferenceManager.getString(PaymentActivity.this, Constants.Key.MOBILE));
            preFill.put("method", payment_method);
            options.put("prefill", preFill);
            co.open(activity, options);
        } catch (Exception e) {
            SnackbarManager.sException(PaymentActivity.this, "Exception Error in payment: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {

        Log.e("dsdsadjshd", "onPaymentSuccess: " + s);
        Log.e("Signature", "onPaymentSuccess: " + paymentData.getSignature());
        Log.e("OrderId", "onPaymentSuccess: " + paymentData.getOrderId());
        Log.e("PaymentId", "onPaymentSuccess: " + paymentData.getPaymentId());

        transaction_id = paymentData.getPaymentId();

//        Intent intent = new Intent(PaymentActivity.this, HomeActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
        if (paymentFieldModel.isDelivery()){
            placeOrder();
        }else {
            Log.e("sdsds", "onPaymentSuccess: 00" );
            finish();
        }

    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        SnackbarManager.sException(PaymentActivity.this, s);
//        Toast.makeText(PaymentActivity.this, "onPaymentError"+s, Toast.LENGTH_SHORT).show();
    }


    private String getCalculatedPrice(String price){
        return ""+(Double.parseDouble(price)*100);
    }
}
