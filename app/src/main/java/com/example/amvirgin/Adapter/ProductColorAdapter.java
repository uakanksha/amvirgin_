package com.example.amvirgin.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;

    public class ProductColorAdapter extends RecyclerView.Adapter<ProductColorAdapter.ProductColorHolder> {
        private int[] images;
        //
        Context context;
        private String[] best_name = {"Women Lace Maxi Dress","Women Lace Maxi Dress"," Women Lace Maxi Dress","Women Lace Maxi Dress","Women Lace Maxi Dress","Women Lace Maxi Dress","Women Lace Maxi Dress"};


        public ProductColorAdapter(int[] images) {
            this.context=context;
            this.images=images;
        }

        @NonNull
        @Override
        public ProductColorHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_shop_color_layout,parent,false);
            ProductColorHolder imageViewHolder =new ProductColorHolder(view);
//
            return imageViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ProductColorHolder holder, final int position) {

            int img_id = images[position];
            String text_id = best_name[position];
            holder.similar_img.setImageResource(img_id);
            holder.offer_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
//
        }

        @Override
        public int getItemCount() {
            return images.length;
        }
        public static class ProductColorHolder extends RecyclerView.ViewHolder {
            ImageView similar_img;
            TextView similar_name,offer_price;
            public ProductColorHolder(@NonNull View itemView) {
                super(itemView);
                similar_img = itemView.findViewById(R.id.img_color);
                offer_price=itemView.findViewById(R.id.offer_price_color);
            }
        }
    }