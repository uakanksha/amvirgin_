package com.example.amvirgin.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.example.amvirgin.Fragments.CreditsFragment;
import com.example.amvirgin.entertainmentModule.Activity.MovieDetailSlidingActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.classes.MovieDetailModel;
import com.example.amvirgin.classes.Seasons;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.example.amvirgin.R.layout.spinner_item_overview;

public class SlidingMovieDetailAdapter extends PagerAdapter implements View.OnClickListener {
    Context context;
    String poster;
    MovieDetailModel.Data data;
    List<Seasons> seasons;
    private ArrayList seasons_arrayList;
    private ArrayAdapter<CharSequence> mSeasonsAdapter;
    private LinearLayout show_ll, hide_ll, episodes_ll, credits_ll, ll_top, ep_ll, list_ll, watch_trailer_ll;
    private TextView description_text, title_tv, type_tv, duration_tv, genre_tv, pgRating_tv, language_tv;
    private ImageView banner_img;
    FragmentManager fragmentManager;

    public SlidingMovieDetailAdapter(Context context) {
        this.context = context;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = LayoutInflater.from(context.getApplicationContext())
                .inflate(R.layout.movie_detail_layout, view, false);

        banner_img = imageLayout.findViewById(R.id.banner_img);
        title_tv = imageLayout.findViewById(R.id.title_tv);
        type_tv = imageLayout.findViewById(R.id.type_tv);
        duration_tv = imageLayout.findViewById(R.id.duration_tv);
        genre_tv = imageLayout.findViewById(R.id.genre_tv);
        pgRating_tv = imageLayout.findViewById(R.id.pgRating_tv);
        language_tv = imageLayout.findViewById(R.id.language_tv);
        description_text = imageLayout.findViewById(R.id.description_text);
        final RecyclerView rv_episodes = imageLayout.findViewById(R.id.rv_episodes);

        final Spinner sp_seasons = imageLayout.findViewById(R.id.sp_seasons);
        final String[] seasons;
        final ArrayAdapter<CharSequence>[] mSeasonsAdapter;

        show_ll = imageLayout.findViewById(R.id.show_ll);
        hide_ll = imageLayout.findViewById(R.id.hide_ll);
        episodes_ll = imageLayout.findViewById(R.id.episodes_ll);
        credits_ll = imageLayout.findViewById(R.id.credits_ll);

        show_ll.setOnClickListener((View.OnClickListener) this);
        hide_ll.setOnClickListener((View.OnClickListener) this);
        episodes_ll.setOnClickListener((View.OnClickListener) this);
        credits_ll.setOnClickListener((View.OnClickListener) this);

        /*seasons = context.getResources().getStringArray(R.array.seasons);
        mSeasonsAdapter = new ArrayAdapter<CharSequence>(context, spinner_item_overview, seasons);
        mSeasonsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        sp_seasons.setAdapter(mSeasonsAdapter);
        sp_seasons.setAdapter(mSeasonsAdapter);*/

//        getDetails();

        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<MovieDetailModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class).getMovieDetails("");
        serviceWrapper.HandleResponse(call, new ResponseHandler<MovieDetailModel>(context) {
            @Override
            public void onResponse(MovieDetailModel response) {

                poster = response.getData().getPoster();

                data = response.getData();

                Picasso.with(context).load(poster).into(banner_img,
                        new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
//                                        pb.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });

                title_tv.setText(response.getData().getTitle());
//                trailer_url = response.getData().getTrailer();
                type_tv.setText(response.getData().getType());
                duration_tv.setText(response.getData().getDuration());
                genre_tv.setText(response.getData().getGenre());
                pgRating_tv.setText(response.getData().getPgRating());
                description_text.setText(response.getData().getDescription());
                if (description_text.getText().toString().length() >= 250) {
                    show_ll.setVisibility(View.VISIBLE);
                    hide_ll.setVisibility(View.VISIBLE);
                } else {
                    show_ll.setVisibility(View.GONE);
                    hide_ll.setVisibility(View.GONE);
                }
                if (response.getData().getHasSeasons()) {
//                    seasons = new ArrayList<>();
                    seasons_arrayList = new ArrayList<>();
                    for (int i = 1; i == response.getData().getContent().get(0).getSeason(); i++) {
//                        seasons.add(new Seasons("Season "+i));
                        seasons_arrayList.add("Season " + i);
                    }

                    ArrayAdapter<CharSequence> mSeasonsAdapter = new ArrayAdapter<CharSequence>(context, spinner_item_overview, seasons_arrayList);
                    mSeasonsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_seasons.setAdapter(mSeasonsAdapter);

                    LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                    rv_episodes.setLayoutManager(manager);

//                    EpisodesAdapter adapter = new EpisodesAdapter(context, data.getContent().get(0).getContent(), response.getData().getPoster());
//                    rv_episodes.setAdapter(adapter);


                }
            }
        });


        view.addView(imageLayout);

        return imageLayout;
    }


    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.show_ll:
                show_ll.setVisibility(View.GONE);
                hide_ll.setVisibility(View.VISIBLE);
                description_text.setMaxLines(Integer.MAX_VALUE);
                break;

            case R.id.hide_ll:
                hide_ll.setVisibility(View.GONE);
                show_ll.setVisibility(View.VISIBLE);
                description_text.setMaxLines(5);
                break;

            case R.id.credits_ll:
//                episodes_view.setVisibility(View.INVISIBLE);
//                credits_view.setVisibility(View.VISIBLE);
//                OpenFragment(new CreditsFragment());

                Bundle bundle = new Bundle();
                bundle.putSerializable("data", data);

                if (context instanceof MovieDetailSlidingActivity) {

                    MovieDetailSlidingActivity myActivity = (MovieDetailSlidingActivity)context;

                    fragmentManager = myActivity.getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    CreditsFragment creditsFragment = new CreditsFragment();
                    creditsFragment.setArguments(bundle);

                    creditsFragment.show(fragmentTransaction,"about");
                }

//                fragmentManager = ((AppCompatActivity)context).getSupportFragmentManager();

////                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down);
////                transact.add(R.id.item_container, productAlertFragment).addToBackStack(null).commit();

                break;

           /* case R.id.watch_trailer_ll:

                Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
                intent.putExtra("trailer_url", trailer_url);
                startActivity(intent);
                break;

            case R.id.play_rl:
                poster_frame.setVisibility(View.GONE);
                main_media_frame.setVisibility(View.VISIBLE);
                break;*/
        }
    }

    /*private void getDetails() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<MovieDetailModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class).getMovieDetails();
        serviceWrapper.HandleResponse(call, new ResponseHandler<MovieDetailModel>(context) {
            @Override
            public void onResponse(MovieDetailModel response) {

                poster = response.getData().getPoster();

                data = response.getData();

                Picasso.with(context).load(poster).into(banner_img,
                        new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
//                                        pb.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });

                title.setText(response.getData().getTitle());
                trailer_url = response.getData().getTrailer();
                type_tv.setText(response.getData().getType());
                duration_tv.setText(response.getData().getDuration());
                genre_tv.setText(response.getData().getGenre());
                pgRating_tv.setText(response.getData().getPgRating());
                description_text.setText(response.getData().getDescription());
                if (description_text.getText().toString().length() >= 250) {
                    show_ll.setVisibility(View.VISIBLE);
                    hide_ll.setVisibility(View.VISIBLE);
                } else {
                    show_ll.setVisibility(View.GONE);
                    hide_ll.setVisibility(View.GONE);
                }
                if (response.getData().getHasSeasons()) {
                    seasons = new ArrayList<>();
                    seasons_arrayList = new ArrayList<>();
                    for (int i = 1; i == response.getData().getContent().get(0).getSeason(); i++) {
//                        seasons.add(new Seasons("Season "+i));
                        seasons_arrayList.add("Season " + i);
                    }

                    mSeasonsAdapter = new ArrayAdapter<CharSequence>(context, spinner_item_overview, seasons_arrayList);
                    mSeasonsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_seasons.setAdapter(mSeasonsAdapter);

                    EpisodesAdapter adapter = new EpisodesAdapter(context, response.getData().getContent().get(0)
                            .getContent(), response.getData().getPoster());
                    rv_episodes.setAdapter(adapter);


                }
            }
        });
    }*/
}
