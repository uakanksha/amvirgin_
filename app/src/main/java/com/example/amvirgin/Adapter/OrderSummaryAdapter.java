package com.example.amvirgin.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;


public class OrderSummaryAdapter extends RecyclerView.Adapter<OrderSummaryAdapter.OrderSummaryViewHolder> implements AdapterView.OnItemSelectedListener{

    Context context;
    String[] country = { "QTY","1", "2", "3", "4", "5"};

    public OrderSummaryAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public OrderSummaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_shop_ordersummary_layout, parent, false);
        OrderSummaryViewHolder cvh = new OrderSummaryViewHolder(view);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderSummaryViewHolder holder, int position) {
//            holder.product_ll.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context, ProductDetailActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(intent);
//                }
//            });
        holder.spinner_qty.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(context,android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        holder.spinner_qty.setAdapter(aa);
        holder.price_withoutOffer.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);


    }

    @Override
    public int getItemCount() {
        return 4;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public class OrderSummaryViewHolder extends RecyclerView.ViewHolder {
        LinearLayout product_ll;
        Spinner spinner_qty;
        TextView price_withoutOffer;

        public OrderSummaryViewHolder(@NonNull View itemView) {
            super(itemView);
//                product_ll = itemView.findViewById(R.id.product_ll);
            price_withoutOffer=itemView.findViewById(R.id.price_withoutOffer);
            spinner_qty=itemView.findViewById(R.id.qty_spinner);
        }
    }
}