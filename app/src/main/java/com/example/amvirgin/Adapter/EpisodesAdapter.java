package com.example.amvirgin.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.classes.MovieDetailModel;
import com.example.amvirgin.interfaces.EpisodeClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EpisodesAdapter extends RecyclerView.Adapter<EpisodesAdapter.EpisodesViewHolder> {

    private List<MovieDetailModel.Data.Content.Content_> episodes;
    private List<MovieDetailModel.Data.Recommended> recommendeds;
    Context context;
    private MovieDetailModel.Data  model_data;
    private String poster;
    EpisodeClickListener episodeClickListener;

    public EpisodesAdapter(Context context, MovieDetailModel.Data  model_data, List<?> episodes,
                           String poster, EpisodeClickListener episodeClickListener) {
        this.context = context;
        this.model_data = model_data;
        if (model_data.getHasSeasons()){
            this.episodes = (List<MovieDetailModel.Data.Content.Content_>)episodes;
        }else {
            recommendeds = (List<MovieDetailModel.Data.Recommended>)episodes;
        }

        this.poster = poster;
        this.episodeClickListener = episodeClickListener;
    }

    @NonNull
    @Override
    public EpisodesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_episodes_layout, parent, false);
        EpisodesViewHolder cvh = new EpisodesViewHolder(view);

        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull EpisodesViewHolder holder, final int position) {

        if (model_data.getHasSeasons()){
            holder.description_container.setVisibility(View.VISIBLE);
            holder.tv_ep_title.setText(episodes.get(position).getTitle());
            holder.tv_ep_desc.setText(episodes.get(position).getDescription());
            Picasso.with(context).load(poster).into(holder.iv_ep_poster,
                    new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
//                                        pb.setVisibility(View.GONE);
                        }
                        @Override
                        public void onError() {

                        }
                    });
        }else{
            holder.tv_ep_title.setText(recommendeds.get(position).getTitle());
            holder.tv_ep_desc.setVisibility(View.GONE);
            if (recommendeds.get(position).getPoster()!=null&&recommendeds.get(position).getPoster().length()>0)
            Picasso.with(context).load(recommendeds.get(position).getPoster()).into(holder.iv_ep_poster,
                    new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
//                                        pb.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }
//        holder.tv_ep_title.setText(episodes.get(0).getContent().get(position).getTitle());
//        holder.tv_ep_desc.setText(episodes.get(0).getContent().get(position).getDescription());

//        holder.tv_ep_title.setText(episodes.get(position).getTitle());
//        holder.tv_ep_desc.setText(episodes.get(position).getDescription());



        holder.iv_ep_poster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model_data.getHasSeasons()){
                    episodeClickListener.onSeriesItemClick(episodes, position);

//                    episodeClickListener.onItemClick(episodes.get(0).getContent().get(position).getOptions().get(0).getUrl(),
//                            episodes.get(0).getContent().get(position).getOptions().get(0).getSubtitle().getUrl());
                }else {
                    episodeClickListener.onMovieItemClick(recommendeds.get(position));

//                    episodeClickListener.onItemClick(recommendeds.get(position).getTrailer(),
//                            "");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        int size = 0;
        if (model_data.getHasSeasons()){
            size = episodes.size();
        }else {
            size = recommendeds.size();
        }
        Log.e("sdsdas", "getItemCount: "+size);
        return size;
    }

    public class EpisodesViewHolder extends RecyclerView.ViewHolder {

        TextView tv_ep_title, tv_ep_desc;
        ImageView iv_ep_poster;
        LinearLayout description_container;
        public EpisodesViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_ep_title = itemView.findViewById(R.id.tv_ep_title);
            tv_ep_desc = itemView.findViewById(R.id.tv_ep_desc);
            iv_ep_poster = itemView.findViewById(R.id.iv_ep_poster);
            description_container = itemView.findViewById(R.id.description_layout);
        }
    }

}
