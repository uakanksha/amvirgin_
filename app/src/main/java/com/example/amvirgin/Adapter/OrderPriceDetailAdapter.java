package com.example.amvirgin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;


public class OrderPriceDetailAdapter extends RecyclerView.Adapter<OrderPriceDetailAdapter.PriceDetailViewHolder> {

    Context context;


    public OrderPriceDetailAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public PriceDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_order_price_details, parent, false);
        PriceDetailViewHolder cvh = new PriceDetailViewHolder(view);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull PriceDetailViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }



    public class PriceDetailViewHolder extends RecyclerView.ViewHolder {


        public PriceDetailViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}