package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;


public class ShopCategoryAdapter extends RecyclerView.Adapter<ShopCategoryAdapter.ImageViewHolder> {
        private int[] images;
//
        Context context;
        private String[] best_name = {"New Arrival","Am Virgin","Men","Women","Home","Kitchen","Electronics","Accessories"};


        public ShopCategoryAdapter(int[] images, Context context) {
            this.context= context;
            this.images=images;
        }

        @NonNull
        @Override
        public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_category_layout,parent,false);
            ImageViewHolder imageViewHolder =new ImageViewHolder(view);
//
            return imageViewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ImageViewHolder holder, final int position) {

            int img_id = images[position];
            String text_id = best_name[position];
            holder.shopCategory_img.setImageResource(img_id);
            holder.shopCategoy_name.setText(text_id);
//
            holder.ll_shopCategory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context.getApplicationContext(), ShopProductListActivity.class);
//
                    context.startActivity(i);
//                Toast.makeText(view.getContext(),"dghfjhd",Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return images.length;
        }
        public static class ImageViewHolder extends RecyclerView.ViewHolder {
            ImageView shopCategory_img;
            TextView shopCategoy_name,offer_price;
            LinearLayout ll_shopCategory;
            public ImageViewHolder(@NonNull View itemView) {
                super(itemView);
                shopCategory_img = itemView.findViewById(R.id.shopCategory_img);
                shopCategoy_name = itemView.findViewById(R.id.shop_categoryName);
                ll_shopCategory = itemView.findViewById(R.id.ll_shopCategory);
//                offer_price=itemView.findViewById(R.id.offer_price);
            }
        }
    }