package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.entertainmentModule.Activity.CollectionActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.Slide;
import com.example.amvirgin.utils.AppLocal;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.LayoutInflater.from;

public class ParentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "ParentAdapter";
    ArrayList<Object> alldata = null;
    Context context;
    private static final int TOPPICKS = 1;
    private static final int SHOP = 2;
    private static final int JUSTADDED = 3;
    private static final int TRENDING = 4;
    private static final int HOMESLIDER = 0;
    public ParentAdapter(Context context, ArrayList<Object> getlist) {
        this.context = context;

        this.alldata = getlist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        switch (viewType) {

            case HOMESLIDER:
                return new HomesliderViewHolder(from(parent.getContext()).inflate(R.layout.homeslider, parent, false));

            case TOPPICKS:
                return new ToppicksViewHolder(from(parent.getContext()).inflate(R.layout.toppicks, parent, false));

            case SHOP:
                return new ShopViewHolder(from(parent.getContext()).inflate(R.layout.shoppicks, parent, false));


            case JUSTADDED:
                return new JustaddedViewHolder(from(parent.getContext()).inflate(R.layout.jusaddedpicks, parent, false));


            case TRENDING:

                return new TrendingViewHolder(from(parent.getContext()).inflate(R.layout.trendingpicks, parent, false));
            default:
                return null;
        }

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {

            case HOMESLIDER:

                final List<Slide> slides = new ArrayList<>();
                slides.add(new Slide(R.drawable.banner1, "URI: The Surgical \nStrike"));
                ((HomesliderViewHolder) holder).indicator.setupWithViewPager(((HomesliderViewHolder) holder).mPager, true);
                ((HomesliderViewHolder) holder).mPager.setAdapter(new SlidingImageAdapter(context, AppLocal.data, slides, AppLocal.type));
                ( (HomesliderViewHolder)holder).t.scheduleAtFixedRate(new TimerTask() {

                    @Override
                    public void run() {
                                           if (context != null) {
                                               ((HomeActivity)context). runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (AppLocal.type.equalsIgnoreCase("data")) {
                            if (((HomesliderViewHolder) holder).mPager.getCurrentItem() < AppLocal.data.size() - 1) {
                                ((HomesliderViewHolder) holder).mPager.setCurrentItem(((HomesliderViewHolder) holder).mPager.getCurrentItem() + 1);
                            } else {
                                ((HomesliderViewHolder) holder).mPager.setCurrentItem(0);
                            }
                        }
                        else if (AppLocal.type.equalsIgnoreCase("nodata")){
                            if (((HomesliderViewHolder) holder).mPager.getCurrentItem() < slides.size() - 1) {
                                ((HomesliderViewHolder) holder).mPager.setCurrentItem(((HomesliderViewHolder) holder).mPager.getCurrentItem() + 1);
                            } else {
                                ((HomesliderViewHolder) holder). mPager.setCurrentItem(0);
                            }
                        }
                    }
                });
            }
                    }

                },  4000, 3000);
                break;



            case TOPPICKS:
            /*    rv_top_picks.setHasFixedSize(true);
                rv_top_picks.setItemViewCacheSize(20);
                rv_top_picks.setDrawingCacheEnabled(true);*/


                ((ToppicksViewHolder) holder).tv_sectionheading.setText("TOP PICKS");
                ((ToppicksViewHolder) holder).btn_trendingsee_all1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, CollectionActivity.class);
                        intent.putExtra("header", "Top Picks");
                        context.startActivity(intent);
                        Log.d(TAG, "onClick:ToppicksViewHolder clicked ");
                    }
                });

                ((ToppicksViewHolder)holder).rv_top_picks.setHasFixedSize(true);
                ((ToppicksViewHolder)holder).rv_top_picks.setItemViewCacheSize(20);
                ((ToppicksViewHolder)holder).rv_top_picks.setDrawingCacheEnabled(true);
                HomeDisplayAdapter homeDisplayAdapter=   new HomeDisplayAdapter(context, alldata, "toppicks");
                ((ToppicksViewHolder) holder).rv_top_picks.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                ((ToppicksViewHolder) holder).rv_top_picks.setAdapter(homeDisplayAdapter);

                Log.e("parent adapter ", " ToppicksViewHolder onBindViewHolder" );
                return;

            case SHOP:
                ((ShopViewHolder) holder).tv_shoppicks.setText("SHOP");
                ((ShopViewHolder) holder).btn_shoppicks_see_all.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent intent = new Intent(context, CollectionActivity.class);
//                        context.startActivity(intent);
                        Log.d(TAG, "onClick:ShopViewHolder clicked ");

                    }
                });
                ((ShopViewHolder)holder).rv_shop.setHasFixedSize(true);
                ((ShopViewHolder)holder).rv_shop.setItemViewCacheSize(20);
                ((ShopViewHolder)holder).rv_shop.setDrawingCacheEnabled(true);
                Log.e("parent adapter ", " ShopViewHolder onBindViewHolder" );
                HomeDisplayAdapter homeDisplayAdapter1=  new HomeDisplayAdapter(context, alldata, "shop");
               ((ShopViewHolder) holder).rv_shop.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                ((ShopViewHolder) holder).rv_shop.setAdapter(homeDisplayAdapter1);

                return;
            case JUSTADDED:
                ((JustaddedViewHolder) holder).tv_sectionheading.setText("JUST ADDED");
                ((JustaddedViewHolder) holder).btn_trendingsee_all1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, CollectionActivity.class);
                        intent.putExtra("header", "Just Added");
                        context.startActivity(intent);
                        Log.d(TAG, "onClick:JustaddedViewHolder clicked ");
                    }
                });

                ((JustaddedViewHolder)holder).rv_justadded_picks.setHasFixedSize(true);
                ((JustaddedViewHolder)holder).rv_justadded_picks.setItemViewCacheSize(20);
                ((JustaddedViewHolder)holder).rv_justadded_picks.setDrawingCacheEnabled(true);
                HomeDisplayAdapter homeDisplayAdapter2= new HomeDisplayAdapter(context, alldata, "justadded");

                PagerSnapHelper snapHelper = new PagerSnapHelper() {
                    @Override
                    public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                        View centerView = findSnapView(layoutManager);
                        if (centerView == null)
                            return RecyclerView.NO_POSITION;

                        int position = layoutManager.getPosition(centerView);
                        int targetPosition = -1;
                        if (layoutManager.canScrollHorizontally()) {
                            if (velocityX < 0) {
                                targetPosition = position - 1;
                            } else {
                                targetPosition = position + 1;
                            }
                        }

                        if (layoutManager.canScrollVertically()) {
                            if (velocityY < 0) {
                                targetPosition = position - 1;
                            } else {
                                targetPosition = position + 1;
                            }
                        }

                        final int firstItem = 0;
                        final int lastItem = layoutManager.getItemCount() - 1;
                        targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                        return targetPosition;
                    }
                };
                snapHelper.attachToRecyclerView(((JustaddedViewHolder) holder).rv_justadded_picks);

                ((JustaddedViewHolder) holder).rv_justadded_picks.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                ((JustaddedViewHolder) holder).rv_justadded_picks.setAdapter(homeDisplayAdapter2);
                Log.e("parent adapter ", " JustaddedViewHolder onBindViewHolder" );

                return;
            case TRENDING:

                ((TrendingViewHolder)holder).rv_trending_now.setHasFixedSize(true);
                ((TrendingViewHolder)holder).rv_trending_now.setItemViewCacheSize(20);
                ((TrendingViewHolder)holder).rv_trending_now.setDrawingCacheEnabled(true);
                HomeDisplayAdapter homeDisplayAdapter3=   new HomeDisplayAdapter(context, alldata, "trending");

                PagerSnapHelper snapHelper2 = new PagerSnapHelper() {
                    @Override
                    public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                        View centerView = findSnapView(layoutManager);
                        if (centerView == null)
                            return RecyclerView.NO_POSITION;

                        int position = layoutManager.getPosition(centerView);
                        int targetPosition = -1;
                        if (layoutManager.canScrollHorizontally()) {
                            if (velocityX < 0) {
                                targetPosition = position - 1;
                            } else {
                                targetPosition = position + 1;
                            }
                        }

                        if (layoutManager.canScrollVertically()) {
                            if (velocityY < 0) {
                                targetPosition = position - 1;
                            } else {
                                targetPosition = position + 1;
                            }
                        }

                        final int firstItem = 0;
                        final int lastItem = layoutManager.getItemCount() - 1;
                        targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                        return targetPosition;
                    }
                };
                snapHelper2.attachToRecyclerView(((TrendingViewHolder) holder).rv_trending_now);



                ((TrendingViewHolder) holder).rv_trending_now.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
                ((TrendingViewHolder) holder).rv_trending_now.setAdapter(homeDisplayAdapter3);
                Log.e("parent adapter ", " TrendingViewHolder onBindViewHolder" );

                return;

        }

    }




    @Override
    public int getItemCount() {
        return alldata.size();


    }


    public static class ToppicksViewHolder extends RecyclerView.ViewHolder {

        TextView tv_sectionheading;
        Button btn_trendingsee_all1;
        RecyclerView rv_top_picks;

        public ToppicksViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_sectionheading = itemView.findViewById(R.id.tv_sectionheading);
            btn_trendingsee_all1 = itemView.findViewById(R.id.btn_trendingsee_all1);
            rv_top_picks = itemView.findViewById(R.id.rv_top_picks);


        }
    }

    public static class ShopViewHolder extends RecyclerView.ViewHolder {
        TextView tv_shoppicks;
        Button btn_shoppicks_see_all;
        RecyclerView rv_shop;

        public ShopViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_shoppicks = itemView.findViewById(R.id.tv_shoppicks);
            btn_shoppicks_see_all = itemView.findViewById(R.id.btn_shoppicks_see_all);
            rv_shop = itemView.findViewById(R.id.rv_shop);
        }
    }


    public static class JustaddedViewHolder extends RecyclerView.ViewHolder {

        TextView tv_sectionheading;
        Button btn_trendingsee_all1;
        RecyclerView rv_justadded_picks;

        public JustaddedViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_sectionheading = itemView.findViewById(R.id.tv_sectionheading);
            btn_trendingsee_all1 = itemView.findViewById(R.id.btn_trendingsee_all1);
            rv_justadded_picks = itemView.findViewById(R.id.rv_justadded_picks);

        }
    }

    public int getItemViewType(int i) {

        if (i == 0) {
            return HOMESLIDER;}

        else if (i == 1 ) {
            return TOPPICKS;
        } else if (i == 2)
            return SHOP;
        else if (i == 3)
            return JUSTADDED;

        else if (i == 4)
            return TRENDING;


        else
            return 0;


    }

    public static class TrendingViewHolder extends RecyclerView.ViewHolder {

        RecyclerView rv_trending_now;

        public TrendingViewHolder(@NonNull View itemView) {
            super(itemView);
            rv_trending_now = itemView.findViewById(R.id.rv_trending_now);

        }
    }

    public static class HomesliderViewHolder extends RecyclerView.ViewHolder {
        private static ViewPager mPager;
        private TabLayout indicator;
        Timer t = new Timer();

        public HomesliderViewHolder(@NonNull View itemView) {
            super(itemView);
            mPager = (ViewPager)itemView.findViewById(R.id.pager);
            indicator = itemView.findViewById(R.id.indicator);

        }
    }

}
