package com.example.amvirgin.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.amvirgin.Fragments.ChatMateFragment;
import com.example.amvirgin.Fragments.CreditsFragment;
import com.example.amvirgin.Fragments.ReviewImageFragment;
import com.example.amvirgin.Fragments.ReviewRatingFragment;

public class MyPagerReviewAdapter extends FragmentPagerAdapter {
    String name, image;
    int key;

    public MyPagerReviewAdapter(FragmentManager fm, String name, String image, int key) {
        super(fm);
        this.name = name;
        this.image = image;
        this.key = key;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ReviewRatingFragment fm = new ReviewRatingFragment(name, image, key);
                return fm;
            case 1:
                return new ReviewImageFragment(name, image, key);

        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}