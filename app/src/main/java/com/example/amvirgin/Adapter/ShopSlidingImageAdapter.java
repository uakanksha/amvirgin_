package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.example.amvirgin.MovieDetailActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.Slide;

import java.util.ArrayList;
import java.util.List;

public class ShopSlidingImageAdapter extends PagerAdapter {

    List<Slide> slides;
    Context context;
    private LayoutInflater inflater;
    ArrayList<String> bannerMovieNames;

    public ShopSlidingImageAdapter(Context context, List<Slide> slides) {
        this.context = context;
        this.slides = slides;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = LayoutInflater.from(context).inflate(R.layout.shop_banner_layout, view, false);

//        assert imageLayout != null;
        ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.banner_img);
        TextView movie_name = (TextView) imageLayout.findViewById(R.id.movie_name);
        RelativeLayout banner_container = (RelativeLayout) imageLayout.findViewById(R.id.banner_container);


        imageView.setImageResource(slides.get(position).getImage());
        movie_name.setText(slides.get(position).getMovie());

        banner_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieDetailActivity.class);
                context.startActivity(intent);
            }
        });

        view.addView(imageLayout);

        return imageLayout;
    }

    @Override
    public int getCount() {
        return slides.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
