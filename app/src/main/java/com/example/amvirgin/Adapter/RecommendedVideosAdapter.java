package com.example.amvirgin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;

import java.util.ArrayList;

public class RecommendedVideosAdapter extends RecyclerView.Adapter <RecommendedVideosAdapter.RecommendedVideosViewHolder>{
    private Context context;
    ArrayList rVideos;

    public RecommendedVideosAdapter(Context context, ArrayList rVideos) {
        this.context = context;
        this.rVideos = rVideos;
    }

    @NonNull
    @Override
    public RecommendedVideosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_recommended_videos_layout, parent, false);
        RecommendedVideosViewHolder holder = new RecommendedVideosViewHolder(view);

        return  holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecommendedVideosViewHolder holder, int position) {
        holder.rVideos_img.setImageResource((Integer) rVideos.get(position));
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class RecommendedVideosViewHolder extends RecyclerView.ViewHolder{
        ImageView rVideos_img;

        public RecommendedVideosViewHolder(@NonNull View itemView) {
            super(itemView);
            rVideos_img = itemView.findViewById(R.id.rVideos_img);
        }
    }

}
