package com.example.amvirgin.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.interfaces.AttributeSelectionListener;
import com.example.amvirgin.shopModule.shopModel.ProductDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class ProductVariantsAdapter extends RecyclerView.Adapter<ProductVariantsAdapter.ProductVariantsViewHolder> {

    Context context;
    List<ProductDetailsModel.Option_> options;
    List<ProductDetailsModel.Option> opt;
    AttributeSelectionListener attributeSelectionListener;

    ArrayList val;
    String valStr;

    public ProductVariantsAdapter(Context context, List<ProductDetailsModel.Option> opt, List<ProductDetailsModel.Option_> options,
                                  AttributeSelectionListener attributeSelectionListener) {
        this.context = context;
        this.options = options;
        this.opt = opt;
        this.attributeSelectionListener = attributeSelectionListener;
    }

    @NonNull
    @Override
    public ProductVariantsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_variants_layout, parent, false);
        ProductVariantsViewHolder holder = new ProductVariantsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductVariantsViewHolder holder, int position) {

        if (options != null) {

            holder.tv_key.setText(options.get(position).getLabel() + " : ");

            if (options.get(position).getValue().getClass().
                    getSimpleName().equalsIgnoreCase("ArrayList")) {
                val = (ArrayList) options.get(position).getValue();

                holder.tv_value.setText(val.get(0).toString()+"  ");

            } else if (options.get(position).getValue()
                    .getClass().getSimpleName().equalsIgnoreCase("String")) {
                valStr = (String) options.get(position).getValue();

                holder.tv_value.setText(valStr+"  ");
            }

            holder.tv_key.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (attributeSelectionListener != null){
                        attributeSelectionListener.onItemClick(1);
                    }

                }
            });

            holder.tv_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (attributeSelectionListener != null){
                        attributeSelectionListener.onItemClick(1);
                    }
                }
            });

        }
        else if (opt != null){
            holder.tv_key.setText(opt.get(position).getLabel() + " : ");

            if (opt.get(position).getValue().getClass().
                    getSimpleName().equalsIgnoreCase("ArrayList")) {
                val = (ArrayList) opt.get(position).getValue();

                holder.tv_value.setText(val.get(0).toString());

            } else if (opt.get(position).getValue()
                    .getClass().getSimpleName().equalsIgnoreCase("String")) {
                valStr = (String) opt.get(position).getValue();

                holder.tv_value.setText(valStr);
            }
        }


    }

    @Override
    public int getItemCount() {
        if (options != null){
            return options.size();
        }
        else if (opt != null){
            return opt.size();
        }
        else {
            return 0;
        }
    }

    public class ProductVariantsViewHolder extends RecyclerView.ViewHolder{
        TextView tv_key, tv_value;

        public ProductVariantsViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_key = itemView.findViewById(R.id.tv_key);
            tv_value = itemView.findViewById(R.id.tv_value);
        }
    }
}
