package com.example.amvirgin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;

public class OrderDeliveryAdapter extends RecyclerView.Adapter<OrderDeliveryAdapter.OrderDeliveryViewHolder> {

    Context context;


    public OrderDeliveryAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public OrderDeliveryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_order_delivery, parent, false);
        OrderDeliveryViewHolder cvh = new OrderDeliveryViewHolder(view);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDeliveryViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }



    public class OrderDeliveryViewHolder extends RecyclerView.ViewHolder {


        public OrderDeliveryViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}