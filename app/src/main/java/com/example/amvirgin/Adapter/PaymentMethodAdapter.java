package com.example.amvirgin.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodViewHolder> {

        Context context;


        public PaymentMethodAdapter(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public PaymentMethodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_payment_menthod_layout, parent, false);
            PaymentMethodViewHolder cvh = new PaymentMethodViewHolder(view);
            return cvh;
        }

        @Override
        public void onBindViewHolder(@NonNull PaymentMethodViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 1;
        }



        public class PaymentMethodViewHolder extends RecyclerView.ViewHolder {


            public PaymentMethodViewHolder(@NonNull View itemView) {
                super(itemView);
            }
        }
    }