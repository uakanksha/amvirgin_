package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ShopMyOrderDetailsActivity;
import com.example.amvirgin.shopModule.activity.OrderTrackingActivity;
import com.example.amvirgin.shopModule.shopAdapter.MyOrdersItemAdapter;
import com.example.amvirgin.shopModule.shopModel.OrderListModel;

import java.util.List;


public class ShopMyOrderListAdapter extends RecyclerView.Adapter<ShopMyOrderListAdapter.ShopMyOrderViewHolder> {
        Context context;
    List<OrderListModel.Datum> data;

        public ShopMyOrderListAdapter(Context context, List<OrderListModel.Datum> data) {
            this.context = context;
            this.data = data;
        }

        @NonNull
        @Override
        public ShopMyOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_my_orders_layout, parent, false);
            ShopMyOrderViewHolder cvh = new ShopMyOrderViewHolder(view);
            return cvh;
        }

        @Override
        public void onBindViewHolder(@NonNull ShopMyOrderViewHolder holder, final int position) {

            holder.tv_order_no.setText(data.get(position).getOrderNumber());

            if (data.get(position).getItems().size() > 0) {

            LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            holder.rv_my_orders.setLayoutManager(manager);

            MyOrdersItemAdapter adapter = new MyOrdersItemAdapter(context, data.get(position).getItems(),
                        data.get(position).getStatus(), data.get(position).getTotal());
            holder.rv_my_orders.setAdapter(adapter);
            }
            else {
                holder.rv_my_orders.setVisibility(View.GONE);
            }

            holder.tv_order_details.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ShopMyOrderDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("orderId", data.get(position).getKey());
                    context.startActivity(intent);
                }
            });

            holder.track_order_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, OrderTrackingActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("orderId", data.get(position).getKey());
                    context.startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public class ShopMyOrderViewHolder extends RecyclerView.ViewHolder {
            TextView tv_order_no, tv_order_details;
            RecyclerView rv_my_orders;
            LinearLayout track_order_ll;

            public ShopMyOrderViewHolder(@NonNull View itemView) {
                super(itemView);
                tv_order_no = itemView.findViewById(R.id.tv_order_no);
                rv_my_orders = itemView.findViewById(R.id.rv_my_orders);
                tv_order_details = itemView.findViewById(R.id.tv_order_details);
                track_order_ll = itemView.findViewById(R.id.track_order_ll);
            }
        }
    }