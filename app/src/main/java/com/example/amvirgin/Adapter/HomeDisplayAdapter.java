package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.entertainmentModule.Activity.MovieDetailSlidingActivity;
import com.example.amvirgin.R;

import java.util.ArrayList;

import static com.example.amvirgin.utils.AppLocal.justAdded;
import static com.example.amvirgin.utils.AppLocal.shopArrayList;
import static com.example.amvirgin.utils.AppLocal.shopName;
import static com.example.amvirgin.utils.AppLocal.shopType;
import static com.example.amvirgin.utils.AppLocal.topPicks;
import static com.example.amvirgin.utils.AppLocal.trendingPicks;

public class HomeDisplayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    ArrayList<?> datalist;
    Context context;
    String trending;

    private static int TRENDING_DATA_TYPE = 4;
    private static int TOPPICKS_DATA_TYPE = 1;
    private static int JUSTADDED_DATA_TYPE = 3;

    private static int SHOP_DATA_TYPE = 2;


    public HomeDisplayAdapter(Context context,  ArrayList<?> datalist,String trending){
        this.context = context;
        this.datalist = datalist;
        this.trending=trending;
        Log.e("HomeDisplayAdapter", "HomeDisplayAdapter: "+trending );
    }

    @Override
    public int getItemViewType(int position) {

        if (trending.equalsIgnoreCase("trending")){
            return TRENDING_DATA_TYPE;
        }
        else if (trending.equalsIgnoreCase("toppicks")){
            return TOPPICKS_DATA_TYPE;
        }
        else if (trending.equalsIgnoreCase("justadded")){
            return JUSTADDED_DATA_TYPE;
        }
        else if (trending.equalsIgnoreCase("shop")){
            return SHOP_DATA_TYPE;
        }

        else{
            return 0;
        }
//        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TOPPICKS_DATA_TYPE)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_display_layout, parent, false);
            ToppicksViewHolder cvh = new ToppicksViewHolder(view);
            Log.e("home displayadapter ", " TOPPICKS_DATA_TYPE onCreateViewHolder" );
            return cvh;
        }

        else if (viewType == JUSTADDED_DATA_TYPE)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_display_layout, parent, false);
            JustaddedViewHolder cvh = new JustaddedViewHolder(view);
            Log.e("home displayadapter ", " JUSTADDED_DATA_TYPE onCreateViewHolder" );
            return cvh;
        }
        else if (viewType == TRENDING_DATA_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_layout, parent, false);
            TrendingViewHolder cvh = new TrendingViewHolder(view);
            Log.e("home displayadapter ", " TRENDING_DATA_TYPE onCreateViewHolder" );
            return cvh;
        }
        else if (viewType == SHOP_DATA_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_layout, parent, false);
            ShopViewHolder cvh = new ShopViewHolder(view);
            Log.e("home displayadapter ", " SHOP_DATA_TYPE onCreateViewHolder" );
            return cvh;
        }

        else {
            return null;
        }
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        switch (trending)
        {
            case "toppicks":


if(!topPicks.isEmpty()) {
    Glide.with(context).load(topPicks.get(position).getPoster()).placeholder(R.drawable.empty)
            .into(((ToppicksViewHolder) holder).banner_img);

    ((ToppicksViewHolder) holder).name.setVisibility(View.VISIBLE);
    ((ToppicksViewHolder) holder).type.setVisibility(View.VISIBLE);
    ((ToppicksViewHolder) holder).name.setText(topPicks.get(position).getTitle());
    ((ToppicksViewHolder) holder).type.setText(topPicks.get(position).getType());
//                Glide.with(context).load(AppLocal.topPicksArrayList.get(position)).into(((ToppicksViewHolder)holder).banner_img);
//                ((ToppicksViewHolder)holder).name.setText(String.valueOf(AppLocal.topPicksName.get(position)));
//                ((ToppicksViewHolder)holder).type.setText(String.valueOf(AppLocal.topPicksType.get(position)));
                ((ToppicksViewHolder) holder).banner_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       /* Intent intent = new Intent(context, MovieDetailActivity.class);
                        context.startActivity(intent);*/
                        Intent intent = new Intent(context, MovieDetailSlidingActivity.class);
//                            intent.putExtra("slug", (Serializable) justAdded);
                        intent.putExtra("position", String.valueOf(position));
                        intent.putExtra("type", "toppicks");
                        context.startActivity(intent);

                    }
                });
}else {
    ((ToppicksViewHolder) holder).banner_img.setImageResource(R.drawable.empty);
    ((ToppicksViewHolder) holder).name.setVisibility(View.GONE);
    ((ToppicksViewHolder) holder).type.setVisibility(View.GONE);
}
                Log.e("home displayadapter ", " toppicks onBindViewHolder" );
                break;


            case "shop":
                Log.e("API TOPPICKS", "onBindViewHolder: hello"+justAdded);


                Glide.with(context).load(shopArrayList.get(position)).placeholder(R.drawable.empty).into( ((ShopViewHolder)holder).banner_img);
                ((ShopViewHolder)holder).name.setText(String.valueOf(shopName.get(position)));
                ((ShopViewHolder)holder).type.setText(String.valueOf(shopType.get(position)));
                Log.e("home displayadapter ", " shop onBindViewHolder" );

                ((ShopViewHolder)holder).banner_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Intent intent=new Intent(context, MovieDetailActivity.class);
//                        context.startActivity(intent);
                    }
                });

                break;
            case "justadded":
                if(!justAdded.isEmpty()) {
                    Glide.with(context).load(justAdded.get(position).getPoster())
                            .placeholder(R.drawable.empty).into(((JustaddedViewHolder) holder).banner_img);

                    ((JustaddedViewHolder) holder).name.setVisibility(View.VISIBLE);
                    ((JustaddedViewHolder) holder).type.setVisibility(View.VISIBLE);
                    ((JustaddedViewHolder) holder).name.setText(justAdded.get(position).getTitle());
                    ((JustaddedViewHolder) holder).type.setText(justAdded.get(position).getType());
                    Log.e("home displayadapter ", " justadded onBindViewHolder");
     /*   Glide.with(context).load(AppLocal.topPicksArrayList.get(position)).into( ((JustaddedViewHolder)holder).banner_img);
        ((JustaddedViewHolder)holder).name.setText(String.valueOf(AppLocal.topPicksName.get(position)));
        ((JustaddedViewHolder)holder).type.setText(String.valueOf(AppLocal.topPicksType.get(position)));*/

                    ((JustaddedViewHolder) holder).banner_img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, MovieDetailSlidingActivity.class);
//                            intent.putExtra("slug", (Serializable) justAdded);
                            intent.putExtra("position", String.valueOf(position));
                            intent.putExtra("type", "justadded");
                            context.startActivity(intent);
                        }
                    });
                }else
                {
                    ((JustaddedViewHolder) holder).banner_img.setImageResource(R.drawable.empty);
                    ((JustaddedViewHolder) holder).name.setVisibility(View.GONE);
                    ((JustaddedViewHolder) holder).type.setVisibility(View.GONE);

                }        break;
            case "trending":
//                Log.e("home displayadapter ", " trending onBindViewHolder"+trendingPicks.get(position).getBackdrop()+"///"+ trendingPicks.get(position).getTitle());

                if(!trendingPicks.isEmpty()) {
                    Glide.with(context).load(trendingPicks.get(position).getPoster())
                            .placeholder(R.drawable.empty).into(((TrendingViewHolder) holder).banner_img);

                    ((TrendingViewHolder) holder).name.setVisibility(View.VISIBLE);
                    ((TrendingViewHolder) holder).type.setVisibility(View.VISIBLE);
                    ((TrendingViewHolder) holder).name.setText(trendingPicks.get(position).getTitle());
                    ((TrendingViewHolder) holder).type.setText(trendingPicks.get(position).getType());


       /* Glide.with(context).load(AppLocal.topPicksArrayList.get(position)).into( ((TrendingViewHolder)holder).banner_img);
        ((TrendingViewHolder)holder).name.setText(String.valueOf(AppLocal.topPicksName.get(position)));
        ((TrendingViewHolder)holder).type.setText(String.valueOf(AppLocal.topPicksType.get(position)));*/
                    ((TrendingViewHolder) holder).banner_img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(context, MovieDetailSlidingActivity.class);
//                            intent.putExtra("slug", (Serializable) justAdded);
                            intent.putExtra("position", String.valueOf(position));
                            intent.putExtra("type", "trending");
                            context.startActivity(intent);


                            Log.e("0clicked", "onClick: " + trending);
                        }
                    });
                }else
                {
                    ((TrendingViewHolder) holder).banner_img.setImageResource(R.drawable.empty);
                    ((TrendingViewHolder) holder).name.setVisibility(View.GONE);
                    ((TrendingViewHolder) holder).type.setVisibility(View.GONE);
                }
                break;

        }




    }

    @Override
    public int getItemCount() {
        if(trending.equalsIgnoreCase("toppicks") && topPicks.size()>0)
            return topPicks.size();


        else if(trending.equalsIgnoreCase("justadded")&& justAdded.size()>0)

        {
            return justAdded.size();
        }
        else if(trending.equalsIgnoreCase("trending")&& trendingPicks.size()>0)

        {
            return trendingPicks.size();
        }

        else if(trending.equalsIgnoreCase("shop")&& shopArrayList.size()>0)

        {return shopArrayList.size();

        }else
            return 1;
    }

    public class BannerViewHolder extends RecyclerView.ViewHolder{

        ImageView banner_img;
        TextView name, type;
        RelativeLayout banner_container_top;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            banner_container_top = itemView.findViewById(R.id.banner_container_top);

        }
    }
    public class ToppicksViewHolder extends RecyclerView.ViewHolder{

        ImageView banner_img;
        TextView name, type;
        RelativeLayout banner_container_top;

        public ToppicksViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            banner_container_top = itemView.findViewById(R.id.banner_container_top);

        }
    }
    public class ShopViewHolder extends RecyclerView.ViewHolder{

        ImageView banner_img;
        TextView name, type;
        RelativeLayout banner_container_top;

        public ShopViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            banner_container_top = itemView.findViewById(R.id.banner_container_top);

        }
    }


    public class JustaddedViewHolder extends RecyclerView.ViewHolder{

        ImageView banner_img;
        TextView name, type;
        RelativeLayout banner_container_top;

        public JustaddedViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            banner_container_top = itemView.findViewById(R.id.banner_container_top);

        }
    }


    public class TrendingViewHolder extends RecyclerView.ViewHolder{

        ImageView banner_img;
        TextView name, type;
        RelativeLayout banner_container_top;

        public TrendingViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.trendingbanner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            banner_container_top = itemView.findViewById(R.id.trendingbanner_container);

        }
    }

}
