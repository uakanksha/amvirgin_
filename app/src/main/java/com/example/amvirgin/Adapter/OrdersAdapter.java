package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.ReviewProductActivity;
import com.example.amvirgin.shopModule.shopAdapter.ProductAttributeAdapter;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.OrderDetailsModel;
import com.example.amvirgin.shopModule.shopModel.OrderListModel;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrderViewHolder> {

    Context context;
    List<OrderDetailsModel.Item> items;
    String status;
    Integer total;
    List<AddToCartModel.Option> options_ = null;
    List<OrderListModel.Option> opt = null;

    public OrdersAdapter(Context context, List<OrderDetailsModel.Item> items, String status, Integer total) {
        this.context = context;
        this.items = items;
        this.status = status;
        this.total = total;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_shop_myorder_layout, parent, false);
        OrderViewHolder cvh = new OrderViewHolder(view);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        if (items.size() > 0) {

            holder.tv_product_name.setText(items.get(position).getProduct().getName());
            holder.tv_product_short_desc.setText(items.get(position).getProduct().getBrand());

            holder.tv_product_price.setText("₹ " + String.valueOf(total));

            holder.tv_product_qty.setText(String.valueOf(items.get(position).getQuantity()));

            if (items.size() > 1) {
                holder.view.setVisibility(View.VISIBLE);
            } else {
                holder.view.setVisibility(View.GONE);
            }

            if (items.get(position).getProduct().getImage() != null) {
                Glide.with(context).load(items.get(position).getProduct().getImage())
                        .into(holder.iv_product_image);
            } else {
                holder.iv_product_image.setImageResource(R.drawable.placeholder);
            }
        }
        holder.tv_order_status.setText(status);
//        holder.tv_date.setText("(" + items.get(position).getCreatedAt() + ")");
        holder.tv_date.setVisibility(View.GONE);

        LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        holder.rv_product_attributes.setLayoutManager(manager);

        ProductAttributeAdapter adapter = new ProductAttributeAdapter(context, options_, opt, items.get(position).getOptions());
        holder.rv_product_attributes.setAdapter(adapter);

        holder.tv_writeAReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReviewProductActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("name", items.get(position).getProduct().getName());
                intent.putExtra("image", items.get(position).getProduct().getImage());
                intent.putExtra("key", items.get(position).getProduct().getKey());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }



    public class OrderViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_product_image;
        TextView tv_product_name, tv_product_short_desc, tv_order_status, tv_date, tv_product_price, tv_product_qty, tv_writeAReview;
        View view;
        RecyclerView rv_product_attributes;
        LinearLayout ll_review;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_product_image = itemView.findViewById(R.id.iv_product_image);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_product_short_desc = itemView.findViewById(R.id.tv_product_short_desc);
            tv_order_status = itemView.findViewById(R.id.tv_order_status);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_product_price = itemView.findViewById(R.id.tv_product_price);
            tv_product_qty = itemView.findViewById(R.id.tv_product_qty);
            view = itemView.findViewById(R.id.view);
            rv_product_attributes = itemView.findViewById(R.id.rv_product_attributes);
            ll_review = itemView.findViewById(R.id.ll_review);
            tv_writeAReview = itemView.findViewById(R.id.tv_writeAReview);
        }
    }
}