package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ViewSimilarAdapter extends RecyclerView.Adapter<ViewSimilarAdapter.ImageViewHolder> {
    private int[] images;
    //
    Context context;
    private String[] best_name = {"Women Lace Maxi Dress","Women Lace Maxi Dress"," Women Lace Maxi Dress","Women Lace Maxi Dress","Women Lace Maxi Dress","Women Lace Maxi Dress","Women Lace Maxi Dress"};


    public ViewSimilarAdapter(int[] images) {
        this.context=context;
        this.images=images;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_similar_layout,parent,false);
        ImageViewHolder imageViewHolder =new ImageViewHolder(view);
//
        return imageViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder,final int position) {

        int img_id = images[position];
        String text_id = best_name[position];
        holder.similar_img.setImageResource(img_id);
        holder.similar_name.setText(text_id);
        holder.offer_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
//
    }

    @Override
    public int getItemCount() {
        return images.length;
    }
    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView similar_img;
        TextView similar_name,offer_price;
        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            similar_img = itemView.findViewById(R.id.img_similar);
            similar_name = itemView.findViewById(R.id.txt_similar);
            offer_price=itemView.findViewById(R.id.offer_price);
        }
    }
}


