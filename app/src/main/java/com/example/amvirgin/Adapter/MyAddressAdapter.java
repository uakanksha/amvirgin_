package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.shopModule.activity.AddAddressActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;

import java.util.List;


public class MyAddressAdapter extends RecyclerView.Adapter<MyAddressAdapter.MyAddressViewHolder>{

        Context context;
        String[] country = { "QTY","1", "2", "3", "4", "5"};

        public MyAddressAdapter(Context context, List<GetAddressModel.Datum> data) {
            this.context = context;
        }

        @NonNull
        @Override
        public MyAddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_myaddress_layout, parent, false);
            MyAddressViewHolder cvh = new MyAddressViewHolder(view);
            return cvh;
        }

        @Override
        public void onBindViewHolder(@NonNull MyAddressViewHolder holder, int position) {
            holder.editAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AddAddressActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });


        }

        @Override
        public int getItemCount() {
            return 5;
        }


        public class MyAddressViewHolder extends RecyclerView.ViewHolder {
            private Button editAddress;

            public MyAddressViewHolder(@NonNull View itemView) {
                super(itemView);
                editAddress=itemView.findViewById(R.id.btn_edit_address);


            }
        }
    }