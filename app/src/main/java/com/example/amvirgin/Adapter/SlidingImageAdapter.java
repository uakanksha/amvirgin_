package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.amvirgin.SubscriptionSection.SubscriptionActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.Slide;
import com.example.amvirgin.classes.BannerSliderModel;

import java.util.ArrayList;
import java.util.List;

public class SlidingImageAdapter extends PagerAdapter {

    List<Slide> slides;
    Context context;
    private LayoutInflater inflater;
    ArrayList<String> bannerMovieNames;
    List<BannerSliderModel.BannerSliderData> data;
    private String banner_img;
    private String type;

    public SlidingImageAdapter(Context context, List<BannerSliderModel.BannerSliderData> data, List<Slide> slides, String type) {
        this.context = context;
        this.data = data;
        this.slides = slides;
        this.type = type;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = LayoutInflater.from(context).inflate(R.layout.banner_layout, view, false);

//        assert imageLayout != null;
        ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.banner_img);
        TextView movie_name = (TextView) imageLayout.findViewById(R.id.movie_name);
        RelativeLayout banner_container = (RelativeLayout) imageLayout.findViewById(R.id.banner_container);
        LinearLayout banner_subs_ll = (LinearLayout) imageLayout.findViewById(R.id.banner_subs_ll);

        banner_subs_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SubscriptionActivity.class);
                context.startActivity(intent);
            }
        });


//        imageView.setImageResource(slides.get(position).getImage());
        if (type.equalsIgnoreCase("data")){


        movie_name.setText(data.get(position).getTitle());
        banner_img = data.get(position).getPoster();

        if (banner_img != null){
//            String imgPath = "http://younggeeks.net/wowsapi/uploads/images/"+user_img;

            Glide.with(context).load(banner_img).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                    holder.progressBar.setVisibility(View.GONE);
                    return false;
                }
            }).into(imageView);

           /* Picasso.with(context).load(imgPath).into(holder.circle, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                   holder.progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });*/
        }
        else {
//            holder.progressBar.setVisibility(View.GONE);
//            holder.circle.setImageResource(R.drawable.user);
        }

       /* banner_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieDetailSlidingActivity.class);
                context.startActivity(intent);
            }
        });*/

        }
        else if (type.equalsIgnoreCase("nodata")){
            imageView.setImageResource(slides.get(position).getImage());
           /* banner_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MovieDetailActivity.class);
                    context.startActivity(intent);
                }
            });*/
        }

        view.addView(imageLayout);

        return imageLayout;
    }

    @Override
    public int getCount() {
//        return data.size();
        return type.equalsIgnoreCase("data")? data.size(): slides.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
