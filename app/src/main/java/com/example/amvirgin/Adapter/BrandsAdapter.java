package com.example.amvirgin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.ShopBrands;

import java.util.List;

public class BrandsAdapter extends RecyclerView.Adapter<BrandsAdapter.CategoriesViewHolder> {

    Context context;
    List<ShopBrands> shopBrands;
    String type;
    private static final int BRANDS_DATA_TYPE = 1;
    private static final int POPULAR_DATA_TYPE = 2;

    public BrandsAdapter(Context context, List<ShopBrands> shopBrands, String type) {
        this.context = context;
        this.shopBrands = shopBrands;
        this.type = type;
    }

    @Override
    public int getItemViewType(int position) {

        if (type.equalsIgnoreCase("brands")){
            return BRANDS_DATA_TYPE;
        }
        else {
            return POPULAR_DATA_TYPE;
        }
//        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_brands_layout, parent, false);
//        CategoriesViewHolder cvh = new CategoriesViewHolder(view);
//        return cvh;

        if (viewType == BRANDS_DATA_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_brands_layout, parent, false);
            CategoriesViewHolder cvh = new CategoriesViewHolder(view);

            return cvh;
        } else if (viewType == POPULAR_DATA_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_popular_stuff, parent, false);
            CategoriesViewHolder cvh = new CategoriesViewHolder(view);

            return cvh;
        }
        else {
            return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesViewHolder holder, int position) {

        holder.img.setImageResource(shopBrands.get(position).getImg());
        holder.brand_name.setText(shopBrands.get(position).getBrand_name());

    }

    @Override
    public int getItemCount() {
        return shopBrands.size();
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView brand_name;

        public CategoriesViewHolder(@NonNull View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.img);
            brand_name = itemView.findViewById(R.id.brand_name);
        }
    }
}
