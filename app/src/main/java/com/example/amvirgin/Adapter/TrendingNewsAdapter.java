package com.example.amvirgin.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;

public class TrendingNewsAdapter extends RecyclerView.Adapter{

    private Context context;

    public TrendingNewsAdapter(Context context){
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_news_listing_layout, parent, false);
        TrendingNewsViewHolder viewHolder = new TrendingNewsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class TrendingNewsViewHolder extends RecyclerView.ViewHolder{

        public TrendingNewsViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
