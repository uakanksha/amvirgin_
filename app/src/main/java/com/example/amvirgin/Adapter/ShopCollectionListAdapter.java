package com.example.amvirgin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.shopModule.activity.ProductDetailActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.ProductListModel;

import java.util.List;

public class ShopCollectionListAdapter extends RecyclerView.Adapter<ShopCollectionListAdapter.ShopCollectionViewHolder> {

    Context context;
    List<ProductListModel.Payload> data;

    public ShopCollectionListAdapter(Context context, List<ProductListModel.Payload> data){
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ShopCollectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_shop_collection_layout, parent, false);
        ShopCollectionViewHolder cvh = new ShopCollectionViewHolder(view);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopCollectionViewHolder holder, final int position) {
        holder.product_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ProductId = data.get(position).getKey();
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("ProductId", ProductId);
                context.startActivity(intent);
            }
        });
        holder.tv_product_title.setText(data.get(position).getName());
        holder.tv_short_desc.setText(data.get(position).getBrand());
        holder.tv_current_price.setText(String.valueOf("₹ "+data.get(position).getPrice().getSelling()));
        holder.tv_original_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tv_original_price.setText(String.valueOf("₹ "+data.get(position).getPrice().getOriginal()));

        if (data.get(position).getImage() != null){
            Glide.with(context).load(data.get(position).getImage()).into(holder.product_img);
        }
        else {
            holder.product_img.setImageResource(R.drawable.placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ShopCollectionViewHolder extends RecyclerView.ViewHolder{
        ImageView product_img;
        LinearLayout product_ll;
        TextView tv_product_title, tv_short_desc, tv_current_price, tv_original_price, tv_discount_percentage;

        public ShopCollectionViewHolder(@NonNull View itemView) {
            super(itemView);
            product_ll = itemView.findViewById(R.id.product_ll);
            tv_original_price = itemView.findViewById(R.id.tv_original_price);
            product_img = itemView.findViewById(R.id.product_img);
            tv_product_title = itemView.findViewById(R.id.tv_product_title);
            tv_short_desc = itemView.findViewById(R.id.tv_product_short_desc);
            tv_current_price = itemView.findViewById(R.id.tv_current_price);
            tv_discount_percentage = itemView.findViewById(R.id.tv_discount_percentage);
        }
    }
}
