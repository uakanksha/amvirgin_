package com.example.amvirgin.interfaces;

import com.example.amvirgin.classes.BannerSliderModel;
import com.example.amvirgin.classes.HomepageTrendingResponse;
import com.example.amvirgin.classes.MovieDetailModel;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.classes.SendOTPModel;
import com.example.amvirgin.classes.SeriesCollectionDataModel;
import com.example.amvirgin.utils.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("customer/register")
    Call<RegisterUserModel> registerUser(
            @Field("email") String email,
            @Field("mobile") String mobile,
            @Field("name") String name,
            @Field("password") String password,
            @Field("otp") String otp);

    @GET("customer/sliders")
    Call<BannerSliderModel> getBannerSlider();

    @FormUrlEncoded
    @POST()
    Call<RegisterUserModel> loginUser(
            @Url String url,
            @Field("mobile") String mobile,
            @Field("email") String email,
            @Field("password") String password,
            @Field("otp") String otp);

    @FormUrlEncoded
    @POST("customer/login?type=3")
    Call<RegisterUserModel> loginUserWithotp(
            @Field("mobile") String mobile,
            @Field("otp") String otp);

    @GET("")
    Call<SendOTPModel> sendOTP(
            @Url String url);

    //Get User Profile
    @GET("customer/profile")
    Call<RegisterUserModel> getUserProfile(
            @Header("Content-Type") String content,
            @Header("Authorization") String authorization);

    //Get homepage trending
    @GET("customer/trending/picks")
    Call<HomepageTrendingResponse> Homepagetrending();

    //Get Movie Details
    @GET("")
    Call<MovieDetailModel> getMovieDetails(@Url String url);

    //User Logout
    @POST("customer/logout")
    Call<RegisterUserModel> userLogout(
            @Header("Content-Type") String content,
            @Header("Authorization") String authorization);

    //Series Collection
    @GET("")
    Call<SeriesCollectionDataModel> getCollection(@Url String url);

}
