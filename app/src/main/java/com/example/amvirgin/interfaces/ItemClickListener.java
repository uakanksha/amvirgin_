package com.example.amvirgin.interfaces;

public interface ItemClickListener {
    void onItemClick(int videoId);
}
