package com.example.amvirgin.interfaces;


import com.example.amvirgin.classes.MovieDetailModel;

import java.util.List;

public interface EpisodeClickListener {
   // void onItemClick(String url, String subtitle_url);
    void onMovieItemClick(MovieDetailModel.Data.Recommended value);
    void onSeriesItemClick(List<MovieDetailModel.Data.Content.Content_> content, int position);
}
