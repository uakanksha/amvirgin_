package com.example.amvirgin.interfaces;

public interface FragmentLifecycle {
    public void onPauseFragment();
    public void onResumeFragment();
}
