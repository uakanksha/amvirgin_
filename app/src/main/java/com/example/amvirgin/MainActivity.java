package com.example.amvirgin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.amvirgin.Fragments.HomeFragment;
import com.example.amvirgin.Fragments.MoreFragment;
import com.example.amvirgin.shopModule.fragment.ShopFragment;
import com.example.amvirgin.NewsModule.NewsFragment;
import com.example.amvirgin.classes.SessionManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    ImageView iv_home, iv_shop, iv_news, iv_chatmate, iv_more;
    TextView tv_home, tv_shop, tv_news, tv_chatmate, tv_more;
    LinearLayout home_ll, shop_ll, news_ll, chatmate_ll, more_ll;
    Toolbar toolbar;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //CHHDSG
        if(android.os.Build.VERSION.SDK_INT >= 21){
            Window window = this.getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.colorback));

        }

      /*  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        setContentView(R.layout.activity_main);
        session = new SessionManager(getApplicationContext());

        SharedPreferences pref = getApplicationContext().getSharedPreferences("settings", Activity.MODE_PRIVATE);
        String login = pref.getString("login", "");

        iv_home = findViewById(R.id.iv_home);
        iv_shop = findViewById(R.id.iv_shop);
        iv_news = findViewById(R.id.iv_news);
        iv_chatmate = findViewById(R.id.iv_chatmate);
        iv_more = findViewById(R.id.iv_more);

        tv_home = findViewById(R.id.tv_home);
        tv_shop = findViewById(R.id.tv_shop);
        tv_news = findViewById(R.id.tv_news);
        tv_chatmate = findViewById(R.id.tv_chatmate);
        tv_more = findViewById(R.id.tv_more);

        home_ll = findViewById(R.id.home_ll);
        shop_ll = findViewById(R.id.shop_ll);
        news_ll = findViewById(R.id.news_ll);
        chatmate_ll = findViewById(R.id.chatmate_ll);
        more_ll = findViewById(R.id.more_ll);

        toolbar = findViewById(R.id.toolbar);

        home_ll.setOnClickListener(this);
        shop_ll.setOnClickListener(this);
        news_ll.setOnClickListener(this);
        chatmate_ll.setOnClickListener(this);
        more_ll.setOnClickListener(this);

        iv_home.setImageResource(R.drawable.home_red_18dp);
        tv_home.setTextColor(getResources().getColor(R.color.colorRed));

        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */

//        Bundle extras = getIntent().getExtras();
//        String back = extras.getString("back");

      if(session.isLoggedIn() && getIntent().hasExtra("back")){
//        if (login.equalsIgnoreCase("yes")){
            OpenFragment(new MoreFragment());
            iv_home.setImageResource(R.drawable.home_white_18dp);
            tv_home.setTextColor(getResources().getColor(R.color.colorwhite));

            iv_shop.setImageResource(R.drawable.shop_white_18dp);
            tv_shop.setTextColor(getResources().getColor(R.color.colorwhite));

            iv_news.setImageResource(R.drawable.news_white_18dp);
            tv_news.setTextColor(getResources().getColor(R.color.colorwhite));

            iv_chatmate.setImageResource(R.drawable.chat_white_18dp);
            tv_chatmate.setTextColor(getResources().getColor(R.color.colorwhite));

            iv_more.setImageResource(R.drawable.more_red_18dp);
            tv_more.setTextColor(getResources().getColor(R.color.colorRed));
        }
        else {
            OpenFragment(new HomeFragment());
        }

//        NUM_PAGES = bannerImagesArrayList.size();
//        sliderDotspanel =  findViewById(R.id.SliderDots);
    }

    public void OpenFragment(Fragment fragment)
    {
        fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_left, R.anim.exit_out_right);
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.home_ll:
                iv_home.setImageResource(R.drawable.home_red_18dp);
                tv_home.setTextColor(getResources().getColor(R.color.colorRed));

                iv_shop.setImageResource(R.drawable.shop_white_18dp);
                tv_shop.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_news.setImageResource(R.drawable.news_white_18dp);
                tv_news.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_chatmate.setImageResource(R.drawable.chat_white_18dp);
                tv_chatmate.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_more.setImageResource(R.drawable.more_white_18dp);
                tv_more.setTextColor(getResources().getColor(R.color.colorwhite));

                OpenFragment(new HomeFragment());
//                toolbar.setVisibility(View.VISIBLE);

                break;

            case R.id.shop_ll:
                iv_home.setImageResource(R.drawable.home_white_18dp);
                tv_home.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_shop.setImageResource(R.drawable.shop_red_18dp);
                tv_shop.setTextColor(getResources().getColor(R.color.colorRed));

                iv_news.setImageResource(R.drawable.news_white_18dp);
                tv_news.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_chatmate.setImageResource(R.drawable.chat_white_18dp);
                tv_chatmate.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_more.setImageResource(R.drawable.more_white_18dp);
                tv_more.setTextColor(getResources().getColor(R.color.colorwhite));

                OpenFragment(new ShopFragment());
//                toolbar.setVisibility(View.GONE);

                break;

            case R.id.news_ll:
                iv_home.setImageResource(R.drawable.home_white_18dp);
                tv_home.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_shop.setImageResource(R.drawable.shop_white_18dp);
                tv_shop.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_news.setImageResource(R.drawable.news_red_18dp);
                tv_news.setTextColor(getResources().getColor(R.color.colorRed));

                iv_chatmate.setImageResource(R.drawable.chat_white_18dp);
                tv_chatmate.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_more.setImageResource(R.drawable.more_white_18dp);
                tv_more.setTextColor(getResources().getColor(R.color.colorwhite));

                OpenFragment(new NewsFragment());
//                toolbar.setVisibility(View.VISIBLE);

                break;

            case R.id.chatmate_ll:
                iv_home.setImageResource(R.drawable.home_white_18dp);
                tv_home.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_shop.setImageResource(R.drawable.shop_white_18dp);
                tv_shop.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_news.setImageResource(R.drawable.news_white_18dp);
                tv_news.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_chatmate.setImageResource(R.drawable.chat_red_18dp);
                tv_chatmate.setTextColor(getResources().getColor(R.color.colorRed));

                iv_more.setImageResource(R.drawable.more_white_18dp);
                tv_more.setTextColor(getResources().getColor(R.color.colorwhite));

//                toolbar.setVisibility(View.VISIBLE);

                break;

            case R.id.more_ll:
                iv_home.setImageResource(R.drawable.home_white_18dp);
                tv_home.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_shop.setImageResource(R.drawable.shop_white_18dp);
                tv_shop.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_news.setImageResource(R.drawable.news_white_18dp);
                tv_news.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_chatmate.setImageResource(R.drawable.chat_white_18dp);
                tv_chatmate.setTextColor(getResources().getColor(R.color.colorwhite));

                iv_more.setImageResource(R.drawable.more_red_18dp);
                tv_more.setTextColor(getResources().getColor(R.color.colorRed));

//                toolbar.setVisibility(View.VISIBLE);
                OpenFragment(new MoreFragment());

                break;


        }

    }
}

