package com.example.amvirgin;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amvirgin.Adapter.EpisodesAdapter;
import com.example.amvirgin.Fragments.CreditsFragment;
import com.example.amvirgin.Fragments.EpisodesFragment;
import com.example.amvirgin.Fragments.HeaderFragment;
import com.example.amvirgin.Fragments.HomeFragment;
import com.example.amvirgin.PlayerSection.VideoPlayerConfig;
import com.example.amvirgin.classes.MovieDetailModel;
import com.example.amvirgin.classes.Seasons;
import com.example.amvirgin.classes.SendOTPModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;
import static com.example.amvirgin.R.layout.spinner_item_overview;

public class MovieDetailActivity extends AppCompatActivity implements View.OnClickListener, Player.EventListener {

    TextView description_text, show, hide;
    LinearLayout show_ll, hide_ll, episodes_ll, credits_ll, ll_top, ep_ll, list_ll, watch_trailer_ll;
    RelativeLayout main_ll, ll_btnTop;
    View episodes_view, credits_view;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    NestedScrollView mNestedScrollView;
    Animation animShow, animHide;
    FragmentManager fm;
    boolean a = true;
    HeaderFragment headerFragment;
    Fragment hFragment;

    private ImageView banner_img;
    private TextView title, type_tv, duration_tv, genre_tv, pgRating_tv, language_tv;
    private String trailer_url;
    private FrameLayout poster_frame, main_media_frame;
    private RelativeLayout play_rl;
    MovieDetailModel.Data data;

    RecyclerView rv_episodes;
    Spinner sp_seasons;
    List<Seasons> seasons;
    ArrayList seasons_arrayList;
    private ArrayAdapter<CharSequence> mSeasonsAdapter;
    private ArrayList episodes = new ArrayList<>(Arrays.asList("Episode 1", "Episode 2", "Episode 3", "Episode 4", "Episode 5",
            "Episode 6", "Episode 7"));

    private final String STATE_RESUME_WINDOW = "resumeWindow";
    private final String STATE_RESUME_POSITION = "resumePosition";
    private final String STATE_PLAYER_FULLSCREEN = "playerFullscreen";

    //    VideoView videoView;
//    String url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
    String url = "http://amvirgine.zobofy.com/storage/trailers/SXmpAwuQW8aW8gTjGMQr0TlWIVLO4wRdI9YmKoq3.mp4";

    PlayerView playerView;
    ProgressBar pb;
    ImageView settings;

    SimpleExoPlayer player;
    Handler mHandler;
    Runnable mRunnable;

    private boolean mExoPlayerFullscreen = false;
    private FrameLayout mFullScreenButton;
    private ImageView mFullScreenIcon;
    private Dialog mFullScreenDialog;

    private int mResumeWindow;
    private long mResumePosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        if(android.os.Build.VERSION.SDK_INT >= 21){
            Window window = this.getWindow();
            window.setStatusBarColor(this.getResources().getColor(R.color.colorback));
        }

        if (savedInstanceState != null) {
            mResumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW);
            mResumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
            mExoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN);
        }

        playerView = findViewById(R.id.playerView);
        pb = findViewById(R.id.pb);
        settings = findViewById(R.id.settings);

        initializePlayer();

        initFullscreenDialog();
        initFullscreenButton();

        buildMediaSource(Uri.parse(url));

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MovieDetailActivity.this, "ccccccccc", Toast.LENGTH_SHORT).show();
                showSettingsDialog();
            }
        });

        description_text =findViewById(R.id.description_text);
        show = findViewById(R.id.show);
        hide = findViewById(R.id.hide);

        show_ll = findViewById(R.id.show_ll);
        hide_ll = findViewById(R.id.hide_ll);
        episodes_ll = findViewById(R.id.episodes_ll);
        credits_ll = findViewById(R.id.credits_ll);
        episodes_view = findViewById(R.id.episodes_view);
        credits_view = findViewById(R.id.credits_view);
        mNestedScrollView = findViewById(R.id.mNestedScrollView);
        ll_top = findViewById(R.id.top);
        ll_btnTop = findViewById(R.id.ll_btnTop);
        ep_ll = findViewById(R.id.ep_ll);
        list_ll = findViewById(R.id.list_ll);
        main_ll = findViewById(R.id.main_ll);
//        hFragment = findViewById(R.id.hFragment);
        rv_episodes = findViewById(R.id.rv_episodes);
        sp_seasons = findViewById(R.id.sp_seasons);

        watch_trailer_ll = findViewById(R.id.watch_trailer_ll);

        banner_img = findViewById(R.id.banner_img);
        title = findViewById(R.id.title);
        type_tv = findViewById(R.id.type_tv);
        duration_tv = findViewById(R.id.duration_tv);
        genre_tv = findViewById(R.id.genre_tv);
        pgRating_tv = findViewById(R.id.pgRating_tv);
        poster_frame = findViewById(R.id.poster_frame);
        main_media_frame = findViewById(R.id.main_media_frame);
        play_rl = findViewById(R.id.play_rl);

        show_ll.setOnClickListener(this);
        hide_ll.setOnClickListener(this);
        episodes_ll.setOnClickListener(this);
        credits_ll.setOnClickListener(this);
        watch_trailer_ll.setOnClickListener(this);
        play_rl.setOnClickListener(this);

//        description_text.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);

//        OpenFragment(new EpisodesFragment());
        
        getDetails();

//        seasons = getResources().getStringArray(R.array.seasons);

//        seasons.add(new Seasons("Season 1"));
//        seasons.add(new Seasons("Season 2"));
//
//        for (int i = 0; i<seasons.size(); i++){
//            seasons_arrayList.add(seasons.get(i).getSeason_name());
//        }



        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rv_episodes.setLayoutManager(manager);



        if (mNestedScrollView != null) {
            mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    float y = ep_ll.getY();
                   /* RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.setMargins(0, (int) y+10, 0, 0);
                    ll_btnTop.setLayoutParams(params);
*/

                    animShow = AnimationUtils.loadAnimation( getApplicationContext(), R.anim.slide_in_down);
                    animHide = AnimationUtils.loadAnimation( getApplicationContext(), R.anim.slide_in_up);
                    hFragment = new HeaderFragment();
                    fm = getSupportFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_in_up,
                            R.anim.slide_in_down, R.anim.slide_in_up);

                    if (scrollY > y) {
                        if (a) {

                            transaction.add(R.id.main_ll, hFragment)
                                    .addToBackStack(null).commit();
                            a = false;
                        }
                    } else {
                        if (!a) {

                        if (hFragment != null){
                                fm.popBackStackImmediate();
                                a = true;
                        }
                        }
                    }
                }
            });
        }
    }

    private void getDetails() {
        ServiceWrapper serviceWrapper=new ServiceWrapper(getApplicationContext());
        Call<MovieDetailModel> call=serviceWrapper.getRetrofit().create(ApiInterface.class).getMovieDetails("");
        serviceWrapper.HandleResponse(call, new ResponseHandler<MovieDetailModel>(getApplicationContext()) {
            @Override
            public void onResponse(MovieDetailModel response) {

                data = response.getData();

                Picasso.with(getApplicationContext()).load(response.getData().getPoster()).into(banner_img,
                        new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
//                                        pb.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                title.setText(response.getData().getTitle());
                trailer_url = response.getData().getTrailer();
                type_tv.setText(response.getData().getType());
                duration_tv.setText(response.getData().getDuration());
                genre_tv.setText(response.getData().getGenre());
                pgRating_tv.setText(response.getData().getPgRating());
                description_text.setText(response.getData().getDescription());
                if (description_text.getText().toString().length() >= 250)
                {
                    show_ll.setVisibility(View.VISIBLE);
                    hide_ll.setVisibility(View.VISIBLE);
                }
                else{
                    show_ll.setVisibility(View.GONE);
                    hide_ll.setVisibility(View.GONE);
                }
                if (response.getData().getHasSeasons()){
                    seasons = new ArrayList<>();
                    seasons_arrayList = new ArrayList<>();
                    for (int i = 1; i == response.getData().getContent().get(0).getSeason(); i++){
//                        seasons.add(new Seasons("Season "+i));
                        seasons_arrayList.add("Season "+i);
                    }

                    mSeasonsAdapter = new ArrayAdapter<CharSequence>(getApplicationContext(), spinner_item_overview, seasons_arrayList);
                    mSeasonsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    sp_seasons.setAdapter(mSeasonsAdapter);

//                    EpisodesAdapter adapter = new EpisodesAdapter(getApplicationContext(), response.getData().getContent().get(0)
//                    .getContent(), response.getData().getPoster());
//                    rv_episodes.setAdapter(adapter);
                }


            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.show_ll:
                show_ll.setVisibility(View.GONE);
                hide_ll.setVisibility(View.VISIBLE);
                description_text.setMaxLines(Integer.MAX_VALUE);
                break;

            case R.id.hide_ll:
                hide_ll.setVisibility(View.GONE);
                show_ll.setVisibility(View.VISIBLE);
                description_text.setMaxLines(5);
                break;

            case R.id.episodes_ll:

                fm = getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_in_down);
                transaction.add(R.id.main_ll, new HeaderFragment())
                        .addToBackStack(null).commit();

//                episodes_view.setVisibility(View.VISIBLE);
//                credits_view.setVisibility(View.INVISIBLE);
//                OpenFragment(new EpisodesFragment());
                break;

            case R.id.credits_ll:
//                episodes_view.setVisibility(View.INVISIBLE);
//                credits_view.setVisibility(View.VISIBLE);
//                OpenFragment(new CreditsFragment());

                Bundle bundle = new Bundle();
                bundle.putSerializable("data", data);

                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                CreditsFragment creditsFragment = new CreditsFragment();
                creditsFragment.setArguments(bundle);
//                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down);
//                transact.add(R.id.item_container, productAlertFragment).addToBackStack(null).commit();
                creditsFragment.show(getSupportFragmentManager(),"about");
                break;

            case R.id.watch_trailer_ll:

                Intent intent = new Intent(getApplicationContext(), PlayerActivity.class);
                intent.putExtra("trailer_url", trailer_url);
                startActivity(intent);
                break;

            case R.id.play_rl:
                poster_frame.setVisibility(View.GONE);
                main_media_frame.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void showSettingsDialog() {
//        Toast.makeText(getApplicationContext(),
//                "hhhhhhhh", Toast.LENGTH_SHORT).show();
        final CharSequence[] items = {"Option-1", "Option-2", "Option-3", "Option-4"};
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setTitle("Video Quality");
        alt_bld.setSingleChoiceItems(items, -1, new DialogInterface
                .OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Toast.makeText(getApplicationContext(),
                        "Group Name = "+items[item], Toast.LENGTH_SHORT).show();
//                dialog.dismiss();// dismiss the alertbox after chose option

            }
        });
        alt_bld.setPositiveButton("OK", null);
        alt_bld.setNegativeButton("CANCEL", null);
        alt_bld.show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putInt(STATE_RESUME_WINDOW, mResumeWindow);
        outState.putLong(STATE_RESUME_POSITION, mResumePosition);
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, mExoPlayerFullscreen);

        super.onSaveInstanceState(outState);
    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {

//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ((ViewGroup) playerView.getParent()).removeView(playerView);
        mFullScreenDialog.addContentView(playerView, new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(MovieDetailActivity.this, R.drawable.ic_fullscreen_skrink));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }


    private void closeFullscreenDialog() {

//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ((ViewGroup) playerView.getParent()).removeView(playerView);
        ((FrameLayout) findViewById(R.id.main_media_frame)).addView(playerView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(MovieDetailActivity.this, R.drawable.ic_fullscreen_expand));
    }

    private void initFullscreenButton() {

        PlaybackControlView controlView = playerView.findViewById(R.id.exo_controller);
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        //mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mExoPlayerFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }


    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }

    private void initializePlayer() {
        if (player == null) {
            // 1. Create a default TrackSelector
            LoadControl loadControl = new DefaultLoadControl(
                    new DefaultAllocator(true, 16),
                    VideoPlayerConfig.MIN_BUFFER_DURATION,
                    VideoPlayerConfig.MAX_BUFFER_DURATION,
                    VideoPlayerConfig.MIN_PLAYBACK_START_BUFFER,
                    VideoPlayerConfig.MIN_PLAYBACK_RESUME_BUFFER, -1, true);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector =
                    new DefaultTrackSelector(videoTrackSelectionFactory);
            // 2. Create the player
            //player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), trackSelector, loadControl);
            playerView.setPlayer(player);
        }
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
            player.getPlaybackState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
        if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        resumePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        pb.setVisibility(View.GONE);

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
