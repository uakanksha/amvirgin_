package com.example.amvirgin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.Adapter.MyPagerReviewAdapter;
import com.google.android.material.tabs.TabLayout;

public class ReviewProductActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout cancel;
    private TabLayout indicator;
    private String name, image;
    ImageView iv_img;
    TextView tv_name;
    private int key;
    Button btn_submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_product);

        cancel = (RelativeLayout)findViewById(R.id.rlCancel);
        indicator = (TabLayout)findViewById(R.id.indicatorReview);
        iv_img = findViewById(R.id.iv_img);
        tv_name = findViewById(R.id.tv_name);
        btn_submit = findViewById(R.id.btn_submit);

        if (getIntent().getExtras() != null){
            name = getIntent().getExtras().getString("name");
            image = getIntent().getExtras().getString("image");
            key = getIntent().getExtras().getInt("key");

            tv_name.setText(name);
            if (image != null) {
                Glide.with(this).load(image).into(iv_img);
            }
            else {
                iv_img.setImageResource(R.drawable.placeholder);
            }
        }

//        ViewPager pager = (ViewPager) findViewById(R.id.viewPagerReview);
//        pager.setAdapter(new MyPagerReviewAdapter(getSupportFragmentManager(), name, image, key));
//        indicator.setupWithViewPager(pager, true);

        cancel.setOnClickListener(this);
        btn_submit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlCancel:
                finish();
                break;

            case R.id.btn_submit:
                finish();
                break;
        }
    }
}
