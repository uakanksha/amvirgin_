package com.example.amvirgin.SubscriptionSection;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.amvirgin.SubscriptionSection.SelectPackFragment;
import com.example.amvirgin.SubscriptionSection.SubscriptionFlow;

import java.security.AccessControlContext;
import java.util.List;

public class SubscriptionFlowAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private List<SubscriptionFlow> subscriptionFlow;

    public SubscriptionFlowAdapter(Context context, FragmentManager fm, List<SubscriptionFlow> subscriptionFlow) {
        super(fm);
        mContext = context;
        this.subscriptionFlow = subscriptionFlow;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (subscriptionFlow.get(position).getFlow_id() == 0){
            return new SelectPackFragment(subscriptionFlow.get(position).getFlow_title());
        }
        else if (subscriptionFlow.get(position).getFlow_id() == 1){
            return new EnterDetailsFragment();
        }
        else if (subscriptionFlow.get(position).getFlow_id() == 2){
            return new PayFragment();
        }
        /*else if (subscriptionFlow.get(position).getFlow_id() == 3){
            return new StartWatchingFragment();
        }*/
        else {
            return null;
        }
       /* switch (position){
            case 0:
                return new SelectPackFragment(subscriptionFlow.get(position).getFlow_title());
            case 1:
                return new EnterDetailsFragment();
            case 2:
                return new PayFragment();
            case 3:
                return new StartWatchingFragment();
            default:
                return new SelectPackFragment(subscriptionFlow.get(position).getFlow_title());
        }*/
//        return new SelectPackFragment(subscriptionFlow.get(position).getFlow_title());
//        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return (subscriptionFlow.get(position).getFlow_title());
    }

    @Override
    public int getCount() {
        return subscriptionFlow.size();
    }
}
