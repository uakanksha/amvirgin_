package com.example.amvirgin.SubscriptionSection;

public class SubscriptionFlow {
    private int flow_id;
    private String flow_title;

    public SubscriptionFlow(int flow_id, String flow_title) {
        this.flow_id = flow_id;
        this.flow_title = flow_title;
    }

    public int getFlow_id() {
        return flow_id;
    }

    public void setFlow_id(int flow_id) {
        this.flow_id = flow_id;
    }

    public String getFlow_title() {
        return flow_title;
    }

    public void setFlow_title(String flow_title) {
        this.flow_title = flow_title;
    }
}
