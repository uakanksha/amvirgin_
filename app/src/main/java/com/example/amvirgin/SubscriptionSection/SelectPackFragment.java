package com.example.amvirgin.SubscriptionSection;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SubscriptionsListModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;

public class SelectPackFragment extends Fragment {
    String flow_title;
    private RecyclerView rv_subs_plans;
    ArrayList subs_price = new ArrayList(Arrays.asList("₹149", "₹299", "₹529", "₹949"));
    ArrayList subs_duration = new ArrayList(Arrays.asList("1 Month", "3 Months", "6 Months", "12 Months"));
    ArrayList subs_price_per_month = new ArrayList(Arrays.asList("₹149/month", "₹99/month", "₹89/month", "₹79/month"));
    SubscriptionActivity subscriptionActivity;
    int subscription_price, subscription_duration;
    SubscriptionClickListener subscriptionClickListener;
    String token;
    private TextView tv_no_items;

    public SelectPackFragment(String flow_title) {
        this.flow_title = flow_title;
        this.subscriptionClickListener = subscriptionClickListener;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_select_pack, container, false);
        rv_subs_plans = view.findViewById(R.id.rv_subs_plans);
        tv_no_items = view.findViewById(R.id.tv_no_items);

        getSubscriptionsList();

        return view;
    }

    private void getSubscriptionsList() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<SubscriptionsListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getSubscriptionsList();
        serviceWrapper.HandleResponse(call, new ResponseHandler<SubscriptionsListModel>(getContext()) {
            @Override
            public void onResponse(SubscriptionsListModel response) {
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0) {

                            List<SubscriptionsListModel.Datum> subscriptions = response.getData();

                            GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
                            rv_subs_plans.setLayoutManager(manager);

                            SubscriptionListAdapter adapter = new SubscriptionListAdapter(getContext(),subscriptions, subs_price, subs_duration,
                                    subs_price_per_month, new SubscriptionClickListener() {
                                @Override
                                public void onItemClick(Integer subs_price, Integer subs_duration) {
                                    subscription_price = subs_price;
                                    subscription_duration = subs_duration;

                                    PreferenceManager.saveInt(getContext(), "subscription_price", subscription_price);
                                    PreferenceManager.saveInt(getContext(), "subscription_duration", subscription_duration);

                                    if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
                                        token = PreferenceManager.getUser(getContext()).getToken();
                                    }
                                    if (token != null) {
                                        subscriptionActivity = (SubscriptionActivity) getActivity();
                                        subscriptionActivity.mPager.setCurrentItem(2);
                                        subscriptionActivity.tv_select_pack.setTextColor(getResources().getColor(R.color.OffWhite));
                                        subscriptionActivity.tv_pay.setTextColor(getResources().getColor(R.color.colorwhite));

                                        subscriptionActivity.select_pack_fl_red.setVisibility(View.GONE);
                                        subscriptionActivity.select_pack_fl_white.setVisibility(View.VISIBLE);

                                        subscriptionActivity.pay_fl_red.setVisibility(View.VISIBLE);
                                        subscriptionActivity.pay_fl_white.setVisibility(View.GONE);
                                    } else {
                                        subscriptionActivity = (SubscriptionActivity) getActivity();
                                        subscriptionActivity.mPager.setCurrentItem(1);
                                        subscriptionActivity.tv_select_pack.setTextColor(getResources().getColor(R.color.OffWhite));
                                        subscriptionActivity.tv_enter_details.setTextColor(getResources().getColor(R.color.colorwhite));

                                        subscriptionActivity.select_pack_fl_red.setVisibility(View.GONE);
                                        subscriptionActivity.select_pack_fl_white.setVisibility(View.VISIBLE);

                                        subscriptionActivity.enter_details_fl_red.setVisibility(View.VISIBLE);
                                        subscriptionActivity.enter_details_fl_white.setVisibility(View.GONE);
                                    }

                                }
                            });
                            rv_subs_plans.setAdapter(adapter);
                        }
                        else {
                            tv_no_items.setVisibility(View.VISIBLE);
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(getContext(), response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(getContext(), response.getMessage());
                }
            }
        });

    }

}
