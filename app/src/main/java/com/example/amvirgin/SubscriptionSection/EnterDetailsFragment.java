package com.example.amvirgin.SubscriptionSection;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amvirgin.R;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.classes.SendOTPModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.SessionId;
import com.example.amvirgin.utils.ToastMessage;
import com.example.amvirgin.utils.User;
import com.mukesh.OtpView;

import retrofit2.Call;

public class EnterDetailsFragment extends Fragment {

    RelativeLayout enter_details_and_continue_rl, enter_otp_and_submit_rl, enter_details_if_not_a_member;
    EditText mobile_num;
    Button continue_btn, submit_otp_btn, get_otp_btn;
    ToastMessage toastMessage;
    String number;
    SubscriptionActivity subscriptionActivity;
    String[] age;
    private ArrayAdapter<CharSequence> mAgeAdapter;
    Spinner sp_age;
    private OtpView otpTextView;
    private String strOtp;

    private EditText password_reg, confirm_password, full_name, email_id_reg, phone_number;
    private Button register_btn;
    private CheckBox cb_agreement;
    private String nameStr, emailStr, passwordStr, conf_passwordStr, phoneStr;

    private EditText email_id, password;
    private Button login_btn;
    private String strEmail, strPassword, strMobile, strOTP, stringOTP;
    private int type;

    LinearLayout login_ll, register_ll;
    TextView tv_register, tv_login, tv_phone;

    public EnterDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enter_details, container, false);

        password_reg = view.findViewById(R.id.password_reg);
        confirm_password = view.findViewById(R.id.confirm_password);
        full_name = view.findViewById(R.id.full_name);
        email_id_reg = view.findViewById(R.id.email_id_reg);
        register_btn = view.findViewById(R.id.register_btn);
        cb_agreement = view.findViewById(R.id.cb_agreement);
        phone_number = view.findViewById(R.id.phone_number);

        email_id = view.findViewById(R.id.email_id);
        password = view.findViewById(R.id.password);
        login_btn = view.findViewById(R.id.login_btn);

        login_ll = view.findViewById(R.id.login_ll);
        register_ll = view.findViewById(R.id.register_ll);
        tv_register = view.findViewById(R.id.tv_register);
        tv_login = view.findViewById(R.id.tv_login);

        otpTextView = view.findViewById(R.id.otp_view);
        submit_otp_btn = view.findViewById(R.id.submit_otp_btn);

        enter_otp_and_submit_rl = view.findViewById(R.id.enter_otp_and_submit_rl);
        tv_phone = view.findViewById(R.id.tv_phone);

        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login_ll.setVisibility(View.GONE);
                register_ll.setVisibility(View.VISIBLE);
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register_ll.setVisibility(View.GONE);
                login_ll.setVisibility(View.VISIBLE);
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidInputForLogin()) {
                    loginUser();
                }
            }
        });

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidInputToRegister()){
                    sendOTP();
                }
            }
        });

        submit_otp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidOTP())
                registerUser();
            }
        });

/*        enter_details_and_continue_rl = view.findViewById(R.id.enter_details_and_continue_rl);
        enter_otp_and_submit_rl = view.findViewById(R.id.enter_otp_and_submit_rl);
        enter_details_if_not_a_member = view.findViewById(R.id.enter_details_if_not_a_member);

        mobile_num = view.findViewById(R.id.mobile_num);
        continue_btn = view.findViewById(R.id.continue_btn);
        submit_btn = view.findViewById(R.id.submit_btn);
        get_otp_btn = view.findViewById(R.id.get_otp_btn);

        sp_age = view.findViewById(R.id.sp_age);

        otpTextView = view.findViewById(R.id.otp_view);

        age = this.getResources().getStringArray(R.array.age);

        mAgeAdapter = new ArrayAdapter<CharSequence>(getContext(), spinner_item_overview, age);
        mAgeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_age.setAdapter(mAgeAdapter);

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidInputToContinue()){
                    continueToEnterOTP();
                }
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidOTP()){
                    loginUser();
                }

            }
        });

        get_otp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enter_details_if_not_a_member.setVisibility(View.GONE);
                enter_otp_and_submit_rl.setVisibility(View.VISIBLE);
            }
        });*/

        return view;
    }

    private void registerUser() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> callRegister = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .registerUser(emailStr, phoneStr, nameStr, passwordStr, strOtp);
        serviceWrapper.HandleResponse(callRegister, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                if (response.getStatus() == 200 || response.getStatus() == 201) {
//                    toastMessage.showSuccessShortCustomToast(response.getMessage());
                    Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();

                    User user = new User(response.getData());
                    PreferenceManager.saveUser(getContext(), user);

                    subscriptionActivity = (SubscriptionActivity) getActivity();
                    subscriptionActivity.mPager.setCurrentItem(2);

                    subscriptionActivity.tv_select_pack.setTextColor(getResources().getColor(R.color.OffWhite));
                    subscriptionActivity.tv_enter_details.setTextColor(getResources().getColor(R.color.OffWhite));
                    subscriptionActivity.tv_pay.setTextColor(getResources().getColor(R.color.colorwhite));

                    subscriptionActivity.pay_fl_red.setVisibility(View.VISIBLE);
                    subscriptionActivity.pay_fl_white.setVisibility(View.GONE);

                    subscriptionActivity.enter_details_fl_red.setVisibility(View.GONE);
                    subscriptionActivity.enter_details_fl_white.setVisibility(View.VISIBLE);

//                    Intent intent = new Intent(getContext(), HomeActivity.class);
//                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                }
                else{
//                    toastMessage.showErrorShortCustomToast(response.getMessage());
                    Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean isValidInputToRegister() {
        nameStr = full_name.getText().toString().trim();
        emailStr = email_id_reg.getText().toString().trim();
        passwordStr = password_reg.getText().toString();
        conf_passwordStr = confirm_password.getText().toString();
        phoneStr = phone_number.getText().toString();

        String mobilePattern = "[0-9]{10}";
        boolean a = false;

        if (TextUtils.isEmpty(nameStr)){
            full_name.setError("Please Enter Name");
            full_name.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Name", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(emailStr)){
            email_id.setError("Please Enter Email");
            email_id.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Email", Toast.LENGTH_SHORT).show();
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()){
            email_id.setError("Please Enter a valid Email");
            email_id.requestFocus();
//            Toast.makeText(getContext(), "Please Enter a valid Email", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(phoneStr)){
            phone_number.setError("Please Enter Phone Number");
            phone_number.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
        else if (!phoneStr.matches(mobilePattern)){
            phone_number.setError("Please Enter a valid Contact Number");
            phone_number.requestFocus();
//            Toast.makeText(getContext(), "Please Enter a valid Contact Number", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(passwordStr)){
            password.setError("Please Enter Password");
            password.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Password", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(conf_passwordStr)){
            confirm_password.setError("Please Confirm Password");
            confirm_password.requestFocus();
//            Toast.makeText(getContext(), "Please Confirm Password", Toast.LENGTH_SHORT).show();
        }
        else if (!passwordStr.equals(conf_passwordStr)){
            confirm_password.setError("Passwords doesn't match");
            confirm_password.requestFocus();
//            Toast.makeText(getContext(), "Passwords doesn't match", Toast.LENGTH_SHORT).show();
        }

        else if (!cb_agreement.isChecked()){
            Toast.makeText(getContext(), "You must agree to AmVirgin Terms and Conditions!!", Toast.LENGTH_SHORT).show();
//            toastMessage.showErrorShortCustomToast("You must agree to AmVirgin Terms and Conditions!!");
        }
        else {
            a = true;
        }

        return a;
    }

    private void sendOTP() {
        Log.d("click14","click");
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<SendOTPModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .sendOTP("customer?mobile="+phoneStr+"&type=2");
        serviceWrapper.HandleResponse(call, new ResponseHandler<SendOTPModel>(getContext()) {
            @Override
            public void onResponse(SendOTPModel response) {
//                Bundle bundle = new Bundle();
//                bundle.putString("phone_number", phoneStr);
//                bundle.putString("email", emailStr);
//                bundle.putString("name", nameStr);
//                bundle.putString("password", passwordStr);
//                bundle.putString("type", "register");

                register_ll.setVisibility(View.GONE);
                enter_otp_and_submit_rl.setVisibility(View.VISIBLE);

                tv_phone.setText("+91 - "+phoneStr);

//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction transact = fm.beginTransaction();
//                OTPVerificationFragment otpVerificationFragment = new OTPVerificationFragment();
//                otpVerificationFragment.setArguments(bundle);
//                transact.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down);
//                otpVerificationFragment.show(getFragmentManager(),"about");
            }
        });
    }

    private boolean isValidInputForLogin() {
        strEmail= email_id.getText().toString().trim();
        Log.d("Email",strEmail);
        strPassword= password.getText().toString().trim();
        Log.d("Password",strPassword);
//        strMobile = mobile_number.getText().toString().trim();
//        Log.d("strMobile",strMobile);
        String MobilePattern = "[0-9]{10}";

        boolean s = false;

        if (strEmail.equals("")){
            email_id.setError("Please Enter Email or Mobile");
            email_id.requestFocus();
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches() && !strEmail.matches(MobilePattern)){
            email_id.setError("Please Enter a Valid Email or Phone");
            email_id.requestFocus();
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches() || !strEmail.matches(MobilePattern)){
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()){
                type = 1;
            }
            else if (strEmail.matches(MobilePattern)){
                type = 2;
                strMobile = strEmail;
                strEmail = "";
            }
            if (strPassword.equals("")){
                password.setError("Please Enter Password");
                password.requestFocus();
            }
            else {
                s = true;
            }

        }

        Log.d("s", String.valueOf(s));
        return s;
    }

    private void loginUser() {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .loginUser("customer/login?type="+type, strMobile, strEmail, strPassword, strOTP);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {

                if (response.getStatus() == 200) {
//                    toastMessage.showSuccessShortCustomToast(response.getMessage());
                    Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();

                    User user = new User(response.getData());
                    PreferenceManager.saveUser(getContext(), user);

                    subscriptionActivity = (SubscriptionActivity) getActivity();
                    subscriptionActivity.mPager.setCurrentItem(2);

                    subscriptionActivity.tv_select_pack.setTextColor(getResources().getColor(R.color.OffWhite));
                    subscriptionActivity.tv_enter_details.setTextColor(getResources().getColor(R.color.OffWhite));
                    subscriptionActivity.tv_pay.setTextColor(getResources().getColor(R.color.colorwhite));

                    subscriptionActivity.pay_fl_red.setVisibility(View.VISIBLE);
                    subscriptionActivity.pay_fl_white.setVisibility(View.GONE);

                    subscriptionActivity.enter_details_fl_red.setVisibility(View.GONE);
                    subscriptionActivity.enter_details_fl_white.setVisibility(View.VISIBLE);

//                    Intent intent = new Intent(getContext(), HomeActivity.class);
//                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                }

                else if (response.getStatus() == 404){
//                    toastMessage.showSuccessShortCustomToast("This Mobile/Email is not Registered!!");
                    Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();

                    login_ll.setVisibility(View.GONE);
                    register_ll.setVisibility(View.VISIBLE);
//                    Intent intentLogin = new Intent(getContext(), LoginActivity.class);
//                    intentLogin.putExtra("reg", "not");
//                    startActivity(intentLogin);
                }
                else {
                    Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
//                    toastMessage.showErrorShortCustomToast(response.getMessage());
                }
            }
        });

    }

    private boolean isValidOTP() {
        boolean valid=true;
//        strOtp= number1.getText().toString().trim()+number2.getText().toString().trim()+number3.getText().toString().trim()+
//                number4.getText().toString().trim();
        strOtp = otpTextView.getText().toString();
//        if (otpTextView.getOTP().length() < 4){
//            Toast.makeText(OtpVerificationActivity.this, "Please enter a valid OTP!! ",  Toast.LENGTH_SHORT).show();
//        }
//        else {
//            Toast.makeText(OtpVerificationActivity.this, "Valid OTP!! ",  Toast.LENGTH_SHORT).show();
//        }
        if (strOtp.equals("")){
            Toast.makeText(getContext(), "Please Enter Otp", Toast.LENGTH_SHORT).show();
            valid=false;
        }
        else if (strOtp.length()<4){
            Toast.makeText(getContext(), "Please Enter 4 digits Otp", Toast.LENGTH_SHORT).show();
            valid=false;
        }
        /*else if (!strOtp.equalsIgnoreCase("1234")){
            Toast.makeText(getContext(), "Please Enter a valid Otp", Toast.LENGTH_SHORT).show();
            valid=false;
        }*/
//        else if (otpTextView.getOTP().length() < 4){
//            Toast.makeText(OtpVerificationActivity.this, "Please enter a valid OTP!! ",  Toast.LENGTH_SHORT).show();
//            valid = false;
//        }
        return valid;
    }

    /*private void loginUser() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .loginUserWithotp(number, strOtp);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                if (response.getStatus() == 200) {
//                    toastMessage.showSuccessShortCustomToast(response.getMessage());
                    Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();

                    User user = new User(response.getData());
                    PreferenceManager.saveUser(getContext(), user);

//                    Intent intent = new Intent(getContext(), HomeActivity.class);
//                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                    subscriptionActivity = (SubscriptionActivity) getActivity();
                    subscriptionActivity.mPager.setCurrentItem(2);
                }
                else {
//                    toastMessage.showErrorShortCustomToast(response.getMessage());
//                    Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    subscriptionActivity = (SubscriptionActivity) getActivity();
                    subscriptionActivity.mPager.setCurrentItem(2);
                }
            }
        });
    }

    private boolean isValidInputToContinue() {
        boolean a = false;
        number = mobile_num.getText().toString().trim();
        String MobilePattern = "[0-9]{10}";

        if (number.isEmpty()){
//            toastMessage.showErrorShortCustomToast("Please Enter Mobile Number!!");
            Toast.makeText(getContext(), "Please Enter Mobile Number!!", Toast.LENGTH_SHORT).show();
        }
        else if (!number.matches(MobilePattern)){
//            toastMessage.showErrorShortCustomToast("Please Enter a Valid Number!!");
            Toast.makeText(getContext(), "Please Enter a Valid Number!!", Toast.LENGTH_SHORT).show();
        }
        else{
            a = true;
        }
        return a;
    }

    private void continueToEnterOTP() {
            ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
            Call<SendOTPModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                    .sendOTP("customer?mobile="+number+"&type=2");
            serviceWrapper.HandleResponse(call, new ResponseHandler<SendOTPModel>(getContext()) {
                @Override
                public void onResponse(SendOTPModel response) {
                    if (response.getStatus() == 409){
                        Toast.makeText(getContext(), "You are already a member of AmVirgin!!", Toast.LENGTH_SHORT).show();
                        enter_details_and_continue_rl.setVisibility(View.GONE);
                        enter_otp_and_submit_rl.setVisibility(View.VISIBLE);
                    }
                    else if (response.getStatus() == 404){
                        Toast.makeText(getContext(), "You are not a member of AmVirgin!!", Toast.LENGTH_SHORT).show();
                        enter_details_and_continue_rl.setVisibility(View.GONE);
                        enter_details_if_not_a_member.setVisibility(View.VISIBLE);
                    }
                }
            });
        }*/
    }

