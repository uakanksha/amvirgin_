package com.example.amvirgin.SubscriptionSection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SubscriptionsListModel;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubscriptionListAdapter extends RecyclerView.Adapter<SubscriptionListAdapter.SubscriptionListViewHolder> {
    Context context;
    ArrayList subs_price;
    ArrayList subs_duration;
    ArrayList subs_price_per_month;
    SubscriptionClickListener subscriptionClickListener;
    SubscriptionActivity subscriptionActivity;
    List<SubscriptionsListModel.Datum> subscriptions;
    boolean a;
    String token;

    public SubscriptionListAdapter(Context context, List<SubscriptionsListModel.Datum> subscriptions, ArrayList subs_price, ArrayList subs_duration,
                                   ArrayList subs_price_per_month, SubscriptionClickListener subscriptionClickListener) {
        this.context = context;
        this.subs_price = subs_price;
        this.subs_duration = subs_duration;
        this.subs_price_per_month = subs_price_per_month;
        this.subscriptions = subscriptions;
        this.subscriptionClickListener = subscriptionClickListener;
    }

    @NonNull
    @Override
    public SubscriptionListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_subscription_list_layout, parent, false);
        SubscriptionListViewHolder holder = new SubscriptionListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionListViewHolder holder, final int position) {

        holder.subs_price.setText("₹"+subscriptions.get(position).getOriginalPrice());
        holder.subs_duration.setText(subscriptions.get(position).getDuration()+" Days ");
        holder.subs_price_per_month.setText("₹"+subscriptions.get(position).getDiscountedPrice());

//        holder.subs_price.setText((String) subs_price.get(position));
//        holder.subs_duration.setText((String) subs_duration.get(position));
//        holder.subs_price_per_month.setText((String) subs_price_per_month.get(position));

        holder.subs_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (subscriptionClickListener !=null){
                    subscriptionClickListener.onItemClick(subscriptions.get(position).getOriginalPrice(),
                            subscriptions.get(position).getDuration());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class SubscriptionListViewHolder extends RecyclerView.ViewHolder{
        TextView subs_price, subs_duration, subs_price_per_month;
        CardView subs_plan;

        public SubscriptionListViewHolder(@NonNull View itemView) {
            super(itemView);
            subs_price = itemView.findViewById(R.id.subs_price);
            subs_duration = itemView.findViewById(R.id.subs_duration);
            subs_price_per_month = itemView.findViewById(R.id.subs_price_per_month);
            subs_plan = itemView.findViewById(R.id.subs_plan);
        }
    }

}
