package com.example.amvirgin.SubscriptionSection;

public interface SubscriptionClickListener {
    void onItemClick(Integer subs_price, Integer subs_duration);
}
