package com.example.amvirgin.SubscriptionSection;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionActivity extends AppCompatActivity {

    private RecyclerView rv_subscription_list;
    private Context context;
    private TabLayout mTabLayout;
    public CustomViewPager mPager;
    private FragmentManager fm;
    private List<SubscriptionFlow> subscriptionFlow;
    private String token;
    private ImageView backbtn;
    public TextView tv_select_pack, tv_enter_details, tv_pay;
    public LinearLayout select_pack_ll, enter_details_ll, pay_ll;
    public FrameLayout select_pack_fl_white, select_pack_fl_red, enter_details_fl_white, enter_details_fl_red,
            pay_fl_white, pay_fl_red;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);

        if (PreferenceManager.getBoolean(context, Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(context).getToken();
        }

        mTabLayout = findViewById(R.id.tab_layout);
        mPager = findViewById(R.id.pager);
        backbtn = findViewById(R.id.backbtn);

        tv_select_pack = findViewById(R.id.tv_select_pack);
        tv_enter_details = findViewById(R.id.tv_enter_details);
        tv_pay = findViewById(R.id.tv_pay);

        select_pack_ll = findViewById(R.id.select_pack_ll);
        enter_details_ll = findViewById(R.id.enter_details_ll);
        pay_ll = findViewById(R.id.pay_ll);

        select_pack_fl_white = findViewById(R.id.select_pack_fl_white);
        select_pack_fl_red = findViewById(R.id.select_pack_fl_red);
        enter_details_fl_white = findViewById(R.id.enter_details_fl_white);
        enter_details_fl_red = findViewById(R.id.enter_details_fl_red);
        pay_fl_white = findViewById(R.id.pay_fl_white);
        pay_fl_red = findViewById(R.id.pay_fl_red);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPager.getCurrentItem() == 0){
                    finish();
                }
                else if (mPager.getCurrentItem() == 1){
                    mPager.setCurrentItem(0);

                    tv_select_pack.setTextColor(getResources().getColor(R.color.colorwhite));
                    tv_enter_details.setTextColor(getResources().getColor(R.color.OffWhite));

                    select_pack_fl_red.setVisibility(View.VISIBLE);
                    select_pack_fl_white.setVisibility(View.GONE);

                    enter_details_fl_red.setVisibility(View.GONE);
                    enter_details_fl_white.setVisibility(View.VISIBLE);
                }
                else if (mPager.getCurrentItem() == 2){
                    if (token != null){
                        mPager.setCurrentItem(0);

                        tv_select_pack.setTextColor(getResources().getColor(R.color.colorwhite));
                        tv_pay.setTextColor(getResources().getColor(R.color.OffWhite));

                        select_pack_fl_red.setVisibility(View.VISIBLE);
                        select_pack_fl_white.setVisibility(View.GONE);

                        pay_fl_red.setVisibility(View.GONE);
                        pay_fl_white.setVisibility(View.VISIBLE);
                    }
                    else {
                        mPager.setCurrentItem(1);
                        tv_select_pack.setTextColor(getResources().getColor(R.color.OffWhite));
                        tv_enter_details.setTextColor(getResources().getColor(R.color.colorwhite));

                        select_pack_fl_red.setVisibility(View.GONE);
                        select_pack_fl_white.setVisibility(View.VISIBLE);

                        enter_details_fl_red.setVisibility(View.VISIBLE);
                        enter_details_fl_white.setVisibility(View.GONE);
                    }
                }
            }
        });

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }


        if (token !=null) {
//            subscriptionFlow = new ArrayList<>();
//            subscriptionFlow.add(new SubscriptionFlow(0, "Select Pack"));
//            subscriptionFlow.add(new SubscriptionFlow(2, "Pay"));
//            subscriptionFlow.add(new SubscriptionFlow(3, "Start Watching"));

//            tv_enter_details.setVisibility(View.GONE);
        }

        else {
//            subscriptionFlow = new ArrayList<>();
//            subscriptionFlow.add(new SubscriptionFlow(0, "Select Pack"));
//            subscriptionFlow.add(new SubscriptionFlow(1, "Enter Details"));
//            subscriptionFlow.add(new SubscriptionFlow(2, "Pay"));
//            subscriptionFlow.add(new SubscriptionFlow(3, "Start Watching"));
        }

        subscriptionFlow = new ArrayList<>();
        subscriptionFlow.add(new SubscriptionFlow(0, "Select Pack"));
        subscriptionFlow.add(new SubscriptionFlow(1, "Enter Details"));
        subscriptionFlow.add(new SubscriptionFlow(2, "Pay"));

     /*   for (int i = 0; i <subscriptionFlow.size(); i++){
            mTabLayout.addTab(mTabLayout.newTab().setText(subscriptionFlow.get(i).getFlow_title()));
        }*/

//        mTabLayout.addTab(mTabLayout.newTab().setText("Select Pack"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Enter Details"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Pay"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Start Watching"));

        mPager.setPagingEnabled(false);

        mPager.setAdapter(new SubscriptionFlowAdapter(context, getSupportFragmentManager(), subscriptionFlow));
//        mTabLayout.setupWithViewPager(mPager, true);

//        rv_subscription_list = findViewById(R.id.rv_subscription_list);

//        LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
//        rv_subscription_list.setLayoutManager(manager);
//
//        SubscriptionListAdapter adapter = new SubscriptionListAdapter(context);
//        rv_subscription_list.setAdapter(adapter);
    }
}
