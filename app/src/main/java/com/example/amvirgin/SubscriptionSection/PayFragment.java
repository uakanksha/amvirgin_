package com.example.amvirgin.SubscriptionSection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.payment.PaymentActivity;
import com.example.amvirgin.payment.model.PaymentFieldModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

public class PayFragment extends Fragment implements View.OnClickListener {

    RadioGroup rg_payment_methods;
    Button proceed_btn;
    private TextView subs_duration, subs_total_price;
    private int subscription_price, subscription_duration;
    private String payment_method;
    private int selected_payment_method;
    private RadioButton rb_card, rb_net_banking, rb_cod, rb_phonepe, rb_paypal, rb_razorpay;
    HashMap<String, String> value = new HashMap<>();

    public PayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pay, container, false);

        subs_duration = view.findViewById(R.id.subs_duration);
        subs_total_price = view.findViewById(R.id.subs_total_price);

        subscription_price = PreferenceManager.getInt(getContext(), "subscription_price");
        subscription_duration = PreferenceManager.getInt(getContext(), "subscription_duration");

        subs_total_price.setText("₹"+subscription_price);
        subs_duration.setText(subscription_duration+" Days");
        proceed_btn = view.findViewById(R.id.proceed_btn);

        rg_payment_methods = view.findViewById(R.id.rg_payments);
        proceed_btn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.proceed_btn:
                Intent intent_payment = new Intent(getContext(), PaymentActivity.class);
                intent_payment.putExtra(Constants.CHECKOUT_DETAILS, new PaymentFieldModel(
                        "Product ID",
                        "BINGLE WATCH",
                        "All episodes without any limits",
                        "https://github.githubassets.com/images/modules/open_graph/github-octocat.png",
                        ""+subscription_price,
                        "0",
                        ""+subscription_price,
                        false,
                        "",
                        -1));

                intent_payment.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_payment);
                Objects.requireNonNull(getActivity()).finish();
                break;
        }

    }
}
