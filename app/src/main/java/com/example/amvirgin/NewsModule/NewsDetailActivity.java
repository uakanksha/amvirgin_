package com.example.amvirgin.NewsModule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.amvirgin.Adapter.RecommendedVideosAdapter;
import com.example.amvirgin.Adapter.TrendingNewsAdapter;
import com.example.amvirgin.R;

import java.util.ArrayList;
import java.util.Arrays;

public class NewsDetailActivity extends AppCompatActivity {
    private Context context;
    private ImageView backbtn;
    private RecyclerView rv_trending_news, rv_recommended_videos;
    private ArrayList rVideos = new ArrayList(Arrays.asList(R.drawable.newsdetail1, R.drawable.newsdetail2, R.drawable.newsdetail3,
            R.drawable.newsdetail1, R.drawable.newsdetail2));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        backbtn = findViewById(R.id.backbtn);
        rv_trending_news = findViewById(R.id.rv_trending_news);
        rv_recommended_videos = findViewById(R.id.rv_recommended_videos);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rv_trending_news.setLayoutManager(manager);

        TrendingNewsAdapter adapter = new TrendingNewsAdapter(context);
        rv_trending_news.setAdapter(adapter);

        LinearLayoutManager managerRecVideos = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
        rv_recommended_videos.setLayoutManager(managerRecVideos);

        RecommendedVideosAdapter videosAdapter = new RecommendedVideosAdapter(context, rVideos);
        rv_recommended_videos.setAdapter(videosAdapter);

    }
}
