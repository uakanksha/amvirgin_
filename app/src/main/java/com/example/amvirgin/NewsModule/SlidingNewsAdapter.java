package com.example.amvirgin.NewsModule;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.amvirgin.MovieDetailActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.Slide;
import com.example.amvirgin.classes.BannerSliderModel;

import java.util.ArrayList;
import java.util.List;

public class SlidingNewsAdapter extends FragmentPagerAdapter {

    List<NewsCategories> newsCategories;
    Context mContext;
    private LayoutInflater inflater;

    public SlidingNewsAdapter(Context context, FragmentManager fm, List<NewsCategories> newsCategories) {
        super(fm);
        mContext = context;
        this.newsCategories = newsCategories;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return new NewsListingFragment(newsCategories.get(position).getCategory_name());
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return (newsCategories.get(position).getCategory_name());
    }

    @Override
    public int getCount() {
        return newsCategories.size();
    }
}
