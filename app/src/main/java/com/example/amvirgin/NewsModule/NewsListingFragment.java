package com.example.amvirgin.NewsModule;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.amvirgin.R;

import java.util.ArrayList;
import java.util.Arrays;

public class NewsListingFragment extends Fragment {
    private String category_name;
    private RecyclerView rv_news_list;
    private ArrayList news_images = new ArrayList<>(Arrays.asList(R.drawable.news1, R.drawable.news2, R.drawable.news3,
            R.drawable.news4, R.drawable.news1, R.drawable.news2, R.drawable.news3,
            R.drawable.news4, R.drawable.news1, R.drawable.news2));

    public NewsListingFragment(String category_name) {
        this.category_name = category_name;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_listing, container, false);
        rv_news_list = view.findViewById(R.id.rv_news_list);

        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv_news_list.setLayoutManager(manager);

        NewsListingAdapter adapter = new NewsListingAdapter(getContext(), news_images);
        rv_news_list.setAdapter(adapter);

        return view;
    }
}
