package com.example.amvirgin.NewsModule;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;

import java.util.ArrayList;

public class NewsListingAdapter extends RecyclerView.Adapter<NewsListingAdapter.ListingViewHolder> {

    Context mContext;
    ArrayList news_images;

    public NewsListingAdapter(Context mContext, ArrayList news_images){
        this.mContext = mContext;
        this.news_images = news_images;
    }

    @NonNull
    @Override
    public ListingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_news_listing_layout, parent, false);
        ListingViewHolder lvh = new ListingViewHolder(view);
        return lvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ListingViewHolder holder, int position) {
        holder.news_image.setImageResource((Integer) news_images.get(position));
        holder.news_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NewsDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public static class ListingViewHolder extends RecyclerView.ViewHolder{
        LinearLayout news_ll;
        ImageView news_image;

        public ListingViewHolder(@NonNull View itemView) {
            super(itemView);
            news_ll = itemView.findViewById(R.id.news_ll);
            news_image = itemView.findViewById(R.id.news_image);
        }
    }

}
