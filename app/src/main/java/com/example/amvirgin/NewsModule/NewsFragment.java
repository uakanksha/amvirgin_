package com.example.amvirgin.NewsModule;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;

import com.example.amvirgin.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class NewsFragment extends Fragment {
    private TabLayout mTabLayout;
    private ViewPager mPager;
    private List<NewsCategories> newsCategories;

    public NewsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
//        Toolbar toolbar1 = getActivity().findViewById(R.id.toolbar);
//        toolbar1.setVisibility(View.GONE);

        mTabLayout = view.findViewById(R.id.tab_layout);
        mPager = view.findViewById(R.id.pager);

//        mTabLayout.addTab(mTabLayout.newTab().setText("Latest"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Trending"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("World"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Cricket"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Entertainment"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Technology"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Sports"));
//        mTabLayout.addTab(mTabLayout.newTab().setText("Education"));

        newsCategories = new ArrayList<>();
        newsCategories.add(new NewsCategories("Latest", 0));
        newsCategories.add(new NewsCategories("Trending", 1));
        newsCategories.add(new NewsCategories("World", 2));
        newsCategories.add(new NewsCategories("Cricket", 3));
        newsCategories.add(new NewsCategories("Entertainment", 4));
        newsCategories.add(new NewsCategories("Technology", 5));
        newsCategories.add(new NewsCategories("Sports", 6));
        newsCategories.add(new NewsCategories("Education", 7));

        for (int i = 0; i <newsCategories.size(); i++){
            mTabLayout.addTab(mTabLayout.newTab().setText(newsCategories.get(i).getCategory_name()));
        }

        mPager.setAdapter(new SlidingNewsAdapter(getContext(), getFragmentManager(), newsCategories));
        mTabLayout.setupWithViewPager(mPager, true);

        return view;
    }
}
