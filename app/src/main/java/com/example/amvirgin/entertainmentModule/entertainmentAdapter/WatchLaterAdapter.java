package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.SplashActivity;
import com.example.amvirgin.entertainmentModule.Activity.MovieDetailSlidingActivity;
import com.example.amvirgin.entertainmentModule.entertainmentModel.ListWatchLaterModel;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.util.List;

public class WatchLaterAdapter extends RecyclerView.Adapter<WatchLaterAdapter.ViewHolder> {

    Context context;
    List<ListWatchLaterModel.Datum> list;
    LayoutInflater inflater;
    private static final String TAG = "WatchLaterAdapter";
    public WatchLaterAdapter(Context context, List<ListWatchLaterModel.Datum> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public WatchLaterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.watchlater_layout,parent, false );
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchLaterAdapter.ViewHolder holder, final int position) {
        if (list.get(position).getVideo()!=null){
            holder.name.setText(list.get(position).getVideo().getTitle());
            holder.type.setText(list.get(position).getVideo().getType());
            if (list.get(position).getVideo().getPoster()!=null){
                if (!list.get(position).getVideo().getPoster().equalsIgnoreCase("")){
                    //http://3.6.127.206/storage/
                    if (list.get(position).getVideo().getPoster().contains("http:")){
                        Glide.with(context).load(list.get(position).getVideo().getPoster()).into(holder.banner_img);

                    }else {
                        Glide.with(context).load("http://3.6.127.206/storage/"+list.get(position).getVideo().getPoster()).into(holder.banner_img);
                    }
                }else {
                    holder.banner_img.setImageResource(R.drawable.placeholder);
                }
            }else {
                holder.banner_img.setImageResource(R.drawable.placeholder);
            }
        }else {
            holder.layout_click.setVisibility(View.GONE);
        }
        holder.banner_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getVideo()!=null){
                    Intent in = new Intent(context, MovieDetailSlidingActivity.class);
                    PreferenceManager.saveBoolean(context, Constants.share_link_test, true);
                    PreferenceManager.saveInt(context, Constants.sharing_id, list.get(position).getVideoId());
                    context.startActivity(in);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView banner_img;
        TextView name, type;
        CardView layout_click;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            layout_click= itemView.findViewById(R.id.layout_click);
        }
    }
}
