package com.example.amvirgin.entertainmentModule.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.amvirgin.R;
import com.example.amvirgin.classes.MovieDetailModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.WatchLaterModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.payment.PaymentActivity;
import com.example.amvirgin.payment.model.PaymentFieldModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import retrofit2.Call;


public class VideoDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private TextView title_tv, price_tv, description_tv, duration_tv, released_tv, cast_tv;
    TextView pgRating_tv, director_tv;
    private Button rent, buy, add_watchlist;
    int id;
    private ImageView poster_iv;
    private String price, title, productID, description, icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
        Bundle bundle = getIntent().getExtras();
        init();
        assert bundle != null;
        id = bundle.getInt("VIDEO_ID", 0);
        if (id!=0){
            getDetails(id);
        }else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setTitle("Video Details");
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorwhite));
        }
        poster_iv= findViewById(R.id.poster_iv);
        title_tv = findViewById(R.id.title);
        price_tv = findViewById(R.id.price_tv);
        description_tv = findViewById(R.id.description);
        released_tv = findViewById(R.id.released_tv);
        cast_tv = findViewById(R.id.cast_tv);
        director_tv = findViewById(R.id.director_tv);
        duration_tv = findViewById(R.id.duration_tv);
        pgRating_tv = findViewById(R.id.pgRating_tv);
        rent = findViewById(R.id.rent);
        buy = findViewById(R.id.buy);
        add_watchlist = findViewById(R.id.add_watchlist);
        rent.setOnClickListener(this);
        buy.setOnClickListener(this);
        add_watchlist.setOnClickListener(this);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getDetails(final Integer id) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(VideoDetailsActivity.this);
        Call<MovieDetailModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .getMovieDetails("customer/videos/" + id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<MovieDetailModel>(VideoDetailsActivity.this) {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(MovieDetailModel response) {
                if (response.getStatus()==200){
                    if (response.getData().getPoster()!=null)
                        Picasso.with(VideoDetailsActivity.this).load(response.getData().getPoster()).into(poster_iv);
                    title_tv.setText(response.getData().getTitle());
                    price_tv.setText("₹ "+response.getData().getPrice());
                    description_tv.setText(response.getData().getDescription());
                    released_tv.setText(""+response.getData().getReleased());
                    String cast = response.getData().getCast().replaceAll("/", "\n");
                    cast_tv.setText(""+cast);
                    String director = response.getData().getDirector().replaceAll("/", "\n");
                    director_tv.setText(director);
                    duration_tv.setText(""+response.getData().getDuration());
                    rent.setText("Rent HD ₹ "+response.getData().getPrice());
                    buy.setText("Buy HD ₹ "+response.getData().getPrice());
                    pgRating_tv.setText("★ "+response.getData().getRating()+"/5");

                    price = response.getData().getPrice().toString();
                    title = response.getData().getTitle();
                    productID = "video"+id;
                    description  = response.getData().getDescription();
                    icon  = response.getData().getPoster();

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rent:
                Intent intent_payment = new Intent(VideoDetailsActivity.this, PaymentActivity.class);
                intent_payment.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent_payment.putExtra(Constants.CHECKOUT_DETAILS, new PaymentFieldModel(
                        productID,
                        title,
                        description,
                        icon,
                        price,
                        "0",
                        price,
                        false,
                        "",
                        -1));
                intent_payment.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_payment);
                finish();
                break;
            case R.id.buy:
                Intent payment = new Intent(VideoDetailsActivity.this, PaymentActivity.class);
                payment.putExtra(Constants.CHECKOUT_DETAILS, new PaymentFieldModel(
                        productID,
                        title,
                        description,
                        icon,
                        price,
                        "0",
                        price,
                        false,
                        "",
                        -1));

                payment.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(payment);
                finish();
                break;
            case R.id.add_watchlist:
                if (PreferenceManager.getBoolean(VideoDetailsActivity.this, Constants.Key.IS_ALREADY_LOGGED_IN)){
                    addWatchLater(
                            PreferenceManager.getString(VideoDetailsActivity.this, Constants.Key.ApiToken)
                            ,id);
                }
                break;
        }
    }

    private void addWatchLater(String token, final int id) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(VideoDetailsActivity.this);
        Call<WatchLaterModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .addWatchLater("Bearer " + token, id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<WatchLaterModel>(VideoDetailsActivity.this) {
            @Override
            public void onResponse(WatchLaterModel response) {
                if (response.getStatus() == 200) {

                        if (response.getMessage().equalsIgnoreCase("Successfully added in list")){
                            SnackbarManager.successResponse(VideoDetailsActivity.this, response.getMessage());
                        }else {
                            SnackbarManager.successResponse(VideoDetailsActivity.this, response.getMessage());
                        }

                } else {
                    SnackbarManager.sException(VideoDetailsActivity.this, response.getMessage());
                }
            }
        });
    }
}
