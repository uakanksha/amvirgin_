
package com.example.amvirgin.entertainmentModule.entertainmentModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListWatchLaterModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("customer_id")
        @Expose
        private Integer customerId;
        @SerializedName("video_id")
        @Expose
        private Integer videoId;
        @SerializedName("video_type")
        @Expose
        private String videoType;
        @SerializedName("customer_type")
        @Expose
        private Object customerType;
        @SerializedName("customer_ip")
        @Expose
        private String customerIp;
        @SerializedName("customer_user_agent")
        @Expose
        private String customerUserAgent;
        @SerializedName("video_count")
        @Expose
        private Object videoCount;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("video")
        @Expose
        private Video video;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getCustomerId() {
            return customerId;
        }

        public void setCustomerId(Integer customerId) {
            this.customerId = customerId;
        }

        public Integer getVideoId() {
            return videoId;
        }

        public void setVideoId(Integer videoId) {
            this.videoId = videoId;
        }

        public String getVideoType() {
            return videoType;
        }

        public void setVideoType(String videoType) {
            this.videoType = videoType;
        }

        public Object getCustomerType() {
            return customerType;
        }

        public void setCustomerType(Object customerType) {
            this.customerType = customerType;
        }

        public String getCustomerIp() {
            return customerIp;
        }

        public void setCustomerIp(String customerIp) {
            this.customerIp = customerIp;
        }

        public String getCustomerUserAgent() {
            return customerUserAgent;
        }

        public void setCustomerUserAgent(String customerUserAgent) {
            this.customerUserAgent = customerUserAgent;
        }

        public Object getVideoCount() {
            return videoCount;
        }

        public void setVideoCount(Object videoCount) {
            this.videoCount = videoCount;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Video getVideo() {
            return video;
        }

        public void setVideo(Video video) {
            this.video = video;
        }

        public class Video {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("slug")
            @Expose
            private String slug;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("duration")
            @Expose
            private String duration;
            @SerializedName("released")
            @Expose
            private String released;
            @SerializedName("cast")
            @Expose
            private String cast;
            @SerializedName("director")
            @Expose
            private String director;
            @SerializedName("trailer")
            @Expose
            private String trailer;
            @SerializedName("poster")
            @Expose
            private String poster;
            @SerializedName("backdrop")
            @Expose
            private String backdrop;
            @SerializedName("genreId")
            @Expose
            private Integer genreId;
            @SerializedName("sectionId")
            @Expose
            private Integer sectionId;
            @SerializedName("qualitySlug")
            @Expose
            private String qualitySlug;
            @SerializedName("languageSlug")
            @Expose
            private String languageSlug;
            @SerializedName("rating")
            @Expose
            private Double rating;
            @SerializedName("pgRating")
            @Expose
            private String pgRating;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("hits")
            @Expose
            private Integer hits;
            @SerializedName("trending")
            @Expose
            private Integer trending;
            @SerializedName("rank")
            @Expose
            private Integer rank;
            @SerializedName("topPick")
            @Expose
            private Integer topPick;
            @SerializedName("showOnHome")
            @Expose
            private Integer showOnHome;
            @SerializedName("subscriptionType")
            @Expose
            private String subscriptionType;
            @SerializedName("price")
            @Expose
            private Integer price;
            @SerializedName("hasSeasons")
            @Expose
            private Integer hasSeasons;
            @SerializedName("seasons")
            @Expose
            private Integer seasons;
            @SerializedName("active")
            @Expose
            private Integer active;
            @SerializedName("pending")
            @Expose
            private Integer pending;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            public String getReleased() {
                return released;
            }

            public void setReleased(String released) {
                this.released = released;
            }

            public String getCast() {
                return cast;
            }

            public void setCast(String cast) {
                this.cast = cast;
            }

            public String getDirector() {
                return director;
            }

            public void setDirector(String director) {
                this.director = director;
            }

            public String getTrailer() {
                return trailer;
            }

            public void setTrailer(String trailer) {
                this.trailer = trailer;
            }

            public String getPoster() {
                return poster;
            }

            public void setPoster(String poster) {
                this.poster = poster;
            }

            public String getBackdrop() {
                return backdrop;
            }

            public void setBackdrop(String backdrop) {
                this.backdrop = backdrop;
            }

            public Integer getGenreId() {
                return genreId;
            }

            public void setGenreId(Integer genreId) {
                this.genreId = genreId;
            }

            public Integer getSectionId() {
                return sectionId;
            }

            public void setSectionId(Integer sectionId) {
                this.sectionId = sectionId;
            }

            public String getQualitySlug() {
                return qualitySlug;
            }

            public void setQualitySlug(String qualitySlug) {
                this.qualitySlug = qualitySlug;
            }

            public String getLanguageSlug() {
                return languageSlug;
            }

            public void setLanguageSlug(String languageSlug) {
                this.languageSlug = languageSlug;
            }

            public Double getRating() {
                return rating;
            }

            public void setRating(Double rating) {
                this.rating = rating;
            }

            public String getPgRating() {
                return pgRating;
            }

            public void setPgRating(String pgRating) {
                this.pgRating = pgRating;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public Integer getHits() {
                return hits;
            }

            public void setHits(Integer hits) {
                this.hits = hits;
            }

            public Integer getTrending() {
                return trending;
            }

            public void setTrending(Integer trending) {
                this.trending = trending;
            }

            public Integer getRank() {
                return rank;
            }

            public void setRank(Integer rank) {
                this.rank = rank;
            }

            public Integer getTopPick() {
                return topPick;
            }

            public void setTopPick(Integer topPick) {
                this.topPick = topPick;
            }

            public Integer getShowOnHome() {
                return showOnHome;
            }

            public void setShowOnHome(Integer showOnHome) {
                this.showOnHome = showOnHome;
            }

            public String getSubscriptionType() {
                return subscriptionType;
            }

            public void setSubscriptionType(String subscriptionType) {
                this.subscriptionType = subscriptionType;
            }

            public Integer getPrice() {
                return price;
            }

            public void setPrice(Integer price) {
                this.price = price;
            }

            public Integer getHasSeasons() {
                return hasSeasons;
            }

            public void setHasSeasons(Integer hasSeasons) {
                this.hasSeasons = hasSeasons;
            }

            public Integer getSeasons() {
                return seasons;
            }

            public void setSeasons(Integer seasons) {
                this.seasons = seasons;
            }

            public Integer getActive() {
                return active;
            }

            public void setActive(Integer active) {
                this.active = active;
            }

            public Integer getPending() {
                return pending;
            }

            public void setPending(Integer pending) {
                this.pending = pending;
            }
        }
    }

}
