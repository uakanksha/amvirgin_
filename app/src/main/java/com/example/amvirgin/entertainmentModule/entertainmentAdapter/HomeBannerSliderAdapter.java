package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.SubscriptionSection.SubscriptionActivity;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;

import java.util.List;

public class HomeBannerSliderAdapter extends RecyclerView.Adapter<HomeBannerSliderAdapter.BannerSliderViewHolder>{

    Context context;
    List<HomeModel.Slider> sliders;

    public HomeBannerSliderAdapter(Context context, List<HomeModel.Slider> sliders) {
        this.context = context;
        this.sliders = sliders;
    }

    @NonNull
    @Override
    public BannerSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BannerSliderViewHolder(LayoutInflater.from(context).inflate(R.layout.banner_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BannerSliderViewHolder holder, int position) {
        if (!sliders.get(position).getBanner().equalsIgnoreCase("")) {
            Glide.with(context).load(sliders.get(position).getBanner()).into(holder.bannerImg);
        }
        else {
            holder.bannerImg.setImageResource(R.drawable.placeholder);
        }
        holder.movie_name.setText(sliders.get(position).getTitle());
        holder.banner_subs_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SubscriptionActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sliders.size();
    }

    public class BannerSliderViewHolder extends RecyclerView.ViewHolder{
        ImageView bannerImg;
        TextView movie_name;
        LinearLayout banner_subs_ll;

        public BannerSliderViewHolder(@NonNull View itemView) {
            super(itemView);

            bannerImg = itemView.findViewById(R.id.banner_img);
            movie_name = itemView.findViewById(R.id.movie_name);
            banner_subs_ll = itemView.findViewById(R.id.banner_subs_ll);
        }
    }
}
