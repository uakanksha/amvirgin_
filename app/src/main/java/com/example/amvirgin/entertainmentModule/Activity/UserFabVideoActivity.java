package com.example.amvirgin.entertainmentModule.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.WatchLaterAdapter;
import com.example.amvirgin.entertainmentModule.entertainmentModel.ListWatchLaterModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class UserFabVideoActivity extends AppCompatActivity {

    RecyclerView rv_popular_movies;
    private ImageView backbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_fab_video);
        rv_popular_movies = findViewById(R.id.rv_popular_movies);
        if (PreferenceManager.getBoolean(this, Constants.Key.IS_ALREADY_LOGGED_IN)){
            listWatchLater(PreferenceManager.getString(this, Constants.Key.ApiToken));
        }

        backbtn = findViewById(R.id.backbtn);



        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
            }
        });

    }

    private void listWatchLater(String token) {
        Log.e("dfdsfdsfds", "listWatchLater: "+token );
        ServiceWrapper serviceWrapper = new ServiceWrapper(this);
        Call<ListWatchLaterModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .listWatchLater("Bearer " + token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ListWatchLaterModel>(this) {
            @Override
            public void onResponse(ListWatchLaterModel response) {
                boolean m = false;
                if (response.getStatus() == 200) {
                    boolean check = false;
                    try {
                        if (response.getData().size() > 0) {
                            setData(response.getData());
                        }
                    } catch (Exception e) {
                        SnackbarManager.sException(UserFabVideoActivity.this, response.getMessage());
                    }
                } else {
                    SnackbarManager.sException(UserFabVideoActivity.this, response.getMessage());
                }
            }
        });
    }
    private void setData(List<ListWatchLaterModel.Datum> list){
        GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 3);
        rv_popular_movies.setLayoutManager(manager);
        WatchLaterAdapter adapter = new WatchLaterAdapter(UserFabVideoActivity.this, list);
        rv_popular_movies.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceManager.getBoolean(this, Constants.Key.IS_ALREADY_LOGGED_IN)){
            listWatchLater(PreferenceManager.getString(this, Constants.Key.ApiToken));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
