package com.example.amvirgin.entertainmentModule.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.amvirgin.Adapter.EpisodesAdapter;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.SubscriptionSection.SubscriptionActivity;
import com.example.amvirgin.classes.MovieDetailModel;
import com.example.amvirgin.classes.Seasons;
import com.example.amvirgin.entertainmentModule.Activity.VideoDetailsActivity;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.HomeTrendingAdapter;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.ListWatchLaterModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.WatchLaterModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.interfaces.EpisodeClickListener;
import com.example.amvirgin.interfaces.FragmentLifecycle;
import com.example.amvirgin.interfaces.ItemClickListener;
import com.example.amvirgin.shopModule.Other.SnackbarManager;

import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.android.exoplayer2.BuildConfig;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Format;

import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.RandomTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SubtitleView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import spencerstudios.com.ezdialoglib.EZDialog;

import static com.example.amvirgin.R.layout.spinner_item_overview;

public class MovieDetailFragment extends Fragment implements View.OnClickListener, Player.EventListener, FragmentLifecycle {

    TextView description_text, show, director_tv, whatsapp_share_tv, year_tv, list_tv, pgRating_tv;
    LinearLayout video_details, show_ll, hide_ll, episodes_ll, seasons_ll;
    RelativeLayout playerContainer;
    LinearLayout main_ll;
    RatingBar tv_rating;
    NestedScrollView mNestedScrollView;
    Animation animShow, animHide;
    FragmentManager fm;
    boolean description = false;
    TextView video_type;
    private static final String TAG = "MovieDetailFragment";

    ImageView banner_img, ic_dropdown;
    ImageButton caption_subtitle;
    TextView title, duration_tv, genre_tv, language_tv, cast_tv, play_trailer, title_controller;
    TextView watch_later, tv_share;
    String trailer_url;
    FrameLayout poster_frame, main_media_frame;
    RelativeLayout play_rl;
    List<Seasons> seasons;
    ArrayList seasons_arrayList;
    MovieDetailModel data;
    int position = 0;

    private boolean check = false;
    private boolean isInList = false;
    //private ImageButton exo_ffwd, exo_rew;

    RecyclerView rv_home_trending;
    RecyclerView rv_episodes;
    Spinner sp_seasons;
    String v_title;
    private ArrayAdapter<CharSequence> mSeasonsAdapter;
    private ArrayList episodes = new ArrayList<>(Arrays.asList("Episode 1", "Episode 2", "Episode 3", "Episode 4", "Episode 5",
            "Episode 6", "Episode 7"));

    private final String STATE_RESUME_WINDOW = "resumeWindow";
    private final String STATE_RESUME_POSITION = "resumePosition";
    private final String STATE_PLAYER_FULLSCREEN = "playerFullscreen";

    private MediaSource videoSource;
    private DataSource.Factory dataSourceFactory;
    //    VideoView videoView;
//    String url = "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4";
//    String url = "http://amvirgine.zobofy.com/storage/trailers/SXmpAwuQW8aW8gTjGMQr0TlWIVLO4wRdI9YmKoq3.mp4";

    String url = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4", sub_url;
    PlayerView playerView;
    ProgressBar pb;
    ImageView settings, subtitles, setting_video_quality;

    SimpleExoPlayer player;
    Handler mHandler;
    Runnable mRunnable;


    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";
    public static final String TUNNELING_EXTRA = "tunneling";


    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    protected String userAgent;


    boolean mExoPlayerFullscreen = false;
    FrameLayout mFullScreenButton;
    ImageView mFullScreenIcon;
    Dialog mFullScreenDialog;
    String slug;
    int id;

    int mResumeWindow;
    long mResumePosition;
    List<HomeModel.Item> items;

    String subscriptionType = "";
    float price = 0f;
    boolean check_video_type = false; //if trailer play is false if play full is true
    private PopupMenu popup;

    SubtitleView subtitleView;

    boolean caption = false;
    LinearLayoutManager manager;

    private TrackGroupArray lastSeenTrackGroupArray;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private DefaultTrackSelector trackSelector;

    Bundle savedInstanceState;

    public MovieDetailFragment(int id) {
        this.id = id;
        this.items = items;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_detail, container, false);


        main_ll = view.findViewById(R.id.main_ll);
        mNestedScrollView = view.findViewById(R.id.mNestedScrollView);
        main_media_frame = view.findViewById(R.id.main_media_frame);
        playerView = view.findViewById(R.id.playerView);
        pb = view.findViewById(R.id.pb);
        banner_img = view.findViewById(R.id.banner_img);
        play_rl = view.findViewById(R.id.play_rl);
        rv_home_trending = view.findViewById(R.id.rv_home_trending);
        show = view.findViewById(R.id.show);
        tv_share = view.findViewById(R.id.tv_share);

        sp_seasons = view.findViewById(R.id.sp_seasons);
        ic_dropdown = view.findViewById(R.id.ic_dropdown);
        title = view.findViewById(R.id.title_tv);
        description_text = view.findViewById(R.id.description_text);
        year_tv = view.findViewById(R.id.year_tv);
        duration_tv = view.findViewById(R.id.duration_tv);
        genre_tv = view.findViewById(R.id.genre_tv);
        language_tv = view.findViewById(R.id.language_tv);
        director_tv = view.findViewById(R.id.director_tv);
        rv_episodes = view.findViewById(R.id.recycleview_movie);
        list_tv = view.findViewById(R.id.list_tv);
        pgRating_tv = view.findViewById(R.id.pgRating_tv);
        tv_rating = view.findViewById(R.id.tv_rating);
        cast_tv = view.findViewById(R.id.cast_tv);
        video_details = view.findViewById(R.id.video_details);
        watch_later = view.findViewById(R.id.watch_later);
        playerContainer = view.findViewById(R.id.playerContainer);
        play_trailer = view.findViewById(R.id.play_trailer);
        title_controller = view.findViewById(R.id.title_controller);
        caption_subtitle = view.findViewById(R.id.caption_subtitle);
        mFullScreenIcon = view.findViewById(R.id.exo_fullscreen_icon);
        setting_video_quality = view.findViewById(R.id.setting_video_quality);
        subtitleView = (SubtitleView) view.findViewById(com.google.android.exoplayer2.R.id.exo_subtitles);
        video_type = view.findViewById(R.id.video_type);


        play_rl.setOnClickListener(this);
        tv_share.setOnClickListener(this);


        show.setOnClickListener(this);
        watch_later.setOnClickListener(this);
        play_trailer.setOnClickListener(this);
        caption_subtitle.setOnClickListener(this);
        setting_video_quality.setOnClickListener(this);
        setting_video_quality.setEnabled(false);

        manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv_episodes.setLayoutManager(manager);

        if (savedInstanceState != null) {
            mResumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW);
            mResumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
            mExoPlayerFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN);
        }
        getDetails(id);
        getTrendingDetails();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (savedInstanceState != null) {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startWindow = savedInstanceState.getInt(KEY_WINDOW);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        } else {
            DefaultTrackSelector.ParametersBuilder builder =
                    new DefaultTrackSelector.ParametersBuilder(/* context= */ requireContext());
            boolean tunneling = true;
            if (Util.SDK_INT >= 21 && tunneling) {
                builder.setTunnelingAudioSessionId(C.generateAudioSessionIdV21(/* context= */ requireContext()));
            }
            trackSelectorParameters = builder.build();
            clearStartPosition();
        }
    }

    private void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }


    private DataSource.Factory buildDataSourceFactory() {
        DefaultDataSourceFactory upstreamFactory =
                new DefaultDataSourceFactory(requireContext(), buildHttpDataSourceFactory());
        return upstreamFactory;
    }
    public HttpDataSource.Factory buildHttpDataSourceFactory() {
        String userAgent = Util.getUserAgent(requireContext(), getResources().getString(R.string.app_name));
        return new DefaultHttpDataSourceFactory(userAgent);
    }



    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.play_rl:
                if (trailer_url == null) {
                    ToastMessage.getInstance(getContext()).showErrorShortCustomToast("Video is not Available!!");
                } else {
                    if (subscriptionType.equalsIgnoreCase("free")) {
                        playVideo(trailer_url, sub_url);
                    } else if (subscriptionType.equalsIgnoreCase("subscription")) {
                        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)
                                && PreferenceManager.getBoolean(getContext(), Constants.Key.SUBSCRIPTION)) {
                            playVideo(trailer_url, sub_url);
                        } else if (!PreferenceManager.getBoolean(getContext(), Constants.Key.SUBSCRIPTION)) {
                            open("You don't have any subscription. do you want subscription");
                        } else {
                            open("Login to play this video");
                        }
                    } else if (subscriptionType.equalsIgnoreCase("paid")) {
                        open("This is cost you Rs " + price + " do you want to pay");
                    }
                }
                break;
            case R.id.tv_share:
                shareVideoLink(id);
                break;
            case R.id.show:
                if (description) {
                    description = false;
                    description_text.setMaxLines(2);
                    show.setText("Read More");
                    video_details.setVisibility(View.GONE);
                } else {
                    description = true;
                    description_text.setSingleLine(false);
                    show.setText("Less");
                    video_details.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.watch_later:
                if (PreferenceManager.getString(getContext(), Constants.Key.ApiToken) != null) {
                    addWatchLater(PreferenceManager.getString(getContext(), Constants.Key.ApiToken), id);
                } else {
                    SnackbarManager.successResponse(getContext(), "You have to login first!");
                }
                break;
            case R.id.play_trailer:
                if (check_video_type) {
                    play_trailer.setText("Watch Trailer");
                    play_rl.setVisibility(View.VISIBLE);
                    main_media_frame.setVisibility(View.GONE);
                    releasePlayer();
                } else {
                    play_trailer.setText("Watch Video");
                    check_video_type = true;
                    if (trailer_url != null) {
                        playVideo(trailer_url, sub_url);
                    } else {
                        ToastMessage.getInstance(getContext()).showErrorShortCustomToast("Video is not Available!!");
                    }
                }
                break;

            case R.id.caption_subtitle:
                PopupMenu popup = new PopupMenu(getContext(), v);
                popup.getMenuInflater().inflate(R.menu.subtile_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(item -> {
                    if (item.getItemId() == R.id.hindi) {
                        //player.prepare(videoSource);
                        player.prepare(videoSource, false, false);
                    } else if (item.getItemId() == R.id.english) {
                        if (caption) {
                            Format textFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP,
                                    null, Format.NO_VALUE, Format.NO_VALUE, "en", null, Format.OFFSET_SAMPLE_RELATIVE);
                            MediaSource textMediaSource = new SingleSampleMediaSource.Factory(dataSourceFactory)
                                    .createMediaSource(Uri.parse(sub_url), textFormat, C.TIME_UNSET);
                            MergingMediaSource mediaSource = new MergingMediaSource(videoSource, textMediaSource);
                            player.prepare(mediaSource, false, false);
                        } else {
                            Toast.makeText(getContext(), "Subtitle not available", Toast.LENGTH_SHORT).show();
                        }
                    }
                    return true;
                });
                popup.show();
                break;
            case R.id.setting_video_quality:
                getVideoQuality(v);
                break;
        }

    }

    private void open(String msg) {
        new EZDialog.Builder(getContext())
                .setTitle("AmVirgin")
                .setMessage(msg)
                .setPositiveBtnText("okay")
                .setNegativeBtnText("close")
                .setCancelableOnTouchOutside(false)
                .OnPositiveClicked(() -> {
                    if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
                        if (!PreferenceManager.getBoolean(getContext(), Constants.Key.SUBSCRIPTION) && price <= 0) {
                            Intent sub = new Intent(getContext(), SubscriptionActivity.class);
                            startActivity(sub);
                        } else {
                            Intent intent_payment = new Intent(getContext(), VideoDetailsActivity.class);
                            intent_payment.putExtra("VIDEO_ID", id);
                            startActivity(intent_payment);
                        }
                    } else {
                        //open login
                        startActivity(new Intent(getContext(), LoginActivity.class));
                    }
                })
                .OnNegativeClicked(() -> {

                })
                .build();
    }

    private void playVideo(String url1, String sub_url1) {

        play_rl.setVisibility(View.GONE);
        main_media_frame.setVisibility(View.VISIBLE);

        initializePlayer();
        initFullscreenDialog();
        initFullscreenButton();
        if (sub_url1 == null) {
            sub_url1 = "";
            caption = false;
        } else {
            caption = true;
        }
        if (url1 != null) {
            buildMediaSource(Uri.parse(url1), Uri.parse(sub_url1));
        } else {
            ToastMessage.getInstance(getContext()).showErrorShortCustomToast("Video is not Available!!");
        }

    }

    private void showSettingsDialog() {
//        Toast.makeText(getApplicationContext(),
//                "hhhhhhhh", Toast.LENGTH_SHORT).show();
        final CharSequence[] items = {"Option-1", "Option-2", "Option-3", "Option-4"};
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(getContext());
        alt_bld.setTitle("Video Quality");
        alt_bld.setSingleChoiceItems(items, -1, new DialogInterface
                .OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Toast.makeText(getContext(),
                        "Group Name = " + items[item], Toast.LENGTH_SHORT).show();
//                dialog.dismiss();// dismiss the alertbox after chose option

            }
        });
        alt_bld.setPositiveButton("OK", null);
        alt_bld.setNegativeButton("CANCEL", null);
        alt_bld.show();
    }

    public void showLanguageDialog() {
        final String[] items = {"On", "Off"};
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setTitle("Subtitles");
        dialog.setSingleChoiceItems(items, 0, new DialogInterface
                .OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                position = item;
            }
        });
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                dialog.dismiss();
                if (sub_url == null) {
                    sub_url = "";
                    caption = false;
                } else {
                    caption = true;
                }
                buildMediaSource(Uri.parse(trailer_url), Uri.parse(sub_url));
            }
        });
        dialog.setNegativeButton("CANCEL", null);
        dialog.show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putInt(STATE_RESUME_WINDOW, mResumeWindow);
        outState.putLong(STATE_RESUME_POSITION, mResumePosition);
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, mExoPlayerFullscreen);

        super.onSaveInstanceState(outState);
    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(getContext(), android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }

    private void openFullscreenDialog() {
        Objects.requireNonNull(getActivity()).getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ((ViewGroup) playerView.getParent()).removeView(playerView);
        mFullScreenDialog.addContentView(playerView, new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_fullscreen_exit_black_24dp));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
    }

    private void closeFullscreenDialog() {
        Objects.requireNonNull(getActivity()).getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((ViewGroup) playerView.getParent()).removeView(playerView);
        main_media_frame.addView(playerView);
        mExoPlayerFullscreen = false;
        mFullScreenDialog.dismiss();
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this.getActivity(), R.drawable.ic_fullscreen_black_24dp));
    }

    private void initFullscreenButton() {
        mFullScreenIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mExoPlayerFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }

    private void buildMediaSource(Uri mUri, Uri parse) {

        dataSourceFactory = buildDataSourceFactory();
        videoSource = createLeafMediaSource(mUri, urlExtension(mUri));
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
        if (player.getPlayWhenReady()) {
            setting_video_quality.setEnabled(true);
        } else {
            setting_video_quality.setEnabled(false);
        }
    }

    private String urlExtension(Uri uri){
        String url = uri.toString();
        int l = url.length();
        int lastDot = url.lastIndexOf(".");
        return url.substring(lastDot+1, l);
    }

    private void initializePlayer() {
        if (player == null) {

            TrackSelection.Factory trackSelectionFactory = new RandomTrackSelection.Factory();

            RenderersFactory renderersFactory = buildRenderersFactory(false);
            trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder().build();
            trackSelector = new DefaultTrackSelector(/* context= */ requireContext(), trackSelectionFactory);
            trackSelector.setParameters(trackSelectorParameters);
            lastSeenTrackGroupArray = null;

            player = new SimpleExoPlayer.Builder(/* context= */ requireContext(), renderersFactory)
                    .setTrackSelector(trackSelector)
                    .build();

            playerView.setFastForwardIncrementMs(10000);
            playerView.setRewindIncrementMs(10000);
            playerView.setPlayer(player);
        }
    }


    private MediaSource createLeafMediaSource(
            Uri uri, String extension) {
        @C.ContentType int type = Util.inferContentType(uri, extension);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    public RenderersFactory buildRenderersFactory(boolean preferExtensionRenderer) {
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionRenderer
                        ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        return new DefaultRenderersFactory(requireContext())
                .setExtensionRendererMode(extensionRendererMode);
    }

    public boolean useExtensionRenderers() {
        //BuildConfig.FLAVOR
        return "withExtensions".equals(BuildConfig.FLAVOR);
    }

    private void getVideoQuality(View v) {
//        PopupMenu popup = new PopupMenu(getContext(), v);
//        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                //player.setSelectedTrack(0, (item.getItemId() - 1));
//                return false;
//            }
//        });
//        Menu menu = popup.getMenu();
//        menu.add(Menu.NONE, 0, 0, "Video Quality");
//        Log.e(TAG, "getVideoQuality: "+player.getCurrentTrackGroups().get(0).length);
//        for (int i = 0; i < player.getCurrentTrackGroups().get(0).length; i++) {
//            Format format = player.getCurrentTrackGroups().get(0).getFormat(i);
//            if (format.width!=-1){
//
//            }
//
//            Log.e(TAG, "getVideoQuality: ddd"+format.width);
////            player.
//            if (MimeTypes.isVideo(format.containerMimeType)) {
//                Log.e(TAG, "getVideoQuality: ddd"+format.width);
//
////                if (format.selectionFlags) {
////                    menu.add(1, (i + 1), (i + 1), "Auto");
////                } else {
////                    menu.add(1, (i + 1), (i + 1), format.width + "p");
////                }
//            }
//        }
//        menu.setGroupCheckable(1, true, true);
//        //menu.findItem((player.getSelectedTrack(0) + 1)).setChecked(true);
//        popup.show();
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
            player.getPlaybackState();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePlayer();
        if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        PreferenceManager.saveBoolean(getContext(), Constants.checkFragment, true);

        resumePlayer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

//    @Override
//    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
//
//    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
        pb.setVisibility(View.GONE);


    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED ||
                !playWhenReady) {
            playerView.setKeepScreenOn(false);
        } else if (playbackState == ExoPlayer.STATE_BUFFERING) {
            Log.e(TAG, "onPlayerStateChanged: ExoPlayer.STATE_BUFFERING");
            pb.setVisibility(View.VISIBLE);
        } else if (playbackState == ExoPlayer.STATE_READY) {
            Log.e(TAG, "onPlayerStateChanged: ExoPlayer.STATE_READY");
            pb.setVisibility(View.INVISIBLE);
        } else {
            Log.e(TAG, "onPlayerStateChanged: ExoPlayer.setKeepScreenOn");
            playerView.setKeepScreenOn(true);
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    private void getDetails(final Integer id) {
        if (PreferenceManager.getString(getContext(), Constants.Key.ApiToken) != null) {
            listWatchLater(PreferenceManager.getString(getContext(), Constants.Key.ApiToken));
        }
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<MovieDetailModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .getMovieDetails("customer/videos/" + id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<MovieDetailModel>(getContext()) {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(MovieDetailModel response) {

                data = response;
                if (response.getData().getTrailer() != null)
                    if (response.getData().getTrailer()
                            .equalsIgnoreCase("")) {
                        trailer_url = null;
                    } else {
                        trailer_url = response.getData().getTrailer();
                    }
                if (response.getData().getType().equalsIgnoreCase("movie")) {
                    list_tv.setText("Recommended");
                    manager = new GridLayoutManager(getContext(), 3);
                    rv_episodes.setLayoutManager(manager);
                } else {
                    manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                    rv_episodes.setLayoutManager(manager);
                    list_tv.setText("Episode");
                }
                v_title = data.getData().getTitle();
                title.setText(data.getData().getTitle());
                title_controller.setText("" + data.getData().getTitle());
                pgRating_tv.setText(data.getData().getPgRating());
                trailer_url = data.getData().getTrailer();
                genre_tv.setText(data.getData().getGenre());
                video_type.setText(data.getData().getType());
                language_tv.setText(data.getData().getPgRating());
                duration_tv.setText(data.getData().getDuration());
                String duration = data.getData().getReleased();
                String[] year = duration.split("-");
                year_tv.setText(year[0]);
                director_tv.setText(data.getData().getDirector());
                description_text.setText(data.getData().getDescription());
                description_text.setMaxLines(2);
                description = false;
                show.setText("Read More");
                video_details.setVisibility(View.GONE);
                tv_rating.setRating(Float.parseFloat("" + data.getData().getRating()));
                String cast = data.getData().getCast().replaceAll("/", "\n");
                cast_tv.setText(cast);

                price = response.getData().getPrice();
                subscriptionType = response.getData().getSubscriptionType();

                if (response.getData().getBackdrop() != null)
                    if (!response.getData().getBackdrop().equalsIgnoreCase("")) {
                        Picasso.with(getContext()).load(response.getData().getBackdrop()).into(banner_img,
                                new com.squareup.picasso.Callback() {
                                    @Override
                                    public void onSuccess() {
//                                        pb.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError() {

                                    }
                                });
                    } else {
                        banner_img.setImageResource(R.drawable.placeholder);
                    }


                if (response.getData().getContent().size() > 0) {
                    seasons = new ArrayList<>();
                    seasons_arrayList = new ArrayList<>();
                    for (int i = 1; i <= response.getData().getContent().size(); i++) {
                        seasons_arrayList.add("Season " + i);
                    }
                    sp_seasons.setVisibility(View.VISIBLE);
                    ic_dropdown.setVisibility(View.VISIBLE);
                    mSeasonsAdapter = new ArrayAdapter<CharSequence>(getContext(), spinner_item_overview, seasons_arrayList);
                    mSeasonsAdapter.setDropDownViewResource(R.layout.episids_layout);
                    sp_seasons.setAdapter(mSeasonsAdapter);

                    sp_seasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            EpisodesAdapter adapter = new EpisodesAdapter(getContext(), response.getData(), response.getData().getContent().get(position).getContent(),
                                    response.getData().getPoster(), new EpisodeClickListener() {
                                @Override
                                public void onMovieItemClick(MovieDetailModel.Data.Recommended value) {
                                }

                                @Override
                                public void onSeriesItemClick(List<MovieDetailModel.Data.Content.Content_> content, int position) {
                                    caption = false;
                                    if (PreferenceManager.getString(getContext(), Constants.Key.ApiToken) != null) {
                                        listWatchLater(PreferenceManager.getString(getContext(), Constants.Key.ApiToken));
                                    }
                                    trailer_url = content.get(position).getOptions().get(0).getUrl();
                                    sub_url = content.get(position).getOptions().get(0).getSubtitle().getUrl();

                                    if (PreferenceManager.getString(getContext(), Constants.Key.ApiToken) != null) {
                                        listWatchLater(PreferenceManager.getString(getContext(), Constants.Key.ApiToken));
                                    }
                                    if (trailer_url == null) {
                                        ToastMessage.getInstance(getContext()).showErrorShortCustomToast("Video is not Available!!");
                                    } else {
                                        //  poster_frame.setVisibility(View.GONE);
                                        play_rl.setVisibility(View.GONE);
                                        main_media_frame.setVisibility(View.VISIBLE);
                                        initializePlayer();

                                        initFullscreenDialog();
                                        initFullscreenButton();

                                        if (sub_url == null) {
                                            sub_url = "";
                                            caption = false;
                                        } else {
                                            caption = true;
                                        }
                                        updateDataSeries(content, position);

                                        buildMediaSource(Uri.parse(trailer_url), Uri.parse(sub_url));
                                    }
                                }
                            });
                            rv_episodes.setAdapter(adapter);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });


                } else {
                    sp_seasons.setVisibility(View.GONE);
                    ic_dropdown.setVisibility(View.GONE);
                    EpisodesAdapter adapter = new EpisodesAdapter(getContext(), response.getData(), response.getData().getRecommended(),
                            response.getData().getPoster(), new EpisodeClickListener() {
                        @Override
                        public void onMovieItemClick(MovieDetailModel.Data.Recommended value) {
                            caption = false;
                            setId(value.getId());
                            if (PreferenceManager.getString(getContext(), Constants.Key.ApiToken) != null) {
                                listWatchLater(PreferenceManager.getString(getContext(), Constants.Key.ApiToken));
                            }
                            trailer_url = value.getTrailer();
                            sub_url = "";
                            //  poster_frame.setVisibility(View.GONE);
                            play_rl.setVisibility(View.GONE);
                            main_media_frame.setVisibility(View.VISIBLE);

                            if (PreferenceManager.getString(getContext(), Constants.Key.ApiToken) != null) {
                                listWatchLater(PreferenceManager.getString(getContext(), Constants.Key.ApiToken));
                            }

                            if (trailer_url == null) {
                                ToastMessage.getInstance(getContext()).showErrorShortCustomToast("Video is not Available!!");
                            } else {
                                initializePlayer();
                                initFullscreenDialog();
                                initFullscreenButton();

                                if (sub_url == null) {
                                    sub_url = "";
                                    caption = false;
                                } else {
                                    caption = true;
                                }
                                updateDataMovie(value);
                                if (trailer_url != null)
                                    buildMediaSource(Uri.parse(trailer_url), Uri.parse(sub_url));
                            }
                        }

                        @Override
                        public void onSeriesItemClick(List<MovieDetailModel.Data.Content.Content_> content, int position) {

                        }
                    });
                    rv_episodes.setAdapter(adapter);
                }
            }
        });
    }


    private void seasonVideo(int position) {


    }

    private void updateDataSeries(List<MovieDetailModel.Data.Content.Content_> content, int position) {
        title.setText(content.get(position).getTitle());
        title_controller.setText("" + content.get(position).getTitle());
        description_text.setText(content.get(position).getDescription());
    }

    private void updateDataMovie(MovieDetailModel.Data.Recommended data) {
        title.setText(data.getTitle());
        title_controller.setText("" + data.getTitle());
        pgRating_tv.setText(data.getPgRating());
        language_tv.setText(data.getPgRating());
        trailer_url = data.getTrailer();
        // genre_tv.setText(data.getGenre());
        //language_tv.setText(response.getData().getl);
        duration_tv.setText(data.getDuration());
        String duration = data.getReleased();
        String[] year = duration.split("-");
        year_tv.setText(year[0]);
        director_tv.setText(data.getDirector());
        description_text.setText(data.getDescription());
        description_text.setMaxLines(2);
        description = false;
        show.setText("Read More");
        video_details.setVisibility(View.GONE);
        tv_rating.setRating(Float.parseFloat("" + data.getRating()));
//        String cast = data.getCast().replaceAll("/", "\n");
//        cast_tv.setText(cast);
    }

    private void getTrendingDetails() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<HomeModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getTrendingData("customer/entertainment/trending");
        Log.e("sdasdsadsads", "itemSearch: " + call.request().url());
        serviceWrapper.HandleResponse(call, new ResponseHandler<HomeModel>(getContext()) {
            @Override
            public void onResponse(HomeModel response) {
                Log.e("ssssss", "onResponse: " + response.getData().getTrendingNow());
                if (!response.getData().getTrendingNow().isEmpty()) {
                    try {
                        if (response.getData().getTrendingNow().size() > 0) {

                            setTrendingDetails(response.getData().getTrendingNow());

                        } else {
                            ToastMessage.getInstance(getContext())
                                    .showErrorShortCustomToast("No Results found for your Search!!");
                        }
                    } catch (Exception e) {
                        Log.e("ssssss", "onResponse: " + e);
                        SnackbarManager.sException(getContext(), "" + e);
                    }
                } else {
                    SnackbarManager.sException(getContext(), "Data Not Found!");
                }
            }
        });
    }

    private void setTrendingDetails(List<HomeModel.TrendingNow> trendingNow) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        rv_home_trending.setLayoutManager(manager);
        HomeTrendingAdapter adapter = new HomeTrendingAdapter(getContext(), trendingNow, new ItemClickListener() {
            @Override
            public void onItemClick(int videoId) {
                play_rl.setVisibility(View.VISIBLE);
                main_media_frame.setVisibility(View.GONE);
                pausePlayer();
                getDetails(videoId);
                setId(videoId);
            }
        });
        rv_home_trending.setAdapter(adapter);
    }

    private void shareVideoLink(int id) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "AmVirgin");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "http://amvirgin.zobofy.com/video/" + id + "\n\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "service one"));
        } catch (Exception e) {
            Log.e("share", "shareVideoLink: " + e);
        }
    }

    private void shareLinkWhatsapp(int id) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "AmVirgin");
            String shareMessage = "\nLet me recommend you this application\n\n";
            shareMessage = shareMessage + "http://amvirgin-app.zobofy.com/video/" + id + "\n\n";
            shareIntent.setPackage("com.whatsapp");
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "service one"));
        } catch (Exception e) {
            Log.e("", "shareLinkWhatsapp: " + e);
        }
    }

    private void addWatchLater(String token, final int id) {
        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
            Call<WatchLaterModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                    .addWatchLater("Bearer " + token, id);
            serviceWrapper.HandleResponse(call, new ResponseHandler<WatchLaterModel>(getContext()) {
                @Override
                public void onResponse(WatchLaterModel response) {
                    if (response.getStatus() == 200) {
                        try {
                            if (response.getMessage().equalsIgnoreCase("Successfully added in list")) {
                                watch_later.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_playlist_add_check_red_24dp, 0, 0);
                            } else {
                                removeWatchList(id, PreferenceManager.getString(getContext(), Constants.Key.ApiToken));
                            }

                        } catch (Exception e) {
                            SnackbarManager.sException(getContext(), response.getMessage());
                        }
                    } else {
                        SnackbarManager.sException(getContext(), response.getMessage());
                    }
                }
            });
        } else {
            SnackbarManager.sException(getContext(), "Please login first!");
        }

    }

    private void listWatchLater(String token) {
        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            Log.e("dfdsfdsfds", "listWatchLater: " + token);
            ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
            Call<ListWatchLaterModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                    .listWatchLater("Bearer " + token);
            serviceWrapper.HandleResponse(call, new ResponseHandler<ListWatchLaterModel>(getContext()) {
                @Override
                public void onResponse(ListWatchLaterModel response) {
                    boolean m = false;
                    if (response.getStatus() == 200) {
                        boolean check = false;
                        try {
                            if (response.getData().size() > 0) {

                                for (int i = 0; i < response.getData().size(); i++) {
                                    if (id == response.getData().get(i).getVideoId()) {
                                        check = true;
                                    }
                                }
                                if (check) {
                                    watch_later.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_playlist_add_check_red_24dp, 0, 0);
                                } else {
                                    watch_later.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_playlist_add, 0, 0);
                                }
                            }
                        } catch (Exception e) {
                            SnackbarManager.sException(getContext(), response.getMessage());
                        }
                        isInList = m;
                        Log.e("vvvvvvv", "22222222 " + isInList);
                    } else {
                        SnackbarManager.sException(getContext(), response.getMessage());
                    }
                }
            });
        }
    }

    private void removeWatchList(int id, String token) {
        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
            Call<WatchLaterModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                    .removeWatchLater("Bearer " + token, "customer/watch-later/remove/" + id);
            serviceWrapper.HandleResponse(call, new ResponseHandler<WatchLaterModel>(getContext()) {
                @Override
                public void onResponse(WatchLaterModel response) {
                    if (response.getStatus() == 200) {
                        try {
//                            watch_later.setImageDrawable(getResources().getDrawable(R.drawable.ic_playlist_add));
                            watch_later.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_playlist_add, 0, 0);
                        } catch (Exception e) {
                            SnackbarManager.sException(getContext(), response.getMessage());
                        }
                    } else {
                        SnackbarManager.sException(getContext(), response.getMessage());
                    }
                }
            });
        }
    }


    @Override
    public void onPauseFragment() {
        Log.e(TAG, "onPauseFragment: ");
    }

    @Override
    public void onResumeFragment() {
        if (Util.SDK_INT <= 23) {
            Log.e(TAG, "onResumeFragment: zxczhczxhcjxhzcj");
            if (playerView != null) {
                Log.e(TAG, "onResumeFragment: zxczhczxhcjxhzcj");
                playerView.onPause();
                player.setPlayWhenReady(false);
                player.getPlaybackState();
            }
            releasePlayer();
        }
    }
}
