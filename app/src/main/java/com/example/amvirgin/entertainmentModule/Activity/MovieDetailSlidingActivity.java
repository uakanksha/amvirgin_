package com.example.amvirgin.entertainmentModule.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.Fragment.MovieDetailFragment;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.MovieDetailSlidingAdapter;
import com.example.amvirgin.classes.JustAdded;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SectionCollectionModel;
import com.example.amvirgin.interfaces.FragmentLifecycle;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.util.List;

public class MovieDetailSlidingActivity extends AppCompatActivity {
    private ViewPager pager;
    List<JustAdded> list;
    List<HomeModel.Item> items;
    List<SectionCollectionModel.Datum> data;
//    String position, type;
    int id, size, position;
    FrameLayout video_container;
    boolean sharing_link_check = false;
    Fragment mFragment = null;
    MovieDetailSlidingAdapter adapter;

    private static final String TAG = "MovieDetailSlidingActiv";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail_sliding);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        video_container = findViewById(R.id.video_container);
        pager = findViewById(R.id.pager);

        pager.setVisibility(View.GONE);
        video_container.setVisibility(View.VISIBLE);

        if(PreferenceManager.getBoolean(this, Constants.share_link_test)){
            mFragment = new MovieDetailFragment(PreferenceManager.getInt(this, Constants.sharing_id));
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.video_container, mFragment, "single view");
            fragmentTransaction.commit();

        }else {
            pager.setVisibility(View.VISIBLE);
            video_container.setVisibility(View.GONE);
            id = bundle.getInt("id");
            size = bundle.getInt("size");
            position = bundle.getInt("position");
            items = (List<HomeModel.Item>) bundle.getSerializable("items");
            data = (List<SectionCollectionModel.Datum>) bundle.getSerializable("items_collection");
            adapter = new MovieDetailSlidingAdapter(getSupportFragmentManager(),
                    getApplicationContext(), size, id, items, data);
            pager.setAdapter(adapter);
            pager.setCurrentItem(position);

        }
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                FragmentLifecycle fragmentToShow = (FragmentLifecycle)adapter.getItem(position);
                fragmentToShow.onResumeFragment();
                Log.e(TAG, "onPageScrolled: ");
            }
            @Override
            public void onPageSelected(int position) {
                Log.e(TAG, "onPageSelected: ");
                FragmentLifecycle fragmentToShow = (FragmentLifecycle)adapter.getItem(position);
                fragmentToShow.onPauseFragment();
            }
            @Override
            public void onPageScrollStateChanged(int state) {
                Log.e(TAG, "onPageScrollStateChanged: ");
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        PreferenceManager.saveBoolean(this, Constants.share_link_test, false);
        PreferenceManager.saveInt(this, Constants.sharing_id, 0);
    }
    private void hideSystemUI() {

        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            showSystemUI();
        } else {
            hideSystemUI();
        }
        getWindow().getDecorView().setSystemUiVisibility(uiOptions);
    }
}
