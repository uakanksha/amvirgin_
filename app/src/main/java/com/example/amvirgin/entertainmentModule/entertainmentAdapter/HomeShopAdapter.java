package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;

import java.util.List;

public class HomeShopAdapter extends RecyclerView.Adapter<HomeShopAdapter.HomeShopViewHolder> {

    Context context;
    List<HomeModel.Product> products;

    public HomeShopAdapter(Context context, List<HomeModel.Product> products) {
        this.context = context;
        this.products = products;
    }

    @NonNull
    @Override
    public HomeShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_layout, parent, false);
        HomeShopViewHolder holder = new HomeShopViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeShopViewHolder holder, int position) {
        holder.name.setText(products.get(position).getName());
        holder.type.setText(String.valueOf(products.get(position).getPrice()));
        if (products.get(position).getImages().size() > 0) {
            Glide.with(context).load(products.get(position).getImages().get(0)).into(holder.banner_img);
        }
        else {
            holder.banner_img.setImageResource(R.drawable.placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class HomeShopViewHolder extends RecyclerView.ViewHolder{
        ImageView banner_img;
        TextView name, type;

        public HomeShopViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
        }
    }
}
