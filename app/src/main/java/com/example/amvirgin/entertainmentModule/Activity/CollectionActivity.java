package com.example.amvirgin.entertainmentModule.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.entertainmentModule.entertainmentAdapter.Collectionadapter;
import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SectionCollectionModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;

import java.util.List;

import retrofit2.Call;

public class CollectionActivity extends AppCompatActivity {

    List<SectionCollectionModel.Datum> data;

    RecyclerView rv_popular_movies;
    private TextView tv_header, tv_no_items;
    private ImageView backbtn;
    int section_id;
    String section_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        tv_header = findViewById(R.id.tv_header);
        tv_no_items = findViewById(R.id.tv_no_items);

        Bundle extras = getIntent().getExtras();


        if (extras != null){
            section_title = extras.getString("section_title");
            section_id = extras.getInt("section_id");
            tv_header.setText(section_title);

            if (extras.getString("back").equalsIgnoreCase("search")){
                getSearchList(section_title);
            }
            else {
                getCollectionList(section_id);
            }
        }

        rv_popular_movies = findViewById(R.id.rv_popular_movies);
        backbtn = findViewById(R.id.backbtn);



        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
            }
        });
    }

    private void getSearchList(String section_title) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<SectionCollectionModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getVideoSearch("customer/search?type=entertainment&categoryId=&key="+section_title);
        serviceWrapper.HandleResponse(call, new ResponseHandler<SectionCollectionModel>(CollectionActivity.this) {
            @Override
            public void onResponse(SectionCollectionModel response) {
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0){
                            data = response.getData();

                            GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 3);
                            rv_popular_movies.setLayoutManager(manager);

                            Collectionadapter adapter = new Collectionadapter(getApplicationContext(), data);
                            rv_popular_movies.setAdapter(adapter);
                        }
                        else {
                            tv_no_items.setVisibility(View.VISIBLE );
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(CollectionActivity.this, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(CollectionActivity.this, response.getMessage());
                }
            }
        });
    }

    private void getCollectionList(int section_id) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<SectionCollectionModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getSectionCollectionList("customer/entertainment/section/"+section_id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<SectionCollectionModel>(CollectionActivity.this) {
            @Override
            public void onResponse(SectionCollectionModel response) {
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0){
                            data = response.getData();

                            GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 3);
                            rv_popular_movies.setLayoutManager(manager);

                            Collectionadapter adapter = new Collectionadapter(getApplicationContext(), data);
                            rv_popular_movies.setAdapter(adapter);
                        }
                        else {
                            tv_no_items.setVisibility(View.VISIBLE );
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(CollectionActivity.this, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(CollectionActivity.this, response.getMessage());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
