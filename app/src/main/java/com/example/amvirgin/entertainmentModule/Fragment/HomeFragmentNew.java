package com.example.amvirgin.entertainmentModule.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.amvirgin.R;
import com.example.amvirgin.classes.MovieDetailModel;
import com.example.amvirgin.entertainmentModule.Activity.SearchActivity;
import com.example.amvirgin.entertainmentModule.Activity.UserFabVideoActivity;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.HomeBannerSliderAdapter;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.HomeSectionsAdapter;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.HomeShopAdapter;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.HomeTrendingAdapter;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.interfaces.EpisodeClickListener;
import com.example.amvirgin.interfaces.ItemClickListener;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.CartProductShopActivity;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.LinePagerIndicatorDecoration;
import com.example.amvirgin.utils.PreferenceManager;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.example.amvirgin.R.layout.spinner_item_overview;

public class HomeFragmentNew extends Fragment {

    private RecyclerView rv_home_sliders, rv_home_sections, rv_home_shop_products, rv_home_trending;
    ShimmerFrameLayout shimmer_container;
    private ImageView search, iv_serach, watch_later;
    private Toolbar toolbar, toolbar_search;
    private Spinner language_filter;
    private ArrayAdapter<CharSequence> languageAdapter;
    public HomeFragmentNew() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.home_fragment_new, container, false);

        rv_home_sliders = view.findViewById(R.id.rv_home_sliders);
        rv_home_sections = view.findViewById(R.id.rv_home_sections);
        rv_home_shop_products = view.findViewById(R.id.rv_home_shop_products);
        rv_home_trending = view.findViewById(R.id.rv_home_trending);
        watch_later = view.findViewById(R.id.watch_later);
        language_filter = view.findViewById(R.id.language_filter);


        toolbar = view.findViewById(R.id.toolbar);
        toolbar_search = view.findViewById(R.id.toolbar_search);

        shimmer_container = view.findViewById(R.id.shimmer_container);


        search = view.findViewById(R.id.search);
        iv_serach = view.findViewById(R.id.iv_search);

        String[] language_code = {"en", "hi", "zh"};
        String[] language = {"English", "Hindi", "chinese"};

        languageAdapter = new ArrayAdapter<CharSequence>(getContext(), spinner_item_overview, language);
        languageAdapter.setDropDownViewResource(R.layout.language_layout);
        language_filter.setAdapter(languageAdapter);
        language_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView adapter, View v, int i, long lng) {
                //SnackbarManager.successResponse(getContext(), ""+language[i]);
                //String value =  adapter.getItemAtPosition(i).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        iv_serach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar.setVisibility(View.VISIBLE);
                toolbar_search.setVisibility(View.GONE);
            }
        });

        watch_later.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)){
                    Intent intent = new Intent(getContext(), UserFabVideoActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }else {
                    SnackbarManager.sException(getContext(), "Please Login to view watch later list");
                }

            }
        });

        getAllData("");

        return view;
    }

    private void getAllData(String language_code) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<HomeModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getHomeData();
        serviceWrapper.HandleResponse(call, new ResponseHandler<HomeModel>(getContext()) {
            @Override
            public void onResponse(HomeModel response) {
                if (response.getStatus() == 200) {
                    try {
                        HomeModel.Data data = response.getData();
                        if (data != null) {
                            if (data.getSliders().size() > 0) {
                                setSliderAdapter(data.getSliders());
                            }
                            if (data.getSections().size() > 0) {
                                setSectionsAdapter(data.getSections());
                            }
                            if (data.getProducts().size() > 0) {
                                setProductsAdapter(data.getProducts());
                            }
                            if (data.getTrendingNow().size() > 0) {
                                setTrendingAdapter(data.getTrendingNow());
                            }
                        } else {

                        }
                    } catch (Exception e) {
                        SnackbarManager.sException(getContext(), response.getMessage());
                    }
                } else {
                    SnackbarManager.sException(getContext(), response.getMessage());
                }
            }
        });

    }

    private void setSliderAdapter(List<HomeModel.Slider> sliders) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        rv_home_sliders.setLayoutManager(manager);

        final HomeBannerSliderAdapter adapter = new HomeBannerSliderAdapter(getContext(), sliders);
        rv_home_sliders.setAdapter(adapter);

        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(rv_home_sliders);
        rv_home_sliders.addItemDecoration(new LinePagerIndicatorDecoration());
        Handler handler = new Handler();
        final Handler handler2 = handler;
        final int timer = 3000;
        if (sliders.size() > 1) {
            handler.postDelayed(new Runnable() {
                int count = 0;
                boolean flag = true;

                @Override
                public void run() {
                    if (count < adapter.getItemCount()) {
                        if (count == adapter.getItemCount() - 1) {
                            flag = false;
                        } else if (count == 0) {
                            flag = true;
                        }
                        if (flag) {
                            count++;
                        } else {
                            count--;
                        }
                        rv_home_sliders.smoothScrollToPosition(count);
                        handler2.postDelayed(this, timer);
                    }
                }
            }, timer);
        }

    }

    private void setSectionsAdapter(List<HomeModel.Section> sections) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv_home_sections.setLayoutManager(manager);

        HomeSectionsAdapter adapter = new HomeSectionsAdapter(getContext(), sections);
        rv_home_sections.setAdapter(adapter);
    }

    private void setProductsAdapter(List<HomeModel.Product> products) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        rv_home_shop_products.setLayoutManager(manager);

        HomeShopAdapter adapter = new HomeShopAdapter(getContext(), products);
        rv_home_shop_products.setAdapter(adapter);
    }

    private void setTrendingAdapter(List<HomeModel.TrendingNow> trendingNow) {
        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        rv_home_trending.setLayoutManager(manager);

        HomeTrendingAdapter adapter = new HomeTrendingAdapter(getContext(), trendingNow, new ItemClickListener() {
            @Override
            public void onItemClick(int videoId) {
            }
        });
        rv_home_trending.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        PreferenceManager.saveBoolean(getContext(), Constants.checkFragment, false);
    }
}
