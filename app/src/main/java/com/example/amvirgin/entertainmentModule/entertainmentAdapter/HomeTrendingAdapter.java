package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.Activity.MovieDetailSlidingActivity;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.interfaces.EpisodeClickListener;
import com.example.amvirgin.interfaces.ItemClickListener;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HomeTrendingAdapter extends RecyclerView.Adapter<HomeTrendingAdapter.HomeTrendingViewHolder> {

    Context context;
    List<HomeModel.TrendingNow> trendingNow;
    ItemClickListener itemClickListener;

    public HomeTrendingAdapter(Context context, List<HomeModel.TrendingNow> trendingNow, ItemClickListener itemClickListener) {
        this.context = context;
        this.trendingNow = trendingNow;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public HomeTrendingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_layout, parent, false);
        HomeTrendingViewHolder holder = new HomeTrendingViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeTrendingViewHolder holder, final int position) {
        if (trendingNow.get(position).getPoster()!=null)
        if (!trendingNow.get(position).getPoster().equalsIgnoreCase("")){
            Glide.with(context).load(trendingNow.get(position).getPoster()).into(holder.trendingbanner_img);
        }
        else {
            holder.trendingbanner_img.setImageResource(R.drawable.placeholder);
        }
        holder.trendingbanner_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceManager.getBoolean(context, Constants.checkFragment)){
                    itemClickListener.onItemClick(trendingNow.get(position).getId());
                }else {
                    Intent intent = new Intent(context, MovieDetailSlidingActivity.class);
                    intent.putExtra("items", (Serializable) getTrendingData(trendingNow));
                    intent.putExtra("id", trendingNow.get(position).getId());
                    intent.putExtra("size", trendingNow.size());
                    intent.putExtra("position", position);
                    context.startActivity(intent);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return trendingNow.size();
    }

    public class HomeTrendingViewHolder extends RecyclerView.ViewHolder{
        ImageView trendingbanner_img;

        public HomeTrendingViewHolder(@NonNull View itemView) {
            super(itemView);
            trendingbanner_img = itemView.findViewById(R.id.trendingbanner_img);
        }
    }

    private List<HomeModel.Item> getTrendingData(List<HomeModel.TrendingNow> trendingNow){
        List<HomeModel.Item> items = new ArrayList<>();
        for (int i=0; i<trendingNow.size(); i++){
            items.add(new HomeModel.Item(
                    trendingNow.get(i).getId(),
                    trendingNow.get(i).getTitle(),
                    trendingNow.get(i).getDescription(),
                    trendingNow.get(i).getPoster(),
                    trendingNow.get(i).getType()
            ));
        }
        return items;
    }
}
