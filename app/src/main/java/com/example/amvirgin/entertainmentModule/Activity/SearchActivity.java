package com.example.amvirgin.entertainmentModule.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentAdapter.Collectionadapter;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SectionCollectionModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;

import retrofit2.Call;

public class SearchActivity extends AppCompatActivity {
    private EditText et_search;
    private ImageView iv_search, iv_back;
    private String search_key;
    InputMethodManager imm;
    private RecyclerView recycle_search;

    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        et_search = findViewById(R.id.et_search);
        iv_search = findViewById(R.id.iv_search);
        iv_back = findViewById(R.id.iv_back);
        recycle_search= findViewById(R.id.recycle_seacrh);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                onBackPressed();
            }
        });

         imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        et_search.requestFocus();
        if (et_search.requestFocus()){
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()>=2){
                    getSearchList(s.toString());
                }
            }
        });

//        iv_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                search_key = et_search.getText().toString().trim();
//                imm.toggleSoftInput(InputMethodManager. HIDE_IMPLICIT_ONLY, InputMethodManager.HIDE_NOT_ALWAYS);
//                if (search_key.length()>=2){
//                    getSearchList(search_key);
//                }
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getSearchList(String key) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<SectionCollectionModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getVideoSearch("customer/search?type=entertainment&category=&keyword="+key);
        serviceWrapper.HandleResponse(call, new ResponseHandler<SectionCollectionModel>(SearchActivity.this) {
            @Override
            public void onResponse(SectionCollectionModel response) {
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0){
                            GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 3);
                            recycle_search.setLayoutManager(manager);
                            Collectionadapter adapter = new Collectionadapter(getApplicationContext(), response.getData());
                            recycle_search.setAdapter(adapter);
                        }else {
                            SnackbarManager.successResponse(SearchActivity.this, response.getMessage());
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(SearchActivity.this, response.getMessage());
                    }
                } else {
                    SnackbarManager.sException(SearchActivity.this, response.getMessage());
                }
            }
        });
    }
}
