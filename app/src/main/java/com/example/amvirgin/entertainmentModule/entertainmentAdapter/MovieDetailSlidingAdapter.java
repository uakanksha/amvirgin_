package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.amvirgin.entertainmentModule.Fragment.MovieDetailFragment;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SectionCollectionModel;

import java.util.List;

public class MovieDetailSlidingAdapter extends FragmentPagerAdapter {

    Context context;
    String pos;
    String type;
    Integer id, size;
    List<HomeModel.Item> items;
    List<SectionCollectionModel.Datum> data;
   // List<> searchData;
    public MovieDetailSlidingAdapter(FragmentManager fm, Context context, int size, Integer id,
                                     List<HomeModel.Item> items, List<SectionCollectionModel.Datum> data) {
        super(fm);
        this.context = context;
        this.size = size;
        this.id = id;

        this.items = items;
        this.data = data;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
/*        if (type.equalsIgnoreCase("justadded")){
            return new MovieDetailFragment(AppLocal.justAdded.get(position).getSlug());
        }
        else  if (type.equalsIgnoreCase("trending")){
            return new MovieDetailFragment(AppLocal.trendingPicks.get(position).getSlug());
        }
        else  if (type.equalsIgnoreCase("toppicks")){
            return new MovieDetailFragment(AppLocal.topPicks.get(position).getSlug());
        }
        else {
            return null;
        }*/

         if (data != null){
            // Log.e("ZZZZZZZZZZZZZ", "wwwwww: "+data.get(position).getId());
             if (data.get(position).getId()!=null){
                 return new MovieDetailFragment(data.get(position).getId());
             }else {
                 return new MovieDetailFragment(data.get(position).getKey());
             }
         }
         else if (items != null){
             Log.e("ZZZZZZZZZZZZZ", "wwwwwwwwww: "+items.get(position).getId() );
             return new MovieDetailFragment(items.get(position).getId());
         }
         else {
             return null;
         }

    }

    @Override
    public int getCount() {

  return size;
    }
}
