package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.entertainmentModule.Activity.MovieDetailSlidingActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;

import java.io.Serializable;
import java.util.List;

public class HomeSectionDataAdapter extends RecyclerView.Adapter<HomeSectionDataAdapter.HomeSectionDataViewHolder> {

    Context context;
    List<HomeModel.Item> items;
    Integer id;

    public HomeSectionDataAdapter(Context context, List<HomeModel.Item> items, Integer id) {
        this.context = context;
        this.items = items;
        this.id = id;
    }

    @NonNull
    @Override
    public HomeSectionDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_display_layout, parent, false);
        HomeSectionDataViewHolder holder = new HomeSectionDataViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeSectionDataViewHolder holder, final int position) {
            holder.name.setText(items.get(position).getTitle());
            holder.type.setText(items.get(position).getType());
            if(items.get(position).getPoster()!=null)
            if (!items.get(position).getPoster().equalsIgnoreCase("")) {
                Glide.with(context).load(items.get(position).getPoster()).into(holder.banner_img);
            } else {
                holder.banner_img.setImageResource(R.drawable.placeholder);
            }
            holder.banner_container_top.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MovieDetailSlidingActivity.class);

//                    Bundle bundle = new Bundle();
//                    bundle.putParcelable("items", (Parcelable) items);
//                    intent.putExtras(bundle);

                    intent.putExtra("items", (Serializable) items);
                    intent.putExtra("id", items.get(position).getId());
                    intent.putExtra("size", items.size());
                    intent.putExtra("position", position);
                    context.startActivity(intent);
                }
            });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class HomeSectionDataViewHolder extends RecyclerView.ViewHolder{
        ImageView banner_img;
        TextView name, type;
        RelativeLayout banner_container_top;

        public HomeSectionDataViewHolder(@NonNull View itemView) {
            super(itemView);
            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            banner_container_top = itemView.findViewById(R.id.banner_container_top);
        }
    }
}
