package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.Activity.CollectionActivity;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;

import java.util.Collection;
import java.util.List;

public class HomeSectionsAdapter extends RecyclerView.Adapter<HomeSectionsAdapter.HomeBannerSectionsViewHolder> {

    Context context;
    List<HomeModel.Section> sections;

    public HomeSectionsAdapter(Context context, List<HomeModel.Section> sections) {
        this.context = context;
        this.sections = sections;
    }

    @NonNull
    @Override
    public HomeBannerSectionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_home_sections_layout, parent, false);
        HomeBannerSectionsViewHolder holder = new HomeBannerSectionsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeBannerSectionsViewHolder holder, final int position) {


        if (sections.get(position).getItems().size() > 0) {

            holder.tv_sectionTitle.setText(sections.get(position).getTitle());

            LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false);
            holder.rv_section_data.setLayoutManager(manager);

            HomeSectionDataAdapter adapter = new HomeSectionDataAdapter(context, sections.get(position).getItems()
            , sections.get(position).getId());
            holder.rv_section_data.setAdapter(adapter);
        }
        else if (sections.get(position).getItems().size() == 0){
            holder.tv_sectionTitle.setText(sections.get(position).getTitle());
            holder.tv_no_items.setVisibility(View.VISIBLE);
        }

        holder.btn_section_seeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CollectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("section_id", sections.get(position).getId());
                intent.putExtra("section_title", sections.get(position).getTitle());

                intent.putExtra("back", "section");
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return sections.size();
    }

    public class HomeBannerSectionsViewHolder extends RecyclerView.ViewHolder{
        TextView tv_sectionTitle, tv_no_items;
        Button btn_section_seeAll;
        RecyclerView rv_section_data;
        LinearLayout main_section_ll;

        public HomeBannerSectionsViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_sectionTitle = itemView.findViewById(R.id.tv_sectionTitle);
            btn_section_seeAll = itemView.findViewById(R.id.btn_section_seeAll);
            rv_section_data = itemView.findViewById(R.id.rv_section_data);
            main_section_ll = itemView.findViewById(R.id.main_section_ll);
            tv_no_items = itemView.findViewById(R.id.tv_no_items);
        }
    }
}
