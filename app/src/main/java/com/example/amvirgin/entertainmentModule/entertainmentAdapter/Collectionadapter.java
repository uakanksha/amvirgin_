package com.example.amvirgin.entertainmentModule.entertainmentAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.entertainmentModule.Activity.MovieDetailSlidingActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SectionCollectionModel;

import java.io.Serializable;
import java.util.List;

public class Collectionadapter extends RecyclerView.Adapter<Collectionadapter.BannerViewHolder> {

    Context context;
    List<SectionCollectionModel.Datum> data;

    public Collectionadapter(Context context, List<SectionCollectionModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public BannerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_grid_layout, parent, false);
            BannerViewHolder cvh = new BannerViewHolder(view);
            return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull BannerViewHolder holder, final int position) {

        if (data.get(position).getPoster()!=null&&!data.get(position).getPoster().equalsIgnoreCase("")) {
            Glide.with(context).load(data.get(position).getPoster())
                .into(holder.banner_img);
        }
        else {
            holder.banner_img.setImageResource(R.drawable.placeholder);
        }
        holder.name.setText(data.get(position).getTitle());
        holder.type.setText(data.get(position).getType());

        holder.banner_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, MovieDetailSlidingActivity.class);
                intent.putExtra("items_collection", (Serializable) data);
                intent.putExtra("id", data.get(position).getKey());
                intent.putExtra("size", data.size());
                intent.putExtra("position", position);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class BannerViewHolder extends RecyclerView.ViewHolder{

        ImageView banner_img;
        TextView name, type;
        RelativeLayout banner_container_top;

        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            banner_container_top = itemView.findViewById(R.id.banner_container_top);

        }
    }

}
