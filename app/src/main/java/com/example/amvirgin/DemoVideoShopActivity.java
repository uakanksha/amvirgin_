package com.example.amvirgin;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.VideoView;

public class DemoVideoShopActivity extends AppCompatActivity implements View.OnClickListener {
    private MediaController mediaController;
    private VideoView videoView;
    private Button btn_buyNow;
    private RelativeLayout back;
    private ProgressBar progressBar;
    private String demoVideoURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_video_shop);

        if (getIntent().getExtras() != null){
            demoVideoURL = getIntent().getExtras().getString("demoVideoURL");
        }

        btn_buyNow=(Button)findViewById(R.id.btn_buyNow);
        back=(RelativeLayout) findViewById(R.id.rlBack);
        progressBar=findViewById(R.id.progressbarVideo);

        mediaController=new MediaController(this);
        videoView= (VideoView)findViewById(R.id.videoViewRelative);

//        Toast.makeText(getApplicationContext(),"test",Toast.LENGTH_LONG).show();

        btn_buyNow.setOnClickListener(this);
        back.setOnClickListener(this);
        playVideo();
    }
    public void playVideo(){
//        Uri uri = Uri.parse("http://videocdn.bodybuilding.com/video/mp4/62000/62792m.mp4");
        Uri uri = Uri.parse(demoVideoURL);
        videoView.setVideoURI(uri);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
//        videoView.requestFocus();
        videoView.start();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;

            case R.id.btn_buyNow:
                break;
        }

    }
}
