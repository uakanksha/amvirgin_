package com.example.amvirgin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.entertainmentModule.Activity.MovieDetailSlidingActivity;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.CartProductShopActivity;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.shopAdapter.ShopCartListAdapter;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.CountryListModel;
import com.example.amvirgin.shopModule.shopModel.RetrieveCartModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.SessionId;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static java.security.AccessController.getContext;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN_TIME_OUT=2000;
    Context mContext;
    SharedPreferences.Editor editor;
    String session_id;
    int customerId;
    public static ArrayList<Integer> cart_products = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handleIntent(getIntent());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onStart() {
        super.onStart();
        session_id = PreferenceManager.getString(getApplicationContext(), "session_id");
        PreferenceManager.delete(SplashActivity.this, "cart_count");

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            customerId = PreferenceManager.getUser(getApplicationContext()).getKey();
        }



        if (session_id.isEmpty()) {
            initializeSession();
        } else {
            retrieveCart(session_id);
        }
        getCountryList();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getApplicationContext(), HomeActivity.class);

//                Intent i=new Intent(getApplicationContext(), HomeActivity.class);
                //Intent is used to switch from one activity to another.

                startActivity(i);
                //invoke the SecondActivity.

                finish();
                //the current activity will get finished.
            }
        }, SPLASH_SCREEN_TIME_OUT);
        // ATTENTION: This was auto-generated to handle app links.
    }

    private void retrieveCart(final String session_id) {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());

        Call<AddToCartModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .retrieveCart("customer/cart/retrieve?sessionId="+session_id+"&customerId="+customerId);

        serviceWrapper.HandleResponse(call, new ResponseHandler<AddToCartModel>(this) {
            @Override
            public void onResponse(AddToCartModel response) {
                if (response.getStatus() == 200){

                        if (response.getMessage().equalsIgnoreCase("No cart was found for that session.")){

                        }

                        else if (response.getData().getCart().getItemCount() == 0){

                        }
                        else {
                            PreferenceManager.saveInt(SplashActivity.this, "cart_count", response.
                                    getData().getCart().getItemCount());

                            for (int i = 0; i < response.getData().getCart().getItems().size(); i++){
                                cart_products.add(response.getData().getCart().getItems().get(i).getKey());
                            }
                        }
//json
                }
                else {
//                    SnackbarManager.sException(CartProductShopActivity.this,response.getMessage());
                }
            }
        });
    }

    private void initializeSession() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<InitializeSession> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .initializeSession();
        serviceWrapper.HandleResponse(call, new ResponseHandler<InitializeSession>(SplashActivity.this) {
            @Override
            public void onResponse(InitializeSession response) {
                if (response.getStatus() == 200){

                        PreferenceManager.saveString(getApplicationContext(), "session_id", response.getSession());

                }
                else {
                    SnackbarManager.sException(SplashActivity.this,response.getMessage());
                }
            }
        });
    }

    private void getCountryList(){
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<CountryListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getCountryList();
        serviceWrapper.HandleResponse(call, new ResponseHandler<CountryListModel>(SplashActivity.this) {
            @Override
            public void onResponse(CountryListModel response) {
                if (response.getStatus() == 200){

                        Gson gson = new Gson();
                        String countryListData = gson.toJson(response.getData());
                        PreferenceManager.saveString(getApplicationContext(), "country_list", countryListData);

                }
                else {
                    SnackbarManager.sException(SplashActivity.this, response.getMessage());
                }
            }
        });
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String appLinkAction = intent.getAction();
        Uri appLinkData = intent.getData();
        if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null){
            String last_url = appLinkData.getLastPathSegment();
            Log.e("last_url", "handleIntent: "+last_url);
            Intent in = new Intent(SplashActivity.this, MovieDetailSlidingActivity.class);
            PreferenceManager.saveBoolean(this, Constants.share_link_test, true);
            assert last_url != null;
            PreferenceManager.saveInt(this, Constants.sharing_id, Integer.parseInt(last_url));
            startActivity(in);
            finish();
        }
    }
}
