package com.example.amvirgin.classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomepagetrendingData {
    @SerializedName("trendingPicks")
    @Expose
    private List<TrendingPick> trendingPicks = null;
    @SerializedName("justAdded")
    @Expose
    private List<JustAdded> justAdded = null;
    @SerializedName("topPicks")
    @Expose
    private List<TopPick> topPicks = null;

    public List<TrendingPick> getTrendingPicks() {
        return trendingPicks;
    }

    public void setTrendingPicks(List<TrendingPick> trendingPicks) {
        this.trendingPicks = trendingPicks;
    }

    public List<JustAdded> getJustAdded() {
        return justAdded;
    }

    public void setJustAdded(List<JustAdded> justAdded) {
        this.justAdded = justAdded;
    }

    public List<TopPick> getTopPicks() {
        return topPicks;
    }

    public void setTopPicks(List<TopPick> topPicks) {
        this.topPicks = topPicks;
    }
}
