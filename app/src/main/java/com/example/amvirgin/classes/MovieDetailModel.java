package com.example.amvirgin.classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MovieDetailModel  implements Serializable{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable{

        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("duration")
        @Expose
        private String duration;
        @SerializedName("released")
        @Expose
        private String released;
        @SerializedName("cast")
        @Expose
        private String cast;
        @SerializedName("director")
        @Expose
        private String director;
        @SerializedName("trailer")
        @Expose
        private String trailer;
        @SerializedName("poster")
        @Expose
        private String poster;
        @SerializedName("backdrop")
        @Expose
        private String backdrop;
        @SerializedName("genre")
        @Expose
        private String genre;
        @SerializedName("rating")
        @Expose
        private Double rating;
        @SerializedName("pgRating")
        @Expose
        private String pgRating;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("subscriptionType")
        @Expose
        private String subscriptionType;
        @SerializedName("hasSeasons")
        @Expose
        private Boolean hasSeasons;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("recommended")
        @Expose
        private List<Recommended> recommended = null;
        @SerializedName("content")
        @Expose
        private List<Content> content = null;

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getReleased() {
            return released;
        }

        public void setReleased(String released) {
            this.released = released;
        }

        public String getCast() {
            return cast;
        }

        public void setCast(String cast) {
            this.cast = cast;
        }

        public String getDirector() {
            return director;
        }

        public void setDirector(String director) {
            this.director = director;
        }

        public String getTrailer() {
            return trailer;
        }

        public void setTrailer(String trailer) {
            this.trailer = trailer;
        }

        public String getPoster() {
            return poster;
        }

        public void setPoster(String poster) {
            this.poster = poster;
        }

        public String getBackdrop() {
            return backdrop;
        }

        public void setBackdrop(String backdrop) {
            this.backdrop = backdrop;
        }

        public String getGenre() {
            return genre;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public Double getRating() {
            return rating;
        }

        public void setRating(Double rating) {
            this.rating = rating;
        }

        public String getPgRating() {
            return pgRating;
        }

        public void setPgRating(String pgRating) {
            this.pgRating = pgRating;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSubscriptionType() {
            return subscriptionType;
        }

        public void setSubscriptionType(String subscriptionType) {
            this.subscriptionType = subscriptionType;
        }

        public Boolean getHasSeasons() {
            return hasSeasons;
        }

        public void setHasSeasons(Boolean hasSeasons) {
            this.hasSeasons = hasSeasons;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public List<Recommended> getRecommended() {
            return recommended;
        }

        public void setRecommended(List<Recommended> recommended) {
            this.recommended = recommended;
        }

        public List<Content> getContent() {
            return content;
        }

        public void setContent(List<Content> content) {
            this.content = content;
        }
        public class Recommended implements Serializable{

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("slug")
            @Expose
            private String slug;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("duration")
            @Expose
            private String duration;
            @SerializedName("released")
            @Expose
            private String released;
            @SerializedName("director")
            @Expose
            private String director;
            @SerializedName("trailer")
            @Expose
            private String trailer;
            @SerializedName("rating")
            @Expose
            private Double rating;
            @SerializedName("poster")
            @Expose
            private String poster;
            @SerializedName("pgRating")
            @Expose
            private String pgRating;
            @SerializedName("subscriptionType")
            @Expose
            private String subscriptionType;
            @SerializedName("hasSeasons")
            @Expose
            private Boolean hasSeasons;
            @SerializedName("price")
            @Expose
            private Integer price;
            @SerializedName("description")
            @Expose
            private String description;

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSlug() {
                return slug;
            }

            public void setSlug(String slug) {
                this.slug = slug;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getDuration() {
                return duration;
            }

            public void setDuration(String duration) {
                this.duration = duration;
            }

            public String getReleased() {
                return released;
            }

            public void setReleased(String released) {
                this.released = released;
            }

            public String getDirector() {
                return director;
            }

            public void setDirector(String director) {
                this.director = director;
            }

            public String getTrailer() {
                return trailer;
            }

            public void setTrailer(String trailer) {
                this.trailer = trailer;
            }

            public Double getRating() {
                return rating;
            }

            public void setRating(Double rating) {
                this.rating = rating;
            }

            public String getPoster() {
                return poster;
            }

            public void setPoster(String poster) {
                this.poster = poster;
            }

            public String getPgRating() {
                return pgRating;
            }

            public void setPgRating(String pgRating) {
                this.pgRating = pgRating;
            }

            public String getSubscriptionType() {
                return subscriptionType;
            }

            public void setSubscriptionType(String subscriptionType) {
                this.subscriptionType = subscriptionType;
            }

            public Boolean getHasSeasons() {
                return hasSeasons;
            }

            public void setHasSeasons(Boolean hasSeasons) {
                this.hasSeasons = hasSeasons;
            }

            public Integer getPrice() {
                return price;
            }

            public void setPrice(Integer price) {
                this.price = price;
            }

        }
        public class Content implements Serializable {

            @SerializedName("season")
            @Expose
            private Integer season;
            @SerializedName("episodes")
            @Expose
            private Integer episodes;
            @SerializedName("content")
            @Expose
            private List<Content_> content = null;

            public Integer getSeason() {
                return season;
            }

            public void setSeason(Integer season) {
                this.season = season;
            }

            public Integer getEpisodes() {
                return episodes;
            }

            public void setEpisodes(Integer episodes) {
                this.episodes = episodes;
            }

            public List<Content_> getContent() {
                return content;
            }

            public void setContent(List<Content_> content) {
                this.content = content;
            }

            public class Content_ {

                @SerializedName("title")
                @Expose
                private String title;
                @SerializedName("description")
                @Expose
                private String description;
                @SerializedName("options")
                @Expose
                private List<Option> options = null;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public List<Option> getOptions() {
                    return options;
                }

                public void setOptions(List<Option> options) {
                    this.options = options;
                }

                public class Option {

                    @SerializedName("language")
                    @Expose
                    private String language;
                    @SerializedName("quality")
                    @Expose
                    private String quality;
                    @SerializedName("url")
                    @Expose
                    private String url;
                    @SerializedName("subtitle")
                    @Expose
                    private Subtitle subtitle;

                    public String getLanguage() {
                        return language;
                    }

                    public void setLanguage(String language) {
                        this.language = language;
                    }

                    public String getQuality() {
                        return quality;
                    }

                    public void setQuality(String quality) {
                        this.quality = quality;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public Subtitle getSubtitle() {
                        return subtitle;
                    }

                    public void setSubtitle(Subtitle subtitle) {
                        this.subtitle = subtitle;
                    }



                    public class Subtitle {

                        @SerializedName("available")
                        @Expose
                        private Boolean available;
                        @SerializedName("url")
                        @Expose
                        private String url;

                        public Boolean getAvailable() {
                            return available;
                        }

                        public void setAvailable(Boolean available) {
                            this.available = available;
                        }

                        public String getUrl() {
                            return url;
                        }

                        public void setUrl(String url) {
                            this.url = url;
                        }

                    }


                }


            }

        }

    }

}

