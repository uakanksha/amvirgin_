package com.example.amvirgin.classes;

public class Seasons {

    String season_name;

    public Seasons(String season_name) {
        this.season_name = season_name;
    }

    public String getSeason_name() {
        return season_name;
    }

    public void setSeason_name(String season_name) {
        this.season_name = season_name;
    }
}
