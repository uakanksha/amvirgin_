package com.example.amvirgin.classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopPick {

    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("released")
    @Expose
    private String released;
    @SerializedName("cast")
    @Expose
    private String cast;
    @SerializedName("director")
    @Expose
    private String director;
    @SerializedName("trailer")
    @Expose
    private String trailer;
    @SerializedName("poster")
    @Expose
    private String poster;
    @SerializedName("backdrop")
    @Expose
    private String backdrop;
    @SerializedName("genre")
    @Expose
    private String genre;
    @SerializedName("rating")
    @Expose
    private Double rating;
    @SerializedName("pgRating")
    @Expose
    private String pgRating;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("subscriptionType")
    @Expose
    private String subscriptionType;
    @SerializedName("hasSeasons")
    @Expose
    private Boolean hasSeasons;
    @SerializedName("price")
    @Expose
    private Integer price;

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getPgRating() {
        return pgRating;
    }

    public void setPgRating(String pgRating) {
        this.pgRating = pgRating;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public Boolean getHasSeasons() {
        return hasSeasons;
    }

    public void setHasSeasons(Boolean hasSeasons) {
        this.hasSeasons = hasSeasons;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
