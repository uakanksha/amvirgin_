package com.example.amvirgin.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.Activity.LegalInformationActivity;
import com.example.amvirgin.Activity.ProfileActivity;
import com.example.amvirgin.Activity.SupportActivity;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.SplashActivity;
import com.example.amvirgin.SubscriptionSection.SubscriptionActivity;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.classes.SessionManager;
import com.example.amvirgin.entertainmentModule.Activity.UserFabVideoActivity;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.NetworkUtility;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.example.amvirgin.utils.User;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MoreFragment extends Fragment implements View.OnClickListener {
    private LinearLayout ll_login, ll_logout, watch_later_ll, profile_settings_ll, support_ll, legal_ll;
    private TextView username, email_id, tv_subscribe, tv_profile_settings;
    private RelativeLayout without_login_rl, after_login_rl;
    String token;
    ToastMessage toastMessage;
    private ImageView iv_profile_settings;
    private CircleImageView circle;
    private ProgressBar pb;

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        tv_subscribe = view.findViewById(R.id.tv_subscribe);
        profile_settings_ll = view.findViewById(R.id.profile_settings_ll);
        tv_profile_settings = view.findViewById(R.id.tv_profile_settings);
        iv_profile_settings = view.findViewById(R.id.iv_profile_settings);
        support_ll = view.findViewById(R.id.support_ll);
        circle = view.findViewById(R.id.circle);
        pb = view.findViewById(R.id.pb);
        legal_ll = view.findViewById(R.id.legal_ll);

        toastMessage = new ToastMessage(getContext());

        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getString(getContext(), Constants.Key.ApiToken);
        }

        if (token != null) {
            getUserProfile(token);
        }

        ll_login = view.findViewById(R.id.ll_login);
        ll_logout = view.findViewById(R.id.ll_logout);
        watch_later_ll = view.findViewById(R.id.watch_later_ll);
        watch_later_ll.setOnClickListener(this);
        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)){
            watch_later_ll.setVisibility(View.VISIBLE);
        }else {
            watch_later_ll.setVisibility(View.GONE);
        }
        ll_login.setOnClickListener(this);
        ll_logout.setOnClickListener(this);
        profile_settings_ll.setOnClickListener(this);
        support_ll.setOnClickListener(this);
        legal_ll.setOnClickListener(this);

        username = view.findViewById(R.id.username);
        email_id = view.findViewById(R.id.email_id);
        without_login_rl = view.findViewById(R.id.without_login_rl);
        after_login_rl = view.findViewById(R.id.after_login_rl);

        tv_subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SubscriptionActivity.class);
                intent.setFlags(FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)){
            without_login_rl.setVisibility(View.GONE);
            after_login_rl.setVisibility(View.VISIBLE);
            ll_logout.setVisibility(View.VISIBLE);
            profile_settings_ll.setVisibility(View.VISIBLE);

//            profile_settings_ll.setEnabled(true);
//            tv_profile_settings.setTextColor(getResources().getColor(R.color.OffWhite));
//            iv_profile_settings.setColorFilter(ContextCompat.getColor(getContext(),
//                    R.color.OffWhite), android.graphics.PorterDuff.Mode.MULTIPLY);
        }
        else
            {
            without_login_rl.setVisibility(View.VISIBLE);
            after_login_rl.setVisibility(View.GONE);
            ll_logout.setVisibility(View.GONE);
            profile_settings_ll.setVisibility(View.GONE);

//                profile_settings_ll.setEnabled(false);
//                tv_profile_settings.setTextColor(getResources().getColor(R.color.lightOffWhite));
//                iv_profile_settings.setColorFilter(ContextCompat.getColor(getContext(),
//                        R.color.lightOffWhite), android.graphics.PorterDuff.Mode.MULTIPLY);
        }
        return view;
    }

    private void getUserProfile(String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .getUserProfile("application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                username.setText(response.getData().getName());
                email_id.setText(response.getData().getEmail());

                if (response.getData().getAvatar() != null){
                    Glide.with(getContext()).load(response.getData().getAvatar()).into(circle);
                    pb.setVisibility(View.GONE);
                }
                else {
                    circle.setImageResource(R.drawable.ic_user);
                    pb.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_login:
                Intent i = new Intent(getContext(), LoginActivity.class);
                startActivity(i);
                break;

            case R.id.ll_logout:
                userLogout(token);
                break;
            case R.id.watch_later_ll:
                Intent intent = new Intent(getContext(), UserFabVideoActivity.class);
                startActivity(intent);
                break;

            case R.id.profile_settings_ll:
                Intent intent_profile = new Intent(getContext(), ProfileActivity.class);
                startActivity(intent_profile);
                break;

            case R.id.support_ll:
                Intent intent_support = new Intent(getContext(), SupportActivity.class);
                startActivity(intent_support);
                break;

            case R.id.legal_ll:
                Intent intent_legal = new Intent(getContext(), LegalInformationActivity.class);
                startActivity(intent_legal);
                break;
        }
    }

    private void userLogout(String token) {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .userLogout("application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                if (response.getStatus() == 200){

                    PreferenceManager.deleteUser(getContext());
                    PreferenceManager.delete(getContext(), "session_id");
                    PreferenceManager.delete(getContext(), "cart_count");
                    SplashActivity.cart_products.clear();

                    toastMessage.showSuccessShortCustomToast("You have been logged out!!");
//                    Toast.makeText(getContext(), "You have been logged out!!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getContext(), HomeActivity.class);
                    i.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                else {
                    toastMessage.showErrorShortCustomToast(response.getMessage());
                }
            }
        });
    }
}
