package com.example.amvirgin.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.amvirgin.R;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.classes.SendOTPModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.NetworkUtility;
import com.example.amvirgin.utils.ToastMessage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterFragment extends Fragment implements View.OnClickListener {

//    private ImageView eye_show_pass, eye_hide_pass, eye_show_confPass, eye_hide_confPass;
    private EditText password, confirm_password, full_name, email_id, phone_number;
    private Button register_btn;
    private CheckBox cb_agreement;
    private String nameStr, emailStr, passwordStr, conf_passwordStr, phoneStr;
    ToastMessage toastMessage;

    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        toastMessage = new ToastMessage(getContext());

        password = view.findViewById(R.id.password);
        confirm_password = view.findViewById(R.id.confirm_password);
        full_name = view.findViewById(R.id.full_name);
        email_id = view.findViewById(R.id.email_id);
        register_btn = view.findViewById(R.id.register_btn);
        cb_agreement = view.findViewById(R.id.cb_agreement);
        phone_number = view.findViewById(R.id.phone_number);

        register_btn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.register_btn:

                if (isValidInputToRegister()){
//                    registerUser();
                    sendOTP();
                }

                break;
        }
    }

    private void sendOTP() {
        Log.d("click14","click");
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<SendOTPModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .sendOTP("customer?mobile="+phoneStr+"&type=2");
        serviceWrapper.HandleResponse(call, new ResponseHandler<SendOTPModel>(getContext()) {
            @Override
            public void onResponse(SendOTPModel response) {
                Bundle bundle = new Bundle();
                bundle.putString("phone_number", phoneStr);
                bundle.putString("email", emailStr);
                bundle.putString("name", nameStr);
                bundle.putString("password", passwordStr);
                bundle.putString("type", "register");

                FragmentManager fm = getFragmentManager();
                FragmentTransaction transact = fm.beginTransaction();
                OTPVerificationFragment otpVerificationFragment = new OTPVerificationFragment();
                otpVerificationFragment.setArguments(bundle);
                transact.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down);
                otpVerificationFragment.show(getFragmentManager(),"about");
            }
        });
    }

    private boolean isValidInputToRegister() {
        nameStr = full_name.getText().toString().trim();
        emailStr = email_id.getText().toString().trim();
        passwordStr = password.getText().toString();
        conf_passwordStr = confirm_password.getText().toString();
        phoneStr = phone_number.getText().toString();

        String mobilePattern = "[0-9]{10}";
        boolean a = false;

        if (TextUtils.isEmpty(nameStr)){
            full_name.setError("Please Enter Name");
            full_name.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Name", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(emailStr)){
            email_id.setError("Please Enter Email");
            email_id.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Email", Toast.LENGTH_SHORT).show();
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()){
            email_id.setError("Please Enter a valid Email");
            email_id.requestFocus();
//            Toast.makeText(getContext(), "Please Enter a valid Email", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(phoneStr)){
            phone_number.setError("Please Enter Phone Number");
            phone_number.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
        }
          else if (!phoneStr.matches(mobilePattern)){
            phone_number.setError("Please Enter a valid Contact Number");
            phone_number.requestFocus();
//            Toast.makeText(getContext(), "Please Enter a valid Contact Number", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(passwordStr)){
            password.setError("Please Enter Password");
            password.requestFocus();
//            Toast.makeText(getContext(), "Please Enter Password", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(conf_passwordStr)){
            confirm_password.setError("Please Confirm Password");
            confirm_password.requestFocus();
//            Toast.makeText(getContext(), "Please Confirm Password", Toast.LENGTH_SHORT).show();
        }
        else if (!passwordStr.equals(conf_passwordStr)){
            confirm_password.setError("Passwords doesn't match");
            confirm_password.requestFocus();
//            Toast.makeText(getContext(), "Passwords doesn't match", Toast.LENGTH_SHORT).show();
        }

        else if (!cb_agreement.isChecked()){
//            Toast.makeText(getContext(), "You must agree to AmVirgin Terms and Conditions!!", Toast.LENGTH_SHORT).show();
            toastMessage.showErrorShortCustomToast("You must agree to AmVirgin Terms and Conditions!!");
        }
        else {
            a = true;
        }

        return a;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        SharedPreferences pref = getContext().getSharedPreferences("settings", Activity.MODE_PRIVATE);
        pref.edit().remove("login").apply();
    }

}
