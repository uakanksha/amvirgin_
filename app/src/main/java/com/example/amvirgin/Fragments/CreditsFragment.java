package com.example.amvirgin.Fragments;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.classes.MovieDetailModel;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class CreditsFragment extends DialogFragment {
    TextView description_text, title_tv, type_tv, duration_tv, genre_tv, pgRating_tv, language_tv,
    release_tv, cast_tv, director_tv;
    MovieDetailModel.Data data;
    private ImageView iv_poster, iv_close;

    public CreditsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_credits, container, false);


        description_text = view.findViewById(R.id.description_text);
        iv_poster = view.findViewById(R.id.iv_poster);
        title_tv = view.findViewById(R.id.title_tv);
        type_tv = view.findViewById(R.id.type_tv);
        duration_tv = view.findViewById(R.id.duration_tv);
        genre_tv = view.findViewById(R.id.genre_tv);
        pgRating_tv = view.findViewById(R.id.pgRating_tv);
        language_tv = view.findViewById(R.id.language_tv);
        release_tv = view.findViewById(R.id.release_tv);
        cast_tv = view.findViewById(R.id.cast_tv);
        director_tv = view.findViewById(R.id.director_tv);
        iv_close = view.findViewById(R.id.iv_close);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Bundle bundle = this.getArguments();
        data = (MovieDetailModel.Data) bundle.getSerializable("data");

        Picasso.with(getContext()).load(data.getPoster()).into(iv_poster,
                new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
//                                        pb.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
        title_tv.setText(data.getTitle());
        type_tv.setText(data.getType());
        genre_tv.setText(data.getGenre());
        duration_tv.setText(data.getDuration());
        pgRating_tv.setText(data.getPgRating());
        description_text.setText(data.getDescription());
        release_tv.setText(data.getReleased());
        cast_tv.setText(data.getCast());
        director_tv.setText(data.getDirector());

        return view;
    }

}
