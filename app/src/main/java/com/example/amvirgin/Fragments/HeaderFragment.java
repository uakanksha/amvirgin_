package com.example.amvirgin.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.amvirgin.R;

public class HeaderFragment extends Fragment {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    LinearLayout credits_ll;

    public HeaderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_header, container, false);

        credits_ll = view.findViewById(R.id.credits_ll);

        credits_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                CreditsFragment creditsFragment = new CreditsFragment();
//                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down);
//                transact.add(R.id.item_container, productAlertFragment).addToBackStack(null).commit();
                creditsFragment.show(getFragmentManager(),"about");
            }
        });

        return view;
    }

}
