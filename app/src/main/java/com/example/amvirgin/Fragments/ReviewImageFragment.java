package com.example.amvirgin.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Dimension;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;


public class ReviewImageFragment extends Fragment implements View.OnClickListener {


    private int CAMERA = 2, GALLERY = 1;
    private LinearLayout ll_camera,ll_submit_review;
    private ImageView prodectImage,clear_img, iv_img;
    TextView tv_name;
    private FrameLayout image;
    String name, image1;
    int key;
    RatingBar ratingBar;

    public ReviewImageFragment(String name, String image, int key) {
        this.name = name;
        this.image1 = image;
        this.key = key;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_review_image, container, false);


        ll_camera=rootView.findViewById(R.id.ll_camera_review);
        ll_submit_review=rootView.findViewById(R.id.ll_review_submit);
        prodectImage=rootView.findViewById(R.id.iv_productImg1);
        clear_img=rootView.findViewById(R.id.clear_img1);
        image=rootView.findViewById(R.id.rl_setImgOne);
        iv_img = rootView.findViewById(R.id.iv_img);
        tv_name = rootView.findViewById(R.id.tv_name);
        ratingBar = rootView.findViewById(R.id.ratingBar);

        clear_img.setOnClickListener(this);
        ll_submit_review.setOnClickListener(this);
        ll_camera.setOnClickListener(this);

        if (name != null || !name.equalsIgnoreCase("")){
            tv_name.setText(name);
        }
        if (image1 != null){
            Glide.with(getContext()).load(image1).into(iv_img);
        }
        else {
            iv_img.setImageResource(R.drawable.placeholder);
        }

        return rootView;


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_camera_review:
                showPictureDialog();
                break;
            case R.id.clear_img1:
                prodectImage.setImageResource(R.drawable.ic_add_picture);
                clear_img.setVisibility(View.GONE);
                image.setVisibility(View.INVISIBLE);
                break;
        }

    }
    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Option");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Toast.makeText(getContext(), "gfh", Toast.LENGTH_SHORT);
                                choosePhotoFromGallary();
                                break;
                            case 1:

                                takePhotoFromCamera();
//                                requestCameraPermission();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }
    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }
    private void takePhotoFromCamera() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//        Dexter.withActivity(getActivity())
//                .withPermission(Manifest.permission.CAMERA)
//                .withListener(new PermissionListener() {
//                    @Override
//                    public void onPermissionGranted(PermissionGrantedResponse response) {
//                        // permission is granted
////                        openCamera();
//                    }
//
//                    @Override
//                    public void onPermissionDenied(PermissionDeniedResponse response) {
//                        // check for permanent denial of permission
//                        if (response.isPermanentlyDenied()) {
//                            showSettingsDialog();
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permission, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//                }).check();
        startActivityForResult(intent, CAMERA);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
//            fileUri = selectedImage;
//            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");
//            Log.d("GALLERY uri path", String.valueOf(selectedImage));
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                prodectImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            clear_img.setVisibility(View.VISIBLE);
            image.setVisibility(View.VISIBLE);




        } else if (requestCode == CAMERA && resultCode == RESULT_OK && data != null) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

        }
    }

    @Override
    public void setMenuVisibility(boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {

            ll_camera.setVisibility(View.VISIBLE);
            ll_submit_review.setVisibility(View.VISIBLE);
//        }else {
//
//            ll_camera.setVisibility(View.INVISIBLE);
//            ll_submit_review.setVisibility(View.VISIBLE);
        }

    }

}
