package com.example.amvirgin.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.classes.SessionManager;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.SessionId;
import com.example.amvirgin.utils.ToastMessage;
import com.example.amvirgin.utils.User;

import retrofit2.Call;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class OTPVerificationFragment extends DialogFragment {

    String strEmail, strPassword, strName, strOTP, strPhone, typeForMoving;
    private Button submit_btn;
    private int type = 3;
    private EditText otp;
    private int otpInt = 1234;
    SessionManager session;
    ImageView iv_close;
    ToastMessage toastMessage;

    public OTPVerificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_otpverification, container, false);

        toastMessage = new ToastMessage(getContext());

        Bundle bundle = this.getArguments();
        strPhone = bundle.getString("phone_number", "");
        typeForMoving = bundle.getString("type", "");
        strEmail = bundle.getString("email", "");
        strName = bundle.getString("name", "");
        strPassword = bundle.getString("password", "");


        submit_btn = view.findViewById(R.id.submit_btn);
        otp = view.findViewById(R.id.otp);
        iv_close = view.findViewById(R.id.iv_close);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidInputToVerify()) {
                    if (typeForMoving.equalsIgnoreCase("login")) {
                        loginUser();
                    }
                    else if (typeForMoving.equalsIgnoreCase("register")){
                        registerUser();
                    }
                }
            }
        });

        return view;
    }

    private void registerUser() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> callRegister = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .registerUser(strEmail, strPhone, strName, strPassword, strOTP);
        serviceWrapper.HandleResponse(callRegister, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                if (response.getStatus() == 200 || response.getStatus() == 201) {
                    toastMessage.showSuccessShortCustomToast(response.getMessage());

                    User user = new User(response.getData());
                    PreferenceManager.saveUser(getContext(), user);


                    if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("wishlist")){
//                        Intent intent = new Intent(getContext(), HomeActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("back", "shop");
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("add_wishlist")){
                        int ProductId = PreferenceManager.getInt(getContext(), "key1");
//                        Intent intent = new Intent(getContext(), ProductDetailActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("ProductId", ProductId);
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("place_order")){
//                        Intent intent = new Intent(getContext(), ShopOrderSummaryActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("my_orders")){
//                        Intent intent = new Intent(getContext(), HomeActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("back", "shop");
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else {
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
                else{
                    Log.e("sdsahdsds",  "onResponse: "+response.getMessage());
                    toastMessage.showErrorShortCustomToast(response.getMessage());
                }
            }
        });

    }

    private boolean isValidInputToVerify() {
        boolean s = false;
        strOTP = otp.getText().toString();
        String OTPPattern = "[0-9]{4}";

        if (strOTP.equals("")){
            otp.setError("Please Enter OTP");
            otp.requestFocus();
        }
        else if (!strOTP.matches(OTPPattern)){
            otp.setError("Please Enter 4 Digit OTP");
            otp.requestFocus();
        }
        else {
            s = true;
        }
        Log.d("s", String.valueOf(s));
        return s;
    }

    private void loginUser() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .loginUserWithotp(strPhone, strOTP);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                if (response.getStatus() == 200) {
                    toastMessage.showSuccessShortCustomToast(response.getMessage());

                    User user = new User(response.getData());
                    PreferenceManager.saveUser(getContext(), user);


                    if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("wishlist")){
//                        Intent intent = new Intent(getContext(), HomeActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("back", "shop");
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("add_wishlist")){
                        int ProductId = PreferenceManager.getInt(getContext(), "key1");
//                        Intent intent = new Intent(getContext(), ProductDetailActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("ProductId", ProductId);
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("place_order")){
//                        Intent intent = new Intent(getContext(), ShopOrderSummaryActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("my_orders")){
//                        Intent intent = new Intent(getContext(), HomeActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("back", "shop");
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else {
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
                else {
                    toastMessage.showErrorShortCustomToast(response.getMessage());
                }
            }
        });
    }
    @Override
    public void onDetach() {
        super.onDetach();
//        SharedPreferences pref = getContext().getSharedPreferences("settings", Activity.MODE_PRIVATE);
//        pref.edit().remove("login").apply();
    }

}
