package com.example.amvirgin.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.classes.SendOTPModel;
import com.example.amvirgin.classes.SessionManager;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.ProductDetailActivity;
import com.example.amvirgin.shopModule.activity.ShopMyOrdersActivity;
import com.example.amvirgin.shopModule.activity.ShopOrderSummaryActivity;
import com.example.amvirgin.shopModule.activity.ShopWishListActivity;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.NetworkUtility;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.SessionId;
import com.example.amvirgin.utils.ToastMessage;
import com.example.amvirgin.utils.User;
import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.content.Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED;

public class SignInFragment extends Fragment implements View.OnClickListener {

    private LinearLayout login_with_otp_ll, login_with_email_ll, login_with_otp_fields_ll, login_with_email_fields_ll,
    login_ll, send_otp_ll;
    private int type, otpInt;
    private String strEmail, strPassword, strMobile, strOTP, strPhone;
    private EditText email_id, password, phone_number;
    private Button login_btn, send_otp_btn;
    private SessionManager session;
    ToastMessage toastMessage;
    private String sessionId;

    public SignInFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        toastMessage = new ToastMessage(getContext());

        email_id = view.findViewById(R.id.email_id);
        password = view.findViewById(R.id.password);
        login_with_otp_ll = view.findViewById(R.id.login_with_otp_ll);
        login_with_email_ll = view.findViewById(R.id.login_with_email_ll);
        login_with_otp_fields_ll = view.findViewById(R.id.login_with_otp_fields_ll);
        login_with_email_fields_ll = view.findViewById(R.id.login_with_email_fields_ll);
        login_ll = view.findViewById(R.id.login_ll);
        send_otp_ll = view.findViewById(R.id.send_otp_ll);
        login_btn = view.findViewById(R.id.login_btn);
        send_otp_btn = view.findViewById(R.id.send_otp_btn);
        phone_number = view.findViewById(R.id.phone_number);

        login_with_otp_ll.setOnClickListener(this);
        login_with_email_ll.setOnClickListener(this);
        login_ll.setOnClickListener(this);
        send_otp_ll.setOnClickListener(this);
        login_btn.setOnClickListener(this);
        send_otp_btn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.login_with_otp_ll:

                type = 3;

                login_with_otp_ll.setVisibility(View.GONE);
                login_with_email_ll.setVisibility(View.VISIBLE);
            login_with_otp_fields_ll.setVisibility(View.VISIBLE);
            login_with_email_fields_ll.setVisibility(View.GONE);
            send_otp_ll.setVisibility(View.VISIBLE);
            login_ll.setVisibility(View.GONE);

            break;

            case R.id.login_with_email_ll:

                type = 1;

                login_with_otp_ll.setVisibility(View.VISIBLE);
                login_with_email_ll.setVisibility(View.GONE);
            login_with_otp_fields_ll.setVisibility(View.GONE);
            login_with_email_fields_ll.setVisibility(View.VISIBLE);
                send_otp_ll.setVisibility(View.GONE);
                login_ll.setVisibility(View.VISIBLE);

            break;

            case R.id.login_btn:
                Log.d("click","click");
                if(isValidInputForLogin()) {
                    loginUser();
                }

                break;

            case R.id.send_otp_btn:
                Log.d("click1","click");
                if(isValidInputToSendOTP()) {
                    sendOTP();
                }

                break;
        }
    }

    private void sendOTP() {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<SendOTPModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .sendOTP("customer?mobile="+strPhone+"&type=2");
        serviceWrapper.HandleResponse(call, new ResponseHandler<SendOTPModel>(getContext()) {
            @Override
            public void onResponse(SendOTPModel response) {
                if (response.getStatus() == 409){
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction transact = fm.beginTransaction();
                    OTPVerificationFragment otpVerificationFragment = new OTPVerificationFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("phone_number", strPhone);
                    bundle.putString("type", "login");

                    otpVerificationFragment.setArguments(bundle);

                    transact.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down);
//                transact.add(R.id.item_container, productAlertFragment).addToBackStack(null).commit();
                    otpVerificationFragment.show(getFragmentManager(), "about");
                }
                else if (response.getStatus() == 404){
                    toastMessage.showErrorShortCustomToast("This Mobile is not Registered!!");
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.putExtra("reg", "not");
                    startActivity(intent);
                }
            }
        });
    }

    private boolean isValidInputToSendOTP() {
        boolean s = false;
        strPhone = phone_number.getText().toString();
        String MobilePattern = "[0-9]{10}";

        if (strPhone.equals("")){
            phone_number.setError("Please Enter Phone");
            phone_number.requestFocus();
        }
        else if (!strPhone.matches(MobilePattern)){
            phone_number.setError("Please Enter a valid Phone Number");
            phone_number.requestFocus();
        }
        else {
            s = true;
        }
        Log.d("s", String.valueOf(s));
        return s;
    }

    private boolean isValidInputForLogin() {
        strEmail= email_id.getText().toString().trim();
        Log.d("Email",strEmail);
        strPassword= password.getText().toString().trim();
        Log.d("Password",strPassword);
//        strMobile = mobile_number.getText().toString().trim();
//        Log.d("strMobile",strMobile);
        String MobilePattern = "[0-9]{10}";

        boolean s = false;

        if (strEmail.equals("")){
            email_id.setError("Please Enter Email or Mobile");
            email_id.requestFocus();
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches() && !strEmail.matches(MobilePattern)){
                email_id.setError("Please Enter a Valid Email or Phone");
                email_id.requestFocus();
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches() || !strEmail.matches(MobilePattern)){
            if (android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches()){
                type = 1;
            }
            else if (strEmail.matches(MobilePattern)){
                type = 2;
                strMobile = strEmail;
                strEmail = "";
            }
             if (strPassword.equals("")){
                password.setError("Please Enter Password");
                password.requestFocus();
            }
            else {
                s = true;
            }

        }

        Log.d("s", String.valueOf(s));
        return s;
    }

    private void loginUser() {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .loginUser("customer/login?type="+type, strMobile, strEmail, strPassword, strOTP);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {

                if (response.getStatus() == 200) {
                    toastMessage.showSuccessShortCustomToast(response.getMessage());
                    User user = new User(response.getData());
                    if (response.getData().getSubscription()!=null){
                        PreferenceManager.saveBoolean(getContext(), Constants.Key.SUBSCRIPTION, true);
                    }else {
                        PreferenceManager.saveBoolean(getContext(), Constants.Key.SUBSCRIPTION, false);
                    }
                    PreferenceManager.saveUser(getContext(), user);


                    if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("wishlist")){
//                        Intent intent = new Intent(getContext(), HomeActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("back", "shop");
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("add_wishlist")){
                        int ProductId = PreferenceManager.getInt(getContext(), "key1");
//                        Intent intent = new Intent(getContext(), ProductDetailActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        intent.putExtra("ProductId", ProductId);
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("place_order")){
//                        Intent intent = new Intent(getContext(), ShopOrderSummaryActivity.class);
//                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if (PreferenceManager.getString(getContext(), "redirect").equalsIgnoreCase("my_orders")){
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK | FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("back", "shop");
                        startActivity(intent);
                        getActivity().finish();
                    }
                    else {
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }

               else if (response.getStatus() == 404){
                    toastMessage.showSuccessShortCustomToast("This Mobile/Email is not Registered!!");
                    Intent intentLogin = new Intent(getContext(), LoginActivity.class);
                    intentLogin.putExtra("reg", "not");
                    startActivity(intentLogin);
                }
               else {
                   toastMessage.showErrorShortCustomToast(response.getMessage());
                }
            }
        });
    }

}
