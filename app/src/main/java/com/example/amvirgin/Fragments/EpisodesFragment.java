package com.example.amvirgin.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.amvirgin.Adapter.EpisodesAdapter;
import com.example.amvirgin.R;

import java.util.ArrayList;
import java.util.Arrays;

import static com.example.amvirgin.R.layout.spinner_item_overview;

public class EpisodesFragment extends Fragment {

    RecyclerView rv_episodes;
    Spinner sp_seasons;
    String[] seasons;
    private ArrayAdapter<CharSequence> mSeasonsAdapter;
    private ArrayList episodes = new ArrayList<>(Arrays.asList("Episode 1", "Episode 2", "Episode 3", "Episode 4", "Episode 5",
            "Episode 6", "Episode 7"));

    public EpisodesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_episodes, container, false);

        rv_episodes = view.findViewById(R.id.rv_episodes);
        sp_seasons = view.findViewById(R.id.sp_seasons);

        seasons = getResources().getStringArray(R.array.seasons);
        mSeasonsAdapter = new ArrayAdapter<CharSequence>(getActivity(), spinner_item_overview, seasons);
        mSeasonsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_seasons.setAdapter(mSeasonsAdapter);

        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv_episodes.setLayoutManager(manager);

//        EpisodesAdapter adapter = new EpisodesAdapter(getContext(), episodes, "");
//        rv_episodes.setAdapter(adapter);

        return view;
    }

}
