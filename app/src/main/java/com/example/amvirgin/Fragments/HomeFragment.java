package com.example.amvirgin.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.amvirgin.Adapter.ParentAdapter;
import com.example.amvirgin.R;
import com.example.amvirgin.Slide;
import com.example.amvirgin.classes.BannerSliderModel;
import com.example.amvirgin.classes.HomepageTrendingResponse;
import com.example.amvirgin.classes.HomepagetrendingData;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.AppLocal;
import com.example.amvirgin.utils.NetworkUtility;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment{

    private RecyclerView parentrecycler;
    private ArrayList<Object> objects = new ArrayList<>();
    
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        parentrecycler=view.findViewById(R.id.parentrecycler);
        getBannerSliders();
        getAllData();

        return view;
    }

    private void getBannerSliders() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<BannerSliderModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .getBannerSlider();
        serviceWrapper.HandleResponse(call, new ResponseHandler<BannerSliderModel>(getContext()) {
            @Override
            public void onResponse(BannerSliderModel response) {
                if (response.getStatus() == 200){
                    AppLocal.type = "data";
                    AppLocal.data = response.getData();
                }
                else {
                    AppLocal.type = "nodata";
                }
            }
        });
    }

    private void getAllData() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<HomepageTrendingResponse> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .Homepagetrending();
        serviceWrapper.HandleResponse(call, new ResponseHandler<HomepageTrendingResponse>(getContext()) {
            @Override
            public void onResponse(HomepageTrendingResponse response) {
                if (response.getStatus() == 200) {
                    HomepagetrendingData data = response.getHomepagetrendingData();
                    AppLocal.trendingPicks = data.getTrendingPicks();
                    AppLocal.justAdded = data.getJustAdded();
                    AppLocal.topPicks = data.getTopPicks();

                    setadapter();
                }
            }
        });
    }

    private void setadapter() {
        ParentAdapter adapter = new ParentAdapter(getActivity(),getlist());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        parentrecycler.setHasFixedSize(true);
        parentrecycler.setItemViewCacheSize(20);
        parentrecycler.setDrawingCacheEnabled(true);
        parentrecycler.setLayoutManager(manager);
        parentrecycler.setAdapter(adapter);

        Log.e("Home fragment", "setadapter: parentadapter called" );
    }

    private ArrayList<Object> getlist() {

            objects.add(AppLocal.topPicksArrayList);
            objects.add(AppLocal.topPicks);
            objects.add(AppLocal.shopArrayList);
            objects.add(AppLocal.justAdded);
            objects.add(AppLocal.trendingPicks);
            Log.e("objects==", "getlist: " + objects);

            return objects;

    }

}
