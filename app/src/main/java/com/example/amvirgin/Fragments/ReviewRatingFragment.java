package com.example.amvirgin.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;


public class ReviewRatingFragment extends Fragment {
    String name, image;
    int key;
    ImageView iv_img;
    TextView tv_name;


    public ReviewRatingFragment(String name, String image, int key) {
        this.name = name;
        this.image = image;
        this.key = key;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_review_rating, container, false);

        iv_img = rootView.findViewById(R.id.iv_img);
        tv_name = rootView.findViewById(R.id.tv_name);

        if (name != null || !name.equalsIgnoreCase("")){
            tv_name.setText(name);
        }
        if (image != null){
            Glide.with(getContext()).load(image).into(iv_img);
        }
        else {
            iv_img.setImageResource(R.drawable.placeholder);
        }

        return rootView;
    }



}
