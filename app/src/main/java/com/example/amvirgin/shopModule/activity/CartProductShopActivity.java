package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.sessionmanagement.ValidateSession;
import com.example.amvirgin.shopModule.shopAdapter.ShopCartListAdapter;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;
import com.example.amvirgin.shopModule.shopModel.RetrieveCartModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.gson.GsonBuilder;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lal.adhish.gifprogressbar.GifView;
import retrofit2.Call;

public class CartProductShopActivity extends AppCompatActivity implements View.OnClickListener, PaymentResultListener {
    private RelativeLayout rl_back;
    private RecyclerView recyclerView_productCart;
    private RecyclerView.LayoutManager layoutManager;
    private Button placeOrder,changeaddress;
    private Context mCtx;
    private RelativeLayout cart_main_rl, empty_cart_rl, rlWish;
    boolean valid = false;

    String session_id, token;
    TextView tv_price_subtotal, tv_total_price, tv_tax, tv_total_price_;
    int cartCount;
    int customerId;

    Button start_shopping_btn;
    GifView pGif;
    AddToCartModel list;
    HashMap<String, String> value = new HashMap<>();
    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
        session_id = PreferenceManager.getString(getApplicationContext(), "session_id");
        retrieveCart(session_id);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_product_shop);
        Checkout.preload(getApplicationContext());
        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
            customerId = PreferenceManager.getUser(getApplicationContext()).getKey();
        }

        session_id = PreferenceManager.getString(getApplicationContext(), "session_id");
        cartCount = PreferenceManager.getInt(getApplicationContext(), "cart_count");

        tv_price_subtotal = findViewById(R.id.tv_price_subtotal);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_tax = findViewById(R.id.tv_tax);
        tv_total_price_ = findViewById(R.id.tv_total_price_);

        rlWish = findViewById(R.id.rlWish);
        rlWish.setOnClickListener(this);

        cart_main_rl = findViewById(R.id.cart_main_rl);
        empty_cart_rl = findViewById(R.id.empty_cart_rl);

        start_shopping_btn = findViewById(R.id.start_shopping_btn);

        pGif = (GifView) findViewById(R.id.progressBar);
        pGif.setImageResource(R.drawable.loader_red);

        recyclerView_productCart=findViewById(R.id.recycler_cartProduct);
        rl_back=findViewById(R.id.rlBack);
        placeOrder=(Button)findViewById(R.id.btn_placeOrder);
        changeaddress=(Button)findViewById(R.id.btn_change_add);

//        recyclerView_productCart.setNestedScrollingEnabled(false);
        rl_back.setOnClickListener(this);
        placeOrder.setOnClickListener(this);
        changeaddress.setOnClickListener(this);

        start_shopping_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
                intent.putExtra("back", "cart");
                startActivity(intent);
            }
        });

//        if (session_id.isEmpty()){
//            initializeSession();
//        }
//        else {
//            validateSession(session_id);
//        }

//        if (valid){
//            retrieveCart(session_id);
//        }
        retrieveCart(session_id);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.btn_placeOrder:
//                Payment();
                if(token != null){
//                    Payment();
                    checkForAddresses();
//                    startActivity(new Intent(getApplicationContext(), AddAddressActivity.class));
                }
                else {
                    PreferenceManager.saveString(getApplicationContext(), "redirect", "place_order");
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    i.putExtra("notlogin", "notlogin");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                break;
            case R.id.btn_change_add:
                startActivity(new Intent(getApplicationContext(), MyAddressActivity.class));
                break;

            case R.id.rlWish:
                startActivity(new Intent(getApplicationContext(), ShopWishListActivity.class));
                break;
        }
    }

    private void checkForAddresses() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<GetAddressModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getAddress("application/json", "Bearer "+token, "no-cache");
        serviceWrapper.HandleResponse(call, new ResponseHandler<GetAddressModel>(CartProductShopActivity.this) {
            @Override
            public void onResponse(GetAddressModel response) {
                if (response.getStatus() == 200){

                        if (response.getData().size() > 0){
                            Intent intent = new Intent(getApplicationContext(), ShopOrderSummaryActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        else {
                            Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }

                }
                else {
                    SnackbarManager.sException(CartProductShopActivity.this, response.getMessage());
                }
            }
        });
    }

    private void retrieveCart(final String session_id) {
//        pGif.setVisibility(View.VISIBLE);
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());

        Call<AddToCartModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .retrieveCart("customer/cart/retrieve?sessionId="+session_id+"&customerId="+customerId);

        serviceWrapper.HandleResponse(call, new ResponseHandler<AddToCartModel>(this) {
            @Override
            public void onResponse(AddToCartModel response) {
                pGif.setVisibility(View.GONE);
                if (response.getStatus() == 200){

                        if (response.getMessage().equalsIgnoreCase("No cart was found for that session.")){
                            cart_main_rl.setVisibility(View.GONE);
                            empty_cart_rl.setVisibility(View.VISIBLE);
                        }

                        else if (response.getData().getCart().getItemCount() == 0){
                            cart_main_rl.setVisibility(View.GONE);
                            empty_cart_rl.setVisibility(View.VISIBLE);
                        }
                        else {
                            cart_main_rl.setVisibility(View.VISIBLE);
                            empty_cart_rl.setVisibility(View.GONE);
                            list = response;
                            String type = "cart";
                            AddToCartModel.Cart cartData = response.getData().getCart();
                            List<AddToCartModel.Item> items = response.getData().getCart().getItems();
                            layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerView_productCart.setLayoutManager(layoutManager);
                            ShopCartListAdapter adapter = new ShopCartListAdapter(CartProductShopActivity.this, items,
                                    type, new RemoveFromCartClickListener() {
                                        @Override
                                        public void onItemClick() {
                                            retrieveCart(session_id);
                                        }
                                    });
                            recyclerView_productCart.setAdapter(adapter);

                            tv_price_subtotal.setText(String.valueOf("₹ "+cartData.getSubTotal()));
                            tv_tax.setText(String.valueOf("₹ "+cartData.getTax()));
                            tv_total_price.setText(String.valueOf("₹ "+cartData.getTotal()));
                            tv_total_price_.setText(String.valueOf("₹ "+cartData.getTotal()));

//                            value.put("name", response.getData().getCart().getItems().get(0).getProduct().getName());
//                            value.put("description", response.getData().getCart().getItems().get(0).getProduct().getShortDescription());
//                            value.put("image", response.getData().getCart().getItems().get(0).getProduct().getImages().get(0));
//                            value.put("currency", "INR");
//
//                            value.put("amount", response.getData().getCart().getTotal()+"11");
//                            value.put("email", ""+response.getData().getCart().getCustomer().getEmail());
//                            value.put("contact", ""+response.getData().getCart().getCustomer().getMobile());
                        }
//json
                }
                else {
                    SnackbarManager.sException(CartProductShopActivity.this,response.getMessage());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }



    @Override
    public void onPaymentSuccess(String s) {
        Log.e("dsdsadjshd", "onPaymentSuccess: "+s);
        Intent intent = new Intent(CartProductShopActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onPaymentError(int i, String s) {
        SnackbarManager.sException(CartProductShopActivity.this, i+" onPaymentError "+s);
        Toast.makeText(mCtx, "onPaymentError"+s, Toast.LENGTH_SHORT).show();
    }


    private void initializeSession() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<InitializeSession> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .initializeSession();
        serviceWrapper.HandleResponse(call, new ResponseHandler<InitializeSession>(getApplicationContext()) {
            @Override
            public void onResponse(InitializeSession response) {
                if (response.getStatus() == 200){

                        PreferenceManager.saveString(getApplicationContext(), "session_id", response.getSession());
                        session_id = response.getSession();
                        validateSession(session_id);

                }
                else {
                    SnackbarManager.sException(CartProductShopActivity.this,response.getMessage());
                }
            }
        });
    }

    private boolean validateSession(String session_id) {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<ValidateSession> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .validateSession("customer/sessions/"+ session_id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ValidateSession>(getApplicationContext()) {
            @Override
            public void onResponse(ValidateSession response) {
                if (response.getStatus() == 200){

                        valid = true;

                }
                else {
                    SnackbarManager.sException(CartProductShopActivity.this,response.getMessage());
                }
            }
        });

        return valid;
    }

}
