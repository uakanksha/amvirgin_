package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.SubscriptionSection.SubscriptionActivity;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.utils.Constants;

import java.util.List;

public class ShopBannerSliderAdapter extends RecyclerView.Adapter<ShopBannerSliderAdapter.BannerSliderViewHolder>{

    Context context;
    List<ShopHomeModel.ShopSlider> sliders;

    public ShopBannerSliderAdapter(Context context) {
        this.context = context;
        this.sliders = Constants.shopSliderList;
    }

    @NonNull
    @Override
    public BannerSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BannerSliderViewHolder(LayoutInflater.from(context).inflate(R.layout.shop_banner_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BannerSliderViewHolder holder, int position) {
        if (!sliders.get(position).getBanner().equalsIgnoreCase("")) {
            Glide.with(context).load(sliders.get(position).getBanner()).into(holder.bannerImg);
        }
        else {
            holder.bannerImg.setImageResource(R.drawable.placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return sliders.size();
    }

    public class BannerSliderViewHolder extends RecyclerView.ViewHolder{
        ImageView bannerImg;

        public BannerSliderViewHolder(@NonNull View itemView) {
            super(itemView);

            bannerImg = itemView.findViewById(R.id.banner_img);
        }
    }
}
