package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.ContextWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.interfaces.SortSelectionListener;
import com.example.amvirgin.shopModule.shopModel.SortListModel;

import java.util.List;

public class SortListAdapter extends RecyclerView.Adapter<SortListAdapter.SortListViewHolder> {
    Context context;
    List<SortListModel.Datum> sort_list_data;
    SortSelectionListener sortSelectionListener;
    RadioButton lastCheckedRB = null;

    public SortListAdapter(Context context, List<SortListModel.Datum> sort_list_data, SortSelectionListener sortSelectionListener) {
        this.context = context;
        this.sort_list_data = sort_list_data;
        this.sortSelectionListener = sortSelectionListener;
    }

    @NonNull
    @Override
    public SortListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.rv_sort_list_layout, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_sort_list_layout, parent, false);
        SortListViewHolder holder = new SortListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SortListViewHolder holder, final int position) {
        holder.rb_sort.setText(sort_list_data.get(position).getName());
        holder.rb_sort.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }
                //store the clicked radiobutton
                lastCheckedRB = holder.rb_sort;
                if (sortSelectionListener != null){
                    sortSelectionListener.onItemClick(sort_list_data.get(position).getKey());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sort_list_data.size();
    }

    public class SortListViewHolder extends RecyclerView.ViewHolder{
        RadioButton rb_sort;

        public SortListViewHolder(@NonNull View itemView) {
            super(itemView);
            rb_sort = itemView.findViewById(R.id.rb_sort);
        }
    }

}
