package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.ShopTrendingDealsModel;

import java.util.ArrayList;
import java.util.List;

public class ShopTrendingDealsAdapter extends RecyclerView.Adapter<ShopTrendingDealsAdapter.ShopTrendingDealsViewHolder> {
    Context context;
    List<ShopTrendingDealsModel.Datum> data;

    public ShopTrendingDealsAdapter(Context context, List<ShopTrendingDealsModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ShopTrendingDealsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_shop_collection_layout, parent, false);
        ShopTrendingDealsViewHolder holder = new ShopTrendingDealsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShopTrendingDealsViewHolder holder, int position) {

        if (data.get(position).getImages().size() > 0){
            Glide.with(context).load(data.get(position).getImages().get(0)).into(holder.product_img);
        }
        else {
            holder.product_img.setImageResource(R.drawable.placeholder);
        }
        holder.tv_product_title.setText(data.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ShopTrendingDealsViewHolder extends RecyclerView.ViewHolder{
        ImageView product_img;
        TextView tv_product_title;

        public ShopTrendingDealsViewHolder(@NonNull View itemView) {
            super(itemView);
            product_img = itemView.findViewById(R.id.product_img);
            tv_product_title = itemView.findViewById(R.id.tv_product_title);
        }
    }
}
