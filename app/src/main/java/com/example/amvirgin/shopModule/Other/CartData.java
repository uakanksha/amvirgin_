package com.example.amvirgin.shopModule.Other;

import java.util.ArrayList;
import java.util.HashMap;

public class CartData {

    String sessionId;
    int key;
    String customerId;
    int quantity;

    HashMap<String, String> attributes;

    /*public CartData(String sessionId, int key, String customerId, int quantity, ArrayList<HashMap> attributes) {
        this.sessionId = sessionId;
        this.key = key;
        this.customerId = customerId;
        this.quantity = quantity;
        this.attributes = attributes;
    }*/

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public HashMap<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(HashMap<String, String> attributes) {
        this.attributes = attributes;
    }

}
