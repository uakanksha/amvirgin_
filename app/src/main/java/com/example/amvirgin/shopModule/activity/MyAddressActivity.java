package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.interfaces.AddressSelectionListener;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.shopAdapter.AddressListAdapter;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;

import retrofit2.Call;

public class MyAddressActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout rl_back;
    private RecyclerView recyclerView_myAddress;
    private RecyclerView.LayoutManager layoutManager;
    private LinearLayout add_address;
    private String token;
    private int address_ID;
    TextView tv_deliver_here;
    private int address_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_address);
        recyclerView_myAddress=(RecyclerView)findViewById(R.id.recycler_myAddress);
        add_address = (LinearLayout)findViewById(R.id.ll_add_address);
        tv_deliver_here = findViewById(R.id.tv_deliver_here);

        if (getIntent().getExtras() != null){
            address_id = getIntent().getExtras().getInt("address_id");
        }

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        getAddresses(token);

        rl_back=(RelativeLayout)findViewById(R.id.rlBack);
        rl_back.setOnClickListener(this);
        add_address.setOnClickListener(this);
        tv_deliver_here.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.ll_add_address:
                startActivity(new Intent(getApplicationContext(), AddAddressActivity.class));
                break;
            case R.id.tv_deliver_here:
               selectAddress();
        }
    }

    private void selectAddress() {
        if (address_ID == 0){
            ToastMessage.getInstance(MyAddressActivity.this).showErrorShortCustomToast("Please Select Address!!");
        }
        else {
            Intent intent = new Intent(MyAddressActivity.this, ShopOrderSummaryActivity.class);
            intent.putExtra("address_id", address_ID);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private void getAddresses(final String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<GetAddressModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getAddress("application/json", "Bearer "+token, "no-cache");
        serviceWrapper.HandleResponse(call, new ResponseHandler<GetAddressModel>(MyAddressActivity.this) {
            @Override
            public void onResponse(GetAddressModel response) {
                if (response.getStatus() == 200){

                        if (response.getData().size() > 0){
                            layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerView_myAddress.setLayoutManager(layoutManager);

                            AddressListAdapter myAddressAdapter = new AddressListAdapter(getApplicationContext(), address_id,
                                    response.getData(), new AddressSelectionListener() {
                                @Override
                                public void onItemClick(int address_id) {
                                    address_ID = address_id;
                                }
                            }, new RemoveFromCartClickListener() {
                                @Override
                                public void onItemClick() {
                                    address_ID = 0;
                                    getAddresses(token);
                                }
                            });
                            recyclerView_myAddress.setAdapter(myAddressAdapter);
                        }
                }
                else {
                    SnackbarManager.sException(MyAddressActivity.this, response.getMessage());
                }
            }
        });
    }

}
