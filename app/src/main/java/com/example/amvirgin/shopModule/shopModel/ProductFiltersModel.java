package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ProductFiltersModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("payload")
    @Expose
    private List<Payload> payload = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Payload> getPayload() {
        return payload;
    }

    public void setPayload(List<Payload> payload) {
        this.payload = payload;
    }

    public class Payload {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("mode")
        @Expose
        private String mode;
        @SerializedName("options")
        @Expose
        private List<Object> options = null;
        @SerializedName("builtIn")
        @Expose
        private Boolean builtIn;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public List<Object> getOptions() {
            return options;
        }

        public void setOptions(List<Object> options) {
            this.options = options;
        }

        public Boolean getBuiltIn() {
            return builtIn;
        }

        public void setBuiltIn(Boolean builtIn) {
            this.builtIn = builtIn;
        }

    }

    public static class DataStateDeserializer implements JsonDeserializer<ProductFiltersModel> {

        @Override
        public ProductFiltersModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            ProductFiltersModel model = new Gson().fromJson(json,ProductFiltersModel.class);

            // Product Options

            Type type = new TypeToken<ProductFiltersModel.Payload>() {
            }.getType();

            ProductDetailsModel.Option option = new Gson().fromJson(json, type);
            JsonObject object = json.getAsJsonObject();

            if (object.has("options")){
                JsonElement element = object.get("options");

                if (element != null && !element.isJsonNull()){

                    if (element.isJsonPrimitive()){
                        option.setValue(element.getAsString());
                    }
                    else {
                        option.setValue(element.getAsJsonArray());
                    }
                }
            }

            return model;
        }
    }

}
