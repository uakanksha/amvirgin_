package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.utils.Constants;

import java.util.List;

public class TrendingNowAdapter extends RecyclerView.Adapter<TrendingNowAdapter.TrendingNowViewHolder> {

    Context context;
    List<ShopHomeModel.TrendingNow> trendingNow;

    public TrendingNowAdapter(Context context) {
        this.context = context;
        this.trendingNow = Constants.shopTrendingNow;
    }

    @NonNull
    @Override
    public TrendingNowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_trending_now_layout, parent, false);
        TrendingNowViewHolder holder = new TrendingNowViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TrendingNowViewHolder holder, int position) {
//        if (!trendingNow.get(position).getPoster().equalsIgnoreCase("")){
//            Glide.with(contex).load(trendingNow.get(position).getPoster()).into(holder.img);
//        }
//        else {
        if (trendingNow.get(position).getIcon().getExists()){
            Glide.with(context).load(trendingNow.get(position).getIcon().getUrl()).into(holder.img);
        }
        else {
            holder.img.setImageResource(R.drawable.placeholder);
        }

        holder.main_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String categoryId = String.valueOf(trendingNow.get(position).getKey());
                String categoryName = String.valueOf(trendingNow.get(position).getName());
                Intent intent = new Intent(context, ShopProductListActivity.class);
                intent.putExtra("CategoryName", categoryName);
                intent.putExtra("CategoryId", categoryId);
                intent.putExtra("back", "");
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return trendingNow.size();
    }

    public class TrendingNowViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        RelativeLayout main_rl;

        public TrendingNowViewHolder(@NonNull View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.img);
            main_rl = itemView.findViewById(R.id.main_rl);
        }
    }

}
