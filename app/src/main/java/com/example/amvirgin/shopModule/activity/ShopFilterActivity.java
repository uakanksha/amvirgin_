package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.interfaces.FilterKeyClickListener;
import com.example.amvirgin.shopModule.shopAdapter.FilterKeyAdapter;
import com.example.amvirgin.shopModule.shopAdapter.FilterValueAdapter;
import com.example.amvirgin.shopModule.shopModel.ProductDetailsModel;
import com.example.amvirgin.shopModule.shopModel.ProductFiltersModel;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShopFilterActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout back;
    String category_id;
    RecyclerView rv_filterKey, rv_filterValue;
    private LinearLayout head2;
    TextView tv_no_items, tv_clear;
    int selected_filter_key;
    List<Object> selected_options;
    String selected_key_label;
    View view;
    Button btn_apply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_filter);

        if (getIntent().getExtras() != null){
            category_id = getIntent().getExtras().getString("category_id");
        }

        back =(RelativeLayout)findViewById(R.id.rlBack);
        rv_filterKey = findViewById(R.id.rv_filterKey);
        rv_filterValue = findViewById(R.id.rv_filterValue);
        head2  = findViewById(R.id.head2);
        tv_no_items = findViewById(R.id.tv_no_items);
        view = findViewById(R.id.view);
        btn_apply = findViewById(R.id.btn_apply);
        tv_clear = findViewById(R.id.tv_clear);

        getFilters(category_id);

        back.setOnClickListener(this);
        btn_apply.setOnClickListener(this);
        tv_clear.setOnClickListener(this);
    }

    private void getFilters(String category_id){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ProductDetailsModel.class, new ProductFiltersModel.DataStateDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceInterface service = retrofit.create(ServiceInterface.class);

        Call<ProductFiltersModel> call = service.getProductFilters("customer/categories/filters?category="+category_id);
        call.enqueue(new Callback<ProductFiltersModel>() {
            @Override
            public void onResponse(Call<ProductFiltersModel> call, Response<ProductFiltersModel> response) {
                if (response.body().getStatus() == 200){
                    try{
                        if (response.body().getPayload().size() > 0){
                            view.setVisibility(View.VISIBLE);

                            int position=0;
                            LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                            rv_filterKey.setLayoutManager(manager);

                            FilterKeyAdapter adapter = new FilterKeyAdapter(getApplicationContext(), response.body().getPayload(),
                                    new FilterKeyClickListener() {
                                        @Override
                                        public void onItemClick(int key, String label, List<Object> options) {
                                            selected_filter_key = key;
                                            selected_key_label = label;
                                            selected_options = options;

                                            updateValueAdapter(label, selected_options);
                                        }
                                    }, position);
                            rv_filterKey.setAdapter(adapter);

                            updateValueAdapter(response.body().getPayload().get(position).getLabel(),
                                    response.body().getPayload().get(position).getOptions());

                        }
                        else {
                            rv_filterKey.setVisibility(View.GONE);
                            rv_filterValue.setVisibility(View.GONE);
                            head2.setVisibility(View.GONE);
                            tv_no_items.setVisibility(View.VISIBLE);
                            view.setVisibility(View.GONE);
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ShopFilterActivity.this,response.body().getMessage());
//                        ToastMessage.getInstance(ProductDetailActivity.this).showErrorShortCustomToast(response.body().getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ShopFilterActivity.this,response.body().getMessage());
//                    ToastMessage.getInstance(ProductDetailActivity.this).showErrorShortCustomToast(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProductFiltersModel> call, Throwable t) {

            }
        });

    }

    /*private void getFilters(String category_id) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<ProductFiltersModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getProductFilters("customer/categories/filters?category="+category_id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ProductFiltersModel>(this) {
            @Override
            public void onResponse(ProductFiltersModel response) {
                if (response.getStatus() == 200){
                 try {
                     if (response.getPayload().size() > 0){

                         LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                         rv_filterKey.setLayoutManager(manager);

                         FilterKeyAdapter adapter = new FilterKeyAdapter(getApplicationContext(), response.getPayload(),
                                 new FilterKeyClickListener() {
                                     @Override
                                     public void onItemClick(int key, String label, List<Object> options) {
                                         selected_filter_key = key;
                                         selected_key_label = label;
                                         selected_options = options;
                                         
                                         updateValueAdapter(label, selected_options);
                                     }
                                 });
                         rv_filterKey.setAdapter(adapter);

                     }
                     else {
                         rv_filterKey.setVisibility(View.GONE);
                         rv_filterValue.setVisibility(View.GONE);
                         head2.setVisibility(View.GONE);
                         tv_no_items.setVisibility(View.VISIBLE);
                     }
                 }
                 catch (Exception e){
                     SnackbarManager.sException(ShopFilterActivity.this, response.getMessage());
                 }
                }
                else {
                    SnackbarManager.sException(ShopFilterActivity.this, response.getMessage());
                }
            }
        });
    }*/

    private void updateValueAdapter(String label, List<Object> selected_options) {
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rv_filterValue.setLayoutManager(manager);

        FilterValueAdapter adapter = new FilterValueAdapter(getApplicationContext(), label, selected_options);
        rv_filterValue.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;

            case R.id.btn_apply:
                finish();
                break;

            case R.id.tv_clear:
                this.recreate();
                break;
        }
    }
}
