package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.interfaces.FilterKeyClickListener;
import com.example.amvirgin.shopModule.shopModel.ProductFiltersModel;

import java.util.List;

public class FilterKeyAdapter extends RecyclerView.Adapter<FilterKeyAdapter.FilterKeyViewHolder> {
    Context context;
    List<ProductFiltersModel.Payload> payload;
    FilterKeyClickListener filterKeyClickListener;
    int lastPosition;

    public FilterKeyAdapter(Context context, List<ProductFiltersModel.Payload> payload,
                            FilterKeyClickListener filterKeyClickListener, int lastPosition) {
        this.context = context;
        this.payload = payload;
        this.filterKeyClickListener = filterKeyClickListener;
        this.lastPosition = lastPosition;
    }

    @NonNull
    @Override
    public FilterKeyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_filter_key_layout, parent, false);
        FilterKeyViewHolder holder = new FilterKeyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterKeyViewHolder holder, final int position) {
        holder.tv_key.setText(payload.get(position).getLabel());
        if (payload.get(position).getOptions().size() == 0) {
            holder.tv_key.setVisibility(View.GONE);
        }

        if(lastPosition==position){
            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.colorwhite));
//            holder.tv_key.setTextColor(context.getResources().getColor(R.color.white));
        }
        else {
            holder.main_ll.setBackgroundColor(context.getResources().getColor(R.color.lightgrey));
//            holder.tv_key.setTextColor(context.getResources().getColor(R.color.black));
        }


        holder.main_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lastPosition=position;
                notifyDataSetChanged();
                if (filterKeyClickListener != null){

                        filterKeyClickListener.onItemClick(payload.get(position).getKey(),
                                payload.get(position).getLabel(),
                                payload.get(position).getOptions());

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return payload.size();
    }

    public class FilterKeyViewHolder extends RecyclerView.ViewHolder{
        TextView tv_key;
        LinearLayout main_ll;

        public FilterKeyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_key = itemView.findViewById(R.id.tv_key);
            main_ll = itemView.findViewById(R.id.main_ll);
        }
    }

}
