package com.example.amvirgin.shopModule.Other;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.view.ViewCompat;

import com.example.amvirgin.R;
import com.google.android.material.snackbar.Snackbar;

public class SnackManager {
    public static final int TYPE_NO_INTERNET=1;
    public static final int TYPE_FOUND_INTERNET=2;
    public static final int TYPE_EXCEPTION=3;
    public static final int TYPE_SUCCESS=4;
    private static SnackManager instance = null;
    public  Snackbar snackbar;
    private SnackManager() {
    }

    public static SnackManager getInstance() {
        if(instance == null) {
            instance = new SnackManager();
        }
        return instance;
    }

    public void showSnackbar(Context context, String str,int type) {
        snackbar = Snackbar.make(((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content), str, Snackbar.LENGTH_INDEFINITE);
        configSnackbar(snackbar);
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        switch (type) {
            case 1:
                setRoundBordersBg(context, snackbar, 1);
                snackbar.show();
                break;
            case 2:
                setRoundBordersBg(context, snackbar, 2);
                snackbar.show();
                snackbar.setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
                break;
            case 3:
                setRoundBordersBg(context, snackbar, 3);
                snackbar.show();
                break;
            case 4:
                break;
            default:
                break;
        }
    }

    public void dismiss() {
        if (snackbar != null) {
                snackbar.dismiss();

        }
        snackbar = null;
    }

    public void configSnackbar(Snackbar snack) {
        addMargins(snack);
        ViewCompat.setElevation(snack.getView(), 6f);
    }

    private  void addMargins(Snackbar snack) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) snack.getView().getLayoutParams();
        params.setMargins(12, 12, 12, 12);
        snack.getView().setLayoutParams(params);
    }

    private  void setRoundBordersBg(Context context, Snackbar snackbar, int type) {
        if (type==1)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg_red));
        if (type==2)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg_green));
        if (type==3)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg));
        if (type==4)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg_success));
    }



}
