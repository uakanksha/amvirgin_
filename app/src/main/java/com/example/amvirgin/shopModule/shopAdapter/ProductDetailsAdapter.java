package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.ProductDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsAdapter.ProductDetailsViewHolder> {
    Context context;
    List<ProductDetailsModel.Detail> details;

    ArrayList val;
    String valStr;

    public ProductDetailsAdapter(Context context, List<ProductDetailsModel.Detail> details) {
        this.context = context;
        this.details = details;
    }

    @NonNull
    @Override
    public ProductDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_product_details_layout, parent, false);
        ProductDetailsViewHolder holder = new ProductDetailsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductDetailsViewHolder holder, int position) {

        holder.tv_label.setText(details.get(position).getLabel());

        if (details.get(position).getValue().getClass().
                getSimpleName().equalsIgnoreCase("ArrayList")) {
            val = (ArrayList) details.get(position).getValue();

            holder.tv_value.setText(val.get(0).toString()+"  ");

        } else if (details.get(position).getValue()
                .getClass().getSimpleName().equalsIgnoreCase("String")) {
            valStr = (String) details.get(position).getValue();

            holder.tv_value.setText(valStr+"  ");
        }

    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public class ProductDetailsViewHolder extends RecyclerView.ViewHolder{
        TextView tv_label, tv_value;

        public ProductDetailsViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_label = itemView.findViewById(R.id.tv_label);
            tv_value = itemView.findViewById(R.id.tv_value);
        }
    }

}
