package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class OrderListModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("address")
        @Expose
        private Address address;
        @SerializedName("orderNumber")
        @Expose
        private String orderNumber;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("subTotal")
        @Expose
        private Integer subTotal;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("total")
        @Expose
        private Integer total;
        @SerializedName("paymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("items")
        @Expose
        private List<Item> items = null;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

    }

    public class Address {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("pinCode")
        @Expose
        private Integer pinCode;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("locality")
        @Expose
        private String locality;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("saturdayWorking")
        @Expose
        private Boolean saturdayWorking;
        @SerializedName("sundayWorking")
        @Expose
        private Boolean sundayWorking;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Integer getPinCode() {
            return pinCode;
        }

        public void setPinCode(Integer pinCode) {
            this.pinCode = pinCode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getLocality() {
            return locality;
        }

        public void setLocality(String locality) {
            this.locality = locality;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean getSaturdayWorking() {
            return saturdayWorking;
        }

        public void setSaturdayWorking(Boolean saturdayWorking) {
            this.saturdayWorking = saturdayWorking;
        }

        public Boolean getSundayWorking() {
            return sundayWorking;
        }

        public void setSundayWorking(Boolean sundayWorking) {
            this.sundayWorking = sundayWorking;
        }

    }

    public class Item {

        @SerializedName("product")
        @Expose
        private Product product;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("total")
        @Expose
        private Integer total;
        @SerializedName("options")
        @Expose
        private List<Option> options = null;

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getTotal() {
            return total;
        }

        public void setTotal(Integer total) {
            this.total = total;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

    }

    public class Product {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("price")
        @Expose
        private Price price;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("image")
        @Expose
        private String image;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Price getPrice() {
            return price;
        }

        public void setPrice(Price price) {
            this.price = price;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }

    public class Price {

        @SerializedName("original")
        @Expose
        private Integer original;
        @SerializedName("selling")
        @Expose
        private Integer selling;

        public Integer getOriginal() {
            return original;
        }

        public void setOriginal(Integer original) {
            this.original = original;
        }

        public Integer getSelling() {
            return selling;
        }

        public void setSelling(Integer selling) {
            this.selling = selling;
        }

    }

    public class Option {

        @SerializedName("group")
        @Expose
        private String group;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("value")
        @Expose
        private Object value;
        @SerializedName("interface")
        @Expose
        private String _interface;

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public String getInterface() {
            return _interface;
        }

        public void setInterface(String _interface) {
            this._interface = _interface;
        }

    }

    public static class DataStateDeserializer implements JsonDeserializer<OrderListModel> {

        @Override
        public OrderListModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            OrderListModel model = new Gson().fromJson(json,OrderListModel.class);

            // Product Options

            Type type = new TypeToken<OrderListModel.Option>() {
            }.getType();

            OrderListModel.Option option = new Gson().fromJson(json, type);
            JsonObject object = json.getAsJsonObject();

            if (object.has("value")){
                JsonElement element = object.get("value");

                if (element != null && !element.isJsonNull()){

                    if (element.isJsonPrimitive()){
                        option.setValue(element.getAsString());
                    }
                    else {
                        option.setValue(element.getAsJsonArray());
                    }
                }
            }

            return model;
        }
    }


}

