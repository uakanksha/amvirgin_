package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ProductDetailActivity;
import com.example.amvirgin.shopModule.shopModel.ShopSearchModel;

import java.util.List;

public class ProductSearchAdapter extends RecyclerView.Adapter<ProductSearchAdapter.ProductSearchViewHolder> {
    Context context;
    List<ShopSearchModel.Datum> data;

    public ProductSearchAdapter(Context context, List<ShopSearchModel.Datum> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ProductSearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_product_search_layout, parent, false);
        ProductSearchViewHolder holder = new ProductSearchViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductSearchViewHolder holder, final int position) {
        holder.tv_product_name.setText(data.get(position).getName());

        holder.tv_product_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int ProductId = data.get(position).getKey();
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("ProductId", ProductId);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ProductSearchViewHolder extends RecyclerView.ViewHolder{
        TextView tv_product_name;

        public ProductSearchViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_product_name = itemView.findViewById(R.id.tv_product_name);
        }
    }

}
