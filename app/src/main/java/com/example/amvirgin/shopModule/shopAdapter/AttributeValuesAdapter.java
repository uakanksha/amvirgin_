package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.Adapter.ProductVariantsAdapter;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.interfaces.AttributeSelectionListener;
import com.example.amvirgin.shopModule.shopModel.ProductDetailsModel;

import java.util.ArrayList;
import java.util.List;

public class AttributeValuesAdapter extends RecyclerView.Adapter<AttributeValuesAdapter.AttributeValuesViewHolder> {
    Context context;
    List<ProductDetailsModel.Variant> variants;
    AttributeSelectionListener attributeSelectionListener;
    private TextView lastCheckedTV = null;
    int row_index = -1;

    ArrayList val;
    String valStr;

    public AttributeValuesAdapter(Context context, List<ProductDetailsModel.Variant> variants,
                                  AttributeSelectionListener attributeSelectionListener) {
        this.context = context;
        this.variants = variants;
        this.attributeSelectionListener = attributeSelectionListener;
    }

    @NonNull
    @Override
    public AttributeValuesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_attribute_values_layout, parent, false);
        AttributeValuesViewHolder holder = new AttributeValuesViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AttributeValuesViewHolder holder, final int position) {

        if (variants.get(position).getOptions().size() == 0){
            holder.main_ll.setVisibility(View.GONE);
        }
        else {

            LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            holder.rv_variants.setLayoutManager(manager);

            ProductVariantsAdapter adapter = new ProductVariantsAdapter(context, null, variants.get(position).getOptions(),
                    new AttributeSelectionListener() {
                        @Override
                        public void onItemClick(int key) {
                            if (attributeSelectionListener != null) {
                                attributeSelectionListener.onItemClick(
                                        variants.get(position).getKey());
                            }
                        }
                    });
            holder.rv_variants.setAdapter(adapter);


           /* holder.main_ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    row_index = position;
                    notifyDataSetChanged();
                    if (attributeSelectionListener != null) {
                        attributeSelectionListener.onItemClick(
                                variants.get(position).getKey());
                    }
                }
            });
*/
        }

    }

    @Override
    public int getItemCount() {
        return variants.size();
    }

    public class AttributeValuesViewHolder extends RecyclerView.ViewHolder{
        RecyclerView rv_variants;
        LinearLayout main_ll;

        public AttributeValuesViewHolder(@NonNull View itemView) {
            super(itemView);

            rv_variants = itemView.findViewById(R.id.rv_variants);
            main_ll = itemView.findViewById(R.id.main_ll);
        }
    }

}
