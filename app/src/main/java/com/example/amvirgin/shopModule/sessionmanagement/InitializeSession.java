package com.example.amvirgin.shopModule.sessionmanagement;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InitializeSession {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("session")
    @Expose
    private String session;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

}