package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.gesture.GestureLibraries;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.BestTrandsModel;
import com.example.amvirgin.shopModule.shopModel.CategoryModel;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class BestTrendsAdapter extends RecyclerView.Adapter<BestTrendsAdapter.ItemViewHolder> {

   private List<ShopHomeModel.TrendingDeal> bestTrendsList;
   private Context mCtx;
   private int value;

    public BestTrendsAdapter(Context context) {
        this.mCtx = context;
//        this.value = i;
//        switch (i) {
//            case 0:
                this.bestTrendsList = Constants.shopTreningDeals;
//                break;
//            default:
//                break;
//        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        switch (value) {
//            case 0:
                return new ItemViewHolder(LayoutInflater.from(mCtx).inflate(R.layout.rv_trending_deals_layout, parent, false));
//            default:
//                return null;
//        }
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

//       switch (value){
//           case 0:
              // final BestTrandsModel model=bestTrendsList.get(position);
               holder.itemTitle.setText(bestTrendsList.get(position).getName());
               if (bestTrendsList.get(position).getImages().size() > 0){
                   Glide.with(mCtx).load(bestTrendsList.get(position).getImages().get(0)).into(holder.itemImage);
               }
//               holder.itemImage.setImageResource(model.getImage());
//               break;
//           default:
//               break;
//
//       }

    }

    @Override
    public int getItemCount() {
//        switch (value) {
//            case 0:
                return bestTrendsList.size();
//            default:
//                return 0;
//        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView itemImage;
        private TextView itemTitle;

        public ItemViewHolder(View view) {
            super(view);
//            switch (value) {
//                case 0:
            itemImage = itemView.findViewById(R.id.banner_img);
            itemTitle = itemView.findViewById(R.id.name);
//                    break;
//                default:
//                    break;
//            }
//        }
        }
    }

}
