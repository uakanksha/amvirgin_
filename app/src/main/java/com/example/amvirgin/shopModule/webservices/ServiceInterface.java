package com.example.amvirgin.shopModule.webservices;

import com.example.amvirgin.classes.ChangePasswordModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.ListWatchLaterModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SectionCollectionModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SubscriptionsListModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.WatchLaterModel;
import com.example.amvirgin.shopModule.BeanModel.NavigationItemBeanModel;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.sessionmanagement.ValidateSession;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.CountryListModel;
import com.example.amvirgin.shopModule.shopModel.DeleteAddressModel;
import com.example.amvirgin.shopModule.shopModel.DestroyCartModel;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;
import com.example.amvirgin.shopModule.shopModel.GetWishlistModel;
import com.example.amvirgin.shopModule.shopModel.MoveToWishlistModel;
import com.example.amvirgin.shopModule.shopModel.OrderDetailsModel;
import com.example.amvirgin.shopModule.shopModel.OrderListModel;
import com.example.amvirgin.shopModule.shopModel.OrderTrackingModel;
import com.example.amvirgin.shopModule.shopModel.PlaceOrderModel;
import com.example.amvirgin.shopModule.shopModel.ProductDetailsModel;
import com.example.amvirgin.shopModule.shopModel.ProductFiltersModel;
import com.example.amvirgin.shopModule.shopModel.ProductListModel;
import com.example.amvirgin.shopModule.shopModel.RazorPayOrdersModel;
import com.example.amvirgin.shopModule.shopModel.SaveAddressModel;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.shopModule.shopModel.ShopSearchModel;
import com.example.amvirgin.shopModule.shopModel.ShopTrendingDealsModel;
import com.example.amvirgin.shopModule.shopModel.SortListModel;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface ServiceInterface {


    @GET("customer/categories")
    @Headers({
            "Content: application/json"})
    Call<NavigationItemBeanModel> getNavigationItem();

    //Get Product List
    @GET()
    Call<ProductListModel> getProductList(@Url String url);

    //Get Sort List
    @GET("customer/products/sorts")
    Call<SortListModel> getSortList();

    //Get Product Details
    @GET()
    Call<ProductDetailsModel> getProductDetails(@Url String url);

    //Initialize Session
    @GET("customer/sessions/start")
    Call<InitializeSession> initializeSession();

    //Validate Session
    @GET()
    Call<ValidateSession> validateSession(@Url String url);

    //Add To Cart
    @POST("customer/cart/add")
    Call<AddToCartModel> addToCart(@Body JsonObject jsonObject,
                                   @Header("Content-Type") String content,
                                   @Header("Authorization") String authorization);

    //Retrieve Cart
    @Headers({
            "Content-Type: application/json",
            "Cache-Control: no-cache"
    })
    @GET()
    Call<AddToCartModel> retrieveCart(@Url String url);

    //Destroy Cart
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("customer/cart/destroy")
    Call<DestroyCartModel> destroyCart(@Body JsonObject jsonObject);

    //Update Quantity in Cart
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("customer/cart/update")
    Call<AddToCartModel> updateCart(@Body JsonObject jsonObject);

    //Move To Wishlist
    @PUT("customer/cart/wishlist/{id}")
    Call<MoveToWishlistModel> moveToWishlist( @Path("id") String id,
                                              @Header("Content-Type") String content,
                                              @Header("Authorization") String authorization,
                                              @Body JsonObject jsonObject);

    //Get Wishlist
    @Headers({
            "Cache-Control: no-cache"})
    @GET("customer/wishlist")
    Call<GetWishlistModel> getWishlist(@Header("Content-Type") String content,
                                       @Header("Authorization") String authorization,
                                       @Header("Cache-Control") String cache);

    //Move To Cart
    @PUT("customer/wishlist/cart/{id}")
    Call<MoveToWishlistModel> moveToCart( @Path("id") String id,
                                              @Header("Content-Type") String content,
                                              @Header("Authorization") String authorization,
                                              @Body JsonObject jsonObject);

    //Delete From Wishlist
    @DELETE("customer/wishlist/{id}")
    Call<MoveToWishlistModel> deleteFromWishlist(@Path("id") String id,
                                                 @Header("Content-Type") String content,
                                                 @Header("Authorization") String authorization);

    //Add To Wishlist
    @PUT("customer/wishlist/{id}")
    Call<MoveToWishlistModel> addToWishlist(@Path("id") String id,
                                            @Header("Content-Type") String content,
                                            @Header("Authorization") String authorization);

    //Get Country List
    @GET("customer/countries")
    Call<CountryListModel> getCountryList();

    //Get State List
    @GET()
    Call<CountryListModel> getStateList(@Url String url);

    //Get City List
    @GET()
    Call<CountryListModel> getCityList(@Url String url);

    //Save Address
    @POST("customer/addresses")
    Call<SaveAddressModel> saveAddress(@Header("Content-Type") String content,
                                       @Header("Authorization") String authorization,
                                       @Body JsonObject jsonObject);

    //Get Address
    @GET("customer/addresses")
    Call<GetAddressModel> getAddress(@Header("Content-Type") String content,
                                     @Header("Authorization") String authorization,
                                     @Header("Cache-Control") String cache);

    //Get Entertainment Home Data
    @GET("customer/entertainment/homepage")
    Call<HomeModel> getHomeData();

    //Get Entertainment Section Collection List
    @GET()
    Call<SectionCollectionModel> getSectionCollectionList(@Url String url);

    //Get Entertainment Section Subscriptions List
    @GET("customer/subscriptions")
    Call<SubscriptionsListModel> getSubscriptionsList();

    //Place Order
    @POST("customer/cart/submit")
    Call<PlaceOrderModel> placeOrder(@Header("Content-Type") String content,
                                     @Header("Authorization") String authorization,
                                     @Body JsonObject jsonObject);

    //Get Shop Home Screen Data
    @GET("customer/shop/homepage")
    Call<ShopHomeModel> getShopHomeScreenData();

    //Get All Trending Deals
    @GET("customer/shop/deals")
    Call<ShopTrendingDealsModel> getAllTrendingDeals();


    //Get Search Result Shopping Module
    @GET()
    Call<ShopSearchModel> getShopSearch(@Url String url);

    //Get Search Result Entertain Module
    @GET()
    Call<SectionCollectionModel> getVideoSearch(@Url String url);

    //Get Order List
    @GET("customer/orders")
    Call<OrderListModel> getOrderList(@Header("Content-Type") String content,
                                      @Header("Authorization") String authorization);

    @GET()
    Call<OrderDetailsModel> getOrderDetails(@Url String url,
                                            @Header("Content-Type") String content,
                                            @Header("Authorization") String authorization);

    @GET()
    Call<HomeModel> getTrendingData(@Url String url);

    //RazorPay Create Order
    @POST("https://api.razorpay.com/v1/orders")
    Call<RazorPayOrdersModel> createOrder(@Header("Content-Type") String content,
                                          @Body JSONObject jsonObject);

    //Delete Address
    @DELETE("customer/addresses/{id}")
    Call<DeleteAddressModel> deleteAddress(@Path("id") String id,
                                           @Header("Content-Type") String content,
                                           @Header("Authorization") String authorization);

    //Update Address
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("customer/addresses/{id}")
    Call<SaveAddressModel> updateAddress(@Path("id") String id,
                                      @Header("Content-Type") String content,
                                      @Header("Authorization") String authorization,
                                      @Body JsonObject jsonObject);

    @FormUrlEncoded
    @POST("customer/watch-later")
    Call<WatchLaterModel> addWatchLater(@Header("Authorization") String authorization,
                                        @Field("video_id") int videoId);
    @GET("customer/watch-later")
    Call<ListWatchLaterModel> listWatchLater(@Header("Authorization") String authorization);

    @DELETE()
    Call<WatchLaterModel> removeWatchLater(@Header("Authorization") String authorization,
                                           @Url String url);

    //Get Order Tracking Details
    @GET()
    Call<OrderTrackingModel> getTrackingDetails(@Url String url,
                                                @Header("Content-Type") String content,
                                                @Header("Authorization") String authorization);

    //Get Product Filters
    @GET()
    Call<ProductFiltersModel> getProductFilters(@Url String url);

    //Change Password
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("customer/profile/password")
    Call<ChangePasswordModel> changePassword(@Header("Content-Type") String content,
                                            @Header("Authorization") String authorization,
                                            @Body JsonObject jsonObject);

    //Change Customer Avatar
    @Multipart
    @POST("customer/profile/avatar")
    Call<ChangePasswordModel> changeAvatar(@Header("Authorization") String authorization,
                                           @Part MultipartBody.Part avatar);

    //Update Profile
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("customer/profile")
    Call<ChangePasswordModel> updateProfile(@Header("Content-Type") String content,
                                             @Header("Authorization") String authorization,
                                             @Body JsonObject jsonObject);

    //Submit Query through support form
    @POST("customer/contact-us")
    Call<ChangePasswordModel> submitQuery(@Header("Content-Type") String content,
                                     @Header("Authorization") String authorization,
                                     @Body JsonObject jsonObject);


//    @GET("customer/categories")
//    @Headers({
//            "Content: application/json", "Cache-Control: no-cache"})
//    Call<NavigationItemBeanModel> getNavigationItem();
}
