package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.amvirgin.MovieDetailActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.Slide;
import com.example.amvirgin.shopModule.shopModel.BannerModel;

import java.util.ArrayList;
import java.util.List;

public class BannerPagerAdapter extends PagerAdapter {

    private List<BannerModel> BannerList;
    private Context context;
    private LayoutInflater inflater;
    public BannerPagerAdapter(Context context, List<BannerModel> BannerList) {
        this.context = context;
        this.BannerList = BannerList;
    }
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.banner_item_layout, view, false);
        ImageView imageView =itemView.findViewById(R.id.bannerImg);
        RelativeLayout bannerContainer =itemView.findViewById(R.id.banner_container);
        imageView.setImageResource(BannerList.get(position).getImage());
        bannerContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MovieDetailActivity.class);
                context.startActivity(intent);
            }
        });
        view.addView(itemView);

        return itemView;
    }
    @Override
    public int getCount() {
        return BannerList.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
