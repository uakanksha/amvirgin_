package com.example.amvirgin.shopModule.Other;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.view.ViewCompat;

import com.example.amvirgin.R;
import com.google.android.material.snackbar.Snackbar;

public class SnackbarManager {

    public static Snackbar snackbar;
    public static void sNoInternet(Context context, String str) {
        dismiss();
        if (snackbar ==null) {
            snackbar = Snackbar.make(((Activity) context).getWindow().getDecorView().
                    findViewById(android.R.id.content), str, Snackbar.LENGTH_INDEFINITE);
        }
        configSnackbar(context,snackbar);
        setRoundBordersBg(context,snackbar,1);
        View sbView = snackbar.getView();
        TextView textView =sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void FoundInternet(Context context, String str) {
        dismiss();
        if (snackbar ==null) {
            snackbar = Snackbar.make(((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content), str, Snackbar.LENGTH_SHORT);
        }
        snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        configSnackbar(context,snackbar);
        setRoundBordersBg(context,snackbar,2);
        View sbView = snackbar.getView();
        TextView textView =sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }




    public static void sException(Context context, String str) {
        dismiss();
        if (snackbar ==null){
              snackbar = Snackbar.make(((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content), str, Snackbar.LENGTH_LONG);
        }
        snackbar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        configSnackbar(context,snackbar);
        setRoundBordersBg(context,snackbar,3);
        View sbView = snackbar.getView();
        TextView textView =sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        //shakeAnimation(sbView);
        snackbar.show();
    }

    public static void successResponse(Context context, String str) {
        dismiss();
        if (snackbar ==null) {
            snackbar = Snackbar.make(((Activity) context).getWindow().getDecorView().
                    findViewById(android.R.id.content), str, Snackbar.LENGTH_INDEFINITE);
        }
        configSnackbar(context,snackbar);
        setRoundBordersBg(context,snackbar,4);
        View sbView = snackbar.getView();
        TextView textView =sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void dismiss() {
        if (snackbar != null) {

                snackbar.dismiss();

        }
        snackbar = null;
    }

    public static void configSnackbar(Context context, Snackbar snack) {
        addMargins(snack);
        ViewCompat.setElevation(snack.getView(), 6f);
    }

    private static void addMargins(Snackbar snack) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) snack.getView().getLayoutParams();
        params.setMargins(12, 12, 12, 12);
        snack.getView().setLayoutParams(params);
    }

    private static void setRoundBordersBg(Context context, Snackbar snackbar, int type) {
        if (type==1)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg_red));
        if (type==2)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg_green));
        if (type==3)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg));
        if (type==4)
            snackbar.getView().setBackground(context.getDrawable(R.drawable.snack_bg_success));
    }


}
