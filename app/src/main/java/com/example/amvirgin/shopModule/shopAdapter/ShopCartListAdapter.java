package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.DestroyCartModel;
import com.example.amvirgin.shopModule.shopModel.MoveToWishlistModel;
import com.example.amvirgin.shopModule.shopModel.OrderDetailsModel;
import com.example.amvirgin.shopModule.shopModel.OrderListModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;


public class ShopCartListAdapter extends RecyclerView.Adapter<ShopCartListAdapter.ShopCartViewHolder> implements AdapterView.OnItemSelectedListener{

    Context context;
    String[] quantity = { "QTY","1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    ArrayList<Integer> qty;
    List<AddToCartModel.Item> data;
    String session_id, token;
    int cart_count, wishlist_count;
    RemoveFromCartClickListener removeFromCartClickListener;
    String message;
    String type;
    List<OrderListModel.Option> options_ = null;
    List<OrderDetailsModel.Option> opt = null;

    public ShopCartListAdapter(Context context, List<AddToCartModel.Item> data,
                               String type, RemoveFromCartClickListener removeFromCartClickListener) {
        this.context = context;
        this.data = data;
        this.removeFromCartClickListener = removeFromCartClickListener;
        this.type = type;
    }

    @NonNull
    @Override
    public ShopCartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cartproduct_layout, parent, false);
        ShopCartViewHolder cvh = new ShopCartViewHolder(view);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopCartViewHolder holder, final int position) {

        if (type.equalsIgnoreCase("order")){
            holder.bottom_ll.setVisibility(View.GONE);
        }
        session_id = PreferenceManager.getString(context, "session_id");
        cart_count = PreferenceManager.getInt(context, "cart_count");
        if (PreferenceManager.getBoolean(context, Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(context).getToken();
        }

        holder.tv_product_name.setText(data.get(position).getProduct().getName());
        holder.tv_product_price.setText(String.valueOf("₹ "+data.get(position).getProduct().getPrice().getSelling()));
        holder.price_withoutOffer.setText(String.valueOf("₹ "+data.get(position).getProduct().getPrice().getOriginal()));
        if (data.get(position).getProduct().getImage() != null) {
            Glide.with(context).load(data.get(position).getProduct().getImage()).into(holder.iv_product_img);
        }
        else {
            holder.iv_product_img.setImageResource(R.drawable.placeholder);
        }

        LinearLayoutManager manager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        holder.rv_product_attributes.setLayoutManager(manager);

        ProductAttributeAdapter adapter = new ProductAttributeAdapter(context, data.get(position).getProduct().getOptions(),
                options_, opt);
        holder.rv_product_attributes.setAdapter(adapter);

//        holder.tv_qty.setText(String.valueOf(data.get(position).getQuantity()));

        qty = new ArrayList<Integer>();

        for (int i = 1; i <=data.get(position).getProduct().getMaxAllowedQuantity(); i++){
            qty.add(i);
        }

        holder.spinner_qty.setOnItemSelectedListener(this);
        final ArrayAdapter<Integer> aa = new ArrayAdapter<Integer>(context,android.R.layout.simple_spinner_item, qty){
            @Override
            public boolean isEnabled(int position) {
               /* if(position == 0){
                    return false;
                }
                else {
                    return true;
                }*/
               return true;
            }
        };
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        holder.spinner_qty.setAdapter(aa);
        holder.spinner_qty.setSelection(aa.getPosition(data.get(position).getQuantity()));

        holder.spinner_qty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (i != aa.getPosition(data.get(position).getQuantity())) {
                        updateCart(data.get(position).getQuantity(), data.get(position).getKey(),
                                holder.spinner_qty.getSelectedItem().toString(), holder);
                    }
                    else {
                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        holder.move_to_wishlist_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (token != null) {
                    moveToWishlist(data.get(position).getKey(), session_id, token, data.get(position).getQuantity(), holder);
                }
                else {
                    PreferenceManager.saveString(context, "redirect", "add_wishlist");
                    Intent intent = new Intent(context, LoginActivity.class);
//                    intent.putExtra("notlogin", "notlogin");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });

        holder.price_withoutOffer.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        holder.removeFromCart_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message = "Item Removed from Cart!";
                removeFromCart(data.get(position).getKey(), data.get(position).getQuantity(), holder, message);

            }
        });

    }

    private void moveToWishlist(final int key, String session_id, String token, final Integer quantity, final ShopCartViewHolder holder){
//        Toast.makeText(context, "Moved To Wishlist!!", Toast.LENGTH_SHORT).show();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("sessionId", session_id);

        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<MoveToWishlistModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .moveToWishlist(String.valueOf(key), "application/json", "Bearer "+token, jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<MoveToWishlistModel>(context) {
            @Override
            public void onResponse(MoveToWishlistModel response) {
                if (response.getStatus() == 200){
                    try{
                        ToastMessage.getInstance(context).showSuccessShortCustomToast(response.getMessage());

//                        Intent intent = new Intent(context, CartProductShopActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        context.startActivity(intent);

                        int new_count = cart_count - quantity;
                        PreferenceManager.saveInt(context, "cart_count", new_count);
                        int new_wish_count = wishlist_count + 1;
                        PreferenceManager.saveInt(context, "wishlist_count", new_wish_count);

                        if (removeFromCartClickListener != null){
                            removeFromCartClickListener.onItemClick();
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                    }
                }
                else if (response.getStatus() == 409){
                    try {
                        message = "Item moved to Wishlist!";
                        removeFromCart(key, quantity, holder, message);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                }
            }
        });
    }

    private void updateCart(final Integer quantity, Integer key, final String i, final ShopCartViewHolder holder) {

        final int new_i = Integer.parseInt(i);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("sessionId", session_id);
        jsonObject.addProperty("key", key);
        jsonObject.addProperty("quantity", new_i);


        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<AddToCartModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .updateCart(jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<AddToCartModel>(context) {
            @Override
            public void onResponse(AddToCartModel response) {
                if (response.getStatus() == 200){
                    try {

                        ToastMessage.getInstance(context).showSuccessShortCustomToast(response.getMessage());
                        int new_count = cart_count - quantity;
                        new_count = new_count + new_i;
                        PreferenceManager.saveInt(context, "cart_count", new_count);

//                        Intent intent = new Intent(context, CartProductShopActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        context.startActivity(intent);

                        if (removeFromCartClickListener != null){
                            removeFromCartClickListener.onItemClick();
                        }

                    }
                    catch (Exception e){
                        SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void removeFromCart(int key, final Integer quantity, final ShopCartViewHolder holder, final String message) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("sessionId", session_id);
        jsonObject.addProperty("key", key);
//        jsonObject.addProperty("customerId", customerId);


        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<DestroyCartModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class).destroyCart(jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<DestroyCartModel>(context) {
            @Override
            public void onResponse(DestroyCartModel response) {
                if (response.getStatus() == 200){
                    try {
                        ToastMessage.getInstance(context).showSuccessShortCustomToast(message);

//                        Intent intent = new Intent(context, CartProductShopActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        context.startActivity(intent);

                        int new_count = cart_count - quantity;

                        PreferenceManager.saveInt(context, "cart_count", new_count);

                        if (removeFromCartClickListener != null){
                            removeFromCartClickListener.onItemClick();
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                }
            }
        });
    }

    public class ShopCartViewHolder extends RecyclerView.ViewHolder {
        LinearLayout bottom_ll;
        LinearLayout removeFromCart_ll, move_to_wishlist_ll;
        Spinner spinner_qty;
        TextView price_withoutOffer, tv_product_name, tv_product_price;
        ImageView iv_product_img;
        RecyclerView rv_product_attributes;

        public ShopCartViewHolder(@NonNull View itemView) {
            super(itemView);
            bottom_ll = itemView.findViewById(R.id.bottom_ll);
            price_withoutOffer=itemView.findViewById(R.id.price_withoutOffer);
            spinner_qty=itemView.findViewById(R.id.qty_spinner);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_product_price = itemView.findViewById(R.id.tv_product_price);
            iv_product_img = itemView.findViewById(R.id.iv_product_img);
            removeFromCart_ll = itemView.findViewById(R.id.removeFromCart_ll);
            rv_product_attributes = itemView.findViewById(R.id.rv_product_attributes);
            move_to_wishlist_ll = itemView.findViewById(R.id.move_to_wishlist_ll);
        }
    }
}