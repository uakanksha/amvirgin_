package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class AddToCartModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("cart")
        @Expose
        private Cart cart;

        public Cart getCart() {
            return cart;
        }

        public void setCart(Cart cart) {
            this.cart = cart;
        }

    }

    public class Cart {

        @SerializedName("session")
        @Expose
        private String session;
        @SerializedName("address")
        @Expose
        private Object address;
        @SerializedName("customer")
        @Expose
        private Customer customer;
        @SerializedName("itemCount")
        @Expose
        private Integer itemCount;
        @SerializedName("subTotal")
        @Expose
        private Integer subTotal;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("total")
        @Expose
        private Double total;
        @SerializedName("paymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("items")
        @Expose
        private List<Item> items = null;

        public String getSession() {
            return session;
        }

        public void setSession(String session) {
            this.session = session;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public Integer getItemCount() {
            return itemCount;
        }

        public void setItemCount(Integer itemCount) {
            this.itemCount = itemCount;
        }

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

    }

    public class Customer {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile")
        @Expose
        private String mobile;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

    }

    public class Item {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("product")
        @Expose
        private Product product;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("uniqueId")
        @Expose
        private String uniqueId;
        @SerializedName("itemTotal")
        @Expose
        private Integer itemTotal;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public Integer getItemTotal() {
            return itemTotal;
        }

        public void setItemTotal(Integer itemTotal) {
            this.itemTotal = itemTotal;
        }

    }

    public class Product {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("price")
        @Expose
        private Price price;
        @SerializedName("maxAllowedQuantity")
        @Expose
        private Integer maxAllowedQuantity;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("options")
        @Expose
        private List<Option> options = null;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Price getPrice() {
            return price;
        }

        public void setPrice(Price price) {
            this.price = price;
        }

        public Integer getMaxAllowedQuantity() {
            return maxAllowedQuantity;
        }

        public void setMaxAllowedQuantity(Integer maxAllowedQuantity) {
            this.maxAllowedQuantity = maxAllowedQuantity;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

    }

    public class Price {

        @SerializedName("original")
        @Expose
        private Integer original;
        @SerializedName("selling")
        @Expose
        private Integer selling;

        public Integer getOriginal() {
            return original;
        }

        public void setOriginal(Integer original) {
            this.original = original;
        }

        public Integer getSelling() {
            return selling;
        }

        public void setSelling(Integer selling) {
            this.selling = selling;
        }

    }

    public class Option {

        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("interface")
        @Expose
        private String _interface;
        @SerializedName("group")
        @Expose
        private String group;
        @SerializedName("value")
        @Expose
        private Object value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getInterface() {
            return _interface;
        }

        public void setInterface(String _interface) {
            this._interface = _interface;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

    }

    public static class DataStateDeserializer implements JsonDeserializer<AddToCartModel> {

        @Override
        public AddToCartModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            AddToCartModel model = new Gson().fromJson(json, AddToCartModel.class);

            // Product Options

            Type type = new TypeToken<AddToCartModel.Option>() {
            }.getType();

            AddToCartModel.Option option = new Gson().fromJson(json, type);
            JsonObject object = json.getAsJsonObject();

            if (object.has("value")) {
                JsonElement element = object.get("value");

                if (element != null && !element.isJsonNull()) {

                    if (element.isJsonPrimitive()) {
                        option.setValue(element.getAsString());
                    } else {
                        option.setValue(element.getAsJsonArray());
                    }
                }
            }

            return model;
        }
    }

}

