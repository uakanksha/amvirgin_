package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RetrieveCartModel implements Serializable{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable{

        @SerializedName("cart")
        @Expose
        private Cart cart;

        public Cart getCart() {
            return cart;
        }

        public void setCart(Cart cart) {
            this.cart = cart;
        }

    }

    public class Cart implements Serializable {

        @SerializedName("session")
        @Expose
        private String session;
        @SerializedName("address")
        @Expose
        private Object address;
        @SerializedName("customer")
        @Expose
        private Customer customer;
        @SerializedName("itemCount")
        @Expose
        private Integer itemCount;
        @SerializedName("subTotal")
        @Expose
        private Integer subTotal;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("total")
        @Expose
        private Double total;
        @SerializedName("paymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("items")
        @Expose
        private List<Item> items = null;

        public String getSession() {
            return session;
        }

        public void setSession(String session) {
            this.session = session;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public Integer getItemCount() {
            return itemCount;
        }

        public void setItemCount(Integer itemCount) {
            this.itemCount = itemCount;
        }

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

    }

    public class Customer implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile")
        @Expose
        private String mobile;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

    }

    public class Item implements Serializable{

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("product")
        @Expose
        private Product product;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("uniqueId")
        @Expose
        private String uniqueId;
        @SerializedName("itemTotal")
        @Expose
        private Integer itemTotal;
        @SerializedName("attributes")
        @Expose
        private List<Attribute> attributes = null;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(String uniqueId) {
            this.uniqueId = uniqueId;
        }

        public Integer getItemTotal() {
            return itemTotal;
        }

        public void setItemTotal(Integer itemTotal) {
            this.itemTotal = itemTotal;
        }

        public List<Attribute> getAttributes() {
            return attributes;
        }

        public void setAttributes(List<Attribute> attributes) {
            this.attributes = attributes;
        }

    }

    public class Attribute implements Serializable{

        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("size")
        @Expose
        private String size;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

    }

    public class Category implements Serializable{

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Discount implements Serializable{

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("value")
        @Expose
        private Integer value;
        @SerializedName("applicable")
        @Expose
        private Boolean applicable;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public Boolean getApplicable() {
            return applicable;
        }

        public void setApplicable(Boolean applicable) {
            this.applicable = applicable;
        }

    }

    public class Product implements Serializable{

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("category")
        @Expose
        private Category category;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("discount")
        @Expose
        private Discount discount;
        @SerializedName("shortDescription")
        @Expose
        private String shortDescription;
        @SerializedName("images")
        @Expose
        private List<String> images = null;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Category getCategory() {
            return category;
        }

        public void setCategory(Category category) {
            this.category = category;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Discount getDiscount() {
            return discount;
        }

        public void setDiscount(Discount discount) {
            this.discount = discount;
        }

        public String getShortDescription() {
            return shortDescription;
        }

        public void setShortDescription(String shortDescription) {
            this.shortDescription = shortDescription;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

    }

}
