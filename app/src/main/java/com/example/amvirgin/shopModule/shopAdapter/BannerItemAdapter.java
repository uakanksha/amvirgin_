package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.BannerModel;
import com.example.amvirgin.shopModule.shopModel.CategoryModel;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class BannerItemAdapter extends RecyclerView.Adapter<BannerItemAdapter.ItemViewHolder> {
//    private ArrayList<BannerModel> bannerList;
    public static List<ShopHomeModel.ShopSlider> shopSliderList = null;
    private Context mCtx;
    private int value;

    public BannerItemAdapter(Context context) {
        this.mCtx = context;
//        this.value = i;
//        switch (i) {
//            case 0:
                this.shopSliderList = Constants.shopSliderList;
//                break;
//            default:
//                break;
//        }
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        switch (value) {
//            case 0:
        return new ItemViewHolder(LayoutInflater.from(mCtx).inflate(R.layout.banner_item_layout, parent, false));
//            default:
//                return null;
//        }
    }

        @Override
        public void onBindViewHolder (@NonNull ItemViewHolder holder,int position){
//        switch (value){
            if (shopSliderList.size() > 0){
                Glide.with(mCtx).load(shopSliderList.get(position).getBanner()).into(holder.itemImage);
            }
//                break;
//            default:
//                break;
//
//        }
        }

        @Override
        public int getItemCount () {
//        switch (value) {
//            case 0:
            return shopSliderList.size();
//            default:
//                return 0;
//        }
        }

        public class ItemViewHolder extends RecyclerView.ViewHolder {
            private AppCompatImageView itemImage;

            public ItemViewHolder(View view) {
                super(view);
//            switch (value) {
//                case 0:
                itemImage = itemView.findViewById(R.id.bannerImg);
//                    break;
//                default:
//                    break;
//            }
            }
        }

    }

