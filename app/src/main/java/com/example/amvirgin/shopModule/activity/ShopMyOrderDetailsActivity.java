package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amvirgin.Adapter.OrderDeliveryAdapter;
import com.example.amvirgin.Adapter.OrderPriceDetailAdapter;
import com.example.amvirgin.Adapter.OrdersAdapter;
import com.example.amvirgin.R;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.shopModel.OrderDetailsModel;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ShopMyOrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout rl_OrderCart,rl_back;
    private RecyclerView recyclerView_order,recyclerView_orderDelivery,recyclerView_priceDetail;
    private RecyclerView.LayoutManager layoutManager,layoutManager1,layoutManager2;
    private int orderId;
    private String token;

    private TextView tv_order_number, tv_order_status, tv_order_date;
    private TextView tv_name, tv_address, tv_locality, tv_city, tv_state, tv_mobile, tv_type;
    private  TextView tv_price_subtotal, tv_tax, tv_total_price, tv_payment_mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_my_order_details);
        recyclerView_order=(RecyclerView)findViewById(R.id.rcvOrder);
        recyclerView_orderDelivery=(RecyclerView)findViewById(R.id.rcvOrderDelivery);
        rl_OrderCart=(RelativeLayout)findViewById(R.id.rlCart1);
        recyclerView_priceDetail =(RecyclerView)findViewById(R.id.rcvPriceDetail);

        tv_order_number = findViewById(R.id.tv_order_number);
        tv_order_status = findViewById(R.id.tv_order_status);
        tv_order_date = findViewById(R.id.tv_order_date);

        tv_name = findViewById(R.id.tv_name);
        tv_address = findViewById(R.id.tv_address);
        tv_locality = findViewById(R.id.tv_locality);
        tv_city = findViewById(R.id.tv_city);
        tv_state = findViewById(R.id.tv_state);
        tv_mobile = findViewById(R.id.tv_mobile);
        tv_type = findViewById(R.id.tv_type);

        tv_price_subtotal = findViewById(R.id.tv_price_subtotal);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_tax = findViewById(R.id.tv_tax);
        tv_payment_mode = findViewById(R.id.tv_payment_mode);

        if (getIntent().getExtras() != null){
            orderId = getIntent().getExtras().getInt("orderId");
        }
        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        if (token != null) {
            getOrderDetails(orderId, token);
        }

        layoutManager1 = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView_orderDelivery.setLayoutManager(layoutManager1);

        layoutManager2 = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView_priceDetail.setLayoutManager(layoutManager2);


        OrderDeliveryAdapter orderDeliveryAdapter =new OrderDeliveryAdapter(getApplicationContext());
        recyclerView_orderDelivery.setAdapter(orderDeliveryAdapter);

        OrderPriceDetailAdapter orderPriceDetailAdapter =new OrderPriceDetailAdapter(getApplicationContext());
        recyclerView_priceDetail.setAdapter(orderPriceDetailAdapter);


        rl_back = (RelativeLayout) findViewById(R.id.rlBack);
        rl_back.setOnClickListener(this);
        rl_OrderCart.setOnClickListener(this);
    }

    private void getOrderDetails(int orderId, String token){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(OrderDetailsModel.class, new OrderDetailsModel.DataStateDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceInterface service = retrofit.create(ServiceInterface.class);

        Call<OrderDetailsModel> call = service.getOrderDetails("customer/orders/"+orderId, "application/json", "Bearer "+token);
        call.enqueue(new Callback<OrderDetailsModel>() {
            @Override
            public void onResponse(Call<OrderDetailsModel> call, Response<OrderDetailsModel> response) {
                if (response.body().getStatus() == 200){
                    try {
                        if (response.body().getData() != null){
                            OrderDetailsModel.Datum data = response.body().getData();
                            tv_order_number.setText(data.getOrderNumber());

                            layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerView_order.setLayoutManager(layoutManager);

                            OrdersAdapter ordersAdapter = new OrdersAdapter(getApplicationContext(), data.getItems(),
                                    response.body().getData().getStatus(), response.body().getData().getTotal());
                            recyclerView_order.setAdapter(ordersAdapter);

                            tv_order_status.setText(data.getStatus());
//                            tv_order_date.setText(data.getCreatedAt());
                            tv_order_date.setVisibility(View.GONE);

                            tv_name.setText(data.getAddress().getName());
                            tv_address.setText(data.getAddress().getAddress());
                            tv_locality.setText(data.getAddress().getLocality());
//                            tv_city.setText(data.getAddress().getCity().getName());
//                            tv_state.setText(data.getAddress().getState().getName());

                            tv_state.setVisibility(View.GONE);
                            tv_city.setVisibility(View.GONE);

                            tv_mobile.setText(data.getAddress().getMobile());
                            tv_type.setText(data.getAddress().getType());

                            tv_price_subtotal.setText(String.valueOf("₹ "+data.getSubTotal()));
                            tv_tax.setText(String.valueOf("₹ "+data.getTax()));
                            tv_total_price.setText(String.valueOf("₹ "+data.getTotal()));
                            tv_payment_mode.setText(data.getPaymentMode());

                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ShopMyOrderDetailsActivity.this, response.body().getMessage());
                    }

                }
                else {
                    SnackbarManager.sException(ShopMyOrderDetailsActivity.this,response.body().getMessage());
//                    ToastMessage.getInstance(ProductDetailActivity.this).showErrorShortCustomToast(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsModel> call, Throwable t) {
            }
        });
    }

    /*private void getOrderDetails(int orderId, String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(ShopMyOrderDetailsActivity.this);
        Call<OrderListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getOrderDetails("customer/orders/"+orderId, "application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<OrderListModel>(ShopMyOrderDetailsActivity.this) {
            @Override
            public void onResponse(OrderListModel response) {
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0){
                            OrderListModel.Datum data = response.getData().get(0);
                            tv_order_number.setText(data.getOrderNumber());

                            layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerView_order.setLayoutManager(layoutManager);

                            OrdersAdapter ordersAdapter = new OrdersAdapter(getApplicationContext(), data.getItems(),
                                    response.getData().get(0).getStatus(), response.getData().get(0).getTotal());
                            recyclerView_order.setAdapter(ordersAdapter);

                            tv_order_status.setText(data.getStatus());
//                            tv_order_date.setText(data.getCreatedAt());
                            tv_order_date.setVisibility(View.GONE);

                            tv_name.setText(data.getAddress().getName());
                            tv_address.setText(data.getAddress().getAddress());
                            tv_locality.setText(data.getAddress().getLocality());
//                            tv_city.setText(data.getAddress().getCity().getName());
//                            tv_state.setText(data.getAddress().getState().getName());

                            tv_state.setVisibility(View.GONE);
                            tv_city.setVisibility(View.GONE);

                            tv_mobile.setText(data.getAddress().getMobile());
                            tv_type.setText(data.getAddress().getType());

                            tv_price_subtotal.setText(String.valueOf("₹ "+data.getSubTotal()));
                            tv_tax.setText(String.valueOf("₹ "+data.getTax()));
                            tv_total_price.setText(String.valueOf("₹ "+data.getTotal()));
                            tv_payment_mode.setText(data.getPaymentMode());

                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ShopMyOrderDetailsActivity.this, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ShopMyOrderDetailsActivity.this, response.getMessage());
                }
            }
        });
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.rlCart1:
                startActivity(new Intent(getApplicationContext(), CartProductShopActivity.class));
               break;
        }

    }
}
