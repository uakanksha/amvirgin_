package com.example.amvirgin.shopModule.BeanModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class NavigationItemBeanModel {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum implements Serializable {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("products")
        @Expose
        private Integer products;
        @SerializedName("icon")
        @Expose
        private Icon icon;
        @SerializedName("children")
        @Expose
        private Children children;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getProducts() {
            return products;
        }

        public void setProducts(Integer products) {
            this.products = products;
        }

        public Icon getIcon() {
            return icon;
        }

        public void setIcon(Icon icon) {
            this.icon = icon;
        }

        public Children getChildren() {
            return children;
        }

        public void setChildren(Children children) {
            this.children = children;
        }

    }

    public class Icon {

        @SerializedName("exists")
        @Expose
        private Boolean exists;
        @SerializedName("url")
        @Expose
        private String url;

        public Boolean getExists() {
            return exists;
        }

        public void setExists(Boolean exists) {
            this.exists = exists;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

    public class Children implements Serializable{

        @SerializedName("available")
        @Expose
        private Boolean available;
        @SerializedName("count")
        @Expose
        private Integer count;
        @SerializedName("items")
        @Expose
        private List<Item> items = null;

        public Boolean getAvailable() {
            return available;
        }

        public void setAvailable(Boolean available) {
            this.available = available;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }
    }

    public class Item {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("products")
        @Expose
        private Integer products;
        @SerializedName("icon")
        @Expose
        private Icon_ icon;
        @SerializedName("children")
        @Expose
        private Children_ children;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getProducts() {
            return products;
        }

        public void setProducts(Integer products) {
            this.products = products;
        }

        public Icon_ getIcon() {
            return icon;
        }

        public void setIcon(Icon_ icon) {
            this.icon = icon;
        }

        public Children_ getChildren() {
            return children;
        }

        public void setChildren(Children_ children) {
            this.children = children;
        }

    }

    public class Icon_ {

        @SerializedName("exists")
        @Expose
        private Boolean exists;
        @SerializedName("url")
        @Expose
        private String url;

        public Boolean getExists() {
            return exists;
        }

        public void setExists(Boolean exists) {
            this.exists = exists;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

    public class Children_ {

        @SerializedName("available")
        @Expose
        private Boolean available;
        @SerializedName("count")
        @Expose
        private Integer count;
        @SerializedName("items")
        @Expose
        private List<Item_> items = null;

        public Boolean getAvailable() {
            return available;
        }

        public void setAvailable(Boolean available) {
            this.available = available;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public List<Item_> getItems() {
            return items;
        }

        public void setItems(List<Item_> items) {
            this.items = items;
        }

    }
    public class Item_ {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("products")
        @Expose
        private Integer products;
        @SerializedName("icon")
        @Expose
        private Icon__ icon;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Integer getProducts() {
            return products;
        }

        public void setProducts(Integer products) {
            this.products = products;
        }

        public Icon__ getIcon() {
            return icon;
        }

        public void setIcon(Icon__ icon) {
            this.icon = icon;
        }

    }

    public class Icon__ {

        @SerializedName("exists")
        @Expose
        private Boolean exists;
        @SerializedName("url")
        @Expose
        private String url;

        public Boolean getExists() {
            return exists;
        }

        public void setExists(Boolean exists) {
            this.exists = exists;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

}
