package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.Adapter.ProductColorAdapter;
import com.example.amvirgin.Adapter.ProductVariantsAdapter;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.SplashActivity;
import com.example.amvirgin.shopModule.interfaces.AttributeSelectionListener;
import com.example.amvirgin.shopModule.shopAdapter.AttributeValuesAdapter;
import com.example.amvirgin.shopModule.shopAdapter.ProductDetailsAdapter;
import com.example.amvirgin.shopModule.shopAdapter.ProductSlidingImageAdapter;
import com.example.amvirgin.Adapter.ViewSimilarAdapter;
import com.example.amvirgin.DemoVideoShopActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.Slide;
import com.example.amvirgin.interfaces.ApiUrl;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.sessionmanagement.ValidateSession;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.MoveToWishlistModel;
import com.example.amvirgin.shopModule.shopModel.ProductDetailsModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import lal.adhish.gifprogressbar.GifView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txt_viewMore,txt_viewLess;
    private LinearLayout ll_viewMore,ll_addShoping_bag;
    private static ViewPager mPager;
    private RelativeLayout btn_share;
    private TabLayout indicator;
    private RecyclerView recyclerView_color;
    List<Slide> slides;
    private TextView original_text,color;
    private RecyclerView.LayoutManager layoutManager1,layoutManager2;
    private RecyclerView recyclerView_viewSimilar;
    private ViewSimilarAdapter viewSimilarAdapter;
    private ProductColorAdapter productColorAdapter;
    private ImageView play_video;
    private RelativeLayout back_btn,btn_cart,productWish_btn;
    private BottomSheetDialog bottomSheetDialog;
    private int[] images = {R.drawable.shop1,R.drawable.shop2,R.drawable.shop3,R.drawable.shop4,R.drawable.shop5,R.drawable.shop6,R.drawable.shop7};

    private int productId;
    private Context mCtx;
    ProductDetailsModel.Payload data;
    private TextView tv_product_name, tv_short_desc, tv_product_price, tv_rating, original_price;
    RecyclerView rv_variants;
    Toast mToast;

    private String product_name;

    LinearLayout main_ll;
    GifView pGif;
    private int max_quantity;

    String demoVideoURL;
    
    //Add To Cart
    private LinearLayout ll_addToCart, ll_addToWishlist, ll_goToCart;
    String session_id, token;
    SharedPreferences.Editor editor;
    boolean valid = false;
    TextView tvCart1;
    int cartCount = 0;
    int customerId;

    private CardView cv_select_attributes;
    private TextView tv_done;
    private LinearLayout overlay_ll;
    private RecyclerView rv_attribute_values;
    private List<ProductDetailsModel.Option> options;
    private int size_attr_key;
    private int selected_key;
    Vibrator vibrator;

    LinearLayout product_variants_ll, noProduct_variants_ll;
    RecyclerView rv_available_variants, rv_product_details;
    TextView tv_seller_name, tv_seller_detail, tv_seller_rating;
    ImageView iv_seller_avatar;
    CardView cv_details;

    TextView tv_domestic, tv_international, tv_summary, tv_service_type;

    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
        cartCount = PreferenceManager.getInt(getApplicationContext(), "cart_count");

        if (cartCount > 0){
            tvCart1.setVisibility(View.VISIBLE);
            tvCart1.setText(String.valueOf(cartCount));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        PreferenceManager.delete(getApplicationContext(), "redirect");
        PreferenceManager.delete(getApplicationContext(), "key1");

         vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

        Bundle extras = getIntent().getExtras();
        productId = extras.getInt("ProductId");

        session_id = PreferenceManager.getString(getApplicationContext(), "session_id");
        cartCount = PreferenceManager.getInt(getApplicationContext(), "cart_count");

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        if (session_id.isEmpty()){
            initializeSession();
        }
        else {
            validateSession(session_id);
        }

        product_variants_ll = findViewById(R.id.product_variants_ll);
        rv_available_variants = findViewById(R.id.rv_available_variants);
        rv_product_details = findViewById(R.id.rv_product_details);
        cv_details = findViewById(R.id.cv_details);

        tv_product_name = findViewById(R.id.tv_product_name);
        tv_short_desc = findViewById(R.id.tv_short_desc);
        tv_product_price = findViewById(R.id.tv_product_price);
        tv_rating = findViewById(R.id.tv_rating);
        original_price = findViewById(R.id.original_price);

        tv_domestic = findViewById(R.id.tv_domestic);
        tv_international = findViewById(R.id.tv_international);
        tv_summary = findViewById(R.id.tv_summary);
        tv_service_type = findViewById(R.id.tv_service_type);

        pGif = (GifView) findViewById(R.id.progressBar);
        pGif.setImageResource(R.drawable.loader_red);
        main_ll = findViewById(R.id.main_ll);

        cv_select_attributes = findViewById(R.id.cv_select_attributes);
        tv_done = findViewById(R.id.tv_done);
        overlay_ll = findViewById(R.id.overlay_ll);

        rv_attribute_values = findViewById(R.id.rv_attribute_values);
        rv_variants = findViewById(R.id.rv_variants);
        noProduct_variants_ll = findViewById(R.id.noProduct_variants_ll);

        tv_seller_detail = findViewById(R.id.tv_seller_detail);
        tv_seller_name = findViewById(R.id.tv_seller_name);
        tv_seller_rating = findViewById(R.id.tv_seller_rating);
        iv_seller_avatar = findViewById(R.id.iv_seller_avatar);

        getProductDetails(productId);
        
        //Add To Cart
        ll_addToCart = findViewById(R.id.ll_addToCart);
        ll_addToCart.setOnClickListener(this);

        ll_addToWishlist = findViewById(R.id.ll_addToWishlist);
        ll_addToWishlist.setOnClickListener(this);

        ll_goToCart = findViewById(R.id.ll_goToCart);
        ll_goToCart.setOnClickListener(this);

        tvCart1 = findViewById(R.id.tvCart1);

        if (cartCount > 0){
            tvCart1.setVisibility(View.VISIBLE);
            tvCart1.setText(String.valueOf(cartCount));
        }

        mPager = (ViewPager) findViewById(R.id.pager);
        original_text = findViewById(R.id.original_price);
        original_text.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        txt_viewMore=(TextView)findViewById(R.id.txt_viewMore);
        ll_viewMore=(LinearLayout)findViewById(R.id.ll_manufature_detail);
        txt_viewLess=(TextView)findViewById(R.id.txt_viewLess);
        recyclerView_viewSimilar=(RecyclerView)findViewById(R.id.recycler_viewSimilar);
        back_btn=(RelativeLayout) findViewById(R.id.rlBack);
//        ll_addShoping_bag=findViewById(R.id.ll_addCart);
        play_video=findViewById(R.id.playVideo);
        btn_cart=(RelativeLayout)findViewById(R.id.rlCart1);
        productWish_btn=findViewById(R.id.rlWish);
        btn_share=findViewById(R.id.rlShare);
        color = (TextView)findViewById(R.id.tv_color);
        bottomSheetDialog = new BottomSheetDialog(ProductDetailActivity.this);
        View bottomSheetDial= getLayoutInflater().inflate(R.layout.bottom_popup_color_layout,null);
        bottomSheetDialog.setContentView(bottomSheetDial);
        recyclerView_color = bottomSheetDialog.findViewById(R.id.recycler_productColor);

        layoutManager2 = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView_color.setLayoutManager(layoutManager2);
        recyclerView_viewSimilar.setHasFixedSize(true);
        productColorAdapter = new ProductColorAdapter(images);
        recyclerView_color.setAdapter(productColorAdapter);

        color.setOnClickListener(this);
        btn_share.setOnClickListener(this);
        productWish_btn.setOnClickListener(this);
        btn_cart.setOnClickListener(this);

        layoutManager1 = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView_viewSimilar.setHasFixedSize(true);
        recyclerView_viewSimilar.setLayoutManager(layoutManager1);
        viewSimilarAdapter = new ViewSimilarAdapter(images);
        recyclerView_viewSimilar.setAdapter(viewSimilarAdapter);

        indicator = findViewById(R.id.indicator);
        indicator.setupWithViewPager(mPager, true);

        txt_viewMore.setOnClickListener(this);
        txt_viewLess.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        play_video.setOnClickListener(this);
//        ll_addShoping_bag.setOnClickListener(this);

        tv_done.setOnClickListener(this);

    }

    private void getProductDetails(final int productId) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ProductDetailsModel.class, new ProductDetailsModel.DataStateDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        ServiceInterface service = retrofit.create(ServiceInterface.class);

        Call<ProductDetailsModel> call = service.getProductDetails("customer/products/"+productId);
        call.enqueue(new Callback<ProductDetailsModel>() {
            @Override
            public void onResponse(Call<ProductDetailsModel> call, Response<ProductDetailsModel> response) {
//                .getValue());
                if (response.body().getStatus() == 200){
                    pGif.setVisibility(View.GONE);
                    try{

                        for (int i = 0; i <SplashActivity.cart_products.size(); i++){
                            if (SplashActivity.cart_products.get(i) == response.body().getPayload().getKey()){
                                ll_addToCart.setVisibility(View.GONE);
                                ll_goToCart.setVisibility(View.VISIBLE);
                            }
                        }

                        data = response.body().getPayload();
                        options = data.getOptions();

                        product_name = data.getName();

                        tv_product_name.setText(data.getName());
                        tv_short_desc.setText(data.getDescription());
                        tv_product_price.setText(String.valueOf("₹ "+data.getPrice().getSelling()));
                        original_price.setText(String.valueOf("₹ "+data.getPrice().getOriginal()));
                        tv_rating.setText(String.valueOf(data.getRating()));

                        max_quantity = data.getMaxQuantity();

                        tv_seller_name.setText(data.getSeller().getName());
                        tv_seller_detail.setText(data.getSeller().getDescription());
                        tv_seller_rating.setText(String.valueOf(data.getSeller().getRating()));
                        if (data.getSeller().getAvatar() != null){
                            Glide.with(getApplicationContext()).load(data.getSeller().getAvatar()).into(iv_seller_avatar);
                        }

                        mPager.setAdapter(new ProductSlidingImageAdapter(getApplicationContext(), data.getMedia().getGallery()));

                        demoVideoURL = data.getMedia().getTrailer();

                        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                        rv_variants.setLayoutManager(manager);

                        ProductVariantsAdapter adapter = new ProductVariantsAdapter(getApplicationContext(),
                                response.body().getPayload().getOptions(), null, new AttributeSelectionListener() {
                            @Override
                            public void onItemClick(int key) {

                            }
                        });
                        rv_variants.setAdapter(adapter);

                                if (response.body().getPayload().getVariants() != null){

                                  if (response.body().getPayload().getVariants().size() > 0) {

                                      product_variants_ll.setVisibility(View.VISIBLE);
                                      noProduct_variants_ll.setVisibility(View.GONE);

                                    LinearLayoutManager manager1 = new LinearLayoutManager(getApplicationContext(),
                                            RecyclerView.HORIZONTAL, false);
                                    rv_available_variants.setLayoutManager(manager1);

                                    AttributeValuesAdapter adapter1 = new AttributeValuesAdapter(getApplicationContext(),
                                            response.body().getPayload().getVariants(), new AttributeSelectionListener() {
                                        @Override
                                        public void onItemClick(int key) {
                                            selected_key = key;
                                            getProductDetails(selected_key);

//                                            Intent intent = new Intent(getApplicationContext(), ProductDetailActivity.class);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                            intent.putExtra("ProductId", selected_key);
//                                            startActivity(intent);
                                        }

                                    });

                                    rv_available_variants.setAdapter(adapter1);

                        }
                            }
                            else {
//                                Toast.makeText(ProductDetailActivity.this, "No", Toast.LENGTH_SHORT).show();
                                    noProduct_variants_ll.setVisibility(View.VISIBLE);
                                    product_variants_ll.setVisibility(View.GONE);
                            }


                            if (data.getDetails().size() > 0){
                                cv_details.setVisibility(View.VISIBLE);

                                LinearLayoutManager manager1 = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                                rv_product_details.setLayoutManager(manager1);

                                ProductDetailsAdapter adapter1 = new ProductDetailsAdapter(getApplicationContext(), data.getDetails());
                                rv_product_details.setAdapter(adapter1);
                            }
                            else {
                                cv_details.setVisibility(View.GONE);
                            }

                            tv_domestic.setText(data.getWarranty().getDomestic()+" "+data.getWarranty().getUnit());
                        tv_international.setText(data.getWarranty().getInternational()+" "+data.getWarranty().getUnit());
                        tv_summary.setText(data.getWarranty().getSummary());
                        tv_service_type.setText(data.getWarranty().getServiceType());

                    }
                    catch (Exception e){
                        SnackbarManager.sException(ProductDetailActivity.this,response.body().getMessage());
//                        ToastMessage.getInstance(ProductDetailActivity.this).showErrorShortCustomToast(response.body().getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ProductDetailActivity.this,response.body().getMessage());
//                    ToastMessage.getInstance(ProductDetailActivity.this).showErrorShortCustomToast(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProductDetailsModel> call, Throwable t) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txt_viewMore:
                ll_viewMore.setVisibility(View.VISIBLE);
                txt_viewMore.setVisibility(View.GONE);
                break;
            case R.id.txt_viewLess:
                ll_viewMore.setVisibility(View.GONE);
                txt_viewMore.setVisibility(View.VISIBLE);
                break;
            case R.id.rlBack:
//                Intent intent = new Intent(getApplicationContext(), ShopProductListActivity.class);
//                intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
               finish();
                break;
           /* case R.id.ll_addCart:

                startActivity(new Intent(getApplicationContext(), ShopOrderSummaryActivity.class));
                break;*/
            case R.id.playVideo:
                if (demoVideoURL == null){
                    ToastMessage.getInstance(this).showErrorShortCustomToast("No Demo Video available for this Product!!");
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), DemoVideoShopActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("demoVideoURL", demoVideoURL);
                    startActivity(intent);
                }

                break;
            case R.id.rlCart1:
//                startActivity(new Intent(getApplicationContext(), CartProductShopActivity.class));
                Intent intent = new Intent(getApplicationContext(), CartProductShopActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            case R.id.ll_goToCart:
                Intent intent_goToCart = new Intent(getApplicationContext(), CartProductShopActivity.class);
                intent_goToCart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_goToCart);
                break;

            case R.id.rlWish:
                startActivity(new Intent(getApplicationContext(), ShopWishListActivity.class));
                break;
            case R.id.rlShare:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Check this out\n"+product_name+"\n" +
                        "http://amvirgin.zobofy.com/productdetail/"+productId);
                startActivity(shareIntent);
                break;
            case R.id.tv_color:
                bottomSheetDialog.show();
                break;
                
            case R.id.ll_addToCart:
                if (valid){
                   /* if (options.size() > 0) {
                        overlay_ll.setVisibility(View.VISIBLE);
                        main_ll.setEnabled(false);
                        cv_select_attributes.setVisibility(View.VISIBLE);
                    }
                    else {
                        addToCart();
                    }*/
                    addToCart();
                }
                break;

            case R.id.ll_addToWishlist:
                int key1;

                if (selected_key != 0){
                    key1 = selected_key;
                }
                else {
                    key1 = productId;
                }
                if (token != null) {
                    addToWishlist(key1, token);
                }
                else {
                    PreferenceManager.saveString(getApplicationContext(), "redirect", "add_wishlist");
                    PreferenceManager.saveInt(getApplicationContext(), "key1", key1);
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    i.putExtra("notlogin", "notlogin");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                break;

        }
    }

    private void addToWishlist(int productId, String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<MoveToWishlistModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .addToWishlist(String.valueOf(productId), "application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<MoveToWishlistModel>(this) {
            @Override
            public void onResponse(MoveToWishlistModel response) {
                if (response.getStatus() == 200){
                    try {
//                        ToastMessage.getInstance(getApplicationContext()).showSuccessShortCustomToast(response.getMessage());
                        showAToast(response.getMessage());
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                    }
                }
                else {
//                    ToastMessage.getInstance(ProductDetailActivity.this).showErrorShortCustomToast(response.getMessage());
                    SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                }
            }
        });
    }

    private void initializeSession() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<InitializeSession> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .initializeSession();
        serviceWrapper.HandleResponse(call, new ResponseHandler<InitializeSession>(getApplicationContext()) {
            @Override
            public void onResponse(InitializeSession response) {
                if (response.getStatus() == 200){
                    try {
                        PreferenceManager.saveString(getApplicationContext(), "session_id", response.getSession());
                        session_id = response.getSession();
                        validateSession(session_id);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                }
            }
        });
    }

    private boolean validateSession(String session_id) {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<ValidateSession> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .validateSession("customer/sessions/"+ session_id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ValidateSession>(getApplicationContext()) {
            @Override
            public void onResponse(ValidateSession response) {
                if (response.getStatus() == 200){
                    try {
                        valid = true;
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                }
            }
        });

        return valid;
    }

    private void addToCart() {

      int key;

        if (selected_key != 0){
            key = selected_key;
        }
        else {
            key = productId;
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("sessionId", session_id);
        jsonObject.addProperty("key", key);

//
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<AddToCartModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .addToCart(jsonObject, "application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<AddToCartModel>(this) {
            @Override
            public void onResponse(AddToCartModel response) {
//                Toast.makeText(getApplicationContext(), String.valueOf(response.getStatus()), Toast.LENGTH_SHORT).show();
                if (response.getStatus() == 200){
                    try {
                        cartCount = cartCount + 1;
                        tvCart1.setVisibility(View.VISIBLE);
                        tvCart1.setText(String.valueOf(response.getData().getCart().getItemCount()));
//                        ToastMessage.getInstance(ProductDetailActivity.this).showSuccessShortCustomToast(response.getMessage());
                        showAToast(response.getMessage());

                        SplashActivity.cart_products.add(key);

                        PreferenceManager.saveInt(getApplicationContext(), "cart_count", cartCount);
                        ll_goToCart.setVisibility(View.VISIBLE);
                        ll_addToCart.setVisibility(View.GONE);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                    }
                }
                else if (response.getStatus() == 400){
                    try {
                        ToastMessage.getInstance(ProductDetailActivity.this).
                                showErrorShortCustomToast(response.getMessage());
                        ll_goToCart.setVisibility(View.VISIBLE);
                        ll_addToCart.setVisibility(View.GONE);

                    }
                    catch (Exception e){
                        SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ProductDetailActivity.this,response.getMessage());
                }
            }
        });
    }

    public void showAToast (String message){
        if (mToast != null) {
            mToast.cancel();
        }
//        ToastMessage.getInstance(ProductDetailActivity.this).showSuccessShortCustomToast(response.getMessage());
        LayoutInflater inflater = ((Activity) this).getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_layout, (ViewGroup)
                ((Activity) this).findViewById(R.id.custom_toast_layout));
        layout.setBackgroundColor(this.getResources().getColor(R.color.colorGreen));
        TextView msgTv = (TextView) layout.findViewById(R.id.tv_msg);
        msgTv.setText(message);

        mToast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.FILL_HORIZONTAL | Gravity.BOTTOM, 0, 0);
        mToast.setView(layout);
        mToast.show();
    }
}
