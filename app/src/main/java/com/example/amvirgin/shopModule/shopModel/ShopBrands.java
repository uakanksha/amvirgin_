package com.example.amvirgin.shopModule.shopModel;

public class ShopBrands {
    private int img;
    private String brand_name;



    public ShopBrands(int img, String brand_name) {
        this.img = img;
        this.brand_name = brand_name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }
}
