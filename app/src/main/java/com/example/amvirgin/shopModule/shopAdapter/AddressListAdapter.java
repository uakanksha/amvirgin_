package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.AddAddressActivity;
import com.example.amvirgin.shopModule.activity.MyAddressActivity;
import com.example.amvirgin.shopModule.activity.ShopOrderSummaryActivity;
import com.example.amvirgin.shopModule.interfaces.AddressSelectionListener;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.shopModel.DeleteAddressModel;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;

public class  AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressListViewHolder> {
    Context context;
    List<GetAddressModel.Datum> data;
    private RadioButton lastCheckedRB = null;
    AddressSelectionListener addressSelectionListener;
    private String session_id, token;
    RemoveFromCartClickListener removeFromCartClickListener;
    int address_id;

    public AddressListAdapter(Context context, int address_id, List<GetAddressModel.Datum> data,
                                  AddressSelectionListener addressSelectionListener,
                                  RemoveFromCartClickListener removeFromCartClickListener) {
        this.context = context;
        this.address_id = address_id;
        this.data = data;
        this.addressSelectionListener = addressSelectionListener;
        this.removeFromCartClickListener = removeFromCartClickListener;
    }

    public AddressListAdapter(Context context, List<GetAddressModel.Datum> data,
                              AddressSelectionListener addressSelectionListener,
                              RemoveFromCartClickListener removeFromCartClickListener) {
        this.context = context;
        this.data = data;
        this.addressSelectionListener = addressSelectionListener;
        this.removeFromCartClickListener = removeFromCartClickListener;
    }

    @NonNull
    @Override
    public AddressListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_address_list_layout, parent, false);
        AddressListViewHolder holder = new AddressListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AddressListViewHolder holder, final int position) {

      /*  for (int i = 0; i <data.size(); i++){
            if (data.get(position).getKey() == address_id){
                holder.rb_address.setChecked(true);
            }
        }*/

        session_id = PreferenceManager.getString(context, "session_id");
        if (PreferenceManager.getBoolean(context, Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(context).getToken();
        }

        holder.tv_name.setText(data.get(position).getName());
        holder.tv_address.setText(data.get(position).getAddress());
        holder.tv_locality.setText(data.get(position).getLocality());
        holder.tv_city.setText(data.get(position).getCity().getName());
        holder.tv_state.setText(data.get(position).getState().getName()+" - ");
        holder.tv_pincode.setText(String.valueOf(data.get(position).getPinCode()));
        holder.tv_type.setText(data.get(position).getType());
        holder.tv_mobile.setText(data.get(position).getMobile());

        holder.rb_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }

                //store the clicked radiobutton
                lastCheckedRB = holder.rb_address;
                if (addressSelectionListener != null){
                    addressSelectionListener.onItemClick(data.get(position).getKey());
                }

            }
        });

        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAddress(data.get(position).getKey(), holder);
                lastCheckedRB = null;
            }
        });

        holder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddAddressActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("data", (Serializable) data);
                intent.putExtra("position", position);
                intent.putExtra("type_of_edit", "edit");
                context.startActivity(intent);
            }
        });
    }

    private void deleteAddress(Integer id, final AddressListViewHolder holder) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<DeleteAddressModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .deleteAddress(String.valueOf(id), "application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<DeleteAddressModel>(context) {
            @Override
            public void onResponse(DeleteAddressModel response) {
                if (response.getStatus() == 200){
                    try
                    {
                        ToastMessage.getInstance(holder.tv_delete.getContext()).showSuccessShortCustomToast(response.getMessage());
                        if (removeFromCartClickListener != null){
                            removeFromCartClickListener.onItemClick();
                        }
//                        Intent intent = new Intent(context, MyAddressActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
//                        context.startActivity(intent);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class AddressListViewHolder extends RecyclerView.ViewHolder{

        TextView tv_name, tv_address, tv_locality, tv_city, tv_state, tv_pincode, tv_type;
        TextView tv_edit, tv_delete, tv_mobile;
        RadioButton rb_address;
        LinearLayout edit_del_address_ll;

        public AddressListViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_name);
            tv_address = itemView.findViewById(R.id.tv_address);
            tv_locality = itemView.findViewById(R.id.tv_locality);
            tv_city = itemView.findViewById(R.id.tv_city);
            tv_state = itemView.findViewById(R.id.tv_state);
            tv_pincode = itemView.findViewById(R.id.tv_pincode);
            tv_type = itemView.findViewById(R.id.tv_type);
            rb_address = itemView.findViewById(R.id.rb_address);
            tv_edit = itemView.findViewById(R.id.tv_edit);
            tv_delete = itemView.findViewById(R.id.tv_delete);
            tv_mobile = itemView.findViewById(R.id.tv_mobile);
            edit_del_address_ll = itemView.findViewById(R.id.edit_del_address_ll);
        }
    }
}
