package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.ObjectStreamClass;
import java.lang.reflect.Type;
import java.util.List;

public class ProductDetailsModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("payload")
    @Expose
    private Payload payload;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public class Payload {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("category")
        @Expose
        private Category category;
        @SerializedName("seller")
        @Expose
        private Seller seller;
        @SerializedName("brand")
        @Expose
        private Brand brand;
        @SerializedName("price")
        @Expose
        private Price price;
        @SerializedName("fulfillmentBy")
        @Expose
        private String fulfillmentBy;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("stock")
        @Expose
        private Stock stock;
        @SerializedName("warranty")
        @Expose
        private Warranty warranty;
        @SerializedName("maxQuantity")
        @Expose
        private Integer maxQuantity;
        @SerializedName("media")
        @Expose
        private Media media;
        @SerializedName("details")
        @Expose
        private List<Detail> details = null;
        @SerializedName("options")
        @Expose
        private List<Option> options = null;
        @SerializedName("variants")
        @Expose
        private List<Variant> variants = null;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Category getCategory() {
            return category;
        }

        public void setCategory(Category category) {
            this.category = category;
        }

        public Seller getSeller() {
            return seller;
        }

        public void setSeller(Seller seller) {
            this.seller = seller;
        }

        public Brand getBrand() {
            return brand;
        }

        public void setBrand(Brand brand) {
            this.brand = brand;
        }

        public Price getPrice() {
            return price;
        }

        public void setPrice(Price price) {
            this.price = price;
        }

        public String getFulfillmentBy() {
            return fulfillmentBy;
        }

        public void setFulfillmentBy(String fulfillmentBy) {
            this.fulfillmentBy = fulfillmentBy;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Stock getStock() {
            return stock;
        }

        public void setStock(Stock stock) {
            this.stock = stock;
        }

        public Warranty getWarranty() {
            return warranty;
        }

        public void setWarranty(Warranty warranty) {
            this.warranty = warranty;
        }

        public Integer getMaxQuantity() {
            return maxQuantity;
        }

        public void setMaxQuantity(Integer maxQuantity) {
            this.maxQuantity = maxQuantity;
        }

        public Media getMedia() {
            return media;
        }

        public void setMedia(Media media) {
            this.media = media;
        }

        public List<Detail> getDetails() {
            return details;
        }

        public void setDetails(List<Detail> details) {
            this.details = details;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

        public List<Variant> getVariants() {
            return variants;
        }

        public void setVariants(List<Variant> variants) {
            this.variants = variants;
        }

    }

    public class Detail {

        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("group")
        @Expose
        private String group;
        @SerializedName("value")
        @Expose
        private Object value = null;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

    }


    public class Category {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Seller {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("rating")
        @Expose
        private Double rating;
        @SerializedName("avatar")
        @Expose
        private String avatar;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Double getRating() {
            return rating;
        }

        public void setRating(Double rating) {
            this.rating = rating;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

    }

    public class Brand {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("logo")
        @Expose
        private String logo;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

    }

    public class Price {

        @SerializedName("original")
        @Expose
        private Integer original;
        @SerializedName("selling")
        @Expose
        private Integer selling;

        public Integer getOriginal() {
            return original;
        }

        public void setOriginal(Integer original) {
            this.original = original;
        }

        public Integer getSelling() {
            return selling;
        }

        public void setSelling(Integer selling) {
            this.selling = selling;
        }

    }

    public class Stock {

        @SerializedName("isLow")
        @Expose
        private Boolean isLow;
        @SerializedName("remaining")
        @Expose
        private Integer remaining;

        public Boolean getIsLow() {
            return isLow;
        }

        public void setIsLow(Boolean isLow) {
            this.isLow = isLow;
        }

        public Integer getRemaining() {
            return remaining;
        }

        public void setRemaining(Integer remaining) {
            this.remaining = remaining;
        }

    }

    public class Warranty {

        @SerializedName("domestic")
        @Expose
        private Integer domestic;
        @SerializedName("international")
        @Expose
        private Integer international;
        @SerializedName("unit")
        @Expose
        private String unit;
        @SerializedName("summary")
        @Expose
        private String summary;
        @SerializedName("serviceType")
        @Expose
        private String serviceType;
        @SerializedName("covered")
        @Expose
        private String covered;
        @SerializedName("excluded")
        @Expose
        private String excluded;

        public Integer getDomestic() {
            return domestic;
        }

        public void setDomestic(Integer domestic) {
            this.domestic = domestic;
        }

        public Integer getInternational() {
            return international;
        }

        public void setInternational(Integer international) {
            this.international = international;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public String getServiceType() {
            return serviceType;
        }

        public void setServiceType(String serviceType) {
            this.serviceType = serviceType;
        }

        public String getCovered() {
            return covered;
        }

        public void setCovered(String covered) {
            this.covered = covered;
        }

        public String getExcluded() {
            return excluded;
        }

        public void setExcluded(String excluded) {
            this.excluded = excluded;
        }

    }

    public class Media {

        @SerializedName("trailer")
        @Expose
        private String trailer;
        @SerializedName("gallery")
        @Expose
        private List<String> gallery = null;

        public String getTrailer() {
            return trailer;
        }

        public void setTrailer(String trailer) {
            this.trailer = trailer;
        }

        public List<String> getGallery() {
            return gallery;
        }

        public void setGallery(List<String> gallery) {
            this.gallery = gallery;
        }

    }

    public class Option {

        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("interface")
        @Expose
        private String _interface;
        @SerializedName("group")
        @Expose
        private String group;
        @SerializedName("value")
        @Expose
        private Object value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getInterface() {
            return _interface;
        }

        public void setInterface(String _interface) {
            this._interface = _interface;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

    }

    public class Variant {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("options")
        @Expose
        private List<Option_> options = null;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Option_> getOptions() {
            return options;
        }

        public void setOptions(List<Option_> options) {
            this.options = options;
        }

    }

    public class Option_ {

        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("interface")
        @Expose
        private String _interface;
        @SerializedName("group")
        @Expose
        private String group;
        @SerializedName("value")
        @Expose
        private Object value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getInterface() {
            return _interface;
        }

        public void setInterface(String _interface) {
            this._interface = _interface;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public Object getValue() {
            return value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

    }

    public static class DataStateDeserializer implements JsonDeserializer<ProductDetailsModel>{

        @Override
        public ProductDetailsModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {

            ProductDetailsModel model = new Gson().fromJson(json,ProductDetailsModel.class);

            // Product Options

            Type type = new TypeToken<ProductDetailsModel.Option>() {
            }.getType();

            ProductDetailsModel.Option option = new Gson().fromJson(json, type);
            JsonObject object = json.getAsJsonObject();

            if (object.has("value")){
                JsonElement element = object.get("value");

                if (element != null && !element.isJsonNull()){

                    if (element.isJsonPrimitive()){
                        option.setValue(element.getAsString());
                    }
                    else {
                        option.setValue(element.getAsJsonArray());
                    }
                }
            }

            // Product Details

            Type type_details = new TypeToken<ProductDetailsModel.Detail>() {
            }.getType();

            ProductDetailsModel.Detail detail = new Gson().fromJson(json, type_details);
            JsonObject object_detail = json.getAsJsonObject();

            if (object_detail.has("value")){
                JsonElement element = object.get("value");

                if (element != null && !element.isJsonNull()){

                    if (element.isJsonPrimitive()){
                        detail.setValue(element.getAsString());
                    }
                    else {
                        detail.setValue(element.getAsJsonArray());
                    }
                }
            }


            //Product Variant Options

            Type type_v = new TypeToken<ProductDetailsModel>() {
            }.getType();

            ProductDetailsModel model_v = new Gson().fromJson(json, type_v);
            JsonObject objec_v = json.getAsJsonObject();

            if (objec_v.has("variants")) {

                if (model.getPayload().getVariants().size() > 0) {

                    Type type1 = new TypeToken<ProductDetailsModel.Option_>() {
                    }.getType();

                    for (int i = 0; i < model.getPayload().getVariants().size(); i++) {
                        ProductDetailsModel.Option_ option_ = new Gson().fromJson(json, type1);
                        JsonObject object1 = json.getAsJsonObject();

                        if (object1.has("value")) {
                            JsonElement element = object1.get("value");

                            if (element != null && !element.isJsonNull()) {

                                if (element.isJsonPrimitive()) {
                                    option_.setValue(element.getAsString());
                                } else {
                                    option_.setValue(element.getAsJsonArray());
                                }
                            }
                        }
                    }
                }

            }


            return model;
        }
    }


}