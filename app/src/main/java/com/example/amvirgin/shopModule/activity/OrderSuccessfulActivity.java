package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.R;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class OrderSuccessfulActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout back;
    private TextView tv_order_number;
    private String order_id;
    private Button continue_shopping_btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method_final);

        if (getIntent().getExtras() != null){
            order_id = getIntent().getExtras().getString("order_id");
        }

        back=(RelativeLayout)findViewById(R.id.rlBack);
        tv_order_number = findViewById(R.id.tv_order_number);
        continue_shopping_btn = findViewById(R.id.continue_shopping_btn);

        tv_order_number.setText("Your Order number is "+order_id);

        back.setOnClickListener(this);
        continue_shopping_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.continue_shopping_btn:
                Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
                intent.putExtra("back", "cart");
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(OrderSuccessfulActivity.this, HomeActivity.class);
        intent.putExtra("back", "cart");
        intent.setFlags(FLAG_ACTIVITY_CLEAR_TASK|FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
