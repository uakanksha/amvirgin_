package com.example.amvirgin.shopModule.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.shopModule.BeanModel.NavigationItemBeanModel;
import com.example.amvirgin.shopModule.activity.ProductSearchActivity;
import com.example.amvirgin.shopModule.activity.ShopMyOrdersActivity;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.CartProductShopActivity;
import com.example.amvirgin.Listener.OnBackPressedListener;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.CustomViewModels.ShopViewModelFactory;
import com.example.amvirgin.shopModule.CustomViewModels.ShopViewModels;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;
import com.example.amvirgin.shopModule.activity.ShopWishListActivity;
import com.example.amvirgin.shopModule.sessionmanagement.InitializeSession;
import com.example.amvirgin.shopModule.sessionmanagement.ValidateSession;
import com.example.amvirgin.shopModule.shopAdapter.CategoryListAdapter;
import com.example.amvirgin.shopModule.shopAdapter.ProductSearchAdapter;
import com.example.amvirgin.shopModule.shopModel.ShopBrands;
import com.example.amvirgin.shopModule.shopAdapter.HomePageAdapter;
import com.example.amvirgin.shopModule.shopAdapter.ParentLevelAdapter;
import com.example.amvirgin.shopModule.shopModel.ParentMenuModel;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.shopModule.shopModel.ShopSearchModel;
import com.example.amvirgin.shopModule.shopModel.ShopTrendingDealsModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.example.amvirgin.utils.Constants.shopping_search;
import static com.facebook.FacebookSdk.getApplicationContext;

public class ShopFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener, OnBackPressedListener {

    private String[] best_name = {"New Arrival","Am Virgin","Men","Women","Home","Kitchen","Electronics","Accessories"};
    private int[] images = {R.drawable.new_arrival2,R.drawable.amvirgin1,R.drawable.men1,R.drawable.women1,R.drawable.home1,R.drawable.kitchen1,R.drawable.electronics1,R.drawable.accessories1};
    public ArrayList<Object> objects = new ArrayList();
    // new attributes
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle toggle;
    private ExpandableListView mExpandableListView;
    private ParentLevelAdapter parentLevelAdapter;
    private View view;
    private Context mCtx;
    private HomePageAdapter homePageAdapter;
    private RecyclerView.LayoutManager homeManager;
    private RecyclerView homeRecyclerview, rv_category, rv_search_items;
    private  ArrayList<ParentMenuModel> parentList=new ArrayList<>();
    private  List<ShopBrands> shopBrands;
    private ShopViewModels shopViewModels;
    private ShopViewModelFactory factory;
    private RelativeLayout rlCart;
    private int cart_count;
    private TextView tvCart, loggedin_user_name;
    private CircleImageView circle;
    private ProgressBar pb;
    private String token;

    private List<String> search_results;
    String session_id;
    boolean valid;

    private TextView etsearch;
    private ImageView iv_search;

    private TextView tv_my_orders, tv_logout, my_cart, my_wishlist;

    List<ShopHomeModel.ShopSlider> sliderList;
    ShopHomeModel.OfferDetails offerDetails;
    List<ShopHomeModel.BrandInFocu> brandsInFocus;
    List<ShopHomeModel.TrendingDeal> trendingDeals;
    List<ShopHomeModel.PopularStuff> popularStuffs;
    List<ShopHomeModel.TrendingNow> trendingNows;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_shop, container, false);
        mCtx = getContext();

        if (PreferenceManager.getBoolean(getContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getContext()).getToken();
        }

        session_id = PreferenceManager.getString(getApplicationContext(), "session_id");

        if (session_id.isEmpty()){
            initializeSession();
        }
        else {
            validateSession(session_id);
        }

        findViews();
        getCategories();
        prepareHomeData();

        tvCart = view.findViewById(R.id.tvCart);

        cart_count = PreferenceManager.getInt(getContext(), "cart_count");

        if (cart_count > 0){
            tvCart.setVisibility(View.VISIBLE);
            tvCart.setText(String.valueOf(cart_count));
        }

        return view;
    }

    private void getCategories() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<NavigationItemBeanModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getNavigationItem();
        serviceWrapper.HandleResponse(call, new ResponseHandler<NavigationItemBeanModel>(getContext()) {
            @Override
            public void onResponse(NavigationItemBeanModel response) {
                if (response.getStatus() == 200){
                    try {
                        LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
                        rv_category.setLayoutManager(manager);

                        CategoryListAdapter adapter = new CategoryListAdapter(getContext(), response.getData());
                        rv_category.setAdapter(adapter);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(getContext(), response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(getContext(), response.getMessage());
                }
            }
        });
    }

    private void getUserProfile(String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .getUserProfile("application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                loggedin_user_name.setText(response.getData().getName());

                if (response.getData().getAvatar() != null){
                    Glide.with(getContext()).load(response.getData().getAvatar()).into(circle);
                    pb.setVisibility(View.GONE);
                }
                else {
                    circle.setImageResource(R.drawable.ic_user);
                    pb.setVisibility(View.GONE);
                }

            }
        });
    }

    public void findViews() {
        toolbar = view.findViewById(R.id.toolbar);
        drawerLayout = view.findViewById(R.id.drawerlayout);
        navigationView = view.findViewById(R.id.nav_view);
        homeRecyclerview=view.findViewById(R.id.homeRecyclerview);
        rv_category = view.findViewById(R.id.rv_category);
        rv_search_items = view.findViewById(R.id.rv_search_items);
        mExpandableListView = view.findViewById(R.id.expandableListView_Parent);
        toggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        rlCart = view.findViewById(R.id.rlCart);
        rlCart.setOnClickListener(this);

        etsearch = view.findViewById(R.id.etsearch);
        iv_search = view.findViewById(R.id.iv_search);
        iv_search.setOnClickListener(this);

        //set  navigation header and footer
        View header = getLayoutInflater().inflate(R.layout.nav_header_main, null);
        View footer = getLayoutInflater().inflate(R.layout.footer_layout, null);
        mExpandableListView.addHeaderView(header);
        mExpandableListView.addFooterView(footer);

        tv_my_orders = footer.findViewById(R.id.tv_my_orders);
        tv_my_orders.setOnClickListener(this);

        my_cart = footer.findViewById(R.id.my_cart);
        my_wishlist = footer.findViewById(R.id.my_wishlist);

        my_cart.setOnClickListener(this);
        my_wishlist.setOnClickListener(this);

        tv_logout = footer.findViewById(R.id.tv_logout);
        tv_logout.setOnClickListener(this);

        if (token != null) {
            getUserProfile(token);
            tv_logout.setVisibility(View.VISIBLE);
        }
        else {
            tv_logout.setVisibility(View.GONE);
        }

        loggedin_user_name = header.findViewById(R.id.loggedin_user_name);
        circle = header.findViewById(R.id.circle);
        pb = header.findViewById(R.id.pb);

        etsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ProductSearchActivity.class);
                intent.putExtra("cat_id", Integer.parseInt("0"));
                startActivity(intent);
            }
        });

        /*etsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()>2){
                    itemSearch(s.toString());
                }
                else {
                    rv_search_items.setVisibility(View.GONE);
                }
            }
        });*/

    }


    public void SetHomePageAdapter() {
//         prepareHomeData();
        if (homePageAdapter == null) {

          homePageAdapter=new HomePageAdapter(mCtx,getObjects());
          homeManager=new LinearLayoutManager(mCtx,RecyclerView.VERTICAL,false);
          homeRecyclerview.setLayoutManager(homeManager);
          homeRecyclerview.setAdapter(homePageAdapter);
        } else {
          homePageAdapter.notify();
        }
    }




    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlCart:
//                startActivity(new Intent(getContext(), CartProductShopActivity.class));
                Intent intent = new Intent(getContext(), CartProductShopActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.my_cart:
//                startActivity(new Intent(getContext(), CartProductShopActivity.class));
                Intent intent_cart = new Intent(getContext(), CartProductShopActivity.class);
                intent_cart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_cart);
                break;
            case R.id.iv_search:
//                if (etsearch.getText().toString().length()>=2){
//                    itemSearch(etsearch.getText().toString());
//                }
//                else {
//                    ToastMessage.getInstance(getContext()).showErrorShortCustomToast("Search with at least 2 letters!!");
//                }
                break;
            case R.id.tv_my_orders:
//                if (token == null){
//                    PreferenceManager.saveString(getContext(), "redirect", "my_orders");
//                    Intent i = new Intent(getContext(), LoginActivity.class);
//                    i.putExtra("notlogin", "notlogin");
//                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(i);
//                }
//                else {
                    Intent intent_orders = new Intent(getContext(), ShopMyOrdersActivity.class);
                    intent_orders.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_orders);
//                }
                break;

            case R.id.my_wishlist:
//                if (token == null){
//                    PreferenceManager.saveString(getContext(), "redirect", "wishlist");
//                    Intent i = new Intent(getContext(), HomeActivity.class);
//                    i.putExtra("notlogin", "notlogin");
//                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(i);
//                }
//                else {
                    Intent intent_wishlist = new Intent(getContext(), ShopWishListActivity.class);
                    intent_wishlist.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_wishlist);
//                }
                break;

            case R.id.tv_logout:
            /*    if (token == null){
                    Intent i = new Intent(getContext(), HomeActivity.class);
                    i.putExtra("notlogin", "notlogin");
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
                else {
                    Intent intent_orders = new Intent(getContext(), ShopMyOrdersActivity.class);
                    intent_orders.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent_orders);
                }*/
            userLogout(token);

                break;
        }

    }

    private void initializeSession() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<InitializeSession> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .initializeSession();
        serviceWrapper.HandleResponse(call, new ResponseHandler<InitializeSession>(getApplicationContext()) {
            @Override
            public void onResponse(InitializeSession response) {
                if (response.getStatus() == 200){
                    try {
                        PreferenceManager.saveString(getApplicationContext(), "session_id", response.getSession());
                        session_id = response.getSession();
                        validateSession(session_id);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(getContext(),response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(getContext(),response.getMessage());
                }
            }
        });
    }

    private boolean validateSession(String session_id) {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<ValidateSession> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .validateSession("customer/sessions/"+ session_id);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ValidateSession>(getApplicationContext()) {
            @Override
            public void onResponse(ValidateSession response) {
                if (response.getStatus() == 200){
                    try {
                        valid = true;
                    }
                    catch (Exception e){
                        SnackbarManager.sException(getContext(),response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(getContext(),response.getMessage());
                }
            }
        });

        return valid;
    }

    private void userLogout(String token) {


        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .userLogout("application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(getContext()) {
            @Override
            public void onResponse(RegisterUserModel response) {
                if (response.getStatus() == 200){

                    PreferenceManager.deleteUser(getContext());
                    PreferenceManager.delete(getContext(), "session_id");
                    PreferenceManager.delete(getContext(), "cart_count");

                    ToastMessage.getInstance(getContext()).showSuccessShortCustomToast("You have been logged out!!");
//                    Toast.makeText(getContext(), "You have been logged out!!", Toast.LENGTH_SHORT).show();
                    Intent intent_logout = new Intent(getContext(), HomeActivity.class);
                    intent_logout.putExtra("back", "cart");
                    intent_logout.setFlags(FLAG_ACTIVITY_CLEAR_TASK|FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent_logout);
                }
                else {
                    ToastMessage.getInstance(getContext()).showErrorShortCustomToast(response.getMessage());
                }
            }
        });
    }

    public void prepareHomeData() {

        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<ShopHomeModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getShopHomeScreenData();
        serviceWrapper.HandleResponse(call, new ResponseHandler<ShopHomeModel>(getContext()) {
            @Override
            public void onResponse(ShopHomeModel response) {
                if (response.getStatus() == 200){
                    try {
                        if (response.getData() != null){

                            sliderList = response.getData().getShopSliders();
                            offerDetails = response.getData().getOfferDetails();
                            brandsInFocus = response.getData().getBrandInFocus();
                            trendingDeals = response.getData().getTrendingDeals();
                            popularStuffs = response.getData().getPopularStuff();
                            trendingNows = response.getData().getTrendingNow();


                            Constants.shopSliderList = sliderList;
                            Constants.incomingdeals = offerDetails;
                            Constants.shopBrandsInFocus = brandsInFocus;
                            Constants.shopTreningDeals = trendingDeals;
                            Constants.shopPopularStuff = popularStuffs;
                            Constants.shopTrendingNow = trendingNows;

                            SetHomePageAdapter();

                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(getContext(), response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(getContext(), response.getMessage());
                }
            }
        });

    }

    private ArrayList<Object> getObjects() {
        try {

            objects.add(Constants.shopSliderList);
            objects.add(Constants.incomingdeals);
            objects.add(Constants.shopBrandsInFocus);
            objects.add(Constants.shopTreningDeals);
            objects.add(Constants.shopPopularStuff);
            objects.add(Constants.shopTrendingNow);
            return objects;
        } catch (Exception e) {
            return objects;
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(context, ""+intent.getBooleanExtra("networkStatus",false), Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(mCtx).registerReceiver(mReceiver,
                new IntentFilter(Constants.Key.LocalBrocastEvent));

        cart_count = PreferenceManager.getInt(getContext(), "cart_count");

        if (cart_count > 0){
            tvCart.setVisibility(View.VISIBLE);
            tvCart.setText(String.valueOf(cart_count));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(mCtx).unregisterReceiver(mReceiver);
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        factory=new ShopViewModelFactory(mCtx);
        shopViewModels= new ViewModelProvider(this,factory).get(ShopViewModels.class);
        shopViewModels.getNavItemList().observe((LifecycleOwner) mCtx, new Observer<List<ParentMenuModel>>() {
            @Override
            public void onChanged(List<ParentMenuModel> parentMenuModels) {
                if (parentMenuModels.size()>0){

                    if (mExpandableListView != null) {
                        mExpandableListView.setDividerHeight(0);
                        parentLevelAdapter = new ParentLevelAdapter(mCtx,parentMenuModels);
                        mExpandableListView.setAdapter(parentLevelAdapter);
                    }

                    mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                        int previousGroup = -1;

                        @Override
                        public void onGroupExpand(int groupPosition) {

                            if (groupPosition != previousGroup)
                                mExpandableListView.collapseGroup(previousGroup);
                            previousGroup = groupPosition;

                        }
                    });

                }
            }
        });
    }

    private void itemSearch(final String key){
        ServiceWrapper serviceWrapper = new ServiceWrapper(getContext());
        Call<ShopSearchModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getShopSearch("customer/search?type=shop&keyword="+key);
        Log.e("sdasdsadsads", "itemSearch: "+call.request().url());
        serviceWrapper.HandleResponse(call, new ResponseHandler<ShopSearchModel>(mCtx) {
            @Override
            public void onResponse(ShopSearchModel response) {
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0) {
//                            search_results = new ArrayList<String>();
//
//                            for (int i = 0; i <response.getData().size(); i++){
//                                search_results.add(response.getData().get(i).getName());
//                            }
                            rv_search_items.setVisibility(View.VISIBLE);

                            LinearLayoutManager manager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                            rv_search_items.setLayoutManager(manager);

                            ProductSearchAdapter adapter = new ProductSearchAdapter(getContext(), response.getData());
                            rv_search_items.setAdapter(adapter);

                            //Creating the instance of ArrayAdapter containing list of fruit names
//                            ArrayAdapter<List<String>> adapter = new ArrayAdapter<List<String>>
//                                    (this, android.R.layout.select_dialog_item, search_results);
//                            //Getting the instance of AutoCompleteTextView
//                            etsearch.setThreshold(2);//will start working from first character
//                            etsearch.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView


//                            Intent intent = new Intent(getContext(), ShopProductListActivity.class);
//                            intent.putExtra("back", "search");
//                            intent.putExtra("CategoryName", key);
//                            shopping_search = response.getData();
//                            startActivity(intent);
                        }else {
                            rv_search_items.setVisibility(View.GONE);
                            ToastMessage.getInstance(getContext())
                                    .showErrorShortCustomToast("No Results found for your Search!!");
//                            Log.e("sdasdsadsads", "onResponse: "+response.getMessage());
//                            SnackbarManager.successResponse(mCtx, response.getMessage());
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(mCtx, response.getMessage());
                    }
                } else {
                    SnackbarManager.sException(mCtx, response.getMessage());
                }
            }

        });
    }
}
