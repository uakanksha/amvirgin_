package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;

import java.util.List;

public class BrandsInFocusAdapter extends RecyclerView.Adapter<BrandsInFocusAdapter.BrandsInFocusViewHolder> {
    Context context;
    List<ShopHomeModel.BrandInFocu> brandsInFocus;

    public BrandsInFocusAdapter(Context context, List<ShopHomeModel.BrandInFocu> brandsInFocus) {
        this.context = context;
        this.brandsInFocus = brandsInFocus;
    }

    @NonNull
    @Override
    public BrandsInFocusViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_brands_layout, parent, false);
        BrandsInFocusViewHolder holder = new BrandsInFocusViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull BrandsInFocusViewHolder holder, int position) {
        holder.brand_name.setText(brandsInFocus.get(position).getName());

//        if (!brandsInFocus.get(position).getPoster().equalsIgnoreCase("")){
//            Glide.with(context).load(brandsInFocus.get(position).getPoster()).into(holder.img);
//        }
//        else {
//            holder.img.setImageResource(R.drawable.placeholder);
//        }

    }

    @Override
    public int getItemCount() {
        return brandsInFocus.size();
    }

    public class BrandsInFocusViewHolder extends RecyclerView.ViewHolder{
        TextView brand_name;
        ImageView img;

        public BrandsInFocusViewHolder(@NonNull View itemView) {
            super(itemView);
            brand_name = itemView.findViewById(R.id.brand_name);
            img = itemView.findViewById(R.id.img);
        }
    }

}
