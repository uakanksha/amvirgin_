package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

//import com.chahinem.pageindicator.PageIndicator;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.BeanModel.NavigationItemBeanModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;
import com.example.amvirgin.shopModule.shopModel.BestTrandsModel;
import com.example.amvirgin.shopModule.shopModel.ShopBrands;
import com.example.amvirgin.shopModule.shopModel.BannerModel;
import com.example.amvirgin.shopModule.shopModel.CategoryModel;
import com.example.amvirgin.shopModule.shopModel.IncommingDealsModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.ColorCycle;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.ItemDecorationGrid;
import com.example.amvirgin.utils.LinePagerIndicatorDecoration;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

public class HomePageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int BANNERLIST = 0;
    private static final int INCOMMINGDEALS= 1;
    private static final int BRANDSINFOCUS = 2;
    private static final int TRENDINGDEALS = 3;
    private static final int POPULARSTUFF= 4;
    private static final int TRENDINGNOW = 5;

    private List<NavigationItemBeanModel.Datum> data;
    private boolean valid = false;

    private Context mCtx;
    private ArrayList<Object> objectData;
    private ColorCycle colorcycle = new ColorCycle();
    public HomePageAdapter(Context context, ArrayList<Object> objectData) {
        this.mCtx = context;
        this.objectData = objectData;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(mCtx);
        switch (viewType){

            case BANNERLIST:
                return new BannerHolders(inflater.inflate(R.layout.banner_layout_viewpager,parent,false));
            case INCOMMINGDEALS:
                return new IncommingDealsHolders(inflater.inflate(R.layout.incomming_deals_view,parent,false));
            case BRANDSINFOCUS:
                return  new BrandsHolders(inflater.inflate(R.layout.brand_in_focus,parent,false));
            case TRENDINGDEALS:
                return  new BestTrendsHolders(inflater.inflate(R.layout.custom_grid_layout,parent,false));
            case POPULARSTUFF:
                return  new PopularStuffHolders(inflater.inflate(R.layout.brand_in_focus,parent,false));
            case TRENDINGNOW:
                return  new TrendingNowHolders(inflater.inflate(R.layout.brand_in_focus,parent,false));
                default:
                    break;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()){

            case BANNERLIST:
                BannerItemView((BannerHolders) holder);
                break;
            case INCOMMINGDEALS:
                IncommingViews((IncommingDealsHolders) holder);
                break;
            case BRANDSINFOCUS:
                BrandViews((BrandsHolders) holder);
                break;
            case TRENDINGDEALS:
                BestTrendsViews((BestTrendsHolders) holder);
                break;
            case POPULARSTUFF:
                PopularStuffViews((PopularStuffHolders) holder);
                break;
            case TRENDINGNOW:
                TrendingNowViews((TrendingNowHolders) holder);
                break;
            default:
                break;

        }
    }

    public void BannerItemView(final BannerHolders holders){
        final ShopBannerSliderAdapter adapter = new ShopBannerSliderAdapter(mCtx);
        holders.bannerRecyclerView.setLayoutManager(new LinearLayoutManager(mCtx, RecyclerView.HORIZONTAL, false));
        holders.bannerRecyclerView.setAdapter(adapter);
        // add pager behavior
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(holders.bannerRecyclerView);
       holders.bannerRecyclerView.addItemDecoration(new LinePagerIndicatorDecoration());
        Handler handler = new Handler();
        final Handler handler2 = handler;
        final int timer = 2000;
        handler.postDelayed(new Runnable() {
            int count = 0;
            boolean flag = true;
            @Override
            public void run() {
                if (count < adapter.getItemCount()) {
                    if (count == adapter.getItemCount() - 1) {
                        flag = false;
                    } else if (count == 0) {
                        flag = true;
                    }
                    if (flag) {
                        count++;
                    } else {
                        count--;
                    }
                    holders.bannerRecyclerView.smoothScrollToPosition(count);
                    handler2.postDelayed(this, timer);
                }
            }
        }, timer);

    }

    public void IncommingViews(final IncommingDealsHolders holders) {
        holders.tv_title.setText(Constants.incomingdeals.getTitle());
        holders.tvscrolling.setSelected(true);

        holders.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mCtx, ShopProductListActivity.class);
                intent.putExtra("CategoryName", "T-shirt");
                intent.putExtra("CategoryId", "22");
                intent.putExtra("back", "");
                mCtx.startActivity(intent);
            }
        });

            holders.tvscrolling.setText(Constants.incomingdeals.getStatements().get(0)+
                    "                                           "+Constants.incomingdeals.getStatements().get(1)+
                    "                                           "+Constants.incomingdeals.getStatements().get(2));

        int countDown = Constants.incomingdeals.getCountDown();
        new CountDownTimer(countDown, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                holders.tvDays.setText(TimeUnit.HOURS.toDays(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)) + "");

                holders.tvHours.setText((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished))) + "");

                holders.tvMinutes.setText((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))) + "");

                holders.tvSeconds.setText((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + "");
            }

            @Override

            public void onFinish() {
            }
        }.start();

    }


    public void  BrandViews(BrandsHolders holders){
        holders.brandTitle.setText("Brands In Focus");
        BrandInFocusAdapter brandInFocusAdapter = new BrandInFocusAdapter(mCtx);
        holders.rvBrands.setLayoutManager(new LinearLayoutManager(mCtx, RecyclerView.HORIZONTAL, false));
        holders.rvBrands.setAdapter(brandInFocusAdapter);
    }

    public void  BestTrendsViews(BestTrendsHolders holders){
        if (Constants.shopTreningDeals.size() >0) {
            holders.tvHeader.setText("Trending Deals");
            TrendingDealsAdapter bestTrendsAdapter = new TrendingDealsAdapter(mCtx);
//        holders.rvbestTrends.setItemAnimator(new DefaultItemAnimator());
//        holders.rvbestTrends.addItemDecoration(new ItemDecorationGrid(2, 2));
            holders.rvbestTrends.setLayoutManager(new LinearLayoutManager(mCtx, RecyclerView.HORIZONTAL, false));
            holders.rvbestTrends.setAdapter(bestTrendsAdapter);

            holders.tvviewall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mCtx, ShopProductListActivity.class);
                    intent.putExtra("back", "trending");
                    intent.putExtra("CategoryName", "Trending Deals");
                    mCtx.startActivity(intent);
                }
            });
        }
        else {
            holders.trending_main_ll.setVisibility(View.GONE);
        }
    }

    public void PopularStuffViews(PopularStuffHolders holders){
        holders.brandTitle.setText("Popular Stuff");
        PopularStuffAdapter popularStuffAdapter = new PopularStuffAdapter(mCtx);
        holders.rvBrands.setLayoutManager(new GridLayoutManager(mCtx, 2));
        holders.rvBrands.setAdapter(popularStuffAdapter);
    }

    public void TrendingNowViews(TrendingNowHolders holders){
        holders.brandTitle.setText("Trending Now");
        TrendingNowAdapter trendingNowAdapter = new TrendingNowAdapter(mCtx);
        holders.rvBrands.setLayoutManager(new GridLayoutManager(mCtx, 2));
        holders.rvBrands.setAdapter(trendingNowAdapter);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return BANNERLIST;
        }
        if (position == 1) {
            return INCOMMINGDEALS;
        }
        if (position == 2) {
            return BRANDSINFOCUS;
        }
        if (position == 3){
            return TRENDINGDEALS;
        }
        if (position == 4){
            return POPULARSTUFF;
        }
        if (position == 5){
            return TRENDINGNOW;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return objectData.size();
    }

    public class BannerHolders extends RecyclerView.ViewHolder {
        private RecyclerView bannerRecyclerView;
        BannerHolders(View view) {
            super(view);
            bannerRecyclerView=view.findViewById(R.id.bannerRecyclerView);
        }
    }

    public class IncommingDealsHolders extends RecyclerView.ViewHolder {
        private TextView tvDays,tvHours,tvMinutes,tvSeconds,tvscrolling, tv_title, tv_view;
        IncommingDealsHolders(View view) {
            super(view);
            tv_title = view.findViewById(R.id.tv_title);
            tvDays =view.findViewById(R.id.days);
            tvHours = view.findViewById(R.id.hours);
            tvMinutes = view.findViewById(R.id.minutes);
            tvSeconds =view.findViewById(R.id.seconds);
            tvscrolling =view.findViewById(R.id.scrollingtext);
            tv_view = view.findViewById(R.id.tv_view);
        }
    }

    public class BestTrendsHolders extends RecyclerView.ViewHolder {
        private RecyclerView rvbestTrends;
        private LinearLayout llHeader, trending_main_ll;
        private TextView tvHeader, tvviewall;
        BestTrendsHolders(View view) {
            super(view);
            rvbestTrends=view.findViewById(R.id.rvgrid);
            llHeader =view.findViewById(R.id.llHeader);
            tvHeader =view.findViewById(R.id.tvHeaderText);
            tvviewall = view.findViewById(R.id.tvviewall);
            trending_main_ll = view.findViewById(R.id.trending_main_ll);
//            llHeader.setBackgroundColor(colorcycle.getColor());
        }
    }

    public class BrandsHolders extends RecyclerView.ViewHolder {
        private RecyclerView rvBrands;
        private TextView brandTitle;

        BrandsHolders(View view) {
            super(view);
            rvBrands=view.findViewById(R.id.rvbarndinfocus);
            brandTitle=view.findViewById(R.id.tvtitle);
        }
    }

    public class PopularStuffHolders extends RecyclerView.ViewHolder {
        private RecyclerView rvBrands;
        private TextView brandTitle;

        PopularStuffHolders(View view) {
            super(view);
            brandTitle=view.findViewById(R.id.tvtitle);
            rvBrands=view.findViewById(R.id.rvbarndinfocus);
        }
    }

    public class TrendingNowHolders extends RecyclerView.ViewHolder {
        private RecyclerView rvBrands;
        private TextView brandTitle;

        TrendingNowHolders(View view) {
            super(view);
            brandTitle=view.findViewById(R.id.tvtitle);
            rvBrands=view.findViewById(R.id.rvbarndinfocus);
        }
    }

    public void setNotify(ArrayList<Object> objectData){
        this.objectData=objectData;
        notifyDataSetChanged();
    }

}
