package com.example.amvirgin.shopModule.shopModel;

public class IncommingDealsModel {

    private long dealTimeInMilisec=172800000;

    public long getDealTimeInMilisec() {
        return dealTimeInMilisec;
    }

    public void setDealTimeInMilisec(long dealTimeInMilisec) {
        this.dealTimeInMilisec = dealTimeInMilisec;
    }
}
