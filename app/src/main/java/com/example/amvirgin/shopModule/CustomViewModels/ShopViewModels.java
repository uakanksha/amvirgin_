package com.example.amvirgin.shopModule.CustomViewModels;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.amvirgin.shopModule.BeanModel.NavigationItemBeanModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.shopModel.ParentMenuModel;
import com.example.amvirgin.shopModule.shopModel.SubMenuModel;
import com.example.amvirgin.shopModule.shopModel.SubSubMenuModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class ShopViewModels extends ViewModel {
    private MutableLiveData<List<ParentMenuModel>> navItemList;
    private Context mCtx;
    public ShopViewModels(Context mCtx) {
        this.mCtx=mCtx;
    }

    public LiveData<List<ParentMenuModel>> getNavItemList() {
        if (navItemList == null) {
            navItemList = new MutableLiveData<List<ParentMenuModel>>();
            loadNavItemList();
        }
        return navItemList;
    }

    private void loadNavItemList() {
        final ArrayList<ParentMenuModel> parentList=new ArrayList<>();
        ServiceWrapper serviceWrapper=new ServiceWrapper(mCtx);
        Call<NavigationItemBeanModel> call=serviceWrapper.getRetrofit().create(ServiceInterface.class).getNavigationItem();
        serviceWrapper.HandleResponse(call, new ResponseHandler<NavigationItemBeanModel>(mCtx) {
            @Override
            public void onResponse(NavigationItemBeanModel response) {
                if (response.getStatus() == 200)
                {
                    try {
                        if (response.getData().size() > 0) {
                            for (int i = 0; i < response.getData().size(); i++) {
                                ParentMenuModel parentMenuModel = new ParentMenuModel();
                                parentMenuModel.setKey(response.getData().get(i).getKey());
                                parentMenuModel.setName(response.getData().get(i).getName().trim());
                                parentMenuModel.setSlug(response.getData().get(i).getSlug().trim());
                                parentMenuModel.setProducts(response.getData().get(i).getProducts());
                                if (response.getData().get(i).getIcon().getExists()){
                                    parentMenuModel.setHasIcon(true);
                                    parentMenuModel.setUrl(response.getData().get(i).getIcon().getUrl());
                                }
                                else {
                                    parentMenuModel.setHasIcon(false);
                                }
//                                parentMenuModel.setIcon(response.getData().get(i).getIcon());
//                                parentMenuModel.setChildList(response.getData().get(i).getChildren().getItems());
//                                parentMenuModel.setIcon(response.getData().get(i).getIcon());
//                                parentMenuModel.setHasIcon(response.getData().get(i).getHasIcon());
//                                parentMenuModel.setHasInner(response.getData().get(i).getHasInner());

                                if (response.getData().get(i).getChildren().getItems().size() > 0) {
                                    parentMenuModel.setHasInner(true);
                                    for (int j = 0; j < response.getData().get(i).getChildren().getItems().size(); j++) {
                                        SubMenuModel subMenuModel = new SubMenuModel();
                                        subMenuModel.setKey(response.getData().get(i).getChildren().getItems().get(j).getKey());
                                        subMenuModel.setName(response.getData().get(i).getChildren().getItems().get(j).getName().trim());
                                        subMenuModel.setSlug(response.getData().get(i).getChildren().getItems().get(j).getSlug().trim());
                                        subMenuModel.setProducts(response.getData().get(i).getChildren().getItems().get(j).getProducts());
                                        if (response.getData().get(i).getChildren().getItems().get(j).getIcon().getExists()){
                                            subMenuModel.setHasIcon(true);
                                            subMenuModel.setUrl(response.getData().get(i).getChildren().getItems().get(j).getIcon().getUrl());
                                        }
                                        else {
                                            subMenuModel.setHasIcon(false);
                                        }
//                                        subMenuModel.setHasInner(response.getData().get(i).getInner().get(j).getHasInner());

                                        if (response.getData().get(i).getChildren().getItems().get(j).getChildren().getItems().size() > 0) {
                                            subMenuModel.setHasInner(true);
                                            for (int z = 0; z < response.getData().get(i).getChildren().getItems()
                                                    .get(j).getChildren().getItems().size(); z++) {
                                                SubSubMenuModel subSubMenuModel = new SubSubMenuModel();
                                                subSubMenuModel.setKey(response.getData().get(i).getChildren().getItems()
                                                        .get(j).getChildren().getItems().get(z).getKey());
                                                subSubMenuModel.setName(response.getData().get(i).getChildren().getItems()
                                                        .get(j).getChildren().getItems().get(z).getName().trim());
                                                subSubMenuModel.setSlug(response.getData().get(i).getChildren().getItems()
                                                        .get(j).getChildren().getItems().get(z).getSlug());
                                                subSubMenuModel.setProducts(response.getData().get(i).getChildren().getItems()
                                                        .get(j).getChildren().getItems().get(z).getProducts());
                                                if (response.getData().get(i).getChildren().getItems().get(j).
                                                        getChildren().getItems().get(z).getIcon().getExists()){
                                                    subSubMenuModel.setHasIcon(true);
                                                    subSubMenuModel.setUrl(response.getData().get(i).
                                                            getChildren().getItems().get(j).getChildren().getItems().get(z).getIcon().getUrl());
                                                }
                                                else {
                                                    subSubMenuModel.setHasIcon(false);
                                                }


                                                subMenuModel.childChildList.add(subSubMenuModel);

                                            }
                                        }
                                        else {
                                            subMenuModel.setHasInner(false);
                                        }
                                        parentMenuModel.childList.add(subMenuModel);
                                    }

                                }
                                else {
                                    parentMenuModel.setHasInner(false);
                                }
                                parentList.add(parentMenuModel);
                            }
                            navItemList.postValue(parentList);
                        }
                    }catch (NullPointerException exception){
                        SnackbarManager.sException(mCtx,exception.getMessage());
                    }

                } else {
                    SnackbarManager.sException(mCtx,response.getMessage());
                }

            }
        });

    }


}
