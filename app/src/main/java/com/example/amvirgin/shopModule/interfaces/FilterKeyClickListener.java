package com.example.amvirgin.shopModule.interfaces;

import java.util.List;

public interface FilterKeyClickListener {
    void onItemClick(int key, String label, List<Object> options);
}
