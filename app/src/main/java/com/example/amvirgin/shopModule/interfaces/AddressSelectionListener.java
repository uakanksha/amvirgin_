package com.example.amvirgin.shopModule.interfaces;

public interface AddressSelectionListener {
     void onItemClick(int address_id);
}
