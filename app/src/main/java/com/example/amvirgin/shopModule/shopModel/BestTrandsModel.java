package com.example.amvirgin.shopModule.shopModel;

public class BestTrandsModel {
    int id;
    String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
