package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopHomeModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("shopSliders")
        @Expose
        private List<ShopSlider> shopSliders = null;
        @SerializedName("offerDetails")
        @Expose
        private OfferDetails offerDetails;
        @SerializedName("brandInFocus")
        @Expose
        private List<BrandInFocu> brandInFocus = null;
        @SerializedName("trendingDeals")
        @Expose
        private List<TrendingDeal> trendingDeals = null;
        @SerializedName("popularStuff")
        @Expose
        private List<PopularStuff> popularStuff = null;
        @SerializedName("trendingNow")
        @Expose
        private List<TrendingNow> trendingNow = null;

        public List<ShopSlider> getShopSliders() {
            return shopSliders;
        }

        public void setShopSliders(List<ShopSlider> shopSliders) {
            this.shopSliders = shopSliders;
        }

        public OfferDetails getOfferDetails() {
            return offerDetails;
        }

        public void setOfferDetails(OfferDetails offerDetails) {
            this.offerDetails = offerDetails;
        }

        public List<BrandInFocu> getBrandInFocus() {
            return brandInFocus;
        }

        public void setBrandInFocus(List<BrandInFocu> brandInFocus) {
            this.brandInFocus = brandInFocus;
        }

        public List<TrendingDeal> getTrendingDeals() {
            return trendingDeals;
        }

        public void setTrendingDeals(List<TrendingDeal> trendingDeals) {
            this.trendingDeals = trendingDeals;
        }

        public List<PopularStuff> getPopularStuff() {
            return popularStuff;
        }

        public void setPopularStuff(List<PopularStuff> popularStuff) {
            this.popularStuff = popularStuff;
        }

        public List<TrendingNow> getTrendingNow() {
            return trendingNow;
        }

        public void setTrendingNow(List<TrendingNow> trendingNow) {
            this.trendingNow = trendingNow;
        }

    }

    public class ShopSlider {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("banner")
        @Expose
        private String banner;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("target")
        @Expose
        private String target;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getBanner() {
            return banner;
        }

        public void setBanner(String banner) {
            this.banner = banner;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTarget() {
            return target;
        }

        public void setTarget(String target) {
            this.target = target;
        }

    }

    public class OfferDetails {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("statements")
        @Expose
        private List<String> statements = null;
        @SerializedName("countDown")
        @Expose
        private Integer countDown;
        @SerializedName("visible")
        @Expose
        private Boolean visible;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public List<String> getStatements() {
            return statements;
        }

        public void setStatements(List<String> statements) {
            this.statements = statements;
        }

        public Integer getCountDown() {
            return countDown;
        }

        public void setCountDown(Integer countDown) {
            this.countDown = countDown;
        }

        public Boolean getVisible() {
            return visible;
        }

        public void setVisible(Boolean visible) {
            this.visible = visible;
        }

    }

    public class BrandInFocu {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("products")
        @Expose
        private Integer products;
        @SerializedName("icon")
        @Expose
        private Icon icon;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getProducts() {
            return products;
        }

        public void setProducts(Integer products) {
            this.products = products;
        }

        public Icon getIcon() {
            return icon;
        }

        public void setIcon(Icon icon) {
            this.icon = icon;
        }

    }

    public class TrendingDeal {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("images")
        @Expose
        private List<String> images = null;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getImages() {
            return images;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

    }

    public class PopularStuff {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("products")
        @Expose
        private Integer products;
        @SerializedName("icon")
        @Expose
        private Icon icon;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getProducts() {
            return products;
        }

        public void setProducts(Integer products) {
            this.products = products;
        }

        public Icon getIcon() {
            return icon;
        }

        public void setIcon(Icon icon) {
            this.icon = icon;
        }

    }

    public class TrendingNow {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("products")
        @Expose
        private Integer products;
        @SerializedName("icon")
        @Expose
        private Icon icon;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getProducts() {
            return products;
        }

        public void setProducts(Integer products) {
            this.products = products;
        }

        public Icon getIcon() {
            return icon;
        }

        public void setIcon(Icon icon) {
            this.icon = icon;
        }


    }

    private class Icon__ {
        @SerializedName("exists")
        @Expose
        private Boolean exists;
        @SerializedName("url")
        @Expose
        private String url;

        public Boolean getExists() {
            return exists;
        }

        public void setExists(Boolean exists) {
            this.exists = exists;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public class Icon {

        @SerializedName("exists")
        @Expose
        private Boolean exists;
        @SerializedName("url")
        @Expose
        private String url;

        public Boolean getExists() {
            return exists;
        }

        public void setExists(Boolean exists) {
            this.exists = exists;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

    }

}


