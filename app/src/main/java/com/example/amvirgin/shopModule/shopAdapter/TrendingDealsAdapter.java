package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ProductDetailActivity;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.utils.Constants;

import java.util.List;

public class TrendingDealsAdapter extends RecyclerView.Adapter<TrendingDealsAdapter.TrendingDealsViewHolder> {

    Context context;
    List<ShopHomeModel.TrendingDeal> trendingDeals;

    public TrendingDealsAdapter(Context context) {
        this.context = context;
        this.trendingDeals = Constants.shopTreningDeals;
    }

    @NonNull
    @Override
    public TrendingDealsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_trending_deals_layout, parent, false);
        TrendingDealsViewHolder holder = new TrendingDealsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TrendingDealsViewHolder holder, final int position) {
        if (trendingDeals.get(position).getImages().size() > 0) {
            if (!trendingDeals.get(position).getImages().get(0).equalsIgnoreCase("")) {
                Glide.with(context).load(trendingDeals.get(position).getImages().get(0)).into(holder.banner_img);
            } else {
                holder.banner_img.setImageResource(R.drawable.placeholder);
            }
        }
        else {
            holder.banner_img.setImageResource(R.drawable.placeholder);
        }
        holder.name.setText(trendingDeals.get(position).getName());
        holder.container_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ProductId = trendingDeals.get(position).getKey();
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("ProductId", ProductId);
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return trendingDeals.size();
    }

    public class TrendingDealsViewHolder extends RecyclerView.ViewHolder{
        ImageView banner_img;
        TextView name, type;
        LinearLayout container_ll;

        public TrendingDealsViewHolder(@NonNull View itemView) {
            super(itemView);

            banner_img = itemView.findViewById(R.id.banner_img);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            container_ll = itemView.findViewById(R.id.container_ll);
        }
    }
}
