package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amvirgin.Adapter.OrderSummaryAdapter;
import com.example.amvirgin.R;
import com.example.amvirgin.payment.model.PaymentFieldModel;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.payment.PaymentActivity;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.shopAdapter.ShopCartListAdapter;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;
import com.example.amvirgin.shopModule.shopModel.RetrieveCartModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;

public class ShopOrderSummaryActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView_orderSummary;
    private RecyclerView.LayoutManager layoutManager;
    private RelativeLayout rl_back;
    private Button btn_addAddress ,pamentContinue;
    private String token, session_id;
    TextView tv_price_subtotal, tv_total_price, tv_tax, tv_total_price_;
    TextView tv_name, tv_address, tv_locality, tv_state, tv_city, tv_pincode, tv_mobile, tv_type;
    int address_id;
    int last_address;
    List<AddToCartModel.Item> cartData;
    AddToCartModel.Cart cart_data;
    int customerId;

    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_order_summary);

        PreferenceManager.delete(getApplicationContext(), "redirect");

        if (getIntent().getExtras() != null){
            address_id = getIntent().getExtras().getInt("address_id");
        }

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
                customerId = PreferenceManager.getUser(getApplicationContext()).getKey();
        }

        session_id = PreferenceManager.getString(getApplicationContext(), "session_id");
        retrieveCart(session_id);

        tv_price_subtotal = findViewById(R.id.tv_price_subtotal);
        tv_total_price = findViewById(R.id.tv_total_price);
        tv_tax = findViewById(R.id.tv_tax);
        tv_total_price_ = findViewById(R.id.tv_total_price_);

        tv_name = findViewById(R.id.tv_name);
        tv_address = findViewById(R.id.tv_address);
        tv_locality = findViewById(R.id.tv_locality);
        tv_state = findViewById(R.id.tv_state);
        tv_city = findViewById(R.id.tv_city);
        tv_pincode = findViewById(R.id.tv_pincode);
        tv_mobile = findViewById(R.id.tv_mobile);
        tv_type = findViewById(R.id.tv_type);

        recyclerView_orderSummary=(RecyclerView)findViewById(R.id.recycler_orderSummary);
        rl_back = (RelativeLayout)findViewById(R.id.rlBack);
        btn_addAddress = (Button) findViewById(R.id.btn_addAddress);
        pamentContinue=(Button)findViewById(R.id.countinue_Payment);

        layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView_orderSummary.setLayoutManager(layoutManager);

        OrderSummaryAdapter orderSummaryAdapter = new OrderSummaryAdapter(getApplicationContext());
        recyclerView_orderSummary.setAdapter(orderSummaryAdapter);

        rl_back.setOnClickListener(this);
        btn_addAddress.setOnClickListener(this);
        pamentContinue.setOnClickListener(this);

        getAddresses(token);

    }

    private void getAddresses(String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<GetAddressModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getAddress("application/json", "Bearer "+token, "no-cache");
        serviceWrapper.HandleResponse(call, new ResponseHandler<GetAddressModel>(ShopOrderSummaryActivity.this) {
            @Override
            public void onResponse(GetAddressModel response) {
                if (response.getStatus() == 200){

                        List<GetAddressModel.Datum> data = response.getData();
                        if (data.size() > 0){

                            if (address_id != 0){
                                for (int i = 0; i < data.size(); i++) {
                                    if (data.get(i).getKey() == address_id) {
                                        last_address = i;
                                        // break;  // uncomment to get the first instance
                                    }
                                }
                            }

                            else {
                                last_address = data.size() - 1;
                                address_id = data.get(last_address).getKey();
                            }

                            tv_name.setText(data.get(last_address).getName());
                            tv_address.setText(data.get(last_address).getAddress());
                            tv_locality.setText(data.get(last_address).getLocality());
                            tv_city.setText(data.get(last_address).getCity().getName());
                            tv_state.setText(data.get(last_address).getState().getName());
                            tv_pincode.setText(String.valueOf(data.get(last_address).getPinCode()));
                            tv_mobile.setText(data.get(last_address).getMobile());
                            tv_type.setText(data.get(last_address).getType());

                        }
                }
                else {
                    SnackbarManager.sException(ShopOrderSummaryActivity.this, response.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.btn_addAddress:
//                startActivity(new Intent(getApplicationContext(), MyAddressActivity.class));
                Intent intent = new Intent(ShopOrderSummaryActivity.this, MyAddressActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("address_id", address_id);
                startActivity(intent);
                break;
            case R.id.countinue_Payment:
               // Bundle bundle = new Bundle();
                //bundle.putSerializable("cartData", (Serializable) cartData);

                Intent intent_payment = new Intent(getApplicationContext(), PaymentActivity.class);
//                intent_payment.putExtras(bundle);
//                intent_payment.putExtra("VIDEO_PAYMENT", false);
//                intent_payment.putExtra("total_price", cart_data.getTotal());
//                intent_payment.putExtra("address_id", address_id);
//                intent_payment.putExtra("price_subtotal", tv_price_subtotal.getText().toString());
//                intent_payment.putExtra("total_price_", tv_total_price_.getText().toString());
//                intent_payment.putExtra("tax", tv_tax.getText().toString());
                intent_payment.putExtra(Constants.CHECKOUT_DETAILS, new PaymentFieldModel(
                        "Product ID",
                        "AmVirgin",
                        "Order Total Value",
                        "https://github.githubassets.com/images/modules/open_graph/github-octocat.png",
                        cart_data.getSubTotal().toString(),
                        cart_data.getTax().toString(),
                        cart_data.getTotal().toString(),
                        true,
                        "Free",
                        address_id));

                intent_payment.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent_payment);

//                intent_payment.putExtra("price_subtotal", tv_price_subtotal.getText().toString());
//                intent_payment.putExtra("total_price_", tv_total_price_.getText().toString());
//                intent_payment.putExtra("tax", tv_tax.getText().toString());

                break;
        }
    }

    private void retrieveCart(final String session_id) {
//        pGif.setVisibility(View.VISIBLE);
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());

        Call<AddToCartModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .retrieveCart("customer/cart/retrieve?sessionId="+session_id+"&customerId="+customerId);

        serviceWrapper.HandleResponse(call, new ResponseHandler<AddToCartModel>(this) {
            @Override
            public void onResponse(AddToCartModel response) {
//                pGif.setVisibility(View.GONE);
                if (response.getStatus() == 200){
                    try {
                        cartData = response.getData().getCart().getItems();
                        String type = "order";
                        cart_data = response.getData().getCart();
                            List<AddToCartModel.Item> items = response.getData().getCart().getItems();

                            layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerView_orderSummary.setLayoutManager(layoutManager);
                            ShopCartListAdapter adapter = new ShopCartListAdapter(ShopOrderSummaryActivity.this, items, type,
                                    new RemoveFromCartClickListener() {
                                        @Override
                                        public void onItemClick() {
                                            retrieveCart(session_id);
                                        }
                                    });
                            recyclerView_orderSummary.setAdapter(adapter);

                            tv_price_subtotal.setText(String.valueOf("₹ "+cart_data.getSubTotal()));
                            tv_tax.setText(String.valueOf("₹ "+cart_data.getTax()));
                            tv_total_price.setText(String.valueOf("₹ "+cart_data.getTotal()));
                            tv_total_price_.setText(String.valueOf("₹ "+cart_data.getTotal()));

                    }
                    catch (Exception e){
                        SnackbarManager.sException(ShopOrderSummaryActivity.this,response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ShopOrderSummaryActivity.this,response.getMessage());
                }
            }
        });
    }
}
