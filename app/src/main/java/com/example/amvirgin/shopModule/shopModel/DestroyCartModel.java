package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DestroyCartModel {

        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private Data data;

        public Integer getStatus() {
        return status;
    }

        public void setStatus(Integer status) {
        this.status = status;
    }

        public String getMessage() {
        return message;
    }

        public void setMessage(String message) {
        this.message = message;
    }

        public Data getData() {
        return data;
    }

        public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("cart")
        @Expose
        private Cart cart;

        public Cart getCart() {
            return cart;
        }

        public void setCart(Cart cart) {
            this.cart = cart;
        }

    }

    public class Cart {

        @SerializedName("session")
        @Expose
        private String session;
        @SerializedName("address")
        @Expose
        private Object address;
        @SerializedName("customer")
        @Expose
        private Object customer;
        @SerializedName("itemCount")
        @Expose
        private Integer itemCount;
        @SerializedName("subTotal")
        @Expose
        private Integer subTotal;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("total")
        @Expose
        private Double total;
        @SerializedName("paymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("items")
        @Expose
        private List<Object> items = null;

        public String getSession() {
            return session;
        }

        public void setSession(String session) {
            this.session = session;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public Object getCustomer() {
            return customer;
        }

        public void setCustomer(Object customer) {
            this.customer = customer;
        }

        public Integer getItemCount() {
            return itemCount;
        }

        public void setItemCount(Integer itemCount) {
            this.itemCount = itemCount;
        }

        public Integer getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(Integer subTotal) {
            this.subTotal = subTotal;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Object> getItems() {
            return items;
        }

        public void setItems(List<Object> items) {
            this.items = items;
        }

    }
}
