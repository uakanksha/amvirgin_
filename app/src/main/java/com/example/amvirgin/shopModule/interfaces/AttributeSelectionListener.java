package com.example.amvirgin.shopModule.interfaces;

public interface AttributeSelectionListener {
    void onItemClick(int key);
}
