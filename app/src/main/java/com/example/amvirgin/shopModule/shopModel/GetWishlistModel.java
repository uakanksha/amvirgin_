package com.example.amvirgin.shopModule.shopModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetWishlistModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("key")
        @Expose
        private Integer key;
        @SerializedName("slug")
        @Expose
        private String slug;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("price")
        @Expose
        private Price price;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("options")
        @Expose
        private List<Option> options = null;

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Price getPrice() {
            return price;
        }

        public void setPrice(Price price) {
            this.price = price;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public List<Option> getOptions() {
            return options;
        }

        public void setOptions(List<Option> options) {
            this.options = options;
        }

    }

    public class Option {

        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("value")
        @Expose
        private List<String> value = null;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }

    }

    public class Price {

        @SerializedName("original")
        @Expose
        private Integer original;
        @SerializedName("selling")
        @Expose
        private Integer selling;

        public Integer getOriginal() {
            return original;
        }

        public void setOriginal(Integer original) {
            this.original = original;
        }

        public Integer getSelling() {
            return selling;
        }

        public void setSelling(Integer selling) {
            this.selling = selling;
        }

    }

}

