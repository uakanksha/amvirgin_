package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.BeanModel.NavigationItemBeanModel;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;

import java.util.List;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryListViewHolder> {
    Context mCtx;
    List<NavigationItemBeanModel.Datum> data;

    public CategoryListAdapter(Context mCtx, List<NavigationItemBeanModel.Datum> data) {
        this.mCtx = mCtx;
        this.data = data;
    }

    @NonNull
    @Override
    public CategoryListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_layout, parent, false);
        CategoryListViewHolder holder = new CategoryListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryListViewHolder holder, final int position) {
        holder.itemName.setText(data.get(position).getName());
        holder.itemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String categoryId = String.valueOf(data.get(position).getKey());
                String categoryName = String.valueOf(data.get(position).getName());
                Intent intent = new Intent(mCtx, ShopProductListActivity.class);
                intent.putExtra("CategoryName", categoryName);
                intent.putExtra("CategoryId", categoryId);
                intent.putExtra("back", "");
                mCtx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CategoryListViewHolder extends RecyclerView.ViewHolder{
        TextView itemName;
        ImageView itemImage;
        CardView cv_category_layout;

        public CategoryListViewHolder(@NonNull View itemView) {
            super(itemView);

            itemImage = itemView.findViewById(R.id.itemImage);
            itemName = itemView.findViewById(R.id.itemName);
            cv_category_layout = itemView.findViewById(R.id.cv_cat_layout);
        }
    }
}
