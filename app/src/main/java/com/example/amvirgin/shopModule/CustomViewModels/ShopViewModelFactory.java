package com.example.amvirgin.shopModule.CustomViewModels;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ShopViewModelFactory extends ViewModelProvider.NewInstanceFactory{
    private Context context;
    public ShopViewModelFactory(Context context) {
      this.context=context;
    }
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return  (T)  new ShopViewModels(context);
    }
}
