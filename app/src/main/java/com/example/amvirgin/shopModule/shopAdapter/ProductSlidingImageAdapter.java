package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ImageEnlargeActivity;

import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.List;

public class ProductSlidingImageAdapter extends PagerAdapter {

    List<String> slides;
    Context context;
    String banner_img;

    public ProductSlidingImageAdapter(Context context, List<String> slides) {
        this.context = context;
        this.slides = slides;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = LayoutInflater.from(context).inflate(R.layout.banner_layout_product, view, false);

//        assert imageLayout != null;
        ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.product_img);

        if (slides.size() > 0){
            Glide.with(context).load(slides.get(position).toString()).into(imageView);
        }
        else {
            imageView.setImageResource(R.drawable.placeholder);
        }
//        Glide.with(context).load(slides.get(position).getUrl()).into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ImageEnlargeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("images", (Serializable) slides);
                intent.putExtra("position", position);
                context.startActivity(intent);
            }
        });

        view.addView(imageLayout);

        return imageLayout;
    }

    @Override
    public int getCount() {
//        return data.size();
        return slides.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
