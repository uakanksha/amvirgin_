package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.shopAdapter.ShopWishListAdapter;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.GetWishlistModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import java.util.List;

import lal.adhish.gifprogressbar.GifView;
import retrofit2.Call;

public class ShopWishListActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView_productWish;
    private RecyclerView.LayoutManager layoutManager;
    private RelativeLayout btn_back,btn_shopingbag, empty_wishList_rl, wishList_rl;
    int cart_count;
    private TextView tvCart1;

    private String token;
    Button start_shopping_btn;

    GifView pGif;

    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
        cart_count = PreferenceManager.getInt(getApplicationContext(), "cart_count");

        if (cart_count > 0){
            tvCart1.setVisibility(View.VISIBLE);
            tvCart1.setText(String.valueOf(cart_count));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_wish_list);

        PreferenceManager.delete(getApplicationContext(), "redirect");

        cart_count = PreferenceManager.getInt(getApplicationContext(), "cart_count");

        tvCart1 = findViewById(R.id.tvCart1);
        empty_wishList_rl = findViewById(R.id.empty_wishList_rl);
        wishList_rl = findViewById(R.id.wishList_rl);

        start_shopping_btn = findViewById(R.id.start_shopping_btn);

        pGif = (GifView) findViewById(R.id.progressBar);
        pGif.setImageResource(R.drawable.loader_red);

        start_shopping_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
                intent.putExtra("back", "cart");
                startActivity(intent);
            }
        });
        if (cart_count > 0){
            tvCart1.setVisibility(View.VISIBLE);
            tvCart1.setText(String.valueOf(cart_count));
        }

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        if (token != null) {
            getWishList(token);
        }
        else {
            PreferenceManager.saveString(getApplicationContext(), "redirect", "wishlist");
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.putExtra("notlogin", "notlogin");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        recyclerView_productWish=(RecyclerView)findViewById(R.id.recycler_wishListProduct);

        btn_back=(RelativeLayout) findViewById(R.id.rlBack);
        btn_shopingbag=(RelativeLayout) findViewById(R.id.rlCart1);

        btn_back.setOnClickListener(this);
        btn_shopingbag.setOnClickListener(this);

    }

    private void getWishList(final String token) {


        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<GetWishlistModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getWishlist("application/json", "Bearer "+token, "no-cache");
        serviceWrapper.HandleResponse(call, new ResponseHandler<GetWishlistModel>(this) {
            @Override
            public void onResponse(GetWishlistModel response) {
                pGif.setVisibility(View.GONE);
                if (response.getStatus() == 200){
                    try {
                        List<GetWishlistModel.Datum> data = response.getData();

                        if (data.size() > 0) {

                            wishList_rl.setVisibility(View.VISIBLE);
                            empty_wishList_rl.setVisibility(View.GONE);

//                        layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            layoutManager = new GridLayoutManager(ShopWishListActivity.this, 2);
                            recyclerView_productWish.setLayoutManager(layoutManager);

                            ShopWishListAdapter shopWishListAdapter = new ShopWishListAdapter(getApplicationContext(), data,
                                    new RemoveFromCartClickListener() {
                                        @Override
                                        public void onItemClick() {
                                            getWishList(token);
                                            cart_count = PreferenceManager.getInt(getApplicationContext(), "cart_count");
                                            if (cart_count > 0){
                                                tvCart1.setVisibility(View.VISIBLE);
                                                tvCart1.setText(String.valueOf(cart_count));
                                            }
                                        }
                                    });
                            recyclerView_productWish.setAdapter(shopWishListAdapter);
                        }
                        else {
                            wishList_rl.setVisibility(View.GONE);
                            empty_wishList_rl.setVisibility(View.VISIBLE);
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ShopWishListActivity.this,response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ShopWishListActivity.this,response.getMessage());
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;

            case R.id.rlCart1:
                startActivity(new Intent(getApplicationContext(), CartProductShopActivity.class));
                break;
        }
    }
}
