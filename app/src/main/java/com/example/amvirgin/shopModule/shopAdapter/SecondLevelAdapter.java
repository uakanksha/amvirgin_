package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;
import com.example.amvirgin.shopModule.shopModel.ParentMenuModel;
import com.example.amvirgin.shopModule.shopModel.SubMenuModel;
import com.example.amvirgin.shopModule.shopModel.SubSubMenuModel;

import java.util.ArrayList;

public class SecondLevelAdapter extends BaseExpandableListAdapter {
    String headers;
    int id;
    OnItemClickListener mItemClickListener;
    private Context mContext;
    private ArrayList<SubSubMenuModel> childLists;
    private ArrayList<SubMenuModel> subMenuLists;


    public SecondLevelAdapter(Context context, String str, int i, ArrayList<SubSubMenuModel> arrayList,ArrayList<SubMenuModel> subMenuLists) {
        this.mContext = context;
        this.childLists = arrayList;
        this.headers = str;
        this.id = i;
        this.subMenuLists=subMenuLists;


    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childLists.get(groupPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.child_level_two, parent, false);
        }
        TextView txtChild = convertView.findViewById(R.id.tv_child_two);
        txtChild.setText(childLists.get(childPosition).getName());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String categoryId = String.valueOf(childLists.get(childPosition).getKey());
                String categoryName = String.valueOf(childLists.get(childPosition).getName());
                Intent intent = new Intent(mContext, ShopProductListActivity.class);
                intent.putExtra("CategoryName", categoryName);
                intent.putExtra("CategoryId", categoryId);
                intent.putExtra("back", "");
                mContext.startActivity(intent);
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childLists.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return 1;
    }

    @Override
    public int getGroupCount() {
        return 1;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        SubMenuModel subMenuModel = subMenuLists.get(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.child_level_one, parent, false);
        }
        TextView lblListHeader = convertView.findViewById(R.id.tv_title_one);
        ImageView indicator = convertView.findViewById(R.id.explist_indicator);
        lblListHeader.setText(headers);

        if (isExpanded) {
            indicator.setImageResource(R.drawable.ic_expand_less);
        } else {
            indicator.setImageResource(R.drawable.ic_expand);
        }
        if (subMenuModel.isHasInner()){
            indicator.setVisibility(View.VISIBLE);
        }else {
            indicator.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void SetOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int id, String name);
    }

}
