package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.SplashActivity;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.ProductDetailActivity;
import com.example.amvirgin.shopModule.interfaces.RemoveFromCartClickListener;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.GetWishlistModel;
import com.example.amvirgin.shopModule.shopModel.MoveToWishlistModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;


public class ShopWishListAdapter extends RecyclerView.Adapter<ShopWishListAdapter.ShopWishViewHolder> {

    Context context;
    List<GetWishlistModel.Datum> data;
    private String session_id, token;
    int cart_count;
    RemoveFromCartClickListener removeFromCartClickListener;
    String message;

    public ShopWishListAdapter(Context context, List<GetWishlistModel.Datum> data,
                               RemoveFromCartClickListener removeFromCartClickListener) {
        this.context = context;
        this.data = data;
        this.removeFromCartClickListener = removeFromCartClickListener;
    }

    @NonNull
    @Override
    public ShopWishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_shop_wishlist_layout, parent, false);
        ShopWishViewHolder cvh = new ShopWishViewHolder(view);
        return cvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ShopWishViewHolder holder, final int position) {
        cart_count = PreferenceManager.getInt(context, "cart_count");
        session_id = PreferenceManager.getString(context, "session_id");
        if (PreferenceManager.getBoolean(context, Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(context).getToken();
        }

        holder.tv_product_name.setText(data.get(position).getName());
        holder.tv_product_price.setText(String.valueOf(data.get(position).getPrice().getSelling()));
        holder.original_price1.setText(String.valueOf("₹ "+data.get(position).getPrice().getOriginal()));
        holder.tv_product_rating.setText(String.valueOf(data.get(position).getRating()));

        if (data.get(position).getImage() != null) {
            Glide.with(context).load(data.get(position).getImage()).into(holder.iv_product_img);
        }
        else {
            holder.iv_product_img.setImageResource(R.drawable.placeholder);
        }

        holder.original_price1.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        holder.iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message = "Item Removed from Wishlist!";
                deleteFromWishlist(data.get(position).getKey() , token, holder, message);
            }
        });
        
        holder.move_to_cart_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (token != null) {
                    moveToCart(data.get(position).getKey(), session_id, token, holder);
                }
                else {
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.putExtra("notlogin", "notlogin");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });

    }

    private void deleteFromWishlist(Integer key, String token, final ShopWishViewHolder holder, final String message) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<MoveToWishlistModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .deleteFromWishlist(String.valueOf(key), "application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<MoveToWishlistModel>(context) {
            @Override
            public void onResponse(MoveToWishlistModel response) {
                if (response.getStatus() == 200){
                    try {
                        ToastMessage.getInstance(holder.itemView.getContext()).showSuccessShortCustomToast(message);

//                        Intent intent = new Intent(context, ShopWishListActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        context.startActivity(intent);

                        if (removeFromCartClickListener != null){
                            removeFromCartClickListener.onItemClick();
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                }
            }
        });
    }

    private void moveToCart(final Integer key, String session_id, final String token, final ShopWishViewHolder holder) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("sessionId", session_id);

        ServiceWrapper serviceWrapper = new ServiceWrapper(context);
        Call<MoveToWishlistModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .moveToCart(String.valueOf(key), "application/json", "Bearer "+token, jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<MoveToWishlistModel>(context) {
            @Override
            public void onResponse(MoveToWishlistModel response) {
//                if (response.getStatus() == 200 && response.getMessage().equalsIgnoreCase
//                        ("No cart was found for that session.")) {
//                    addToCart(session_id, key, holder);
//                }
                 if (response.getStatus() == 200) {
                    try {
                        ToastMessage.getInstance(holder.itemView.getContext()).showSuccessShortCustomToast(response.getMessage());

//                        Intent intent = new Intent(context, ShopWishListActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        context.startActivity(intent);

                        SplashActivity.cart_products.add(key);

                        int new_count = cart_count + 1;
                        PreferenceManager.saveInt(context, "cart_count", new_count);

                        if (removeFromCartClickListener != null) {
                            removeFromCartClickListener.onItemClick();
                        }

                    } catch (Exception e) {
                        SnackbarManager.sException(holder.itemView.getContext(), response.getMessage());
                    }
                }

                else if (response.getStatus() == 404){
                    try{
                        message = "Item Moved to Cart!";
                        deleteFromWishlist(key , token, holder, message);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(holder.itemView.getContext(), response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(holder.itemView.getContext(),response.getMessage());
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ShopWishViewHolder extends RecyclerView.ViewHolder {

        TextView original_price1, tv_product_name, tv_product_price, tv_product_rating;
        ImageView iv_product_img, iv_close;
        LinearLayout wish_ll, move_to_cart_ll;

        public ShopWishViewHolder(@NonNull View itemView) {
            super(itemView);

            original_price1=itemView.findViewById(R.id.original_price1);
            tv_product_name = itemView.findViewById(R.id.tv_product_name);
            tv_product_price = itemView.findViewById(R.id.tv_product_price);
            tv_product_rating = itemView.findViewById(R.id.tv_product_rating);
            iv_product_img = itemView.findViewById(R.id.iv_product_img);
            wish_ll = itemView.findViewById(R.id.wish_ll);
            move_to_cart_ll = itemView.findViewById(R.id.move_to_cart_ll);
            iv_close = itemView.findViewById(R.id.iv_close);
        }
    }

}