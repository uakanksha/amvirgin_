package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.shopModel.AddToCartModel;
import com.example.amvirgin.shopModule.shopModel.OrderDetailsModel;
import com.example.amvirgin.shopModule.shopModel.OrderListModel;

import java.util.ArrayList;
import java.util.List;

public class ProductAttributeAdapter extends RecyclerView.Adapter<ProductAttributeAdapter.ProductAttributeViewHolder> {
    Context context;
    List<AddToCartModel.Option> attributes;

    ArrayList val;
    String valStr;
    List<OrderListModel.Option> options;
    List<OrderDetailsModel.Option> optionList;

    public ProductAttributeAdapter(Context context, List<AddToCartModel.Option> attributes,
                                   List<OrderListModel.Option> options, List<OrderDetailsModel.Option> optionList) {
        this.context = context;
        this.attributes = attributes;
        this.options = options;
        this.optionList = optionList;
    }

    @NonNull
    @Override
    public ProductAttributeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.rv_product_attribute_layout, parent, false);
        ProductAttributeViewHolder viewHolder = new ProductAttributeViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductAttributeViewHolder holder, int position) {
//        holder.tv_attributes.setText((CharSequence) attributes.get(position));
//        JSONArray jsonA = JSONArray.fromObject(attributes);
//        System.out.println(jsonA);

//        JsonArray jsonArray = new JsonArray(attributes);
        if (attributes != null) {

            holder.tv_key.setText(attributes.get(position).getLabel() + ": ");

            if (attributes.get(position).getValue().getClass().getSimpleName().equalsIgnoreCase("ArrayList")) {
                val = (ArrayList) attributes.get(position).getValue();
                holder.tv_value.setText(val.get(0).toString());
            } else if (attributes.get(position).getValue().getClass().getSimpleName().equalsIgnoreCase("String")) {
                valStr = (String) attributes.get(position).getValue();

                holder.tv_value.setText(valStr);
            }
        }
        else if (options != null){
            holder.tv_key.setText(options.get(position).getLabel() + ": ");

            if (options.get(position).getValue().getClass().getSimpleName().equalsIgnoreCase("ArrayList")) {
                val = (ArrayList) options.get(position).getValue();
                holder.tv_value.setText(val.get(0).toString());
            } else if (options.get(position).getValue().getClass().getSimpleName().equalsIgnoreCase("String")) {
                valStr = (String) options.get(position).getValue();

                holder.tv_value.setText(valStr);
            }
        }
        else if (optionList != null){
            holder.tv_key.setText(optionList.get(position).getLabel() + ": ");

            if (optionList.get(position).getValue().getClass().getSimpleName().equalsIgnoreCase("ArrayList")) {
                val = (ArrayList) optionList.get(position).getValue();
                holder.tv_value.setText(val.get(0).toString());
            } else if (optionList.get(position).getValue().getClass().getSimpleName().equalsIgnoreCase("String")) {
                valStr = (String) optionList.get(position).getValue();

                holder.tv_value.setText(valStr);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (attributes != null) {
            return attributes.size();
        }
        else if (options != null){
            return options.size();
        }
        else if (optionList != null){
            return optionList.size();
        }
        else {
            return 0;
        }
    }

    public class ProductAttributeViewHolder extends RecyclerView.ViewHolder{
        TextView tv_key, tv_value;

        public ProductAttributeViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_key = itemView.findViewById(R.id.tv_key);
            tv_value = itemView.findViewById(R.id.tv_value);
        }
    }
}
