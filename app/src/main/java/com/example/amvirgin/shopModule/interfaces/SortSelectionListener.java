package com.example.amvirgin.shopModule.interfaces;

public interface SortSelectionListener {
     void onItemClick(String sort_key);
}
