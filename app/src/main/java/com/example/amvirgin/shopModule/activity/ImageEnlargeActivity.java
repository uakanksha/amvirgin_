package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.amvirgin.shopModule.shopAdapter.ProductSlidingEnlargeImageAdapter;
import com.example.amvirgin.shopModule.shopAdapter.ProductSlidingImageAdapter;
import com.example.amvirgin.R;

import java.util.List;

public class ImageEnlargeActivity extends AppCompatActivity {
    private ImageView close;
    private ViewPager pager;
    List<String> slides;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_enlarge);

        if (getIntent().getExtras() != null){
            position = getIntent().getExtras().getInt("position");
            slides = (List<String>) getIntent().getSerializableExtra("images");
        }

        close = findViewById(R.id.close);
        pager = findViewById(R.id.pager);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        pager.setAdapter(new ProductSlidingEnlargeImageAdapter(getApplicationContext(), slides));
        pager.setCurrentItem(position);

    }
}
