package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.shopModel.OrderTrackingModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import retrofit2.Call;

public class OrderTrackingActivity extends AppCompatActivity {
    private LinearLayout order_details_ll, step1_ll, step2_ll, step3_ll, step4_ll;
    private int orderId;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_tracking);

        if (getIntent().getExtras() != null){
            orderId = getIntent().getExtras().getInt("orderId");
        }

        order_details_ll = findViewById(R.id.order_details_ll);
        step1_ll = findViewById(R.id.step1_ll);
        step2_ll = findViewById(R.id.step2_ll);
        step3_ll = findViewById(R.id.step3_ll);
        step4_ll = findViewById(R.id.step4_ll);

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        if (token != null) {
            getOrderTracking(orderId, token);
        }
        else {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.putExtra("notlogin", "notlogin");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        order_details_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ShopMyOrderDetailsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("orderId", orderId);
                startActivity(intent);
            }
        });

    }

    private void getOrderTracking(int orderId, String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<OrderTrackingModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getTrackingDetails("customer/orders/"+orderId+"/track", "application/json",
                        "Bearer "+ token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<OrderTrackingModel>(this) {
            @Override
            public void onResponse(OrderTrackingModel response) {
                if (response.getStatus() == 200){
                        if (response.getData().getStatus().equalsIgnoreCase("placed")){
                            step1_ll.setVisibility(View.VISIBLE);
                            step2_ll.setVisibility(View.GONE);
                            step3_ll.setVisibility(View.GONE);
                            step4_ll.setVisibility(View.GONE);
                        }
                        else if (response.getData().getStatus().equalsIgnoreCase("dispatched")){
                            step1_ll.setVisibility(View.GONE);
                            step2_ll.setVisibility(View.VISIBLE);
                            step3_ll.setVisibility(View.GONE);
                            step4_ll.setVisibility(View.GONE);
                        }
                        else if (response.getData().getStatus().equalsIgnoreCase("out-for-delivery")){
                            step1_ll.setVisibility(View.GONE);
                            step2_ll.setVisibility(View.GONE);
                            step3_ll.setVisibility(View.VISIBLE);
                            step4_ll.setVisibility(View.GONE);
                        }
                        else if (response.getData().getStatus().equalsIgnoreCase("delivered")){
                            step1_ll.setVisibility(View.GONE);
                            step2_ll.setVisibility(View.GONE);
                            step3_ll.setVisibility(View.GONE);
                            step4_ll.setVisibility(View.VISIBLE);
                        }
                }
                else {
                    SnackbarManager.sException(OrderTrackingActivity.this,response.getMessage());
                }
            }
        });
    }
}
