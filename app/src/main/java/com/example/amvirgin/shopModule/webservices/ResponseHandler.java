package com.example.amvirgin.shopModule.webservices;

import android.content.Context;

import com.example.amvirgin.shopModule.Other.SnackManager;
import com.example.amvirgin.shopModule.Other.SnackbarManager;


public abstract class ResponseHandler<T> {
    Context mContext;
    public ResponseHandler(Context context){
        super();
        this.mContext = context;
    }
    public abstract void onResponse(final T response);

    public void onError(final String errorResponse) {
        SnackManager.getInstance().showSnackbar(mContext,errorResponse,SnackManager.TYPE_EXCEPTION);
    }

    public void onFailure(Throwable throwable) {
        SnackManager.getInstance().showSnackbar(mContext,throwable.getMessage(),SnackManager.TYPE_EXCEPTION);
    }

    public void  onInternetUnavailable(){
        SnackManager.getInstance().showSnackbar(mContext,"Make sure you have an active data connection",
                SnackManager.TYPE_NO_INTERNET);
    }

}
