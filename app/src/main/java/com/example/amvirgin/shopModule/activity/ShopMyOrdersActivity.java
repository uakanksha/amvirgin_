package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.amvirgin.Activity.HomeActivity;
import com.example.amvirgin.Adapter.ShopMyOrderListAdapter;
import com.example.amvirgin.LoginActivity;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.activity.CartProductShopActivity;
import com.example.amvirgin.shopModule.shopModel.OrderListModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;

import lal.adhish.gifprogressbar.GifView;
import retrofit2.Call;

public class ShopMyOrdersActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView_myOrder;
    private RecyclerView.LayoutManager layoutManager;
    private RelativeLayout rl_back,rl_cart;
    private String token;
    GifView pGif;
    RelativeLayout empty_orders_rl, orders_main_rl;
    private Button start_shopping_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_my_orders);

        PreferenceManager.delete(getApplicationContext(), "redirect");

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        recyclerView_myOrder=(RecyclerView)findViewById(R.id.recycler_myOrderProduct);
        rl_back=(RelativeLayout)findViewById(R.id.rlBack);
//        rl_cart=  (RelativeLayout)findViewById(R.id.rlCart1);
//        rl_cart.setOnClickListener(this);
        rl_back.setOnClickListener(this);

        empty_orders_rl = findViewById(R.id.empty_orders_rl);
        orders_main_rl = findViewById(R.id.orders_main_rl);
        start_shopping_btn = findViewById(R.id.start_shopping_btn);

        start_shopping_btn.setOnClickListener(this);

        pGif = (GifView) findViewById(R.id.progressBar);
        pGif.setImageResource(R.drawable.loader_red);

        if (token != null){
            getOrderList(token);
        }
        else {
            PreferenceManager.saveString(getApplicationContext(), "redirect", "my_orders");
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.putExtra("notlogin", "notlogin");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

    }

    private void getOrderList(String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<OrderListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getOrderList("application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<OrderListModel>(ShopMyOrdersActivity.this) {
            @Override
            public void onResponse(OrderListModel response) {
                pGif.setVisibility(View.GONE);
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0){
                            orders_main_rl.setVisibility(View.VISIBLE);
                            empty_orders_rl.setVisibility(View.GONE);

                            layoutManager=new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerView_myOrder.setLayoutManager(layoutManager);

                            ShopMyOrderListAdapter shopMyOrderListAdapter = new ShopMyOrderListAdapter(getApplicationContext(), response.getData());
                            recyclerView_myOrder.setAdapter(shopMyOrderListAdapter);
                        }
                        else {
                            orders_main_rl.setVisibility(View.GONE);
                            empty_orders_rl.setVisibility(View.VISIBLE);
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(ShopMyOrdersActivity.this, response.getMessage());
                    }
                }
                else if (response.getStatus() == 204){
                    orders_main_rl.setVisibility(View.GONE);
                    empty_orders_rl.setVisibility(View.VISIBLE);
                }
                else {
                    SnackbarManager.sException(ShopMyOrdersActivity.this, response.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.rlCart1:
                startActivity(new Intent(getApplicationContext(), CartProductShopActivity.class));
                break;
            case R.id.start_shopping_btn:
                Intent intent=new Intent(getApplicationContext(), HomeActivity.class);
                intent.putExtra("back", "cart");
                startActivity(intent);
                break;
        }
    }
}
