package com.example.amvirgin.shopModule.interfaces;

public interface RemoveFromCartClickListener {
    void onItemClick();
}
