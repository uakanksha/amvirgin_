package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.utils.Constants;

import java.util.List;

public class PopularStuffAdapter extends RecyclerView.Adapter<PopularStuffAdapter.PopularStuffViewHolder> {
    Context context;
    List<ShopHomeModel.PopularStuff> popularStuffs;

    public PopularStuffAdapter(Context context) {
        this.context = context;
        this.popularStuffs = Constants.shopPopularStuff;
    }

    @NonNull
    @Override
    public PopularStuffViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_popular_stuff, parent, false);
        PopularStuffViewHolder holder = new PopularStuffViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PopularStuffViewHolder holder, final int position) {
        holder.brand_name.setText(popularStuffs.get(position).getName());
//        if (!popularStuffs.get(position).getPoster().equalsIgnoreCase("")){
//            Glide.with(context).load(popularStuffs.get(position).getPoster()).into(holder.img);
//        }
//        else {
        if (popularStuffs.get(position).getIcon().getExists()){
            Glide.with(context).load(popularStuffs.get(position).getIcon().getUrl()).into(holder.img);
        }
        else {
            holder.img.setImageResource(R.drawable.placeholder);
        }
//        }
        holder.brand_name.setText(popularStuffs.get(position).getName());
//        holder.tv_desc.setText(popularStuffs.get(position).getDescription());
        holder.main_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String categoryId = String.valueOf(popularStuffs.get(position).getKey());
                String categoryName = String.valueOf(popularStuffs.get(position).getName());
                Intent intent = new Intent(context, ShopProductListActivity.class);
                intent.putExtra("CategoryName", categoryName);
                intent.putExtra("CategoryId", categoryId);
                intent.putExtra("back", "");
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return popularStuffs.size();
    }

    public class PopularStuffViewHolder extends RecyclerView.ViewHolder{
        ImageView img;
        TextView brand_name, tv_desc;
        LinearLayout main_ll;

        public PopularStuffViewHolder(@NonNull View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id.img);
            brand_name = itemView.findViewById(R.id.brand_name);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            main_ll = itemView.findViewById(R.id.main_ll);
        }
    }
}
