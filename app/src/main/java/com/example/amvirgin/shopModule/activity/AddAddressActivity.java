package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.shopModel.CountryListModel;
import com.example.amvirgin.shopModule.shopModel.GetAddressModel;
import com.example.amvirgin.shopModule.shopModel.SaveAddressModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout back;
    private Button btn_saveAddress;
    private EditText et_name, et_mobile, et_mobile_alternate, et_pincode, et_address, et_locality, et_state, et_city;
    private String nameStr, mobileStr, mobile_alternateStr, pincodeStr, addressStr, localityStr, token;
    private int pincode;
    private Spinner sp_country, sp_state, sp_city;
    List<CountryListModel.Datum> countries, states, cities;
    private ArrayList countryList, stateList, cityList;
    private int country_id, state_id, city_id;
    private LinearLayout home_selected_ll, work_selected_ll, other_selected_ll;
    private TextView tv_home_active, tv_work_inactive, tv_home_inactive, tv_work_active, tv_other_active, tv_other_inactive, tv_other_inactive1,
    tv_home_inactive1, tv_work_inactive1;
    private CheckBox cb_saturday, cb_sunday, cb_saturday1, cb_sunday1;
    private String type = "home";
    private String type_of_edit;
    private boolean saturdayWorking, sundayWorking;

    private List<GetAddressModel.Datum> data;
    private int position;
    private String state, city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        back = (RelativeLayout)findViewById(R.id.rlBack);
        btn_saveAddress = findViewById(R.id.btn_saveAddress);

        et_name = findViewById(R.id.et_name);
        et_mobile = findViewById(R.id.et_mobile);
        et_mobile_alternate = findViewById(R.id.et_mobile_alternate);
        et_pincode = findViewById(R.id.et_pincode);
        et_address = findViewById(R.id.et_address);
        et_locality = findViewById(R.id.et_locality);
        et_state = findViewById(R.id.et_state);
        et_city = findViewById(R.id.et_city);

        sp_state = findViewById(R.id.sp_state);
        sp_city = findViewById(R.id.sp_city);

        home_selected_ll = findViewById(R.id.home_selected_ll);
        work_selected_ll = findViewById(R.id.work_selected_ll);
        other_selected_ll = findViewById(R.id.other_selected_ll);

        tv_home_active = findViewById(R.id.tv_home_active);
        tv_work_inactive = findViewById(R.id.tv_work_inactive);
        tv_home_inactive = findViewById(R.id.tv_home_inactive);
        tv_work_active = findViewById(R.id.tv_work_active);
        tv_other_active = findViewById(R.id.tv_other_active);
        tv_other_inactive = findViewById(R.id.tv_other_inactive);
        tv_other_inactive1 = findViewById(R.id.tv_other_inactive1);
        tv_home_inactive1 = findViewById(R.id.tv_home_inactive1);
        tv_work_inactive1 = findViewById(R.id.tv_work_inactive1);

        cb_saturday = findViewById(R.id.cb_saturday);
        cb_sunday = findViewById(R.id.cb_sunday);
        cb_saturday1 = findViewById(R.id.cb_saturday1);
        cb_sunday1 = findViewById(R.id.cb_sunday1);


        if (getIntent().getExtras() != null){
            Bundle bundle = getIntent().getExtras();
            data = (List<GetAddressModel.Datum>) bundle.getSerializable("data");
            position = bundle.getInt("position");
            type_of_edit = bundle.getString("type_of_edit");


            setAddress(data, position);
        }
        else {
            type_of_edit = "add";
        }

        tv_work_active.setOnClickListener(this);
        tv_work_inactive.setOnClickListener(this);
        tv_home_inactive.setOnClickListener(this);
        tv_home_active.setOnClickListener(this);
        tv_other_active.setOnClickListener(this);
        tv_other_inactive.setOnClickListener(this);
        tv_other_inactive1.setOnClickListener(this);
        tv_home_inactive1.setOnClickListener(this);
        tv_work_inactive1.setOnClickListener(this);


        Gson gson = new Gson();
        String json = PreferenceManager.getString(getApplicationContext(), "country_list");
        Type type = new TypeToken<List<CountryListModel.Datum>>() {
        }.getType();
        countries  = gson.fromJson(json, type);

        for (int i = 0; i < countries.size(); i++){
            if (countries.get(i).getName().equalsIgnoreCase("India")){
                country_id = countries.get(i).getId();
            }
        }

        stateList = new ArrayList();
        cityList = new ArrayList();

        stateList.add("Select State");
        cityList.add("Select City");

        ArrayAdapter<String> state_adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, stateList){
            @Override
            public boolean isEnabled(int position){
                if (position ==  0){
                    return false;
                }
                else {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position==0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        ArrayAdapter<String> city_adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item, cityList){
            @Override
            public boolean isEnabled(int position){
                if (position ==  0){
                    return false;
                }
                else {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position==0) {
                    // Set the disable item text color
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };

        state_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sp_state.setAdapter(state_adapter);
        sp_city.setAdapter(city_adapter);

        getStateList(country_id);

        back.setOnClickListener(this);
        btn_saveAddress.setOnClickListener(this);
    }

    private void setAddress(List<GetAddressModel.Datum> data, int position) {
        et_name.setText(data.get(position).getName());
        et_mobile.setText(data.get(position).getMobile());
        et_mobile_alternate.setText(data.get(position).getAlternateMobile());
        et_pincode.setText(String.valueOf(data.get(position).getPinCode()));
        et_address.setText(data.get(position).getAddress());
        et_locality.setText(data.get(position).getLocality());
//        sp_country.setSelection(101);
        state = data.get(position).getState().getName();
        city = data.get(position).getCity().getName();
//        int st_id = data.get(position).getState();
//        sp_state.setSelection();
        if (data.get(position).getType().equalsIgnoreCase("home")){
            home_selected_ll.setVisibility(View.VISIBLE);
            work_selected_ll.setVisibility(View.GONE);
            other_selected_ll.setVisibility(View.GONE);
        }
        else if (data.get(position).getType().equalsIgnoreCase("office")){
            work_selected_ll.setVisibility(View.VISIBLE);
            home_selected_ll.setVisibility(View.GONE);
            other_selected_ll.setVisibility(View.GONE);
            if (data.get(position).getSaturdayWorking()){
                cb_saturday.setChecked(true);
            }
            if (data.get(position).getSundayWorking()){
                cb_sunday.setChecked(true);
            }
        }
        else if (data.get(position).getType().equalsIgnoreCase("other")){
            work_selected_ll.setVisibility(View.GONE);
            home_selected_ll.setVisibility(View.GONE);
            other_selected_ll.setVisibility(View.VISIBLE);
            if (data.get(position).getSaturdayWorking()){
                cb_saturday1.setChecked(true);
            }
            if (data.get(position).getSundayWorking()){
                cb_sunday1.setChecked(true);
            }
        }

    }

    private void getStateList(Integer id) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<CountryListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getStateList("customer/countries/"+id+"/states");
        serviceWrapper.HandleResponse(call, new ResponseHandler<CountryListModel>(getApplicationContext()) {
            @Override
            public void onResponse(final CountryListModel response) {
                if (response.getStatus() == 200){
                    try {

                        stateList = new ArrayList();
                        cityList = new ArrayList();

                        stateList.add("Select State");
                        for (int i = 0; i <response.getData().size(); i++) {
                            stateList.add(response.getData().get(i).getName());
                        }
                        cityList.add("Select City");

                        ArrayAdapter<String> state_adapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_dropdown_item, stateList){
                            @Override
                            public boolean isEnabled(int position){
                                if (position ==  0){
                                    return false;
                                }
                                else {
                                    return true;
                                }
                            }
                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if(position==0) {
                                    // Set the disable item text color
                                    tv.setTextColor(Color.GRAY);
                                }
                                else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };

                        ArrayAdapter<String> city_adapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_dropdown_item, cityList){
                            @Override
                            public boolean isEnabled(int position){
                                if (position ==  0){
                                    return false;
                                }
                                else {
                                    return true;
                                }
                            }
                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if(position==0) {
                                    // Set the disable item text color
                                    tv.setTextColor(Color.GRAY);
                                }
                                else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };

                        state_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        sp_state.setAdapter(state_adapter);
                        sp_city.setAdapter(city_adapter);

                        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0) {
                                    state_id = response.getData().get(position-1).getId();
                                    getCityList(state_id);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        if (state != null) {
                            int sta_id = state_adapter.getPosition(state);
                            sp_state.setSelection(sta_id);
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                }
            }
        });
    }

    private void getCityList(Integer id) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<CountryListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getCityList("customer/countries/states/"+id+"/cities");
        serviceWrapper.HandleResponse(call, new ResponseHandler<CountryListModel>(getApplicationContext()) {
            @Override
            public void onResponse(final CountryListModel response) {
                if (response.getStatus() == 200){
                    try {
                        cityList = new ArrayList();

                        cityList.add("Select City");
                        for (int i = 0; i <response.getData().size(); i++) {
                            cityList.add(response.getData().get(i).getName());
                        }
                        ArrayAdapter<String> city_adapter = new ArrayAdapter<String>(getApplicationContext(),
                                android.R.layout.simple_spinner_dropdown_item, cityList){
                            @Override
                            public boolean isEnabled(int position){
                                if (position ==  0){
                                    return false;
                                }
                                else {
                                    return true;
                                }
                            }
                            @Override
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View view = super.getDropDownView(position, convertView, parent);
                                TextView tv = (TextView) view;
                                if(position==0) {
                                    // Set the disable item text color
                                    tv.setTextColor(Color.GRAY);
                                }
                                else {
                                    tv.setTextColor(Color.BLACK);
                                }
                                return view;
                            }
                        };

                        city_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        sp_city.setAdapter(city_adapter);

                        sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position > 0){
                                    city_id = response.getData().get(position-1).getId();
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        if (city != null) {
                            int cit_id = city_adapter.getPosition(city);
                            sp_city.setSelection(cit_id);
                        }
                    }
                    catch (Exception e){
                        SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;
            case R.id.btn_saveAddress:
                if (isValidAddress()){
                    if (type_of_edit.equalsIgnoreCase("edit")){
                        editAddress();
                    }
                    else {
                        saveAddress();
                    }
//                    startActivity(new Intent(getApplicationContext(), ShopOrderSummaryActivity.class));
                }
                break;

         /*   case R.id.tv_home_active:
                home_selected_ll.setVisibility(View.VISIBLE);
                work_selected_ll.setVisibility(View.GONE);
                break;*/
            case R.id.tv_work_inactive:
                home_selected_ll.setVisibility(View.GONE);
                other_selected_ll.setVisibility(View.GONE);
                work_selected_ll.setVisibility(View.VISIBLE);
                type = "office";
                break;
            case R.id.tv_home_inactive:
                home_selected_ll.setVisibility(View.VISIBLE);
                work_selected_ll.setVisibility(View.GONE);
                other_selected_ll.setVisibility(View.GONE);
                type = "home";
                break;
            case R.id.tv_other_inactive:
                home_selected_ll.setVisibility(View.GONE);
                work_selected_ll.setVisibility(View.GONE);
                other_selected_ll.setVisibility(View.VISIBLE);
                type = "other";
                break;

            case R.id.tv_work_inactive1:
                home_selected_ll.setVisibility(View.GONE);
                other_selected_ll.setVisibility(View.GONE);
                work_selected_ll.setVisibility(View.VISIBLE);
                type = "office";
                break;
            case R.id.tv_home_inactive1:
                home_selected_ll.setVisibility(View.VISIBLE);
                work_selected_ll.setVisibility(View.GONE);
                other_selected_ll.setVisibility(View.GONE);
                type = "home";
                break;
            case R.id.tv_other_inactive1:
                home_selected_ll.setVisibility(View.GONE);
                work_selected_ll.setVisibility(View.GONE);
                other_selected_ll.setVisibility(View.VISIBLE);
                type = "other";
                break;
        }
    }

    private void editAddress() {

        if (type.equalsIgnoreCase("home")){
            saturdayWorking = true;
            sundayWorking = true;
        }
        else if (type.equalsIgnoreCase("office")){
            if (cb_saturday.isChecked()){
                saturdayWorking = true;
            }
            if (!cb_saturday.isChecked()){
                saturdayWorking = false;
            }
            if (cb_sunday.isChecked()){
                sundayWorking = true;
            }
            if (!cb_sunday.isChecked()){
                sundayWorking = false;
            }
        }
        else if (type.equalsIgnoreCase("other")){
            if (cb_saturday1.isChecked()){
                saturdayWorking = true;
            }
            if (!cb_saturday1.isChecked()){
                saturdayWorking = false;
            }
            if (cb_sunday1.isChecked()){
                sundayWorking = true;
            }
            if (!cb_sunday1.isChecked()){
                sundayWorking = false;
            }
        }

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", nameStr);
        jsonObject.addProperty("mobile", mobileStr);
        jsonObject.addProperty("alternateMobile", mobile_alternateStr);
        jsonObject.addProperty("pinCode", Integer.parseInt(pincodeStr));
        jsonObject.addProperty("stateId", state_id);
        jsonObject.addProperty("cityId", city_id);
        jsonObject.addProperty("address", addressStr);
        jsonObject.addProperty("locality", localityStr);
        jsonObject.addProperty("saturdayWorking", saturdayWorking);
        jsonObject.addProperty("sundayWorking", sundayWorking);
        jsonObject.addProperty("type", type);


        ServiceWrapper serviceWrapper = new ServiceWrapper(this);
        Call<SaveAddressModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .updateAddress(String.valueOf(data.get(position).getKey()), "application/json",
                        "Bearer "+token, jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<SaveAddressModel>(AddAddressActivity.this) {
            @Override
            public void onResponse(SaveAddressModel response) {
                if (response.getStatus() == 200){
                    try{
                        ToastMessage.getInstance(AddAddressActivity.this).showSuccessShortCustomToast(response.getMessage());
                        startActivity(new Intent(AddAddressActivity.this, ShopOrderSummaryActivity.class));
                    }
                    catch (Exception e){
                        SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                }
            }
        });
    }

    private void saveAddress() {
        if (type.equalsIgnoreCase("home")){
            saturdayWorking = false;
            sundayWorking = false;
        }
        else if (type.equalsIgnoreCase("office")){
            if (cb_saturday.isChecked()){
                saturdayWorking = true;
            }
            if (!cb_saturday.isChecked()){
                saturdayWorking = false;
            }
            if (cb_sunday.isChecked()){
                sundayWorking = true;
            }
            if (!cb_sunday.isChecked()){
                sundayWorking = false;
            }
        }

        else if (type.equalsIgnoreCase("other")){
            if (cb_saturday1.isChecked()){
                saturdayWorking = true;
            }
            if (!cb_saturday1.isChecked()){
                saturdayWorking = false;
            }
            if (cb_sunday1.isChecked()){
                sundayWorking = true;
            }
            if (!cb_sunday1.isChecked()){
                sundayWorking = false;
            }
        }


        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", nameStr);
        jsonObject.addProperty("mobile", mobileStr);
        jsonObject.addProperty("alternateMobile", mobile_alternateStr);
        jsonObject.addProperty("pinCode", Integer.parseInt(pincodeStr));
        jsonObject.addProperty("stateId", state_id);
        jsonObject.addProperty("cityId", city_id);
        jsonObject.addProperty("address", addressStr);
        jsonObject.addProperty("locality", localityStr);
        jsonObject.addProperty("saturdayWorking", saturdayWorking);
        jsonObject.addProperty("sundayWorking", sundayWorking);
        jsonObject.addProperty("type", type);


        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<SaveAddressModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .saveAddress("application/json", "Bearer "+token, jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<SaveAddressModel>(AddAddressActivity.this) {
            @Override
            public void onResponse(SaveAddressModel response) {
                if (response.getStatus() == 200){
                    try {
                        ToastMessage.getInstance(AddAddressActivity.this).showSuccessShortCustomToast(response.getMessage());
                        startActivity(new Intent(AddAddressActivity.this, ShopOrderSummaryActivity.class));
                    }
                    catch (Exception e){
                        SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(AddAddressActivity.this, response.getMessage());
                }
            }
        });
    }

    private boolean isValidAddress() {
        boolean valid = false;

        nameStr = et_name.getText().toString().trim();
        mobileStr = et_mobile.getText().toString().trim();
        mobile_alternateStr = et_mobile_alternate.getText().toString().trim();
        pincodeStr = et_pincode.getText().toString().trim();
//        pincode = Integer.parseInt(pincodeStr);
        addressStr = et_address.getText().toString().trim();
        localityStr = et_locality.getText().toString();

        if (nameStr.equalsIgnoreCase("")){
            et_name.requestFocus();
            et_name.setError("Please Enter Name!!");
        }
        else if (mobileStr.equalsIgnoreCase("")){
            et_mobile.requestFocus();
            et_mobile.setError("Please Enter Mobile Number!!");
        }
        else if (mobile_alternateStr.equalsIgnoreCase("")){
            et_mobile_alternate.requestFocus();
            et_mobile_alternate.setError("Please Enter Altername Mobile Number!!");
        }
        else if (pincodeStr.equalsIgnoreCase("")){
            et_pincode.requestFocus();
            et_pincode.setError("Please Enter Pincode!!");
        }
        else if (addressStr.equalsIgnoreCase("")){
            et_address.requestFocus();
            et_address.setError("Please Enter Address!!");
        }
        else if (localityStr.equalsIgnoreCase("")){
            et_locality.requestFocus();
            et_locality.setError("Please Enter Locality!!");
        }
        else if (state_id <= 0){
            Toast.makeText(this, "Please Select State!!", Toast.LENGTH_SHORT).show();
        }
       /* else if (city_id <= 0){
            Toast.makeText(this, "Please Select City!!", Toast.LENGTH_SHORT).show();
        }*/
        else {
            valid = true;
        }

        return valid;
    }
}
