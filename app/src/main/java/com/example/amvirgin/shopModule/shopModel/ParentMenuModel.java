package com.example.amvirgin.shopModule.shopModel;

import java.util.ArrayList;

public class ParentMenuModel {
    public ArrayList<SubMenuModel> childList=new ArrayList<>();

    private int key;
    private String slug;
    private String name;
    private String type;
    private int products;

    private boolean hasIcon;
    private String url;
    private boolean hasInner;

    public boolean isHasInner() {
        return hasInner;
    }

    public void setHasInner(boolean hasInner) {
        this.hasInner = hasInner;
    }



    public boolean isHasIcon() {
        return hasIcon;
    }

    public void setHasIcon(boolean hasIcon) {
        this.hasIcon = hasIcon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ParentMenuModel() {

    }

    public ArrayList<SubMenuModel> getChildList() {
        return childList;
    }

    public void setChildList(ArrayList<SubMenuModel> childList) {
        this.childList = childList;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getProducts() {
        return products;
    }

    public void setProducts(int products) {
        this.products = products;
    }



}
