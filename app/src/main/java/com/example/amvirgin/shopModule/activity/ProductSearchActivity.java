package com.example.amvirgin.shopModule.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.shopAdapter.ProductSearchAdapter;
import com.example.amvirgin.shopModule.shopModel.ShopSearchModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.ToastMessage;

import retrofit2.Call;

public class ProductSearchActivity extends AppCompatActivity {
    int cat_id;
    RecyclerView rv_search_items;
    EditText etsearch;
    ImageView iv_back;
    TextView tv_nodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);

        if (getIntent().getExtras() != null){
            cat_id = getIntent().getIntExtra("cat_id", 0);
        }

        rv_search_items = findViewById(R.id.rv_search_items);
        etsearch = findViewById(R.id.etsearch);
        iv_back = findViewById(R.id.iv_back);
        tv_nodata = findViewById(R.id.tv_nodata);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        etsearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 2){
                    getSearchResults(editable.toString());
                }
                else {
                    rv_search_items.setVisibility(View.GONE);
                }

            }
        });
    }

    private void getSearchResults(String key){
        ServiceWrapper serviceWrapper = new ServiceWrapper(ProductSearchActivity.this);
        Call<ShopSearchModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getShopSearch("customer/search?type=shop&keyword="+key);
        Log.e("sdasdsadsads", "itemSearch: "+call.request().url());
        serviceWrapper.HandleResponse(call, new ResponseHandler<ShopSearchModel>(this) {
            @Override
            public void onResponse(ShopSearchModel response) {
                if (response.getStatus() == 200){

                        if (response.getData().size() > 0) {
                            tv_nodata.setVisibility(View.GONE);
//                            search_results = new ArrayList<String>();
//
//                            for (int i = 0; i <response.getData().size(); i++){
//                                search_results.add(response.getData().get(i).getName());
//                            }
                            rv_search_items.setVisibility(View.VISIBLE);

                            LinearLayoutManager manager = new LinearLayoutManager(ProductSearchActivity.this, RecyclerView.VERTICAL, false);
                            rv_search_items.setLayoutManager(manager);

                            ProductSearchAdapter adapter = new ProductSearchAdapter(ProductSearchActivity.this, response.getData());
                            rv_search_items.setAdapter(adapter);

                            //Creating the instance of ArrayAdapter containing list of fruit names
//                            ArrayAdapter<List<String>> adapter = new ArrayAdapter<List<String>>
//                                    (this, android.R.layout.select_dialog_item, search_results);
//                            //Getting the instance of AutoCompleteTextView
//                            etsearch.setThreshold(2);//will start working from first character
//                            etsearch.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView


//                            Intent intent = new Intent(getContext(), ShopProductListActivity.class);
//                            intent.putExtra("back", "search");
//                            intent.putExtra("CategoryName", key);
//                            shopping_search = response.getData();
//                            startActivity(intent);
                        }else {
                            rv_search_items.setVisibility(View.GONE);
                            tv_nodata.setVisibility(View.VISIBLE);
//                            ToastMessage.getInstance(ProductSearchActivity.this)
//                                    .showErrorShortCustomToast("No Results found for your Search!!");
//                            Log.e("sdasdsadsads", "onResponse: "+response.getMessage());
//                            SnackbarManager.successResponse(mCtx, response.getMessage());
                        }
                }
                else if (response.getStatus() == 204){
                    rv_search_items.setVisibility(View.GONE);
                    tv_nodata.setVisibility(View.VISIBLE);
                }
                else {
                    SnackbarManager.sException(ProductSearchActivity.this, response.getMessage());
                }
            }

        });
    }
}
