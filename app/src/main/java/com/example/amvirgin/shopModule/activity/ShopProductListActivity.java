package com.example.amvirgin.shopModule.activity;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amvirgin.Activity.BaseActivity;
import com.example.amvirgin.Adapter.ShopCollectionListAdapter;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.SnackbarManager;
import com.example.amvirgin.shopModule.interfaces.SortSelectionListener;
import com.example.amvirgin.shopModule.shopAdapter.ShopTrendingDealsAdapter;
import com.example.amvirgin.shopModule.shopAdapter.SortListAdapter;
import com.example.amvirgin.shopModule.shopModel.ProductListModel;
import com.example.amvirgin.shopModule.shopModel.ShopTrendingDealsModel;
import com.example.amvirgin.shopModule.shopModel.SortListModel;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.GsonBuilder;

import java.util.List;

import lal.adhish.gifprogressbar.GifView;
import retrofit2.Call;

import static com.example.amvirgin.utils.Constants.shopping_search;

public class   ShopProductListActivity extends BaseActivity implements View.OnClickListener {

//    private ImageView ;
    private RelativeLayout backbtn,shoppingBag_btn,wishList_btn;
    private RecyclerView rv_shop_collection;
    private LinearLayout ll_sort,ll_filter;
    private BottomSheetDialog bottomSheetDialog;

    GifView pGif;

    private Context mCtx;
    String category_name, category_id, sort_key = "", url;
    int page_no = 1;
    private List<ProductListModel.Payload> data;
    
    //Bottom Popup 
    private RecyclerView rv_sort_list;
    private List<SortListModel.Datum> sort_list_data;
    private TextView tv_sort_apply;
    private ImageView iv_search;

    //Header Section
    TextView tv_category_name, tv_total_items, tvCart1;
    int cart_count;

    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
        cart_count = PreferenceManager.getInt(getApplicationContext(), "cart_count");

        if (cart_count > 0){
            tvCart1.setVisibility(View.VISIBLE);
            tvCart1.setText(String.valueOf(cart_count));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        cart_count = PreferenceManager.getInt(getApplicationContext(), "cart_count");

        Bundle extras = getIntent().getExtras();
        category_id = extras.getString("CategoryId");
        category_name = extras.getString("CategoryName");

        pGif = (GifView) findViewById(R.id.progressBar);
        pGif.setImageResource(R.drawable.loader_red);

        rv_shop_collection = findViewById(R.id.rv_shop_collection);
        backbtn = (RelativeLayout)findViewById(R.id.rlBack);
        shoppingBag_btn= (RelativeLayout)findViewById(R.id.rlCart1);
        wishList_btn=(RelativeLayout) findViewById(R.id.rlWish);
        ll_sort=(LinearLayout)findViewById(R.id.ll_sort);
        ll_filter=(LinearLayout)findViewById(R.id.ll_filter);
        iv_search = findViewById(R.id.iv_search);

        //Header Section
        tv_category_name = findViewById(R.id.tv_category_name);
        tv_total_items = findViewById(R.id.tv_total_items);
        tvCart1 = findViewById(R.id.tvCart1);



        if (extras != null) {

            if (extras.getString("back").equalsIgnoreCase("trending")) {
                ll_sort.setEnabled(false);
                getTrendingDeals();
            } else if (extras.getString("back").equalsIgnoreCase("search")) {
                ll_sort.setEnabled(false);
                if (shopping_search.size() > 0) {
                    pGif.setVisibility(View.GONE);

                    tv_total_items.setText(String.valueOf(Constants.shopping_search.size()));

                    GridLayoutManager manager = new GridLayoutManager(mCtx, 2);
                    rv_shop_collection.setLayoutManager(manager);
                    ShopTrendingDealsAdapter adapter = new ShopTrendingDealsAdapter(getApplicationContext(), Constants.shopping_search);
                    rv_shop_collection.setAdapter(adapter);
                }
                else {
                    ToastMessage.getInstance(ShopProductListActivity.this)
                            .showErrorShortCustomToast("No Results found for your Search!!");
                }
            } else {
                getProductList(category_id, sort_key);
            }
        }
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ShopProductListActivity.this, ProductSearchActivity.class);
                intent.putExtra("cat_id", Integer.parseInt(category_id));
                startActivity(intent);
            }
        });




        if (cart_count > 0){
            tvCart1.setVisibility(View.VISIBLE);
            tvCart1.setText(String.valueOf(cart_count));
        }

        tv_category_name.setText(category_name);
        
        //Bottom Popup
        bottomSheetDialog = new BottomSheetDialog(ShopProductListActivity.this);
        View bottomSheetDial= getLayoutInflater().inflate(R.layout.bottom_popup_sort_layout,null);
        bottomSheetDialog.setContentView(bottomSheetDial);
        rv_sort_list = bottomSheetDialog.findViewById(R.id.rv_sort_list);
        tv_sort_apply = bottomSheetDialog.findViewById(R.id.tv_sort_apply);
        
        getSortTypeList();

        tv_sort_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                getProductList(category_id, sort_key);
            }
        });

        ll_filter.setOnClickListener(this);
        ll_sort.setOnClickListener(this);
        wishList_btn.setOnClickListener(this);
        backbtn.setOnClickListener(this);
        shoppingBag_btn.setOnClickListener(this);

    }

    private void getTrendingDeals() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<ShopTrendingDealsModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getAllTrendingDeals();
        serviceWrapper.HandleResponse(call, new ResponseHandler<ShopTrendingDealsModel>(mCtx) {
            @Override
            public void onResponse(ShopTrendingDealsModel response) {
                pGif.setVisibility(View.GONE);
                if (response.getStatus() == 200){
                    try {
                        if (response.getData().size() > 0){
                            tv_total_items.setText(String.valueOf(response.getData().size()));

                            GridLayoutManager manager = new GridLayoutManager(mCtx, 2);
                            rv_shop_collection.setLayoutManager(manager);

                            ShopTrendingDealsAdapter adapter = new ShopTrendingDealsAdapter(getApplicationContext(), response.getData());
                            rv_shop_collection.setAdapter(adapter);
                        }
                    }catch (Exception e){
                        SnackbarManager.sException(mCtx, response.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(mCtx, response.getMessage());
                }
            }
        });
    }

    private void getSortTypeList() {
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<SortListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .getSortList();
        serviceWrapper.HandleResponse(call, new ResponseHandler<SortListModel>(this) {
            @Override
            public void onResponse(SortListModel response) {
                if (response.getStatus() == 200){
                    try {
                        sort_list_data = response.getData();

                        LinearLayoutManager manager = new LinearLayoutManager(mCtx, RecyclerView.VERTICAL, false);
                        rv_sort_list.setLayoutManager(manager);

                        SortListAdapter adapter = new SortListAdapter(mCtx, sort_list_data, new SortSelectionListener() {
                            @Override
                            public void onItemClick(String selected_sort_key) {
                                sort_key = selected_sort_key;
                            }
                        });
                        rv_sort_list.setAdapter(adapter);
                    }
                    catch (Exception e){
                        SnackbarManager.sException(mCtx,response.getMessage());
                    }
                }
                else{
                    SnackbarManager.sException(mCtx,response.getMessage());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rlBack:
                finish();
                break;

            case R.id.rlCart1:
//                startActivity(new Intent(getApplicationContext(), CartProductShopActivity.class));
                Intent intent = new Intent(getApplicationContext(), CartProductShopActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                break;
            case R.id.rlWish:
                startActivity(new Intent(getApplicationContext(), ShopWishListActivity.class));

                break;
            case R.id.ll_sort:
                bottomSheetDialog.show();
                break;

            case R.id.ll_filter:
                Intent intent1 = new Intent(getApplicationContext(), ShopFilterActivity.class);
                intent1.putExtra("category_id", category_id);
                startActivity(intent1);
                break;
        }
    }

    private void getProductList(String category_id, String sort_key) {
//        url = "http://amvirgin-app.zobofy.com/api/customer/products?categoryId="+category_id+
//                "&offset="+page_no+"&sortKey="+sort_key;
        url = "customer/products?sortBy="+sort_key+"&page="+page_no+"&category="+category_id;
        ServiceWrapper serviceWrapper = new ServiceWrapper(getApplicationContext());
        Call<ProductListModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class).getProductList(url);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ProductListModel>(this) {
            @Override
            public void onResponse(ProductListModel response) {
                if (response.getStatus() == 200){
                    pGif.setVisibility(View.GONE);
                    try{
                        data = response.getPayload();

                        tv_total_items.setText(String.valueOf(response.getMeta().getPagination().getItems().getTotal()));

                        if (data.size() > 0) {
                            GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 2);
                            rv_shop_collection.setLayoutManager(manager);

                            ShopCollectionListAdapter adapter = new ShopCollectionListAdapter(getApplicationContext(), data);
                            rv_shop_collection.setAdapter(adapter);
                        }
                        else{
                            setContentView(R.layout.no_data_found_layout);
//                            Toast.makeText(getApplicationContext(), "No Data Found!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception exception){
                        SnackbarManager.sException(ShopProductListActivity.this,exception.getMessage());
                    }
                }
                else {
                    SnackbarManager.sException(ShopProductListActivity.this,response.getMessage());
                }
            }
        });
    }
}
