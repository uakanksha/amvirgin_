package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;
import com.example.amvirgin.shopModule.shopModel.ParentMenuModel;
import com.example.amvirgin.shopModule.shopModel.SubMenuModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ParentLevelAdapter extends BaseExpandableListAdapter {
    private final Context mContext;
    private OnItemClickListener mItemClickListener;
    private List<ParentMenuModel> parentItemLists;

    public ParentLevelAdapter(Context mContext, List<ParentMenuModel> arrayList) {
        this.mContext = mContext;
        this.parentItemLists = arrayList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parentItemLists.get(groupPosition).childList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CustomExpListView secondLevelExpListView = new CustomExpListView(mContext);
        secondLevelExpListView.setGroupIndicator(null);
        secondLevelExpListView.setDividerHeight(0);
        SubMenuModel subMenuModel = (SubMenuModel) getChild(groupPosition, childPosition);
        secondLevelExpListView.setAdapter(new SecondLevelAdapter(mContext, subMenuModel.getName(), subMenuModel.getKey(),
                parentItemLists.get(groupPosition).childList.get(childPosition).childChildList,parentItemLists.get(groupPosition).getChildList()));
        secondLevelExpListView.setGroupIndicator(null);
        secondLevelExpListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {

                if (groupPosition != previousGroup)
                    secondLevelExpListView.collapseGroup(previousGroup);
                previousGroup = groupPosition;

            }
        });
        return secondLevelExpListView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return parentItemLists.get(groupPosition).childList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parentItemLists.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return parentItemLists.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final ParentMenuModel parentMenuModel = (ParentMenuModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.parentlevel, parent, false);
        }
        final TextView name =convertView.findViewById(R.id.tvtitle);
        TextView tvborder=convertView.findViewById(R.id.tvborder);
        AppCompatImageView ivIcon =convertView.findViewById(R.id.ivMenuIcon);
        AppCompatImageView ivdrop=convertView.findViewById(R.id.ivdrop);
        name.setText(parentMenuModel.getName());
        if (parentMenuModel.isHasIcon()) {
            Picasso.with(mContext).load(parentMenuModel.getUrl()).error(R.drawable.ic_shoping_cartwhite).into(ivIcon);
        }else {
            ivIcon.setImageResource(R.drawable.ic_shoping_cartwhite);
        }

        if (isExpanded) {
            tvborder.setVisibility(View.VISIBLE);
            ivdrop.setImageResource(R.drawable.ic_expand_less);
        } else {
            tvborder.setVisibility(View.GONE);
            ivdrop.setImageResource(R.drawable.ic_expand);
        }

        if (parentMenuModel.isHasInner()) {
            ivdrop.setVisibility(View.VISIBLE);
        }else {
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(mContext, name.getText().toString(), Toast.LENGTH_SHORT).show();
                    String categoryId = String.valueOf(parentMenuModel.getKey());
                    String categoryName = String.valueOf(parentMenuModel.getName());
                    Intent intent = new Intent(mContext, ShopProductListActivity.class);
                    intent.putExtra("CategoryName", categoryName);
                    intent.putExtra("CategoryId", categoryId);
                    intent.putExtra("back", "");
                    mContext.startActivity(intent);
                }
            });
            ivdrop.setVisibility(View.GONE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view);
    }


}
