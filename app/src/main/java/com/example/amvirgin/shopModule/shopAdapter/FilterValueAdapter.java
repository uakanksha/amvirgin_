package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.Other.FilterValues;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.List;

public class FilterValueAdapter extends RecyclerView.Adapter {
    Context context;
    String label;
    List<Object> options;
    ArrayList values;
    ArrayList<FilterValues> filterValues;

    private static int TYPE_PRICE = 1;
    private static int TYPE_DISCOUNT = 2;
    private static int TYPE_COLOR = 3;
    private static int TYPE_DEFAULT = 4;

    public FilterValueAdapter(Context context, String label, List<Object> options) {
        this.context = context;
        this.label = label;
        this.options = options;
    }

    @Override
    public int getItemViewType(int position) {
        if (label.equalsIgnoreCase("price")) {
            return TYPE_PRICE;

        }
        else if (label.equalsIgnoreCase("discount")) {
            return TYPE_DISCOUNT;

        }
        if (label.equalsIgnoreCase("color")) {
            return TYPE_COLOR;

        }
        else {
            return TYPE_DEFAULT;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == TYPE_PRICE) { // for call layout
            view = LayoutInflater.from(context).inflate(R.layout.rv_filter_value_color_layout, parent, false);
            return new PriceViewHolder(view);

        } else if (viewType == TYPE_DISCOUNT){ // for email layout
            view = LayoutInflater.from(context).inflate(R.layout.rv_filter_value_discount_layout, parent, false);
            return new DiscountViewHolder(view);
        } else if (viewType == TYPE_COLOR){ // for email layout
            view = LayoutInflater.from(context).inflate(R.layout.rv_filter_value_color_layout, parent, false);
            return new ColorViewHolder(view);
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.rv_filter_value_color_layout, parent, false);
            return new ColorViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        if (getItemViewType(position) == TYPE_PRICE) {
            ((PriceViewHolder) holder).setPriceDetails(options, position);
        } else if (getItemViewType(position) == TYPE_DISCOUNT) {
            ((DiscountViewHolder) holder).setDiscountDetails(options, position);
        } else if (getItemViewType(position) == TYPE_COLOR) {
            ((ColorViewHolder) holder).setColorDetails(options, position);
        }else {
            ((ColorViewHolder) holder).setColorDetails(options, position);
        }
    }

    @Override
    public int getItemCount() {
        return options.size();
    }

    public class PriceViewHolder extends RecyclerView.ViewHolder{
//        TextView tv_count, tv_range;
//        SeekBar seekbar;

        CheckBox cb_color;

        public PriceViewHolder(@NonNull View itemView) {
            super(itemView);

//            tv_count = itemView.findViewById(R.id.tv_count);
//            tv_range = itemView.findViewById(R.id.tv_range);
//            seekbar = itemView.findViewById(R.id.seekbar);

            cb_color = itemView.findViewById(R.id.cb_color);
        }

        private void setPriceDetails(List<Object> options, int position){
//            filterValues = (ArrayList<FilterValues>) options.get(position);

            Object getData = options.get(position);
            LinkedTreeMap<Object,Object> t = (LinkedTreeMap) getData;
//            LinkedTreeMap t;
//            t = (LinkedTreeMap) getData;

            String upper = String.valueOf(t.get("upper"));
            String lower = String.valueOf(t.get("lower"));
            String count = String.valueOf(t.get("count"));

//            tv_count.setText(count+" products found");
//            seekbar.setMax(Double.parseDouble(lower));

            cb_color.setText("₹"+upper+" - ₹"+lower);
        }
    }

    public class DiscountViewHolder extends RecyclerView.ViewHolder{
        RadioButton rb_discount;

        public DiscountViewHolder(@NonNull View itemView) {
            super(itemView);

            rb_discount = itemView.findViewById(R.id.rb_discount);
        }

        private void setDiscountDetails(List<Object> options, int position){
//           filterValues = (ArrayList<FilterValues>) options.get(position);

            Object getData = options.get(position);
            LinkedTreeMap<Object,Object> t = (LinkedTreeMap) getData;
            String limit = t.get("limit").toString();

            rb_discount.setText(limit+"% and above");

        }
    }

    public class ColorViewHolder extends RecyclerView.ViewHolder{
        CheckBox cb_color;

        public ColorViewHolder(@NonNull View itemView) {
            super(itemView);

            cb_color = itemView.findViewById(R.id.cb_color);
        }

        private void setColorDetails(List<Object> options, int position){
            values = (ArrayList) options;

            cb_color.setText(values.get(position).toString());
        }
    }

}
