package com.example.amvirgin.shopModule.shopAdapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.shopModule.activity.ShopProductListActivity;
import com.example.amvirgin.shopModule.shopModel.ShopBrands;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class BrandInFocusAdapter extends RecyclerView.Adapter<BrandInFocusAdapter.ItemViewHolder>{



    private List<ShopHomeModel.BrandInFocu> brandList;
    private Context mCtx;
    private int value;

    public BrandInFocusAdapter(Context context) {
        this.mCtx = context;
//        this.value = i;
//        switch (i) {
//            case 0:
                this.brandList = Constants.shopBrandsInFocus;
//                break;
//            default:
//                break;
//        }
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        switch (value) {
//            case 0:
                //return new ItemViewHolder(LayoutInflater.from(mCtx).inflate(R.layout.custom_brand_focus, parent, false));
                return new ItemViewHolder(LayoutInflater.from(mCtx).inflate(R.layout.rv_brands_layout, parent, false));
//            default:
//                return null;
//        }
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, final int position) {
//        switch (value){
//            case 0:
                holder.itemName.setText(brandList.get(position).getName());

                holder.main_rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String categoryId = String.valueOf(brandList.get(position).getKey());
                        String categoryName = String.valueOf(brandList.get(position).getName());
                        Intent intent = new Intent(mCtx, ShopProductListActivity.class);
                        intent.putExtra("CategoryName", categoryName);
                        intent.putExtra("CategoryId", categoryId);
                        intent.putExtra("back", "");
                        mCtx.startActivity(intent);
                    }
                });
//                holder.itemImage.setImageResource(model.getImg());
//        if (!brandList.get(position).getPoster().equalsIgnoreCase("")){
//            Glide.with(mCtx).load(brandList.get(position).getPoster()).into(holder.itemImage);
//        }
//                break;
//            default:
//                break;

//        }
    }

    @Override
    public int getItemCount() {
//        switch (value) {
//            case 0:
                return brandList.size();
//            default:
//                return 0;
//        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView itemImage;
        private TextView itemName;
        RelativeLayout main_rl;
        public ItemViewHolder(View view) {
            super(view);
//            switch (value) {
//                case 0:
                    itemImage=itemView.findViewById(R.id.img);
                    itemName=itemView.findViewById(R.id.brand_name);
                    main_rl = view.findViewById(R.id.main_rl);
//                    break;
//                default:
//                    break;
//            }
        }
    }

}
