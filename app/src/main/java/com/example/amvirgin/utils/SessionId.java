package com.example.amvirgin.utils;

public class SessionId {
    private String sessionid;

    public SessionId(String sessionid) {
        this.sessionid = sessionid;
    }

    public SessionId() {

    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }
}
