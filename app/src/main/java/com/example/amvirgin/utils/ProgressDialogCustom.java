package com.example.amvirgin.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import com.example.amvirgin.R;


public class ProgressDialogCustom {

    public static ProgressDialog progressDialog;

    public static ProgressDialog showProgressDialog(@NonNull Context context) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context, R.style.StyleProgressDialog);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    progressDialog = null;
                }
            });
                progressDialog.show();

        }
        return progressDialog;
    }

    public static ProgressDialog showProgressDialogTransparent(@NonNull Context context) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context, R.style.StyleProgressDialogtransparent);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    progressDialog = null;
                }
            });
                progressDialog.show();

        }
        return progressDialog;
    }


    public static void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {

                progressDialog.dismiss();

        }
        progressDialog = null;
    }
}
