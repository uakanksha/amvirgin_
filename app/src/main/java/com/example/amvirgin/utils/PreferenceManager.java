package com.example.amvirgin.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


public final class PreferenceManager {

    public static final String PREFERENCE_NAME = "AmVirgin";
    Context ctx;

    private PreferenceManager() {
    }

    private static SharedPreferences getPreferences(Context context) {
        if (context != null) {
            return context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        }
        return null;
    }

    public static String getString(Context context, String key) {
        return getString(context, key, "");
    }

    public static String getString(Context context, String key, String defaultValue) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null && preferences.contains(key)) {
            return preferences.getString(key, defaultValue);
        }
        return defaultValue;
    }

    public static void saveString(Context context, String key, String value) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            preferences.edit().putString(key, value).apply();
        }
    }

    public static boolean getBoolean(Context context, String key) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null && preferences.contains(key)) {
            return preferences.getBoolean(key, false);
        }
        return false;
    }

    public static void saveBoolean(Context context, String key, boolean value) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            preferences.edit().putBoolean(key, value).apply();
        }
    }

    public static long getLong(Context context, String key) {
        final SharedPreferences preferences = getPreferences(context);

            return preferences.getLong(key, 0);

    }


    public static double getdouble(Context context, String key) {
        final SharedPreferences preferences = getPreferences(context);

            return Double.parseDouble(preferences.getString(key, ""));

    }


    public static void savedouble(Context context, String key, double value) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {

                preferences.edit().putString(key, String.valueOf(value)).apply();

        }
    }

    public static void saveLong(Context context, String key, long value) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {

                preferences.edit().putLong(key, value).apply();

        }
    }

    public static float getFloat(Context context, String key) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null && preferences.contains(key)) {
            return preferences.getFloat(key, 0);
        }
        return 0;
    }

    public static void saveFloat(Context context, String key, float value) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            preferences.edit().putFloat(key, value).apply();
        }
    }
    public static void deleteAllData(Context context) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            preferences.edit().clear().apply();
        }
    }

    public static void delete(Context context, String key) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            preferences.edit().remove(key).apply();
        }
    }

    public static int getInt(Context context, String key) {
        final SharedPreferences preferences = getPreferences(context);

            return preferences.getInt(key, 0);

    }

    public static void saveInt(Context context, String key, int value) {
        final SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {

                preferences.edit().putInt(key, value).apply();

        }
    }

    public static void saveUser(Context context, User user) {

        saveBoolean(context, Constants.Key.IS_ALREADY_LOGGED_IN, true);
        saveInt(context, Constants.Key.KEY, user.getKey());
        saveString(context, Constants.Key.NAME, user.getName());
        saveString(context, Constants.Key.EMAIL, user.getEmail());
        saveString(context, Constants.Key.AVATAR, user.getAvatar());
        saveString(context, Constants.Key.MOBILE, user.getMobile());
        saveString(context, Constants.Key.ApiToken, user.getToken());

        if (getBoolean(context, Constants.Key.SUBSCRIPTION)){
            saveBoolean(context, Constants.Key.SUB_ACTIVE, user.getSubscription().getActive());
            saveInt(context, Constants.Key.PLAN_KEY, user.getSubscription().getPlan().getKey());
            saveString(context, Constants.Key.SUB_NAME, user.getSubscription().getPlan().getName());
            saveInt(context, Constants.Key.SUB_DUR_TOTAL_DAY,
                    user.getSubscription()
                            .getPlan()
                            .getDuration()
                            .getActual()
            );

            saveInt(context, Constants.Key.SUB_DUR_REMAINING_DAY,
                    user.getSubscription()
                            .getPlan()
                            .getDuration()
                            .getActual()
            );
        }else {
            Log.e("PreferenceManager", "Not Subscribe ");
        }




    }

    public static void saveSessionId(Context context, SessionId sessionId) {
        saveString(context, Constants.Key.SESSION_ID, sessionId.getSessionid());

    }

    public static void deleteUser(Context context) {
        saveBoolean(context, Constants.Key.IS_ALREADY_LOGGED_IN, false);
        delete(context, Constants.Key.KEY);
        delete(context, Constants.Key.NAME);
        delete(context, Constants.Key.EMAIL);
        delete(context, Constants.Key.ApiToken);
        delete(context, Constants.Key.MOBILE);
        delete(context, Constants.Key.AVATAR);

        delete(context, Constants.Key.SUB_ACTIVE);
        delete(context, Constants.Key.PLAN_KEY);
        delete(context, Constants.Key.SUB_NAME);
        delete(context, Constants.Key.SUB_DUR_TOTAL_DAY);
        delete(context, Constants.Key.SUB_DUR_REMAINING_DAY);

    }

    public static void deleteSessionId(Context context){
        delete(context, Constants.Key.SESSION_ID);
    }

    public static User getUser(Context context) {
        final User user = new User();
        user.setKey(getInt(context, Constants.Key.KEY));
        user.setName(getString(context, Constants.Key.NAME));
        user.setEmail(getString(context, Constants.Key.EMAIL));
        user.setToken(getString(context, Constants.Key.ApiToken));
        user.setMobile(getString(context, Constants.Key.MOBILE));
        user.setAvatar(getString(context, Constants.Key.AVATAR));
        return user;
    }

    public static SessionId getSessionId(Context context) {
        SessionId sessionId = new SessionId();
       sessionId.setSessionid(getString(context, Constants.Key.SESSION_ID));
        return sessionId;
    }






}
