package com.example.amvirgin.utils;

import android.graphics.Color;

import java.util.Random;

public class ColorCycle {
    String[] colors = new String[]{"#00B9F5", "#0288D1", "#F26D6D", "#e15258", "#f9845b", "#838cc7", "#7d669e", "#53bbb4", "#51b46d", "#e0ab18", "#f092b0", "#b7c0c7"};

    public int getColor() {
        return Color.parseColor(this.colors[new Random().nextInt(colors.length)]);
    }
}
