package com.example.amvirgin.utils;
import com.example.amvirgin.shopModule.shopModel.BestTrandsModel;
import com.example.amvirgin.shopModule.shopModel.ShopBrands;
import com.example.amvirgin.shopModule.shopModel.BannerModel;
import com.example.amvirgin.shopModule.shopModel.CategoryModel;
import com.example.amvirgin.shopModule.shopModel.ShopHomeModel;
import com.example.amvirgin.shopModule.shopModel.ShopTrendingDealsModel;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static List<ShopTrendingDealsModel.Datum> shopping_search = new ArrayList();
    public static ArrayList<CategoryModel> categoryList = new ArrayList();
    public static ArrayList<BannerModel> bannerList = new ArrayList();
    public  static ArrayList<ShopBrands>   brandList=new ArrayList<>();
    public static ArrayList<BestTrandsModel> bestTrendsList=new ArrayList<>();

    public static List<ShopHomeModel.ShopSlider> shopSliderList = null;
    public static ShopHomeModel.OfferDetails incomingdeals = null;
    public static List<ShopHomeModel.BrandInFocu> shopBrandsInFocus = null;
    public static List<ShopHomeModel.TrendingDeal> shopTreningDeals = null;
    public static List<ShopHomeModel.PopularStuff> shopPopularStuff = null;
    public static List<ShopHomeModel.TrendingNow> shopTrendingNow = null;

    public static String checkFragment = "checkFragment";
    public static String share_link_test ="share_link_test"; //boolean
    public static String sharing_id ="sharing_link_check"; //boolean
    public static String CHECKOUT_DETAILS ="CHECKOUT_DETAILS"; //boolean
    public class ServiceType {

    }


    public interface Key {
        //user details
        String KEY = "key";
        String NAME = "fullname"; //string
        String EMAIL = "email"; //string
        String MOBILE = "mobile"; //string
        String AVATAR = "avatar"; //string
        String SUB_ACTIVE ="subscription_active"; //boolean
        String PLAN_KEY = "plan_key"; //number
        String SUB_NAME = "subscription_name"; //string
        String SUB_DUR_TOTAL_DAY = "subscription_duration_total_days"; //number
        String SUB_DUR_REMAINING_DAY = "subscription_duration_remaining_days"; //number
        String ApiToken = "apitoken";
        String SUBSCRIPTION = "subscription"; //boolean

        String LocalBrocastEvent="MyLocalBrocastEvent";
        String BUNDLE="bundle";
        String DeviceID="deviceId";
        String FCM_TOKEN="fcmToken";

        String USERID = "userid";

        String DOB = "dob";

        String Type = "type";
        String USERIMAGE = "displayImage";
        String SESSION_ID = "sessionid";
        String IS_ALREADY_LOGGED_IN = "isAlreadyLoggedIn";
        String ADDRESS = "address";
        String apiKey = "AIzaSyBsKXUHqa5DcNk9_usVfjjFSPgwxqTiai4";
        String IS_CAMERA = "camera";
        String IS_GALLERY = "Gallery";
    }



}
