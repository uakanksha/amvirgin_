package com.example.amvirgin.utils;

import com.example.amvirgin.R;
import com.example.amvirgin.classes.BannerSliderModel;
import com.example.amvirgin.classes.JustAdded;
import com.example.amvirgin.classes.TopPick;
import com.example.amvirgin.classes.TrendingPick;
import com.example.amvirgin.entertainmentModule.entertainmentModel.HomeModel;
import com.example.amvirgin.entertainmentModule.entertainmentModel.SectionCollectionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppLocal {
    public static List<TrendingPick> trendingPicks = null;
    public static List<JustAdded> justAdded = null;
    public static List<TopPick> topPicks = null;

    public static ArrayList shopArrayList = new ArrayList<>
            (Arrays.asList(R.drawable.shop1, R.drawable.shop2, R.drawable.shop3, R.drawable.shop4,
                    R.drawable.shop5, R.drawable.shop6, R.drawable.shop7));
    public static ArrayList shopName = new ArrayList<>(Arrays.asList("", "", "", "", "", "", ""));
    public static ArrayList shopType = new ArrayList<>(Arrays.asList("", "", "", "", "", "", ""));

    private ArrayList bannerImagesArrayList = new ArrayList<>
            (Arrays.asList(R.drawable.main1, R.drawable.main2, R.drawable.main3, R.drawable.main4,
                    R.drawable.main5));

    public static ArrayList topPicksArrayList = new ArrayList<>
            (Arrays.asList(R.drawable.poster1, R.drawable.poster2, R.drawable.poster3, R.drawable.poster4,
                    R.drawable.poster5, R.drawable.poster6, R.drawable.poster7));
    public static List<BannerSliderModel.BannerSliderData> data= null;
    public static String type="nodata";

    public static ArrayList topPicksName = new ArrayList<>(Arrays.asList("Thor", "Guel", "Alita", "AntMan", "Justice League",
            "World War", "Black Panther"));
    public static ArrayList topPicksType = new ArrayList<>(Arrays.asList("Movies", "Movies", "Movies", "Movies", "Movies",
            "Movies", "Movies" ));



    //MovieDetailsSliding Acitivity
    public static int id = -1;
    public static int size  = -1;
    public static int position  = -1;
    public static List<HomeModel.Item> items = null;
    public static List<SectionCollectionModel.Datum> data_section = null;
}
