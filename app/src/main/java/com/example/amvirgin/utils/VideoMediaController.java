package com.example.amvirgin.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.MediaController;

import androidx.recyclerview.widget.GridLayoutManager;

import com.example.amvirgin.FullScreenPlayerActivity;
import com.example.amvirgin.R;

public class VideoMediaController extends MediaController {

    private ImageButton fullScreen;
    private String isFullScreen;

    public VideoMediaController(Context context) {
        super(context);
    }

    public void setAnchorView(View view){
        super.setAnchorView(view);

        fullScreen = new ImageButton(super.getContext());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.RIGHT;
        params.rightMargin = 80;
        addView(fullScreen, params);

        isFullScreen = ((Activity)getContext()).getIntent().getStringExtra("fullScreenInd");

        if("y".equals(isFullScreen)){
            fullScreen.setImageResource(R.drawable.ic_exit_fullscreen);
        }else{
            fullScreen.setImageResource(R.drawable.ic_fullscreen);
        }

        fullScreen.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), FullScreenPlayerActivity.class);
                if("y".equals(isFullScreen)){
                    intent.putExtra("fullScreenInd", "");
                }else{
                    intent.putExtra("fullScreenInd", "y");
                }
                ((Activity)getContext()).startActivity(intent);
            }
        });

    }

}
