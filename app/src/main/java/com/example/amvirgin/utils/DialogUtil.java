package com.example.amvirgin.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.example.amvirgin.R;

import java.util.logging.Logger;


public final class DialogUtil {
    private static final String TAG = DialogUtil.class.getSimpleName();
    private static ProgressDialog progressDialog;

    private DialogUtil() {
    }



    public static void showAlertDialog(Activity activity, String message, boolean cancelable) {
        showAlertDialog(activity, activity.getString(R.string.app_name), message, cancelable);
    }

    public static void showAlertDialog(Activity activity, String title, String message, boolean cancelable) {
        showAlertDialog(activity, title, message, activity.getString(android.R.string.ok), null, null, null, null, cancelable);
    }

    public static void showAlertDialog(Activity activity, String title, String message, boolean cancelable, String positiveButtonText,
                                       DialogInterface.OnClickListener positiveButtonClickListener) {
        showAlertDialog(activity, title, message, positiveButtonText, positiveButtonClickListener, null, null, null, cancelable);
    }

    public static void showAlertDialog(Activity activity, String title, String message, boolean cancelable, DialogInterface
            .OnClickListener positiveButtonClickListener) {
        showAlertDialog(activity, title, message, activity.getString(android.R.string.ok), positiveButtonClickListener, null, null, null,
                cancelable);
    }

    public static void showConfirmationDialog(Activity activity,String title, String message, DialogInterface.OnClickListener
            positiveButtonClickListener) {
        showAlertDialog(activity, title, message, activity.getString(android.R.string.yes), positiveButtonClickListener, Color
                .RED, activity.getString(android.R.string.no), null, false);
    }

    public static void showcancelDialog(Activity activity, String message, DialogInterface.OnClickListener
            positiveButtonClickListener) {
        showAlertDialog(activity, "Alert !", message, activity.getString(android.R.string.yes), positiveButtonClickListener, Color
                .RED, activity.getString(android.R.string.no), null, false);
    }

    public static void showAlertDialog(Activity activity, String title, String message, String positiveButtonText, DialogInterface
            .OnClickListener positiveButtonClickListener, final Integer positiveButtonColor, String negativeButtonText, DialogInterface
                                               .OnClickListener negativeButtonClickListener, boolean cancelable) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.MultiSelectionSpinnerTheme);
        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, negativeButtonClickListener);
        }
        if (title != null) {
            builder.setTitle(title);
        }
        final AlertDialog alertDialog = builder.setMessage(message).setPositiveButton(positiveButtonText, positiveButtonClickListener).
                setCancelable(cancelable).create();
        if (positiveButtonColor != null) {
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(positiveButtonColor);
                }
            });
        }
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();

    }

}
