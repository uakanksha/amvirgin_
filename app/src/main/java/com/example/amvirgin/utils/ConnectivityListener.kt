package com.example.amvirgin.utils

interface ConnectivityListener {

    fun onNetworkConnectionChanged(isConnected: Boolean)
}