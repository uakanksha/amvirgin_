package com.example.amvirgin.utils;

import com.example.amvirgin.classes.RegisterUserModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    private int key;
    private String name;
    private String email;
    private String mobile;
    private String token;
    private String avatar;
    private RegisterUserModel.Subscription subscription;

    public RegisterUserModel.Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(RegisterUserModel.Subscription subscription) {
        this.subscription = subscription;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }



    public User(RegisterUserModel.userRegisterData response) {
        this.key = response.getKey();
                this.name = response.getName();
                this.email = response.getEmail();
                this.mobile = response.getMobile();
                this.token = response.getToken();
                this.subscription = response.getSubscription();
                this.avatar = response.getAvatar();
           }

           public User(){

           }
}

