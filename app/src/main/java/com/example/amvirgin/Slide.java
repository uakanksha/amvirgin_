package com.example.amvirgin;

public class Slide {

    private int image;
    private String movie;

    public Slide(int image, String movie) {
        this.image = image;
        this.movie = movie;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
