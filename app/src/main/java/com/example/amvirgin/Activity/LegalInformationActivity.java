package com.example.amvirgin.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.amvirgin.R;

public class LegalInformationActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView iv_back;
    private LinearLayout about_ll, tnc_ll, privacy_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal_information);

        iv_back = findViewById(R.id.iv_back);
        about_ll = findViewById(R.id.about_ll);
        tnc_ll = findViewById(R.id.tnc_ll);
        privacy_ll = findViewById(R.id.privacy_ll);

        iv_back.setOnClickListener(this);
        about_ll.setOnClickListener(this);
        tnc_ll.setOnClickListener(this);
        privacy_ll.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_back:
                finish();
                break;

            case R.id.about_ll:
                Toast.makeText(this, "About Us", Toast.LENGTH_SHORT).show();
                break;

            case R.id.tnc_ll:
                Toast.makeText(this, "Terms & Conditions", Toast.LENGTH_SHORT).show();
                break;

            case R.id.privacy_ll:
                Toast.makeText(this, "Privacy Policy", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
