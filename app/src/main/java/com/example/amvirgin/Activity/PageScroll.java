package com.example.amvirgin.Activity;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.Adapter.HomeDisplayAdapter;
import com.example.amvirgin.Adapter.ParentAdapter;
import com.example.amvirgin.R;
import com.example.amvirgin.utils.AppLocal;

public class PageScroll  extends AppCompatActivity {

    RecyclerView parentrecycler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
        parentrecycler=(RecyclerView)findViewById(R.id.parentrecycler);
        PageScrollAdapter homeDisplayAdapter=   new PageScrollAdapter(this, AppLocal.shopArrayList);
        LinearLayoutManager manager = new LinearLayoutManager(PageScroll.this, RecyclerView.HORIZONTAL, false);
        parentrecycler.setHasFixedSize(true);
        parentrecycler.setItemViewCacheSize(20);
        parentrecycler.setDrawingCacheEnabled(true);
        parentrecycler.setLayoutManager(manager);
        parentrecycler.setAdapter(homeDisplayAdapter);
        PagerSnapHelper  mSnapHelper = new PagerSnapHelper();
        mSnapHelper.attachToRecyclerView(parentrecycler);

    }
}
