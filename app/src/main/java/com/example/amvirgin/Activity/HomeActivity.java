package com.example.amvirgin.Activity;

import android.os.Bundle;

import com.example.amvirgin.shopModule.fragment.ShopFragment;
import com.example.amvirgin.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import java.util.List;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        if (getIntent().hasExtra("back")){
            navView.setSelectedItemId(R.id.nav_shop);
        }
        else if (getIntent().hasExtra("notlogin")){
            navView.setSelectedItemId(R.id.nav_more);
        }

    }
    @Override
    public void onBackPressed() {
       // Fragments();
        super.onBackPressed();
    }

    private void  Fragments(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for(Fragment fragment : fragments){
            if(fragment != null && fragment instanceof ShopFragment)
                ((ShopFragment)fragment).onBackPressed();
        }
    }

}
