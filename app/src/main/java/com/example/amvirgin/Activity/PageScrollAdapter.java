package com.example.amvirgin.Activity;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.amvirgin.Adapter.EpisodesAdapter;
import com.example.amvirgin.Fragments.CreditsFragment;
import com.example.amvirgin.Fragments.HeaderFragment;
import com.example.amvirgin.PlayerActivity;
import com.example.amvirgin.R;

import java.util.ArrayList;
import java.util.Arrays;

import static android.view.LayoutInflater.from;
import static com.example.amvirgin.R.layout.spinner_item_overview;
import static com.example.amvirgin.utils.AppLocal.shopArrayList;

class PageScrollAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    PageScroll context;
    public PageScrollAdapter(PageScroll baseContext, ArrayList shopArrayList) {
        this.context=baseContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PageScrollAdapterbinder(from(parent.getContext()).inflate(R.layout.movie_detail_layout, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        /*Glide.with(context).load(shopArrayList.get(position)).into( ((PageScrollAdapterbinder)holder).banner_img);
        ((PageScrollAdapterbinder)holder).name.setText(String.valueOf(shopName.get(position)));
        ((PageScrollAdapterbinder)holder).type.setText(String.valueOf(shopType.get(position)));*/

        ((PageScrollAdapterbinder)holder).show_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PageScrollAdapterbinder)holder).show_ll.setVisibility(View.GONE);
                ((PageScrollAdapterbinder)holder).hide_ll.setVisibility(View.VISIBLE);
                ((PageScrollAdapterbinder)holder).description_text.setMaxLines(Integer.MAX_VALUE);

            }
        });
        ((PageScrollAdapterbinder)holder).hide_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PageScrollAdapterbinder)holder).  hide_ll.setVisibility(View.GONE);
                ((PageScrollAdapterbinder)holder).  show_ll.setVisibility(View.VISIBLE);
                ((PageScrollAdapterbinder)holder). description_text.setMaxLines(5);
            }
        });
        ((PageScrollAdapterbinder)holder).episodes_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PageScrollAdapterbinder)holder).  fm =((PageScroll)context). getSupportFragmentManager();
                FragmentTransaction transaction = ((PageScrollAdapterbinder)holder).fm.beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_in_down);
                transaction.add(R.id.main_ll, new HeaderFragment())
                        .addToBackStack(null).commit();
            }
        });
        ((PageScrollAdapterbinder)holder).credits_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((PageScrollAdapterbinder)holder).  fragmentManager = ((PageScroll)context).getSupportFragmentManager();
                ((PageScrollAdapterbinder)holder).  fragmentTransaction = ((PageScrollAdapterbinder)holder).fragmentManager.beginTransaction();
                CreditsFragment creditsFragment = new CreditsFragment();
//                fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down);
//                transact.add(R.id.item_container, productAlertFragment).addToBackStack(null).commit();
                creditsFragment.show(((PageScroll)context).getSupportFragmentManager(),"about");
            }
        });
        ((PageScrollAdapterbinder)holder).watchtrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(((PageScroll)context).getApplicationContext(), PlayerActivity.class);
                ((PageScroll)context).startActivity(intent);
            }
        });

//        description_text.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);

//        OpenFragment(new EpisodesFragment());

        ((PageScrollAdapterbinder)holder).seasons = ((PageScroll)context).getResources().getStringArray(R.array.seasons);
        ((PageScrollAdapterbinder)holder).mSeasonsAdapter = new ArrayAdapter<CharSequence>(((PageScroll)context).getApplicationContext(), spinner_item_overview,  ((PageScrollAdapterbinder)holder).seasons);
        ((PageScrollAdapterbinder)holder). mSeasonsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ((PageScrollAdapterbinder)holder). sp_seasons.setAdapter(((PageScrollAdapterbinder)holder).mSeasonsAdapter);

        LinearLayoutManager manager = new LinearLayoutManager(((PageScroll)context).getApplicationContext(), RecyclerView.VERTICAL, false);
        ((PageScrollAdapterbinder)holder).rv_episodes.setLayoutManager(manager);

//        EpisodesAdapter adapter = new EpisodesAdapter(((PageScroll)context).getApplicationContext(), ((PageScrollAdapterbinder)holder).episodes, "");
//        ((PageScrollAdapterbinder)holder). rv_episodes.setAdapter(adapter);

        if (((PageScrollAdapterbinder)holder).mNestedScrollView != null) {
            ((PageScrollAdapterbinder)holder).mNestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    float y = ((PageScrollAdapterbinder)holder).ep_ll.getY();
                   /* RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT
                    );
                    params.setMargins(0, (int) y+10, 0, 0);
                    ll_btnTop.setLayoutParams(params);
*/


                    ((PageScrollAdapterbinder)holder).animShow = AnimationUtils.loadAnimation( ((PageScroll)context), R.anim.slide_in_down);
                    ((PageScrollAdapterbinder)holder).   animHide = AnimationUtils.loadAnimation( ((PageScroll)context), R.anim.slide_in_up);
                    ((PageScrollAdapterbinder)holder).   hFragment = new HeaderFragment();
                    ((PageScrollAdapterbinder)holder).   fm =((PageScroll)context).getSupportFragmentManager();
                    FragmentTransaction transaction =  ((PageScrollAdapterbinder)holder).fm.beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_in_down, R.anim.slide_in_up,
                            R.anim.slide_in_down, R.anim.slide_in_up);

                    if (scrollY > y) {
                        if ( ((PageScrollAdapterbinder)holder).a) {

                            transaction.add(R.id.main_ll,  ((PageScrollAdapterbinder)holder).hFragment)
                                    .addToBackStack(null).commit();
                            ((PageScrollAdapterbinder)holder).a = false;
                        }
                    } else {
                        if (! ((PageScrollAdapterbinder)holder).a) {

                            if ( ((PageScrollAdapterbinder)holder).hFragment != null){
                                ((PageScrollAdapterbinder)holder). fm.popBackStackImmediate();
                                ((PageScrollAdapterbinder)holder).a = true;
                            }
                        }
                    }

                }
            });
        }



    }

    @Override
    public int getItemCount() {
        return shopArrayList.size();
    }

    public class PageScrollAdapterbinder extends RecyclerView.ViewHolder{

        TextView description_text, show, hide,watchtrailer;
        LinearLayout show_ll, hide_ll, episodes_ll, credits_ll, ll_top, ep_ll, list_ll, watch_trailer_ll;
        RelativeLayout main_ll, ll_btnTop;
        View episodes_view, credits_view;
        FragmentManager fragmentManager;
        FragmentTransaction fragmentTransaction;
        NestedScrollView mNestedScrollView;
        Animation animShow, animHide;
        FragmentManager fm;
        boolean a = true;
        HeaderFragment headerFragment;
        Fragment hFragment;

        RecyclerView rv_episodes;
        Spinner sp_seasons;
        String[] seasons;
        private ArrayAdapter<CharSequence> mSeasonsAdapter;
        private ArrayList episodes = new ArrayList<>(Arrays.asList("Episode 1", "Episode 2", "Episode 3", "Episode 4", "Episode 5",
                "Episode 6", "Episode 7"));

        public PageScrollAdapterbinder(@NonNull View itemView) {
            super(itemView);
            if(android.os.Build.VERSION.SDK_INT >= 21){
                Window window = context.getWindow();
                window.setStatusBarColor(context.getResources().getColor(R.color.colorback));
            }

            description_text =itemView.findViewById(R.id.description_text);
            show = itemView.findViewById(R.id.show);
            hide = itemView.findViewById(R.id.hide);
            watchtrailer=itemView.findViewById(R.id.watchtrailer);
            show_ll = (LinearLayout) itemView.findViewById(R.id.show_ll);
            hide_ll = itemView.findViewById(R.id.hide_ll);
            episodes_ll = itemView.findViewById(R.id.episodes_ll);
            credits_ll = itemView.findViewById(R.id.credits_ll);
            episodes_view = itemView.findViewById(R.id.episodes_view);
            credits_view = itemView.findViewById(R.id.credits_view);
            mNestedScrollView = itemView.findViewById(R.id.mNestedScrollView);
            ll_top = itemView.findViewById(R.id.top);
            ll_btnTop = itemView.findViewById(R.id.ll_btnTop);
            ep_ll = itemView.findViewById(R.id.ep_ll);
            list_ll = itemView.findViewById(R.id.list_ll);
            main_ll = itemView.findViewById(R.id.main_ll);
//        hFragment = findViewById(R.id.hFragment);
            rv_episodes = itemView.findViewById(R.id.rv_episodes);
            sp_seasons = itemView.findViewById(R.id.sp_seasons);

            watch_trailer_ll = itemView.findViewById(R.id.watch_trailer_ll);


        }
    }

}
