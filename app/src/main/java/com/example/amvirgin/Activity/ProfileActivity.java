package com.example.amvirgin.Activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.classes.ChangePasswordModel;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout change_name_ll, account_details_ll, account_details_expanded_ll,
            change_password_ll, change_password_expanded_ll, ll_update_picture;

    private CircleImageView circle;
    private int REQUEST_CAMERA = 2, REQUEST_GALLERY = 1;
    private MultipartBody.Part img;
    private String profile_img;
    private Uri fileUri = null;
    private File file;

    private ImageView iv_up, iv_down, iv_back;
    private EditText et_old_pass, et_new_pass, et_confirm_pass;
    private Button btn_update;
    private String token;
    String user_name;
    private ProgressBar pb;

    private TextView tv_name, tv_email, tv_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        change_name_ll = findViewById(R.id.change_name_ll);
        account_details_ll = findViewById(R.id.account_details_ll);
        account_details_expanded_ll = findViewById(R.id.account_details_expanded_ll);
        change_password_ll = findViewById(R.id.change_password_ll);
        change_password_expanded_ll = findViewById(R.id.change_passsword_expanded_ll);

        tv_name = findViewById(R.id.tv_name);
        tv_email = findViewById(R.id.tv_email);
        tv_number = findViewById(R.id.tv_number);
        pb = findViewById(R.id.pb);

        iv_up = findViewById(R.id.iv_up);
        iv_down = findViewById(R.id.iv_down);
        iv_back = findViewById(R.id.iv_back);

        et_old_pass = findViewById(R.id.et_old_pass);
        et_new_pass = findViewById(R.id.et_new_pass);
        et_confirm_pass = findViewById(R.id.et_confirm_pass);
        btn_update = findViewById(R.id.btn_update);

        ll_update_picture = findViewById(R.id.ll_update_picture);
        circle = findViewById(R.id.circle);

        btn_update.setOnClickListener(this);

        iv_up.setOnClickListener(this);
        iv_down.setOnClickListener(this);
        iv_back.setOnClickListener(this);

        change_name_ll.setOnClickListener(this);
        account_details_ll.setOnClickListener(this);
        account_details_expanded_ll.setOnClickListener(this);
        change_password_ll.setOnClickListener(this);
        change_password_expanded_ll.setOnClickListener(this);
        ll_update_picture.setOnClickListener(this);

        if (token != null) {
            getUserProfile(token);
        }

    }

    private void getUserProfile(String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(ProfileActivity.this);
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .getUserProfile("application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(this) {
            @Override
            public void onResponse(RegisterUserModel response) {
                 user_name = response.getData().getName();
                tv_name.setText(response.getData().getName());
                tv_email.setText(response.getData().getEmail());
                tv_number.setText(response.getData().getMobile());

                if (response.getData().getAvatar() != null){
                    Glide.with(getApplicationContext()).load(response.getData().getAvatar()).into(circle);
                    pb.setVisibility(View.GONE);
                }
                else {
                    circle.setImageResource(R.drawable.ic_user);
                    pb.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.change_name_ll:

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ProfileActivity.this);
                View layout_view = getLayoutInflater().inflate(R.layout.change_user_name_layout, null);

                Button btn_update = layout_view.findViewById(R.id.btn_update);
                EditText et_fullname = layout_view.findViewById(R.id.et_fullname);
                ImageView iv_close = layout_view.findViewById(R.id.iv_close);

                et_fullname.setText(user_name);

                alertBuilder.setView(layout_view);
                alertBuilder.setCancelable(false);

                AlertDialog dialog = alertBuilder.create();
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

                iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                btn_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String full_name = et_fullname.getText().toString().trim();
                        if (full_name.equalsIgnoreCase("")){
                            Toast.makeText(ProfileActivity.this, "Please enter a valid name!!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            updateProfile(full_name, dialog);
                        }
                    }
                });

                break;

            case R.id.iv_down:
                change_password_ll.setVisibility(View.GONE);
                change_password_expanded_ll.setVisibility(View.VISIBLE);
                break;

            case R.id.iv_up:
                change_password_expanded_ll.setVisibility(View.GONE);
                change_password_ll.setVisibility(View.VISIBLE);
                break;

            case R.id.btn_update:
                if (token != null) {
                    if (isValidDetails()) {
                        changePassword();
                    }
                }
                break;

            case R.id.ll_update_picture:
                requestStoragePermission();
                break;

            case R.id.iv_back:
                finish();
                break;
        }
    }

    private void updateProfile(String full_name, AlertDialog dialog) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", full_name);

        ServiceWrapper serviceWrapper = new ServiceWrapper(ProfileActivity.this);
        Call<ChangePasswordModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .updateProfile("application/json", "Bearer "+token, jsonObject);

        serviceWrapper.HandleResponse(call, new ResponseHandler<ChangePasswordModel>(this) {
            @Override
            public void onResponse(ChangePasswordModel response) {
                if (response.getStatus() == 200){

                        ToastMessage.getInstance(ProfileActivity.this).showSuccessShortCustomToast(response.getMessage());
                        dialog.dismiss();

                        getUserProfile(token);

                }
                else {
                    ToastMessage.getInstance(ProfileActivity.this).showErrorShortCustomToast(response.getMessage());
                }
            }
        });

    }

    private boolean isValidDetails() {
        boolean valid = false;

        if (et_old_pass.getText().toString().trim().equalsIgnoreCase("")){
            et_old_pass.setError("Old Password should not be empty!!");
            et_old_pass.requestFocus();
        }
        else if (et_new_pass.getText().toString().trim().equalsIgnoreCase("")){
            et_new_pass.setError("New Password should not be empty!!");
            et_new_pass.requestFocus();
        }
        else if (et_confirm_pass.getText().toString().trim().equalsIgnoreCase("")){
            et_confirm_pass.setError("Confirm New Password should not be empty!!");
            et_confirm_pass.requestFocus();
        }
        else if (!et_new_pass.getText().toString().trim().equalsIgnoreCase(et_confirm_pass.getText().toString().trim())){
            et_confirm_pass.setError("Both Passwords should be same!!");
            et_confirm_pass.requestFocus();
        }
        else {
            valid = true;
        }

        return valid;
    }

    private void changePassword() {

        String current = et_old_pass.getText().toString().trim();
        String newPass = et_new_pass.getText().toString().trim();
        String confirm = et_confirm_pass.getText().toString().trim();

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("current", current);
        jsonObject.addProperty("new", newPass);
        jsonObject.addProperty("confirm", confirm);


        ServiceWrapper serviceWrapper = new ServiceWrapper(ProfileActivity.this);
        Call<ChangePasswordModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .changePassword("application/json", "Bearer "+token, jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ChangePasswordModel>(this) {
            @Override
            public void onResponse(ChangePasswordModel response) {
                if (response.getStatus() == 200){

                        ToastMessage.getInstance(ProfileActivity.this).showSuccessShortCustomToast(response.getMessage());

                        change_password_expanded_ll.setVisibility(View.GONE);
                        change_password_ll.setVisibility(View.VISIBLE);

                        et_old_pass.setText("");
                        et_new_pass.setText("");
                        et_confirm_pass.setText("");

                }
                else {
                    ToastMessage.getInstance(ProfileActivity.this).showErrorShortCustomToast(response.getMessage());
                }
            }
        });

    }

    private void requestStoragePermission(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();

                            selectImage();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(ProfileActivity.this, "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this, R.style.MyDialogTheme);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", ProfileActivity.this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void selectImage(){
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Take Photo")){
                    cameraIntent();
                }
                else if (items[i].equals("Choose from Library")){
                    galleryIntent();
                }
                else if (items[i].equals("Cancel")){
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent(){
       /* Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),REQUEST_GALLERY);*/

        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, REQUEST_GALLERY);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK && data != null) {


            //the image URI
            Uri selectedImage = data.getData();
            fileUri = selectedImage;
            //calling the upload file method after choosing the file
            uploadFile(selectedImage, "My Image");

        } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK && data != null) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            Uri tempUri = getImageUri(getApplicationContext(), photo);
            fileUri = tempUri;

//            Uri selectedImage = data.getData();
            uploadFile(tempUri, "My Image");

            Log.d("camera uri path", String.valueOf(tempUri));
            /*  Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ivProfilePic.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(ProfileActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();*/
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private void uploadFile(Uri fileUri, String desc) {
        Log.d("file uri", String.valueOf(fileUri));

        profile_img = getRealPathFromURI(fileUri);
        Log.d("real path uri", getRealPathFromURI(fileUri));

        //creating a file
        file = new File(getRealPathFromURI(fileUri));
        circle.setImageURI(fileUri);

        changeAvatar(file);

    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void changeAvatar(File file) {
        MultipartBody.Part dp_image = null;
        if (file != null) {
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            dp_image = MultipartBody.Part.createFormData("avatar", file.getName(), mFile);

            Log.d("dp image",String.valueOf(dp_image));
        }

        ServiceWrapper serviceWrapper = new ServiceWrapper(ProfileActivity.this);
        Call<ChangePasswordModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .changeAvatar("Bearer "+token, dp_image);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ChangePasswordModel>(this) {
            @Override
            public void onResponse(ChangePasswordModel response) {
                if (response.getStatus() == 200){

                        ToastMessage.getInstance(ProfileActivity.this).showSuccessShortCustomToast(response.getMessage());

                       getUserProfile(token);
                    }

                else {
                    ToastMessage.getInstance(ProfileActivity.this).showErrorShortCustomToast(response.getMessage());
                }
            }
        });
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        this.recreate();
    }*/
}
