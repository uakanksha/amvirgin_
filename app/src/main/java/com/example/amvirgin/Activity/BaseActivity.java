package com.example.amvirgin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.amvirgin.shopModule.Other.SnackManager;
import com.example.amvirgin.shopModule.Other.SnackbarManager;

import com.example.amvirgin.utils.Constants;
import com.novoda.merlin.Connectable;
import com.novoda.merlin.Disconnectable;
import com.novoda.merlin.Merlin;

public class BaseActivity extends AppCompatActivity {
    private Merlin merlin;
    private Context mCtx;

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx=this;
        merlin = new Merlin.Builder()
                .withAllCallbacks().build(this);
        intent = new Intent(Constants.Key.LocalBrocastEvent);
        merlin.registerDisconnectable(new Disconnectable() {
            @Override
            public void onDisconnect() {
                intent.putExtra("networkStatus",false);
                LocalBroadcastManager.getInstance(mCtx).sendBroadcast(intent);
                SnackManager.getInstance().showSnackbar(mCtx,"Make sure you have an active data connection",SnackManager.TYPE_NO_INTERNET);
            }
        });

        merlin.registerConnectable(new Connectable() {
            @Override
            public void onConnect() {
                intent.putExtra("networkStatus",true);
                LocalBroadcastManager.getInstance(mCtx).sendBroadcast(intent);
                if (SnackManager.getInstance().snackbar !=null)
                SnackManager.getInstance().showSnackbar(mCtx,"Connection established",SnackManager.TYPE_FOUND_INTERNET);
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        merlin.bind();
    }

    @Override
    protected void onPause() {
        merlin.unbind();
        super.onPause();
    }



}
