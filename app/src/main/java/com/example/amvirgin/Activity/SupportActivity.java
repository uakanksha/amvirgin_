package com.example.amvirgin.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.amvirgin.R;
import com.example.amvirgin.classes.ChangePasswordModel;
import com.example.amvirgin.classes.RegisterUserModel;
import com.example.amvirgin.interfaces.ApiInterface;
import com.example.amvirgin.shopModule.webservices.ResponseHandler;
import com.example.amvirgin.shopModule.webservices.ServiceInterface;
import com.example.amvirgin.shopModule.webservices.ServiceWrapper;
import com.example.amvirgin.utils.Constants;
import com.example.amvirgin.utils.PreferenceManager;
import com.example.amvirgin.utils.ToastMessage;
import com.google.gson.JsonObject;

import retrofit2.Call;

public class SupportActivity extends AppCompatActivity {

    private EditText et_fullname, et_email_address, et_mobile_number, et_query;
    private Button btn_submit;
    private String name, email, mobile, query, token;
    private ImageView iv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        if (PreferenceManager.getBoolean(getApplicationContext(), Constants.Key.IS_ALREADY_LOGGED_IN)) {
            token = PreferenceManager.getUser(getApplicationContext()).getToken();
        }

        et_fullname = findViewById(R.id.et_fullname);
        et_email_address = findViewById(R.id.et_email_address);
        et_mobile_number = findViewById(R.id.et_mobile_number);
        et_query = findViewById(R.id.et_query);
        btn_submit = findViewById(R.id.btn_submit);
        iv_back = findViewById(R.id.iv_back);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidDetails()){
                    submitQuery();
                }
            }
        });

        if (token != null){
            getUserProfile(token);
        }

    }

    private void getUserProfile(String token) {
        ServiceWrapper serviceWrapper = new ServiceWrapper(SupportActivity.this);
        Call<RegisterUserModel> call = serviceWrapper.getRetrofit().create(ApiInterface.class)
                .getUserProfile("application/json", "Bearer "+token);
        serviceWrapper.HandleResponse(call, new ResponseHandler<RegisterUserModel>(this) {
            @Override
            public void onResponse(RegisterUserModel response) {
                et_fullname.setText(response.getData().getName());
                et_email_address.setText(response.getData().getEmail());
                et_mobile_number.setText(response.getData().getMobile());
            }
        });
    }


    private boolean isValidDetails() {
        name = et_fullname.getText().toString().trim();
        email = et_email_address.getText().toString().trim();
        mobile = et_mobile_number.getText().toString().trim();
        query = et_query.getText().toString().trim();

        boolean valid = false;
        String mobilePattern = "[0-9]{10}";

        if (TextUtils.isEmpty(name)){
            Toast.makeText(SupportActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(email)){
            Toast.makeText(SupportActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            Toast.makeText(SupportActivity.this, "Please Enter a valid Email", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(mobile)){
            Toast.makeText(SupportActivity.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
        }
        else if (!mobile.matches(mobilePattern)){
            Toast.makeText(SupportActivity.this, "Please Enter a valid Mobile Number", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(query)){
            Toast.makeText(SupportActivity.this, "Please Enter Your query", Toast.LENGTH_SHORT).show();
        }
        else {
            valid = true;
        }

        return valid;
    }


    private void submitQuery() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", name);
        jsonObject.addProperty("email", email);
        jsonObject.addProperty("mobile", mobile);
        jsonObject.addProperty("query", query);

        ServiceWrapper serviceWrapper = new ServiceWrapper(SupportActivity.this);
        Call<ChangePasswordModel> call = serviceWrapper.getRetrofit().create(ServiceInterface.class)
                .submitQuery("application/json", "Bearer "+token, jsonObject);
        serviceWrapper.HandleResponse(call, new ResponseHandler<ChangePasswordModel>(this) {
            @Override
            public void onResponse(ChangePasswordModel response) {
                if (response.getStatus() == 200){
                    try {
                        ToastMessage.getInstance(SupportActivity.this).showSuccessShortCustomToast(response.getMessage());
                        finish();
                    }
                    catch (Exception e){
                        ToastMessage.getInstance(SupportActivity.this).showErrorShortCustomToast(response.getMessage());
                    }
                }
                else {
                    ToastMessage.getInstance(SupportActivity.this).showErrorShortCustomToast(response.getMessage());
                }
            }
        });

    }

}
